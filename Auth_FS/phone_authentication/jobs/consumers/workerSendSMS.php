<?php
require __DIR__ . '/../../vendor/autoload.php';
require __DIR__ . '/../../src/constants.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Api\Model\Entity\SMSMessage;
use Api\Model\BrokerFactory;
use Api\Model\CommissioningLog;
use Api\Util\Tracker;
use Api\Model\AppLog;

if (!isset($argv[1]) || empty($argv[1])) {
    die("Carrier not informed!");
}

$carrier = $argv[1];
$queue = RABBITMQ_QUEUE_PREFIX . ':' . $carrier . ':send';

$connection = new AMQPStreamConnection(
    RABBITMQ_HOST,
    RABBITMQ_PORT,
    RABBITMQ_USER,
    RABBITMQ_PASSWORD,
    RABBITMQ_VHOST,
    RABBITMQ_INSIST,
    RABBITMQ_LOGIN_METHOD,
    RABBITMQ_LOGIN_RESPONSE,
    RABBITMQ_LOCALE,
    RABBITMQ_CONNECTION_TIMEOUT,
    RABBITMQ_READ_WRITE_TIMEOUT,
    RABBITMQ_CONTEXT,
    RABBITMQ_KEEPALIVE,
    RABBITMQ_HEARTBEAT
);
$channel = $connection->channel();

$callback = function (AMQPMessage $msg) use ($carrier, $queue) {
    # Variaveis Comuns
    $rabbitMQDeliveryInfoTag = 'delivery_tag';
    $rabbitMQDeliveryInfoChannel = 'channel';
    $startTime = microtime(true);
    $msgBody = json_decode($msg->body);
    $serviceName = "send-sms";
    $ack = true;

    # Inicia LOG de DEBUG
    $logger = new AppLog($serviceName, "worker-{$carrier}-{$serviceName}");
    $logger->info("Starting process {$serviceName} on queue {$queue}");

    # Inicia LOG de COMISSIONAMENTO
    $loggerCommissioning = new CommissioningLog();
    $loggerCommissioning->setAction('worker_send_pincode')->setCarrier($carrier);

    # Valida o PAYLOAD
    if (empty($msgBody->message) || empty($msgBody->phoneNumber) || empty($msgBody->tracker)) {
        $logger->error("Payload Invalid: {$msg->body}");
        $msg->delivery_info[$rabbitMQDeliveryInfoChannel]->basic_ack($msg->delivery_info[$rabbitMQDeliveryInfoTag]);
        return false;
    }

    # Criando objeto SMSMessage
    $message = new SMSMessage();
    $message->setMessage($msgBody->message);
    $message->setTracker($msgBody->tracker);
    $message->setPhoneNumber($msgBody->phoneNumber);
    $logger->debug("Message payload: {$message}");

    # Consulta TRACKER
    $trackerInfo = Tracker::getTracker($message->getTracker());

    # Valida TRACKER
    if ($trackerInfo === false) {
        $logger->error("{$message->getPhoneNumber()} - Tracker invalid or expired");
        $msg->delivery_info[$rabbitMQDeliveryInfoChannel]->basic_ack($msg->delivery_info[$rabbitMQDeliveryInfoTag]);
        return false;
    }

    # Add LOG de COMISSIONAMENTO
    $logStartTime = \DateTime::createFromFormat('U.u', number_format($startTime, 6, '.', ''));
    $loggerCommissioning
        ->setVendorName($trackerInfo->project->getProject() . " " . $trackerInfo->project->getPlataform())
        ->setVendorLocale($trackerInfo->details->locale)
        ->setMsisdn($trackerInfo->phoneNumber->getPhoneNumberInternational(false))
        ->setTrackerId($message->getTracker())
        ->setStartTime($logStartTime->format('Y-m-d H:i:s.u'));

    # Inicia loop de envio via brokers
    $brokerFactory = new BrokerFactory($logger);
    foreach ($trackerInfo->project->getProviders() as $broker) {
        $logger->info("{$message->getPhoneNumber()} - Use broker: {$broker}");

        # Envia SMS via Broker
        $brokerResponse = $brokerFactory->sendSMS($broker, $message);
        $logger->debug("{$message->getPhoneNumber()} - {$broker}.sendSMS Response: " . json_encode($brokerResponse));

        # Add LOG de COMISSIONAMENTO
        $loggerCommissioning
            ->setGatewayName($brokerResponse->data->name)
            ->setGatewayStatus($brokerResponse->data->status)
            ->setGatewayResponseCode($brokerResponse->data->responseCode)
            ->setGatewayResponseDescription($brokerResponse->data->responseDescription)
            ->setGatewayTracker($brokerResponse->data->tracker)
            ->setGatewayBalance($brokerResponse->data->balance)
            ->setGatewayCredit($brokerResponse->data->credit)
            ->setEventStatus($brokerResponse->status)
            ->setEventDescription(($brokerResponse->status === true) ? "SMS enviado com sucesso" : "Erro ao enviar SMS")
            ->setExecutionTime(microtime(true) - $startTime)
            ->record();

        # Continua no loop se falhar
        if ($brokerResponse->status === false) {
            $ack = false;
        } else {
            $ack = true;
            break;
        }
    }

    # Log do tempo de execução do processo
    $processTime = microtime(true) - $startTime;
    $logger->info("{$message->getPhoneNumber()} - Processed in {$processTime} seconds");

    # Mantem mensagem na fila
    if ($ack === false) {
        $logger->error("{$message->getPhoneNumber()} - Processed with ERROR!");
        $msg->delivery_info[$rabbitMQDeliveryInfoChannel]->basic_nack($msg->delivery_info[$rabbitMQDeliveryInfoTag],
            false, false);
    } else {
        # Remove mensagem da fila
        $logger->info("{$message->getPhoneNumber()} - Processed SUCCESSFULLY!");
        $msg->delivery_info[$rabbitMQDeliveryInfoChannel]->basic_ack($msg->delivery_info[$rabbitMQDeliveryInfoTag]);
    }

    return true;
};

$channel->basic_qos(null, 1, null);
$channel->basic_consume($queue, '', false, false, false, false, $callback);

while (count($channel->callbacks)) {
    $channel->wait();
}

$channel->close();
$connection->close();