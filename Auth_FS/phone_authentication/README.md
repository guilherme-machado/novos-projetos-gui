# Phone Authentication

Esse projeto foi desenvolvido com `Slim Framework 3 Skeleton`.

**Informações do Sistema:**
* PHP 7.1
* Apache2
* PHP cURL Extension
* PHP Redis Extension
* PHP SOAP Extension
* PHP MCrypt Extension
* PHP PDO Extension

**Dependências:**
* Slim 3.1
* Monolog
* Redis
* RabbitMQ
* Aprovisionador

## Configurações

O código do projeto encontra-se dentro da pasta `Code`, os comandos abaixo devem ser executados dentro desta pasta.

    $ cd Code/

Antes de executar o projeto devemos ajustar o arquivo `src\constants.php` onde contem as configurações para as interações (_Redis, RabbitMQ, Brokers..._).

    $ composer install

Após a execução do comando acima, o projeto está pronto para iniciar o processo de "Execução".

## Execução

Para rodar a aplicação em seu ambiente, utilize as chamadas abaixo do `docker-compose`.

O código abaixo é responsavel por gerar um container onde iremos rodar o projeto.

    $ docker-compose build

Após a geração do container do projeto, vamos subir todos os container em "VMs" para a execução do projeto.  

    $ docker-compose up -d

Para verificar se os containers estão "UP" execute o comando abaixo.

    $ docker-compose ps

## Encerrar

Para o encerramento do container utilize o comando `docker-compose` abaixo.

    $ docker-compose down
    

# Gerar relatório do Coverage no Docker

    php -dxdebug.coverage_enable=1 /var/www/phone-auth/vendor/phpunit/phpunit/phpunit --coverage-clover /var/www/phone-auth/coverage.xml --configuration /var/www/phone-auth/phpunit.xml --teamcity

That's all Folk's!
