<?php

namespace Tests\Functional;

use Tests\BaseTestCase;

class CallbackFlowTest extends BaseTestCase
{
    public function testCallbackSMSMarket()
    {
        $uri = '/callback/sms-market';
        $payload = [
            'id' => '150',
            'campaign_id' => '35353131393733303932353339.322e3534313636332e70742e313533343936323933322e35373432',
            'status' => '1',
            'date' => '2017-01-02 15:00:00',
            'carrier' => 'Vivo - Celular',
            'number' => '5514999999999',
            'content' => 'Recebido'
        ];
        $response = $this->runApp('GET', $uri, null, $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($responseBodyDecode->status);
    }

    public function testCallbackTwilioWithSuccess()
    {
        $uri = '/callback/twilio/35353131393733303932353339.322e3534313636332e70742e313533343936323933322e35373432';
        $payload = [
            'SmsSid' => 'SM1342fe1b2c904d1ab04f0fc7a58abca9',
            'SmsStatus' => 'sent',
            'MessageStatus' => 'sent',
            'To' => '+5511922334455',
            'MessageSid' => 'SM1342fe1b2c904d1ab04f0fc7a58abca9',
            'AccountSid' => 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            'From' => '+15017250604',
            'ApiVersion' => '2010-04-01'
        ];
        $response = $this->runApp('POST', $uri, null, $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($responseBodyDecode->status);
    }

    public function testCallbackTwilioWithFail()
    {
        $uri = '/callback/twilio/35353131393733303932353339.322e3534313636332e70742e313533343936323933322e35373432';
        $payload = [
            'SmsSid' => 'SM1342fe1b2c904d1ab04f0fc7a58abca9',
            'SmsStatus' => 'undelivered',
            'MessageStatus' => 'undelivered',
            'To' => '+5511922334455',
            'MessageSid' => 'SM1342fe1b2c904d1ab04f0fc7a58abca9',
            'AccountSid' => 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            'From' => '+15017250604',
            'ApiVersion' => '2010-04-01'
        ];
        $response = $this->runApp('POST', $uri, null, $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($responseBodyDecode->status);
    }

    public function testCallbackInfobip()
    {
        $uri = '/callback/infobip/35353131393733303932353339.322e3534313636332e70742e313533343936323933322e35373432';
        $payload = [
            'results' => [
                0 => [
                    'bulkId' => '23568941',
                    'messageId' => 'ff4804ef-6ab6-4abd-984d-ab3b1387e852',
                    'to' => '385981178',
                    'sentAt' => '2015-02-12T09:58:20.323+0100',
                    'doneAt' => '2015-02-12T09:58:20.337+0100',
                    'messageCount' => 1,
                    'price' => [
                        'pricePerMessage' => 0.01000000000000000020816681711721685132943093776702880859375,
                        'currency' => 'EUR',
                    ],
                    'status' => [
                        'id' => 5,
                        'groupId' => 3,
                        'groupName' => 'DELIVERED',
                        'name' => 'DELIVERED_TO_HANDSET',
                        'description' => 'Message delivered to handset',
                    ],
                    'error' => [
                        'groupId' => 0,
                        'groupName' => 'OK',
                        'id' => 0,
                        'name' => 'NO_ERROR',
                        'description' => 'No Error',
                        'permanent' => false,
                    ],
                ],
            ]
        ];
        $response = $this->runApp('POST', $uri, null, $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($responseBodyDecode->status);
    }

    public function testCallbackEmptyRequest()
    {
        $uri = '/callback/test';
        $response = $this->runApp('GET', $uri, null, null);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertFalse($responseBodyDecode->status);
        $this->assertEquals('Provider Invalid', $responseBodyDecode->data);
    }
}