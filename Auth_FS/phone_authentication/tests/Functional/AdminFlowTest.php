<?php

namespace Tests\Functional;

use Tests\BaseTestCase;

class AdminFlowTest extends BaseTestCase
{
    private $authorizationToken = 'Bearer FS-1985f52a-3c19-41b5-a85e-790a42dd4b29';

    public function testUpdateSettingsWithSuccess()
    {
        $response = $this->runApp('POST', '/admin/update-settings', $this->authorizationToken);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($responseBodyDecode->status);
    }

    public function testUpdateWhitelistWithSuccess()
    {
        $response = $this->runApp('POST', '/admin/update-whitelist', $this->authorizationToken);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($responseBodyDecode->status);
    }

    public function testGetTrackerWithSuccess()
    {
        $payload = ['transaction_id' => '35353131393432353532363839.312e3530363336392e70742e313534333933333732382e32363335'];
        $response = $this->runApp('GET', '/admin/get-tracker', $this->authorizationToken, $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($responseBodyDecode->status);
    }

    public function testGetTrackerWithBearerAuthorizationInvalid()
    {
        $payload = ['transaction_id' => '35353131393432353532363839.312e3530363336392e70742e313534333933333732382e32363335'];
        $response = $this->runApp('GET', '/admin/get-tracker', 'Bearer 1234567890', $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(401, $response->getStatusCode());
        $this->assertFalse($responseBodyDecode->status);
        $this->assertEquals('Token inválido', $responseBodyDecode->data);
    }

    public function testGetTrackerWithBasicAuthorization()
    {
        $payload = ['transaction_id' => '35353131393432353532363839.312e3530363336392e70742e313534333933333732382e32363335'];
        $response = $this->runApp('GET', '/admin/get-tracker', 'Basic FS-1985f52a-3c19-41b5-a85e-790a42dd4b29', $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(401, $response->getStatusCode());
        $this->assertFalse($responseBodyDecode->status);
        $this->assertEquals('Token não encontrado', $responseBodyDecode->data);
    }
}