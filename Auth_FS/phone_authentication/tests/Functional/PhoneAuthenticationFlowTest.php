<?php

namespace Tests\Functional;

use Tests\BaseTestCase;

class PhoneAuthenticationFlowTest extends BaseTestCase
{
    private $authorizationToken = 'Bearer d5695a44-a96c-4b01-b286-044e03613930';

    public function testSendPinCodeWithSuccess()
    {
        $payload = ['msisdn' => '109' . rand(10000000, 99999999)];
        $response = $this->runApp('POST', '/phone-auth/send', $this->authorizationToken, $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(202, $response->getStatusCode());
        $this->assertObjectHasAttribute('status', $responseBodyDecode);
        $this->assertTrue($responseBodyDecode->status);
    }

    public function testSendPinCodeMoreThanOnceWithSuccess()
    {
        $payload = ['msisdn' => '109' . rand(10000000, 99999999)];
        $firstResponse = $this->runApp('POST', '/phone-auth/send', $this->authorizationToken, $payload);
        $firstResponseBodyDecode = json_decode($firstResponse->getBody());

        $this->assertEquals(202, $firstResponse->getStatusCode());
        $this->assertObjectHasAttribute('status', $firstResponseBodyDecode);
        $this->assertTrue($firstResponseBodyDecode->status);

        $secondResponse = $this->runApp('POST', '/phone-auth/send', $this->authorizationToken, $payload);
        $secondResponseBodyDecode = json_decode($secondResponse->getBody());

        $this->assertEquals(200, $secondResponse->getStatusCode());
        $this->assertObjectHasAttribute('status', $secondResponseBodyDecode);
        $this->assertTrue($secondResponseBodyDecode->status);
    }

    public function testSendPinCodeWithoutMsisdn()
    {
        $payload = [];
        $response = $this->runApp('POST', '/phone-auth/send', $this->authorizationToken, $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(400, $response->getStatusCode());
        $this->assertObjectHasAttribute('status', $responseBodyDecode);
        $this->assertFalse($responseBodyDecode->status);
        $this->assertEquals('Número de telefone (MSISDN) inválido', $responseBodyDecode->data);
    }

    public function testSendPinCodeWithBearerAuthorizationInvalid()
    {
        $payload = ['msisdn' => '109' . rand(10000000, 99999999)];
        $response = $this->runApp('POST', '/phone-auth/send', 'Bearer 1234567890', $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(401, $response->getStatusCode());
        $this->assertFalse($responseBodyDecode->status);
        $this->assertEquals('Authorization Token inválido', $responseBodyDecode->data);
    }

    public function testSendPinCodeWithBasicAuthorization()
    {
        $payload = ['msisdn' => '109' . rand(10000000, 99999999)];
        $response = $this->runApp('POST', '/phone-auth/send', 'Basic d5695a44-a96c-4b01-b286-044e03613930', $payload);
        $responseBodyDecode = json_decode($response->getBody());

        $this->assertEquals(401, $response->getStatusCode());
        $this->assertFalse($responseBodyDecode->status);
        $this->assertEquals('Token não encontrado', $responseBodyDecode->data);
    }

    public function testResendPinCodeWithSuccess()
    {
        # Send Flow
        $sendPayload = ['msisdn' => '109' . rand(10000000, 99999999)];
        $sendResponse = $this->runApp('POST', '/phone-auth/send', $this->authorizationToken, $sendPayload);
        $sendResponseBodyDecode = json_decode($sendResponse->getBody());

        $this->assertTrue($sendResponseBodyDecode->status);
        $this->assertNotEmpty($sendResponseBodyDecode->data->transaction_id);

        # Resend Flow
        $resendPayload = ['transaction_id' => $sendResponseBodyDecode->data->transaction_id];
        $resendResponse = $this->runApp('POST', '/phone-auth/resend', $this->authorizationToken, $resendPayload);
        $resendResponseBodyDecode = json_decode($resendResponse->getBody());

        $this->assertEquals(200, $resendResponse->getStatusCode());
        $this->assertObjectHasAttribute('status', $resendResponseBodyDecode);
        // This "fail" because MSISDN is invalid, this test consume credit
        // $this->assertTrue($resendResponseBodyDecode->status);
        $this->assertNotEmpty($resendResponseBodyDecode->data->transaction_id);
    }

    public function testResendPinCodeWithoutTracker()
    {
        # Send Flow
        $sendPayload = ['msisdn' => '109' . rand(10000000, 99999999)];
        $sendResponse = $this->runApp('POST', '/phone-auth/send', $this->authorizationToken, $sendPayload);
        $sendResponseBodyDecode = json_decode($sendResponse->getBody());

        $this->assertTrue($sendResponseBodyDecode->status);
        $this->assertNotEmpty($sendResponseBodyDecode->data->transaction_id);

        # Resend Flow
        $resendPayload = [];
        $resendResponse = $this->runApp('POST', '/phone-auth/resend', $this->authorizationToken, $resendPayload);
        $resendResponseBodyDecode = json_decode($resendResponse->getBody());

        $this->assertEquals(403, $resendResponse->getStatusCode());
        $this->assertObjectHasAttribute('status', $resendResponseBodyDecode);
        $this->assertFalse($resendResponseBodyDecode->status);
        $this->assertEquals("Tracker inválido ou expirado", $resendResponseBodyDecode->data);
    }

    public function testValidatePinCodeWithSuccess()
    {
        # Send Flow
        $sendPayload = ['msisdn' => '11922334455'];
        $sendResponse = $this->runApp('POST', '/phone-auth/send', $this->authorizationToken, $sendPayload);
        $sendResponseBodyDecode = json_decode($sendResponse->getBody());

        $this->assertTrue($sendResponseBodyDecode->status);
        $this->assertNotEmpty($sendResponseBodyDecode->data->transaction_id);

        # Validate Flow
        $validatePayload = [
            'transaction_id' => $sendResponseBodyDecode->data->transaction_id,
            'pincode' => '000000'
        ];
        $validateResponse = $this->runApp('POST', '/phone-auth/validate', $this->authorizationToken, $validatePayload);
        $validateResponseBodyDecode = json_decode($validateResponse->getBody());

        $this->assertEquals(200, $validateResponse->getStatusCode());
        $this->assertObjectHasAttribute('status', $validateResponseBodyDecode);
        $this->assertTrue($validateResponseBodyDecode->status);
        $this->assertNotEmpty($validateResponseBodyDecode->data->custom_token);
    }

    public function testValidatePinCodeWithoutTracker()
    {
        # Send Flow
        $sendPayload = ['msisdn' => '11922334455'];
        $sendResponse = $this->runApp('POST', '/phone-auth/send', $this->authorizationToken, $sendPayload);
        $sendResponseBodyDecode = json_decode($sendResponse->getBody());

        $this->assertTrue($sendResponseBodyDecode->status);
        $this->assertNotEmpty($sendResponseBodyDecode->data->transaction_id);

        # Validate Flow
        $validatePayload = ['pincode' => '000000'];
        $validateResponse = $this->runApp('POST', '/phone-auth/validate', $this->authorizationToken, $validatePayload);
        $validateResponseBodyDecode = json_decode($validateResponse->getBody());

        $this->assertEquals(400, $validateResponse->getStatusCode());
        $this->assertFalse($validateResponseBodyDecode->status);
        $this->assertEquals("'transaction_id' ou 'pincode' não informado.", $validateResponseBodyDecode->data);
    }

    public function testValidatePinCodeWithTrackerInvalid()
    {
        # Send Flow
        $sendPayload = ['msisdn' => '11922334455'];
        $sendResponse = $this->runApp('POST', '/phone-auth/send', $this->authorizationToken, $sendPayload);
        $sendResponseBodyDecode = json_decode($sendResponse->getBody());

        $this->assertTrue($sendResponseBodyDecode->status);
        $this->assertNotEmpty($sendResponseBodyDecode->data->transaction_id);

        # Validate Flow
        $validatePayload = ['transaction_id' => '123', 'pincode' => '000000'];
        $validateResponse = $this->runApp('POST', '/phone-auth/validate', $this->authorizationToken, $validatePayload);
        $validateResponseBodyDecode = json_decode($validateResponse->getBody());

        $this->assertEquals(403, $validateResponse->getStatusCode());
        $this->assertFalse($validateResponseBodyDecode->status);
        $this->assertEquals("Tracker inválido ou expirado", $validateResponseBodyDecode->data);
    }

    public function testValidatePinCodeWithPinCodeInvalid()
    {
        # Send Flow
        $sendPayload = ['msisdn' => '11922334455'];
        $sendResponse = $this->runApp('POST', '/phone-auth/send', $this->authorizationToken, $sendPayload);
        $sendResponseBodyDecode = json_decode($sendResponse->getBody());

        $this->assertTrue($sendResponseBodyDecode->status);
        $this->assertNotEmpty($sendResponseBodyDecode->data->transaction_id);

        # Validate Flow
        $validatePayload = [
            'transaction_id' => $sendResponseBodyDecode->data->transaction_id,
            'pincode' => '999999'
        ];
        $validateResponse = $this->runApp('POST', '/phone-auth/validate', $this->authorizationToken, $validatePayload);
        $validateResponseBodyDecode = json_decode($validateResponse->getBody());

        $this->assertEquals(401, $validateResponse->getStatusCode());
        $this->assertFalse($validateResponseBodyDecode->status);
        $this->assertEquals("Pincode inválido", $validateResponseBodyDecode->data);
    }
}