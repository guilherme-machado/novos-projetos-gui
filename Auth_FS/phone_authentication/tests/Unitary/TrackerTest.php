<?php
namespace Test\Unitary;

use Api\Util\Tracker;
use PHPUnit\Framework\TestCase;

class TrackerTest extends TestCase
{
    public function testCreateTrackerSuccess()
    {
        $phoneNumber = $this->getMockBuilder('Api\Model\Entity\PhoneNumber')
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock();

        $phoneNumber->method('getPhoneNumberInternational')->willReturn('5511922334455');
        $phoneNumber->method('getMsisdn')->willReturn('11922334455');

        $result = Tracker::create($phoneNumber, '1', '123456', 'pt');
        $this->assertNotFalse($result);
    }

    public function testCreateTrackerFail()
    {
        $phoneNumber = $this->getMockBuilder('Api\Model\Entity\PhoneNumber')
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock();

        $phoneNumber->method('getPhoneNumberInternational')->willReturn(null);
        $phoneNumber->method('getMsisdn')->willReturn(null);

        $result = Tracker::create($phoneNumber, '1', '123456', 'pt');
        $this->assertFalse($result);
    }

    public function testEncryptPhoneNumber()
    {
        $phoneNumber = $this->getMockBuilder('Api\Model\Entity\PhoneNumber')
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->disableArgumentCloning()
            ->disallowMockingUnknownTypes()
            ->getMock();

        $phoneNumber->method('getPhoneNumberInternational')->willReturn('5511922334455');

        $result = Tracker::encryptPhoneNumber($phoneNumber);
        $this->assertEquals(bin2hex('5511922334455'), $result);
    }

    public function testDecryptPhoneNumber()
    {
        $result = Tracker::decryptPhoneNumber(bin2hex('5511922334455'));
        $this->assertObjectHasAttribute('phoneNumberInternational', $result);
        $this->assertObjectHasAttribute('phoneNumber', $result);
        $this->assertObjectHasAttribute('msisdn', $result);
        $this->assertObjectHasAttribute('ddd', $result);
        $this->assertObjectHasAttribute('ddi', $result);
        $this->assertObjectHasAttribute('symbol', $result);
    }

    public function testEncryptDetails()
    {
        $tokenId = '1';
        $pincode = '123456';
        $locale = 'pt';
        $microtime = '1534799610.4058';


        $result = Tracker::encryptDetails($tokenId, $pincode, $locale, $microtime);
        $this->assertEquals(bin2hex("{$tokenId}.{$pincode}.{$locale}.{$microtime}"), $result);
    }

    public function testDecryptDetails()
    {
        $tokenId = '1';
        $pincode = '123456';
        $locale = 'pt';
        $microtime = '1534799610.4058';

        $result = Tracker::decryptDetails(bin2hex("{$tokenId}.{$pincode}.{$locale}.{$microtime}"));
        $this->assertEquals($tokenId, $result->tokenId);
        $this->assertEquals($pincode, $result->pincode);
        $this->assertEquals($locale, $result->locale);
        $this->assertEquals($microtime, $result->microtime);
    }
}