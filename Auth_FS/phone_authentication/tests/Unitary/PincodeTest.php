<?php
namespace Test\Unitary;

use Api\Util\Pincode;
use PHPUnit\Framework\TestCase;

class PincodeTest extends TestCase
{
    public function testCreatePincode()
    {
        $pincode = Pincode::create();
        $this->assertNotEmpty($pincode);
        $this->assertEquals(6, strlen($pincode));
    }
}