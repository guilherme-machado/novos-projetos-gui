<?php
namespace Test\Unitary;

use Api\Model\ProjectInfo;
use PHPUnit\Framework\TestCase;

class ProjectInfoTest extends TestCase
{
    public function testCreateProjectInfoWithConstructorSuccess()
    {
        $object = json_decode("{\"id\":1,\"token\":\"c4116d9d-6cd7-4a3d-abcf-b98c7f6fcb68\",\"project\":\"VIVO PROTEGE\",\"plataform\":\"ANDROID\",\"carrier\":\"vivo\",\"language_default\":\"pt\",\"providers\":[\"SMS Market\",\"Twilio\",\"Message Dispatch\"],\"phraseologies\":{\"es\":\"#PINCODE# es su codigo de login de #PROJECT#\",\"en\":\"#PINCODE# is your #PROJECT# verification code\",\"hi\":\"\",\"ar\":\"\",\"pt\":\"#PINCODE# e seu codigo de verificacao do #PROJECT#\",\"bn\":\"\",\"ru\":\"\",\"ja\":\"\",\"pa\":\"\",\"de\":\"\",\"jv\":\"\",\"zh\":\"\",\"ms\":\"\",\"te\":\"\",\"vi\":\"\",\"ko\":\"\",\"fr\":\"\",\"mr\":\"\",\"ta\":\"\",\"ur\":\"\"}}");
        $projectInfo = new ProjectInfo($object);

        $this->assertEquals(1, $projectInfo->getId());
    }

    public function testCreateProjectInfoWithIdSuccess()
    {
        $projectInfo = new ProjectInfo();
        $projectInfo->setId(1);
        $this->assertEquals(1, $projectInfo->getId());
    }

    public function testCreateProjectInfoWithTokenSuccess()
    {
        $projectInfo = new ProjectInfo();
        $projectInfo->setToken('c4116d9d-6cd7-4a3d-abcf-b98c7f6fcb68');
        $this->assertEquals('c4116d9d-6cd7-4a3d-abcf-b98c7f6fcb68', $projectInfo->getToken());
    }

    public function testCreateProjectInfoWithProjectSuccess()
    {
        $projectInfo = new ProjectInfo();
        $projectInfo->setProject('VIVO PROTEGE');
        $this->assertEquals('VIVO PROTEGE', $projectInfo->getProject());
    }

    public function testCreateProjectInfoWithPlataformSuccess()
    {
        $projectInfo = new ProjectInfo();
        $projectInfo->setPlataform('ANDROID');
        $this->assertEquals('ANDROID', $projectInfo->getPlataform());
    }

    public function testCreateProjectInfoWithCarrierSuccess()
    {
        $projectInfo = new ProjectInfo();
        $projectInfo->setCarrier('vivo');
        $this->assertEquals('vivo', $projectInfo->getCarrier());
    }

    public function testCreateProjectInfoWithLanguageDefaultSuccess()
    {
        $projectInfo = new ProjectInfo();
        $projectInfo->setLanguageDefault('pt_BR');
        $this->assertEquals('pt_BR', $projectInfo->getLanguageDefault());
    }

    public function testCreateProjectInfoWithProvidersSuccess()
    {
        $projectInfo = new ProjectInfo();
        $projectInfo->setProviders(['SMS Market', 'Twilio']);
        $this->assertEquals(['SMS Market', 'Twilio'], $projectInfo->getProviders());
    }

    public function testCreateProjectInfoWithPhraseologiesSuccess()
    {
        $projectInfo = new ProjectInfo();
        $projectInfo->setPhraseologies(['pt' => 'Texto 1', 'en' => 'Text 2']);
        $this->assertEquals('Text 2', $projectInfo->getPhraseologies('en_US'));
    }

    public function testCreateProjectInfoWithObjectSuccess()
    {
        $object = json_decode("{\"id\":1,\"token\":\"c4116d9d-6cd7-4a3d-abcf-b98c7f6fcb68\",\"project\":\"VIVO PROTEGE\",\"plataform\":\"ANDROID\",\"carrier\":\"vivo\",\"language_default\":\"pt\",\"providers\":[\"SMS Market\",\"Twilio\",\"Message Dispatch\"],\"phraseologies\":{\"es\":\"#PINCODE# es su codigo de login de #PROJECT#\",\"en\":\"#PINCODE# is your #PROJECT# verification code\",\"hi\":\"\",\"ar\":\"\",\"pt\":\"#PINCODE# e seu codigo de verificacao do #PROJECT#\",\"bn\":\"\",\"ru\":\"\",\"ja\":\"\",\"pa\":\"\",\"de\":\"\",\"jv\":\"\",\"zh\":\"\",\"ms\":\"\",\"te\":\"\",\"vi\":\"\",\"ko\":\"\",\"fr\":\"\",\"mr\":\"\",\"ta\":\"\",\"ur\":\"\"}}");
        $projectInfo = new ProjectInfo();
        $projectInfo->setObject($object);

        $returnObject = $projectInfo->getObject();
        $this->assertObjectHasAttribute('id', $returnObject);
        $this->assertObjectHasAttribute('token', $returnObject);
        $this->assertObjectHasAttribute('project', $returnObject);
        $this->assertObjectHasAttribute('plataform', $returnObject);
        $this->assertObjectHasAttribute('carrier', $returnObject);
        $this->assertObjectHasAttribute('language_default', $returnObject);
        $this->assertObjectHasAttribute('providers', $returnObject);
        $this->assertObjectHasAttribute('phraseologies', $returnObject);
    }
}