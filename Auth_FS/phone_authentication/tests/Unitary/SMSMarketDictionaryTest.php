<?php

namespace Test\Unitary;

use Api\Model\SMSMarketDictionary;
use PHPUnit\Framework\TestCase;

class SMSMarketDictionaryTest extends TestCase
{
    public function testConsultDictionaryInvalid()
    {
        $response = SMSMarketDictionary::getStatusInformation('any', '000');
        $this->assertFalse($response);
    }

    public function testConsultRequestStatusValid()
    {
        $response = SMSMarketDictionary::getStatusInformation('request', '000');
        $this->assertTrue($response->status);
        $this->assertEquals('Success queued', $response->message);
    }

    public function testConsultRequestStatusInvalid()
    {
        $response = SMSMarketDictionary::getStatusInformation('request', '999');
        $this->assertFalse($response->status);
        $this->assertEquals('Invalid Code', $response->message);
    }

    public function testConsultCallbackStatusValid()
    {
        $response = SMSMarketDictionary::getStatusInformation('callback', -1);
        $this->assertTrue($response->status);
        $this->assertEquals('Message Queued', $response->message);
    }

    public function testConsultCallbackStatusInvalid()
    {
        $response = SMSMarketDictionary::getStatusInformation('callback', '999');
        $this->assertFalse($response->status);
        $this->assertEquals('Invalid Code', $response->message);
    }
}
