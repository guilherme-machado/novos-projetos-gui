<?php
namespace Test\Unitary;

use Api\Model\Entity\PhoneNumber;
use PHPUnit\Framework\TestCase;

class PhoneNumberTest extends TestCase
{
    public function testCreatePhoneNumberEntitySuccess()
    {
        $phoneNumber = new PhoneNumber('+5511922334455');
        $this->assertEquals('5511922334455', $phoneNumber->getPhoneNumberInternational(true));
    }

    public function testCreatePhoneNumberFormatMsisdnSuccess()
    {
        $phoneNumber = new PhoneNumber();
        $phoneNumber->formatMsisdn('11922334455');
        $this->assertEquals('+5511922334455', $phoneNumber->getPhoneNumberInternational());
    }

    public function testCreatePhoneNumberFormatFullMsisdnSuccess()
    {
        $phoneNumber = new PhoneNumber();
        $phoneNumber->formatMsisdn('+5511922334455');
        $this->assertEquals('+5511922334455', $phoneNumber->getPhoneNumberInternational());
    }

    public function testCreatePhoneNumberAndGetAllFormatsSuccess()
    {
        $phoneNumber = new PhoneNumber('+5511922334455');
        $this->assertEquals('+5511922334455', $phoneNumber->getPhoneNumberInternational());
        $this->assertEquals('5511922334455', $phoneNumber->getPhoneNumberInternational(false));
        $this->assertEquals('11922334455', $phoneNumber->getMsisdn());
        $this->assertEquals('922334455', $phoneNumber->getPhoneNumber());
        $this->assertEquals('11', $phoneNumber->getDdd());
        $this->assertEquals('55', $phoneNumber->getDdi());
        $this->assertEquals('+', $phoneNumber->getSymbol());

        $expected = (object) [
            'phoneNumberInternational' => '+5511922334455',
            'msisdn'                   => '11922334455',
            'phoneNumber'              => '922334455',
            'ddd'                      => '11',
            'ddi'                      => '55',
            'symbol'                   => '+'
        ];
        $this->assertEquals($expected, $phoneNumber->getPhoneNumberObject());
    }
}