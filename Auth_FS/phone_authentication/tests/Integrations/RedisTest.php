<?php
namespace Test\Integrations;

use Api\Model\CacheMemory;
use PHPUnit\Framework\TestCase;

class RedisTest extends TestCase
{
    protected $key;
    protected $value;

    public function setUp()
    {
        parent::setUp();
        require __DIR__ . "/../../src/constants.php";

        $this->key = 'hello';
        $this->value = 'Hello World!';
    }

    public function testInsertCacheMemory()
    {
        $cacheMemoryApi = new CacheMemory();
        $response = $cacheMemoryApi->set($this->key, $this->value);
        $this->assertTrue($response);
    }

    public function testFindCacheMemory()
    {
        $cacheMemoryApi = new CacheMemory();
        $response = $cacheMemoryApi->find($this->key);
        $this->assertContains($this->key, $response[0]);
    }

    public function testConsultCacheMemory()
    {
        $cacheMemoryApi = new CacheMemory();
        $response = $cacheMemoryApi->get($this->key);
        $this->assertContains($this->value, $response);
    }

    public function testDeleteCacheMemory()
    {
        $cacheMemoryApi = new CacheMemory();
        $response = $cacheMemoryApi->del($this->key);
        $this->assertTrue($response);
    }

    public function testInsertCacheMemoryWithObject()
    {
        $cacheMemory = new CacheMemory();
        $obj = new \stdClass();
        $obj->teste = true;
        $response = $cacheMemory->set($obj, 'oi');
        $this->assertFalse($response);
    }

    public function testFindCacheMemoryWithObject()
    {
        $cacheMemory = new CacheMemory();
        $obj = new \stdClass();
        $obj->teste = true;
        $response = $cacheMemory->find($obj);
        $this->assertEmpty($response);
    }

    public function testGetCacheMemoryWithObject()
    {
        $cacheMemory = new CacheMemory();
        $obj = new \stdClass();
        $obj->teste = true;
        $response = $cacheMemory->get($obj);
        $this->assertNull($response);
    }

    public function testDeleteCacheMemoryWithObject()
    {
        $cacheMemory = new CacheMemory();
        $obj = new \stdClass();
        $obj->teste = true;
        $response = $cacheMemory->del($obj);
        $this->assertFalse($response);
    }
}