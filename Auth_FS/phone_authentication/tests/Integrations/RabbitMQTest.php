<?php

namespace Test\Integrations;

use Api\Model\Queuing;
use Api\Model\Entity\SMSMessage;
use PHPUnit\Framework\TestCase;

class RabbitMQTest extends TestCase
{
    public function setUp()
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        require __DIR__ . "/../../src/constants.php";
    }

    public function testSetQueue()
    {
        $api = new Queuing();
        $this->assertObjectHasAttribute('queue', $api);
        $this->assertObjectHasAttribute('queue_retry', $api);
        $this->assertAttributeEmpty('queue', $api);
        $this->assertAttributeEmpty('queue_retry', $api);

        $api->setQueue('teste');
        $this->assertAttributeNotEmpty('queue', $api);
        $this->assertAttributeNotEmpty('queue_retry', $api);
    }

    public function testSetExchange()
    {
        $api = new Queuing();
        $this->assertObjectHasAttribute('exchange', $api);
        $this->assertObjectHasAttribute('exchange_retry', $api);
        $this->assertAttributeEmpty('exchange', $api);
        $this->assertAttributeEmpty('exchange_retry', $api);

        $api->setExchange('teste');
        $this->assertAttributeNotEmpty('exchange', $api);
        $this->assertAttributeNotEmpty('exchange_retry', $api);
    }

    public function testSetSMSMessage()
    {
        $message = new SMSMessage();
        $this->assertEmpty($message->getMessage());
        $this->assertEmpty($message->getTracker());
        $this->assertEmpty($message->getPhoneNumber());

        $messageExpected = 'teste';
        $message->setMessage($messageExpected);
        $this->assertEquals($messageExpected, $message->getMessage());

        $trackerExpected = '00000.00000';
        $message->setTracker($trackerExpected);
        $this->assertEquals($trackerExpected, $message->getTracker());

        $phoneNumberExpected = '0000000000000';
        $message->setPhoneNumber($phoneNumberExpected);
        $this->assertEquals($phoneNumberExpected, $message->getPhoneNumber());

        $api = new Queuing();
        $this->assertObjectHasAttribute('message', $api);
        $this->assertAttributeEmpty('message', $api);

        $api->setMessage($message);
        $this->assertAttributeNotEmpty('message', $api);
    }

    public function testSetTimeout()
    {
        $api = new Queuing();
        $this->assertObjectHasAttribute('timeout', $api);
        $this->assertAttributeEmpty('timeout', $api);

        $api->setTimeout(20000);
        $this->assertAttributeNotEmpty('timeout', $api);
    }

    public function testPublisherWhioutQueue()
    {
        $message = new SMSMessage();
        $message->setPhoneNumber('5511111111111');
        $message->setMessage('testeJson');
        $message->setTracker('11111.11111');

        $api = new Queuing();
        $api->setExchange('teste');
        $api->setMessage($message);
        $api->setTimeout(20000);
        $result = $api->publish();
        $this->assertEquals("Erro ao identificar 'queue'", $result);
    }

    public function testPublisherWhioutExchange()
    {
        $message = new SMSMessage();
        $message->setPhoneNumber('5511111111111');
        $message->setMessage('testeJson');
        $message->setTracker('11111.11111');

        $api = new Queuing();
        $api->setQueue('teste');
        $api->setMessage($message);
        $api->setTimeout(20000);
        $result = $api->publish();
        $this->assertEquals("Erro ao identificar 'exchange'", $result);
    }

    public function testPublisherWhioutMessage()
    {
        $message = new SMSMessage();
        $message->setPhoneNumber('5511111111111');
        $message->setMessage('testeJson');
        $message->setTracker('11111.11111');

        $api = new Queuing();
        $api->setQueue('teste');
        $api->setExchange('teste');
        $api->setTimeout(20000);
        $result = $api->publish();
        $this->assertEquals("Erro ao identificar 'message'", $result);
    }

    public function testPublisherWhioutTimeout()
    {
        $message = new SMSMessage();
        $message->setPhoneNumber('5511111111111');
        $message->setMessage('testeJson');
        $message->setTracker('11111.11111');

        $api = new Queuing();
        $api->setQueue('teste');
        $api->setExchange('exchange_teste');
        $api->setMessage($message);
        $result = $api->publish();
        $this->assertTrue($result);
    }

    public function testPublisherWithInvalidHost()
    {
        $message = new SMSMessage();
        $message->setPhoneNumber('5511111111111');
        $message->setMessage('testeJson');
        $message->setTracker('11111.11111');

        $api = new Queuing('0.0.0.0');
        $api->setQueue('teste');
        $api->setExchange('teste');
        $api->setMessage($message);
        $api->setTimeout(20000);
        $result = $api->publish();
        $this->assertNotTrue($result);
    }

    public function testPublisher()
    {
        $message = new SMSMessage();
        $message->setPhoneNumber('5511111111111');
        $message->setMessage('testeJson');
        $message->setTracker('11111.11111');

        $api = new Queuing();
        $api->setQueue('teste');
        $api->setExchange('exchange_teste');
        $api->setMessage($message);
        $api->setTimeout(20000);
        $result = $api->publish();
        $this->assertTrue($result);
    }
}