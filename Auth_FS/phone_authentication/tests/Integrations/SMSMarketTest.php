<?php
namespace Test\Integrations;

use Api\Model\Entity\SMSMessage;
use Api\Model\SMSMarket;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class SMSMarketTest extends TestCase
{
    public function setUp()
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        require __DIR__ . "/../../src/constants.php";
    }

    public function testSendSMSSuccess()
    {
        # Mock Logger
        $logger = $this->createMock(Logger::class);
        $logger->method('debug')->willReturn(true);
        $logger->method('error')->willReturn(true);

        # Mock SMSMessage
        $message = $this->createMock(SMSMessage::class);
        $message->method('getPhoneNumber')->willReturn('5511922334455');
        $message->method('getMessage')->willReturn('Hello World!');
        $message->method('getTracker')->willReturn('123456.123456');

        $smsmarket = new SMSMarket($logger);
        $response = $smsmarket->sendSMS($message);

        $this->assertObjectHasAttribute('sent', $response);
        $this->assertEquals(true, $response->sent);
    }
}