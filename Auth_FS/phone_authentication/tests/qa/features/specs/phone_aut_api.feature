#language:pt

@phone_auth_fs

Funcionalidade: Apis phone auth
O Phone Authenticator, é a nova ferramenta na qual , está dentro de nosso backend FS.
A Função dela é: Gerar o Token para o Usuário com intuito final de receber e autenticar o mesmo.
Assim qualquér usuário que usurfruir de nossos produtos poderá se autenticar e logar nos mesmos tendo produto ou não.
Existem 4 servidores nos quais se encarregarão de mandar o PINCODE com a devida agilidade que necessitamos, sendo um FS
E os outros 3 serviços serão contingencias quando o nosso não der vazão aos disparos.
Esse chaveamento de plataformas deve ser transparente para automação, deve ser premissa os scripts entenderem e trazerem a informação de onde irá sair.
É imprescindível que os serviçõs abordados nos testes abaixo, garamtam a qualidade e a estabilidade do serviço para que traga segurança para empresa.


 
Esquema do Cenário: Solicitar PIN "Tokens Válidos"

Dado que eu como qa, irei reunir as informações necessárias para estartar as apis
Quando eu estartar as <apis> os <tokens> devem ser validados com sucesso. 
Então  eu irei validar se as requisições foi sucesso "202" - Enfileirado.

Exemplos: 

|        apis        |                    tokens                   |
| "TOKEN_VIVOPROTEGE"|"Bearer c4116d9d-6cd7-4a3d-abcf-b98c7f6fcb68"|
| "TOKEN_VIVOPROTEGE"|"Bearer def2d8d3-9fd2-441c-be1a-999b068fa523"|
| "TOKEN_VIVOPROTEGE"|"Bearer d5db603c-9aaf-47af-92f3-368464e152a1"|
|   "OI SEGURANÇA"   |"Bearer a2892ce5-f0b9-4a6f-921e-e1f344d65952"|
|   "OI SEGURANÇA"   |"Bearer b4c006bd-2693-40a1-a71e-e8e11370e0d0"|
|    "TIM PROTECT"   |"Bearer 68af434e-3d08-46d4-b031-d56cbb31fde1"|
|    "TIM PROTECT"   |"Bearer 0e7daf57-d125-4eec-af9a-dcc8fbe9a737"|
|       "HERO"       |"Bearer f0db260c-0fff-4c33-9d76-d51d7cd553a2"|
|       "HERO"       |"Bearer 6e844b34-8ab3-4792-a18e-3a00ac0179d1"|
|  "TOKEN_VIVOSYNC"  |"Bearer d7219664-d1b3-46d8-a4d7-121291cb8605"|
|  "TOKEN_VIVOSYNC"  |"Bearer 3533e0c5-4863-4d2d-b065-52f93fd3312c"|
|  "TOKEN_VIVOSYNC"  |"Bearer adcbb495-a173-4a80-9b2a-6c6d9af60142"|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69b53cf"|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69b53cf"|
|"TIM PROTECT BACKUP"|"Bearer 46894718-1d64-432e-83e3-6ed805421e50"|
| "OI PROTEÇÃO SYNC" |"Bearer c0f499d5-89fc-4657-aa53-ba27ec725109"|
| "OI PROTEÇÃO SYNC" |"Bearer f2ff170a-3ace-4782-8844-5e2afcd8714d"|
| "OI PROTEÇÃO SYNC" |"Bearer e46336d2-3957-48f7-b05d-e48cac673054"|
|   "CLOUD BY HERO"  |"Bearer f48de281-5967-4699-8886-b446b21ec2f3"|
|   "CLOUD BY HERO"  |"Bearer 0b4f070d-3750-4944-9e7f-c9bbd62120ef"|
|   "CLOUD BY HERO"  |"Bearer 8afffd27-2adf-483e-8222-a49cd0cdfb8f"|
|   "Teste Infobip"  |"Bearer d5695a44-a96c-4b01-b286-044e03613930"|

Esquema do Cenário: Solicitar PIN "TOKENS Inválidos"

 Dado que eu como qa, irei reunir as informações necessárias para estartar as apis
 Quando eu estartar as <apis> os <tokens> devem ser validados com falha. 
 Então eu irei receber a mensagem "401"  - Token não autorizado - inválido

Exemplos: 

|        apis        |                    tokens                    |
| "TOKEN_VIVOPROTEGE"|"Bearer def2d8d3-9fd2-441c-be1a-999b068123456"|
| "TOKEN_VIVOPROTEGE"|"Bearer d5db603c-9aaf-47af-92f3-368464e123456"|
| "TOKEN_VIVOPROTEGE"|"Bearer c4116d9d-6cd7-4a3d-abcf-b98c7f6123456"|
|   "OI SEGURANÇA"   |"Bearer a2892ce5-f0b9-4a6f-921e-e1f344d123456"|
|   "OI SEGURANÇA"   |"Bearer b4c006bd-2693-40a1-a71e-e8e1137123456"|
|    "TIM PROTECT"   |"Bearer 68af434e-3d08-46d4-b031-d56cbb3123456"|
|    "TIM PROTECT"   |"Bearer 0e7daf57-d125-4eec-af9a-dcc8fbe123456"|
|       "HERO"       |"Bearer f0db260c-0fff-4c33-9d76-d51d7cd123456"|
|       "HERO"       |"Bearer 6e844b34-8ab3-4792-a18e-3a00ac0123456"|
|  "TOKEN_VIVOSYNC"  |"Bearer d7219664-d1b3-46d8-a4d7-121291c123456"|
|  "TOKEN_VIVOSYNC"  |"Bearer 3533e0c5-4863-4d2d-b065-52f93fd123456"|
|  "TOKEN_VIVOSYNC"  |"Bearer adcbb495-a173-4a80-9b2a-6c6d9af123456"|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69123456"|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69123456"|
|"TIM PROTECT BACKUP"|"Bearer 46894718-1d64-432e-83e3-6ed8054123456"|
| "OI PROTEÇÃO SYNC" |"Bearer c0f499d5-89fc-4657-aa53-ba27ec7123456"|
| "OI PROTEÇÃO SYNC" |"Bearer f2ff170a-3ace-4782-8844-5e2afcd123456"|
| "OI PROTEÇÃO SYNC" |"Bearer e46336d2-3957-48f7-b05d-e48cac6123456"|
|   "CLOUD BY HERO"  |"Bearer f48de281-5967-4699-8886-b446b21123456"|
|   "CLOUD BY HERO"  |"Bearer 0b4f070d-3750-4944-9e7f-c9bbd62123456"|
|   "CLOUD BY HERO"  |"Bearer 8afffd27-2adf-483e-8222-a49cd0c123456"|
|   "Teste Infobip"  | "Bearer d5695a44-a96c-4b01-b286-044e03613965"|

Esquema do Cenário: Solicitar PIN "MSISDN Inválido"

Dado que eu como qa, irei reunir as informações necessárias para estartar as apis
Quando eu estartar a <apis> com um <tokens> válido e um <msisdn> inválido 
Então eu irei receber a mensagem  "400" -Número de telefone MSISDN inválido

Exemplos:



|        apis        |                    tokens                   | msisdn|
| "TOKEN_VIVOPROTEGE"|"Bearer def2d8d3-9fd2-441c-be1a-999b068fa523"|"12345"|
| "TOKEN_VIVOPROTEGE"|"Bearer d5db603c-9aaf-47af-92f3-368464e152a1"|"12345"|
| "TOKEN_VIVOPROTEGE"|"Bearer c4116d9d-6cd7-4a3d-abcf-b98c7f6fcb68"|"12345"|
|   "OI SEGURANÇA"   |"Bearer a2892ce5-f0b9-4a6f-921e-e1f344d65952"|"12345"|
|   "OI SEGURANÇA"   |"Bearer b4c006bd-2693-40a1-a71e-e8e11370e0d0"|"12345"|
|    "TIM PROTECT"   |"Bearer 68af434e-3d08-46d4-b031-d56cbb31fde1"|"12345"|
|    "TIM PROTECT"   |"Bearer 0e7daf57-d125-4eec-af9a-dcc8fbe9a737"|"12345"|
|       "HERO"       |"Bearer f0db260c-0fff-4c33-9d76-d51d7cd553a2"|"12345"|
|       "HERO"       |"Bearer 6e844b34-8ab3-4792-a18e-3a00ac0179d1"|"12345"|
|  "TOKEN_VIVOSYNC"  |"Bearer d7219664-d1b3-46d8-a4d7-121291cb8605"|"12345"|
|  "TOKEN_VIVOSYNC"  |"Bearer 3533e0c5-4863-4d2d-b065-52f93fd3312c"|"12345"|
|  "TOKEN_VIVOSYNC"  |"Bearer adcbb495-a173-4a80-9b2a-6c6d9af60142"|"12345"|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69b53cf"|"12345"|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69b53cf"|"12345"|
|"TIM PROTECT BACKUP"|"Bearer 46894718-1d64-432e-83e3-6ed805421e50"|"12345"|
| "OI PROTEÇÃO SYNC" |"Bearer c0f499d5-89fc-4657-aa53-ba27ec725109"|"12345"|
| "OI PROTEÇÃO SYNC" |"Bearer f2ff170a-3ace-4782-8844-5e2afcd8714d"|"12345"|
| "OI PROTEÇÃO SYNC" |"Bearer e46336d2-3957-48f7-b05d-e48cac673054"|"12345"|
|   "CLOUD BY HERO"  |"Bearer f48de281-5967-4699-8886-b446b21ec2f3"|"12345"|
|   "CLOUD BY HERO"  |"Bearer 0b4f070d-3750-4944-9e7f-c9bbd62120ef"|"12345"|
|   "CLOUD BY HERO"  |"Bearer 8afffd27-2adf-483e-8222-a49cd0cdfb8f"|"12345"|
|   "Teste Infobip"  |"Bearer d5695a44-a96c-4b01-b286-044e03613930"|"12345"|


Esquema do Cenário: Solicitar Pincode Válido e Reenvio de PIN

Dado que eu como qa, irei reunir as informações necessárias para estartar as apis
Quando eu estartar a <apis> com um <tokens> válido e um <msisdn> já validado 
Então eu irei receber a mensagem  "200"  - Requisição recebida com sucesso
E vou reenviar meu PIN passando corretamente meu token junto com o meu transaction id assim recebendo o retorno "200" e Mensagem enviada com sucesso

Exemplos:

|        apis        |                    tokens                   |    msisdn   |   S.O   |  TransactionID's  |
| "TOKEN_VIVOPROTEGE"|"Bearer def2d8d3-9fd2-441c-be1a-999b068fa523"|"10951480431"|  "iOS"  |"TransactionID -->"|
| "TOKEN_VIVOPROTEGE"|"Bearer d5db603c-9aaf-47af-92f3-368464e152a1"|"10951480432"|  "Web"  |"TransactionID -->"|
| "TOKEN_VIVOPROTEGE"|"Bearer c4116d9d-6cd7-4a3d-abcf-b98c7f6fcb68"|"10951480433"|"Android"|"TransactionID -->"|
|   "OI SEGURANÇA"   |"Bearer a2892ce5-f0b9-4a6f-921e-e1f344d65952"|"10951480434"|"Android"|"TransactionID -->"|
|   "OI SEGURANÇA"   |"Bearer b4c006bd-2693-40a1-a71e-e8e11370e0d0"|"10951480435"|  "iOS"  |"TransactionID -->"|
|    "TIM PROTECT"   |"Bearer 68af434e-3d08-46d4-b031-d56cbb31fde1"|"10951480436"|"Android"|"TransactionID -->"|
|    "TIM PROTECT"   |"Bearer 0e7daf57-d125-4eec-af9a-dcc8fbe9a737"|"10951480437"|  "iOS"  |"TransactionID -->"|
|       "HERO"       |"Bearer f0db260c-0fff-4c33-9d76-d51d7cd553a2"|"10951480438"|"Android"|"TransactionID -->"|
|       "HERO"       |"Bearer 6e844b34-8ab3-4792-a18e-3a00ac0179d1"|"10951480439"|  "iOS"  |"TransactionID -->"|
|  "TOKEN_VIVOSYNC"  |"Bearer d7219664-d1b3-46d8-a4d7-121291cb8605"|"10951480440"|"Android"|"TransactionID -->"|
|  "TOKEN_VIVOSYNC"  |"Bearer 3533e0c5-4863-4d2d-b065-52f93fd3312c"|"10951480441"|  "iOS"  |"TransactionID -->"|
|  "TOKEN_VIVOSYNC"  |"Bearer adcbb495-a173-4a80-9b2a-6c6d9af60142"|"10951480442"|  "Web"  |"TransactionID -->"|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69b53cf"|"10951480443"|"Android"|"TransactionID -->"|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69b53cf"|"10951480444"|  "iOS"  |"TransactionID -->"|
|"TIM PROTECT BACKUP"|"Bearer 46894718-1d64-432e-83e3-6ed805421e50"|"10951480445"|  "Web"  |"TransactionID -->"|
| "OI PROTEÇÃO SYNC" |"Bearer c0f499d5-89fc-4657-aa53-ba27ec725109"|"10951480446"|"Android"|"TransactionID -->"|
| "OI PROTEÇÃO SYNC" |"Bearer f2ff170a-3ace-4782-8844-5e2afcd8714d"|"10951480447"|  "iOS"  |"TransactionID -->"|
| "OI PROTEÇÃO SYNC" |"Bearer e46336d2-3957-48f7-b05d-e48cac673054"|"10951480448"|  "Web"  |"TransactionID -->"|
|   "CLOUD BY HERO"  |"Bearer f48de281-5967-4699-8886-b446b21ec2f3"|"10951480449"|"Android"|"TransactionID -->"|
|   "CLOUD BY HERO"  |"Bearer 0b4f070d-3750-4944-9e7f-c9bbd62120ef"|"10951480450"|  "iOS"  |"TransactionID -->"|
|   "CLOUD BY HERO"  |"Bearer 8afffd27-2adf-483e-8222-a49cd0cdfb8f"|"10951480451"|  "Web"  |"TransactionID -->"|
|   "Teste Infobip"  |"Bearer d5695a44-a96c-4b01-b286-044e03613930"|"10951480452"|  "web"  |"TransactionID -->"|

# Cenário: Validação de Pincode

# Dado que eu como qa, irei reunir as informações necessárias para estartar as apis
# Quando eu estartar a apis de validação do com os tokens e msisdn válidos o PIN será enviado ao meu msisdn e validado com sucesso.
# Então a mensagem exibida após a validação deve ser "200" -  "Pincode validado com sucesso"



Esquema do Cenário: Validação de Pincode com PIN/transaction id inválidos

Dado que eu como qa irei inserir um MSISDN válido
Quando eu estartar a <apis> com o um <tokens> válido e <pins> inválido e transaction_id inválido
Então eu receberei a mensagem "403" Token Inválido ou expirado "u00e1lido ou expirado" -  Pincode inválido

Exemplos: 

|        apis        |                    tokens                   |  pins | TransactionID's |
| "TOKEN_VIVOPROTEGE"|"Bearer def2d8d3-9fd2-441c-be1a-999b068fa523"|"12345"|TransactionID -->|
| "TOKEN_VIVOPROTEGE"|"Bearer d5db603c-9aaf-47af-92f3-368464e152a1"|"12345"|TransactionID -->|
| "TOKEN_VIVOPROTEGE"|"Bearer c4116d9d-6cd7-4a3d-abcf-b98c7f6fcb68"|"12345"|TransactionID -->|
|   "OI SEGURANÇA"   |"Bearer a2892ce5-f0b9-4a6f-921e-e1f344d65952"|"12345"|TransactionID -->|
|   "OI SEGURANÇA"   |"Bearer b4c006bd-2693-40a1-a71e-e8e11370e0d0"|"12345"|TransactionID -->|
|    "TIM PROTECT"   |"Bearer 68af434e-3d08-46d4-b031-d56cbb31fde1"|"12345"|TransactionID -->|
|    "TIM PROTECT"   |"Bearer 0e7daf57-d125-4eec-af9a-dcc8fbe9a737"|"12345"|TransactionID -->|
|       "HERO"       |"Bearer f0db260c-0fff-4c33-9d76-d51d7cd553a2"|"12345"|TransactionID -->|
|       "HERO"       |"Bearer 6e844b34-8ab3-4792-a18e-3a00ac0179d1"|"12345"|TransactionID -->|
|  "TOKEN_VIVOSYNC"  |"Bearer d7219664-d1b3-46d8-a4d7-121291cb8605"|"12345"|TransactionID -->|
|  "TOKEN_VIVOSYNC"  |"Bearer 3533e0c5-4863-4d2d-b065-52f93fd3312c"|"12345"|TransactionID -->|
|  "TOKEN_VIVOSYNC"  |"Bearer adcbb495-a173-4a80-9b2a-6c6d9af60142"|"12345"|TransactionID -->|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69b53cf"|"12345"|TransactionID -->|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69b53cf"|"12345"|TransactionID -->|
|"TIM PROTECT BACKUP"|"Bearer 46894718-1d64-432e-83e3-6ed805421e50"|"12345"|TransactionID -->|
| "OI PROTEÇÃO SYNC" |"Bearer c0f499d5-89fc-4657-aa53-ba27ec725109"|"12345"|TransactionID -->|
| "OI PROTEÇÃO SYNC" |"Bearer f2ff170a-3ace-4782-8844-5e2afcd8714d"|"12345"|TransactionID -->|
| "OI PROTEÇÃO SYNC" |"Bearer e46336d2-3957-48f7-b05d-e48cac673054"|"12345"|TransactionID -->|
|   "CLOUD BY HERO"  |"Bearer f48de281-5967-4699-8886-b446b21ec2f3"|"12345"|TransactionID -->|
|   "CLOUD BY HERO"  |"Bearer 0b4f070d-3750-4944-9e7f-c9bbd62120ef"|"12345"|TransactionID -->|
|   "CLOUD BY HERO"  |"Bearer 8afffd27-2adf-483e-8222-a49cd0cdfb8f"|"12345"|TransactionID -->|
|   "Teste Infobip"  |"Bearer d5695a44-a96c-4b01-b286-044e03613930"|"12345"|TransactionID -->|


Esquema do Cenário: Pincode inválido

Dado que eu como qa irei inserir um MSISDN válido
Quando eu estartar a <apis> com o um <tokens> válido e <pins> inválido
Então eu receberei a mensagem  "401" - "Pincode inv"

Exemplos: 


|        apis        |                    tokens                   |  pins  | TransactionID's |
| "TOKEN_VIVOPROTEGE"|"Bearer def2d8d3-9fd2-441c-be1a-999b068fa523"|"123#$%"|TransactionID -->|
| "TOKEN_VIVOPROTEGE"|"Bearer d5db603c-9aaf-47af-92f3-368464e152a1"|"123#$%"|TransactionID -->|
| "TOKEN_VIVOPROTEGE"|"Bearer c4116d9d-6cd7-4a3d-abcf-b98c7f6fcb68"|"123#$%"|TransactionID -->|
|   "OI SEGURANÇA"   |"Bearer a2892ce5-f0b9-4a6f-921e-e1f344d65952"|"123#$%"|TransactionID -->|
|   "OI SEGURANÇA"   |"Bearer b4c006bd-2693-40a1-a71e-e8e11370e0d0"|"123#$%"|TransactionID -->|
|    "TIM PROTECT"   |"Bearer 68af434e-3d08-46d4-b031-d56cbb31fde1"|"123#$%"|TransactionID -->|
|    "TIM PROTECT"   |"Bearer 0e7daf57-d125-4eec-af9a-dcc8fbe9a737"|"123#$%"|TransactionID -->|
|       "HERO"       |"Bearer f0db260c-0fff-4c33-9d76-d51d7cd553a2"|"123#$%"|TransactionID -->|
|       "HERO"       |"Bearer 6e844b34-8ab3-4792-a18e-3a00ac0179d1"|"123#$%"|TransactionID -->|
|  "TOKEN_VIVOSYNC"  |"Bearer d7219664-d1b3-46d8-a4d7-121291cb8605"|"123#$%"|TransactionID -->|
|  "TOKEN_VIVOSYNC"  |"Bearer 3533e0c5-4863-4d2d-b065-52f93fd3312c"|"123#$%"|TransactionID -->|
|  "TOKEN_VIVOSYNC"  |"Bearer adcbb495-a173-4a80-9b2a-6c6d9af60142"|"123#$%"|TransactionID -->|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69b53cf"|"123#$%"|TransactionID -->|
|"TIM PROTECT BACKUP"|"Bearer c25fb754-7ccb-4ea5-8b26-56b4a69b53cf"|"123#$%"|TransactionID -->|
|"TIM PROTECT BACKUP"|"Bearer 46894718-1d64-432e-83e3-6ed805421e50"|"123#$%"|TransactionID -->|
| "OI PROTEÇÃO SYNC" |"Bearer c0f499d5-89fc-4657-aa53-ba27ec725109"|"123#$%"|TransactionID -->|
| "OI PROTEÇÃO SYNC" |"Bearer f2ff170a-3ace-4782-8844-5e2afcd8714d"|"123#$%"|TransactionID -->|
| "OI PROTEÇÃO SYNC" |"Bearer e46336d2-3957-48f7-b05d-e48cac673054"|"123#$%"|TransactionID -->|
|   "CLOUD BY HERO"  |"Bearer f48de281-5967-4699-8886-b446b21ec2f3"|"123#$%"|TransactionID -->|
|   "CLOUD BY HERO"  |"Bearer 0b4f070d-3750-4944-9e7f-c9bbd62120ef"|"123#$%"|TransactionID -->|
|   "CLOUD BY HERO"  |"Bearer 8afffd27-2adf-483e-8222-a49cd0cdfb8f"|"123#$%"|TransactionID -->|
|   "Teste Infobip"  |"Bearer d5695a44-a96c-4b01-b286-044e03613930"|"123#$%"|TransactionID -->|



