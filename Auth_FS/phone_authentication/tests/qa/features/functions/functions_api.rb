def solicita_pin (msisdn,tokens)
   
    body = {
        "msisdn": "#{msisdn}"
        }
         HTTParty.post("#{DATA['end_point']['api']}/phone-auth/send",
        :body => body.to_json,
        :headers => {"Content-Type" => 'application/json', "Authorization" => "#{tokens}"  
         })
         

end



def resend_pin (transaction_id, token_resend)
   
    body ={
        "transaction_id": "#{transaction_id}"
    }
         HTTParty.post("#{DATA['end_point']['api']}/phone-auth/resend",
        :body => body.to_json,
        :headers => {"Content-Type" => 'application/json', "Authorization" => "#{token_resend}"  
         })
         

end


def captura_pin (transaction_id)
    HTTParty.get("#{DATA['end_point']['api']}/admin/get-tracker?transaction_id=#{transaction_id}" ,
     
   :headers => {"Content-Type" => 'application/json', "Authorization" => "Bearer FS-1985f52a-3c19-41b5-a85e-790a42dd4b29" 
   })
   
 end 

def validate_pin (transaction_id, token_resend, data_pin)
   
    body ={
        "transaction_id": "#{transaction_id}",
        "pincode": "#{data_pin}"
    }
         HTTParty.post("#{DATA['end_point']['api']}/phone-auth/validate",
        :body => body.to_json,
        :headers => {"Content-Type" => 'application/json', "Authorization" => "#{token_resend}"  
         })
         

end

def validate_pin2 (transaction_id2, tokens, pin)
   
    body ={
        "transaction_id": "#{transaction_id2}",
        "pincode": "#{pin}"
    }
         HTTParty.post("#{DATA['end_point']['api']}/phone-auth/validate",
        :body => body.to_json,
        :headers => {"Content-Type" => 'application/json', "Authorization" => "#{tokens}"  
         })
         

end

def memory_recharge
         
        HTTParty.post("#{DATA['end_point']['api']}/admin/update-settings?method=flush",
        :headers => {"Authorization" => "Bearer FS-1985f52a-3c19-41b5-a85e-790a42dd4b29"  
})
end
    
 