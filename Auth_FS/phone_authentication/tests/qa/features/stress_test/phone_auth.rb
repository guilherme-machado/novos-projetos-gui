# require 'capybara/dsl'
# require 'capybara/rspec/matchers'
# require 'selenium-webdriver'
# require 'rspec'
# require 'httparty'
# require 'pry'
# require 'faker'
# require 'site_prism'
# require 'cpf_faker'
# require 'mysql2'
# require 'net/https'

# require_relative 'database.rb'

# @token_hero_android = "Bearer f0db260c-0fff-4c33-9d76-d51d7cd553a2"
# @token_hero_ios = "Bearer 6e844b34-8ab3-4792-a18e-3a00ac0179d1"

# # token_vivo_protege_android = "Bearer c4116d9d-6cd7-4a3d-abcf-b98c7f6fcb68"
# # token_vivo_protege_iOS = "Bearer def2d8d3-9fd2-441c-be1a-999b068fa523"
# # token_vivo_protege_web = "Bearer d5db603c-9aaf-47af-92f3-368464e152a1"

# # token_tim_protect_android = "Bearer 68af434e-3d08-46d4-b031-d56cbb31fde1"
# # token_tim_protect_iOS = "Bearer 0e7daf57-d125-4eec-af9a-dcc8fbe9a737"

# # token_oi_seguranca_android = "Bearer a2892ce5-f0b9-4a6f-921e-e1f344d65952"
# # token_oi_seguranca_ios = "Bearer b4c006bd-2693-40a1-a71e-e8e11370e0d0"


# def solicita_pin(msisdn, token)
#     body = {
#         "msisdn": msisdn
#         }
#         response = HTTParty.post("https://phone-authentication.whitelabel.com.br/phone-auth/send",
#         :body => body.to_json,
#         :headers => {"Content-Type" => 'application/json', "Authorization" => "#{token}"
#          })
#     transaction_id = response.parsed_response["data"]["transaction_id"]
#     transaction_id
# end

# def resend_pin(transaction_id, token_resend)
#     body ={
#         "transaction_id": "#{transaction_id}"
#     }
#          HTTParty.post("https://phone-authentication.whitelabel.com.br/phone-auth/resend",
#         :body => body.to_json,
#         :headers => {"Content-Type" => 'application/json', "Authorization" => "#{token}"  
#          })
# end

# def captura_pin(transaction_id,token)
#     response = HTTParty.get("https://phone-authentication.whitelabel.com.br/phone-auth/extra/get-tracker-info?transaction_id=#{transaction_id}" ,
#    :headers => {"Content-Type" => 'application/json', "Authorization" => "#{token}" 
#    })
#    pincode = response.parsed_response["data"]["details"]["pincode"]
#    pincode
# end 

# def validate_pin(transaction_id, token, pincode)
#     body ={
#         "transaction_id": "#{transaction_id}",
#         "pincode": "#{pincode}"
#     }
#         response = HTTParty.post("https://phone-authentication.whitelabel.com.br/phone-auth/validate",
#         :body => body.to_json,
#         :headers => {"Content-Type" => 'application/json', "Authorization" => "#{token}"  
#          })
#         result = response.parsed_response["status"]
# end

# @db = Database.new
# # @status_pincode = "NULL"

# def test(token)
#     for i in 0..300
#         msisdn = Faker::Base.numerify('109########')
#         dateStart = Time.now.strftime("%Y/%m/%d %H:%M:%S")

#         puts "MSISDN: #{msisdn}"
#         puts "START: #{dateStart}"

#         #SOLICITA O PIN
#         transaction_id = solicita_pin(msisdn, token)
#         puts "TRANSACTION_ID: #{transaction_id}"

#         #CAPTURA O PIN
#         pincode = captura_pin(transaction_id, token)
#         puts "PINCODE: #{pincode}"

#         #VALIDA O PIN
#         result_pin = validate_pin(transaction_id, token, pincode)
#         puts "RESULT_PINCODE: #{result_pin}"

#         #INSERT NO DATABASE
#         if token == @token_hero_android
#             product = "Hero"
#             os = "Android"
#         elsif token == @token_hero_ios
#             product = "Hero"
#             os = "iOS"
#         end

#         @db.insert(dateStart, product, os, msisdn, transaction_id, result_pin)
#         dateEnd = Time.now.strftime("%Y/%m/%d %H:%M:%S")
#         puts "END: #{dateEnd}"
        
#         puts
#         puts "===================================================================="
#         puts
#     end
# end

# def stress(token)
#     t1 = Thread.new{test(token)}
#     t2 = Thread.new{test(token)}
#     t3 = Thread.new{test(token)}
#     t4 = Thread.new{test(token)}
#     t5 = Thread.new{test(token)}
#     t6 = Thread.new{test(token)}

#     t1.join
#     t2.join
#     t3.join
#     t4.join
#     t5.join
#     t6.join
# end

# stress(@token_hero_android)
# stress(@token_hero_ios)