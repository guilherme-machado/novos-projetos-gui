#encoding:utf-8


Dado("que eu como qa, irei reunir as informações necessárias para estartar as apis") do
  @msisdn = Faker::Base.numerify('109514#####')
    end
  
Quando("eu estartar as {string} os {string} devem ser validados com sucesso.") do |nome, tokens|
  @result = solicita_pin(@msisdn, tokens)
  @body = puts @result.response.body
  @code = puts @result.response.code  
    end
  

Então("eu irei validar se as requisições foi sucesso {string} - Enfileirado.") do |status|
    puts @msisdn
    expect(@result.response.code).to eql status
    expect(@result.response.body).to include "PINCODE"
      end


Quando("eu estartar as {string} os {string} devem ser validados com falha.") do |apis, tokens|
    @result = solicita_pin(@msisdn, tokens)
    @body = puts @result.response.body
    @code = puts @result.response.code  
      end
    
Então("eu irei receber a mensagem {string}  - Token não autorizado - inválido") do |status|
      puts @msisdn
      expect(@result.response.code).to eql status
      expect(@result.response.body).to include "Token inv"
      end

       
Quando("eu estartar a {string} com um {string} válido e um {string} inválido") do |apis, tokens, msisdn|
    @result = solicita_pin(msisdn, tokens)
    @body = puts @result.response.body
    @code = puts @result.response.code  
      end
    
Então("eu irei receber a mensagem  {string} -Número de telefone MSISDN inválido") do |status|
     expect(@result.response.code).to eql status
     expect(@result.response.body).to include "telefone (MSISDN) inv"
      end

      #Expandir cobertura do reenvio de pin furamente. Atualmente cobre apenas os "200"

Quando("eu estartar a {string} com um {string} válido e um {string} já validado") do |apis, tokens, msisdn|
    @token_resend = tokens
    for cadastro in 1..2 do
    @result = solicita_pin(msisdn, tokens)
    sleep(1)  
      end
    
    # @body =  @result.response.body
    # @code =  @result.response.code  
      end
      
Então("eu irei receber a mensagem  {string}  - Requisição recebida com sucesso") do |status|
    expect(@result.response.code).to eql status
    expect(@result.response.body).to include "#PINCODE# e seu codigo de verificacao do #PROJECT#"
    data = @result.parsed_response["data"]["transaction_id"]
    puts data
    @transaction_id = data
    $Transaction_1d2 = data
      end
Então("vou reenviar meu PIN passando corretamente meu token junto com o meu transaction id assim recebendo o retorno {string} e Mensagem enviada com sucesso") do |status|
    @reenv_pin = resend_pin(@transaction_id, @token_resend)
    expect(@reenv_pin.response.code).to eql status
    expect(@reenv_pin.response.body).to include "#PINCODE# e seu codigo de verificacao do #PROJECT#"
# Quando("eu estartar a apis de validação do com os tokens e msisdn válidos o PIN será enviado ao meu msisdn e validado com sucesso.") do 
    response = captura_pin(@transaction_id)
    @data_pin  = response.parsed_response["data"]["details"]["pincode"]
    expect(response.response.code).to eql "200"
    expect(response.response.body).to include "true"
    puts @data_pin
    # Então("a mensagem exibida após a validação deve ser {string} -  {string}") do |status, message|
    pincode = validate_pin(@transaction_id, @token_resend, @data_pin)
    expect(pincode.response.code).to eql "200"
      end

Dado("que eu como qa irei inserir um MSISDN válido") do
 @transaction_id = Faker::Base.numerify('##########################.#######################################################')
     end

Quando("eu estartar a {string} com o um {string} válido e {string} inválido e transaction_id inválido") do |apis, tokens, pins|
    @result = validate_pin2(@transaction_id, tokens, pins)
    puts @transaction_id
     end

Então("eu receberei a mensagem {string} Token Inválido ou expirado {string} -  Pincode inválido") do |status, message|
  puts @result
  expect(@result.response.code). to eql status
  expect(@result.response.body).to include message
    end      

 Quando("eu estartar a {string} com o um {string} válido e {string} inválido") do |apis, tokens, pins|
   @result = validate_pin2($Transaction_1d2, tokens, pins)
  puts $Transaction_1d2
    end
    
 Então("eu receberei a mensagem  {string} - {string}") do |status, message|
  puts @result
  expect(@result.response.code). to eql status
  expect(@result.response.body).to include message

  # memory_recharge.memory_recharge
  # expect(memory_recharge.response.code).to eql "200"
  # expect(memory_recharge.reponse.body).to include "true"
 end
  

