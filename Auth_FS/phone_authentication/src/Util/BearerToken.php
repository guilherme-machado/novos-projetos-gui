<?php
namespace Api\Util;

class BearerToken
{
    /**
     * Valida o header 'Bearer' e retorna token de acesso
     * @param $headerAuthorization
     * @return string|bool
     */
    public static function getToken($headerAuthorization)
    {
        if (!empty($headerAuthorization) && preg_match('/Bearer\s(\S+)/', $headerAuthorization, $matches)) {
            return $matches[1];
        }
        return false;
    }
}