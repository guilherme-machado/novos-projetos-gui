<?php

namespace Api\Util;

use Api\Model\CacheMemory;
use Api\Model\ProjectInfo;
use Api\Model\Entity\PhoneNumber;

class Tracker
{
    /**
     * @param PhoneNumber $phoneNumber
     * @param $tokenId
     * @param $pincode
     * @param $locale
     * @return bool|string
     */
    public static function create(PhoneNumber $phoneNumber, $tokenId, $pincode, $locale)
    {
        if (empty($phoneNumber->getMsisdn()) || empty($tokenId) || empty($pincode) || empty($locale)) {
            return false;
        }
        return self::encryptPhoneNumber($phoneNumber) . "." . self::encryptDetails($tokenId, $pincode, $locale);
    }

    /**
     * @param PhoneNumber $phoneNumber
     * @return string
     */
    public static function encryptPhoneNumber(PhoneNumber $phoneNumber)
    {
        return bin2hex($phoneNumber->getPhoneNumberInternational(false));
    }

    /**
     * @param $hex
     * @return PhoneNumber
     */
    public static function decryptPhoneNumber($hex)
    {
        $msisdn = hex2bin($hex);
        return new PhoneNumber($msisdn);
    }

    /**
     * @param string $tokenId
     * @param string $pincode
     * @param string $locale
     * @param string $microtime
     * @return string
     */
    public static function encryptDetails($tokenId, $pincode, $locale, $microtime = null)
    {
        if ($microtime === null) {
            $microtime = microtime(true);
        }
        return bin2hex($tokenId . "." . $pincode . "." . $locale . "." . $microtime);
    }

    /**
     * @param $hex
     * @return object
     */
    public static function decryptDetails($hex)
    {
        $vars = hex2bin($hex);
        list($tokenId, $pincode, $locale, $microtime) = explode('.', $vars, 4);
        return (object)['tokenId' => $tokenId, 'pincode' => $pincode, 'locale' => $locale, 'microtime' => $microtime];
    }

    /**
     * @param $tracker
     * @param bool $validateTracker
     * @return bool|object
     */
    public static function getTracker($tracker, $validateTracker = true)
    {
        # Consulta TRACKER
        $cacheMemoryApi = new CacheMemory();
        $cacheMemoryResponse = $cacheMemoryApi->get($tracker);

        $trackerIsValid = false;
        if (isset($cacheMemoryResponse) && !empty($cacheMemoryResponse)) {
            $trackerIsValid = true;
        }
        $data['trackerValid'] = $trackerIsValid;

        # Valida TRACKER
        if ($validateTracker === true && $trackerIsValid === false) {
            return false;
        }

        # Descriptografa as informações do TRACKER
        list($phoneCrypted, $detailsCrypted) = explode('.', $tracker);
        $data['phoneNumber'] = self::decryptPhoneNumber($phoneCrypted);
        $data['details'] = self::decryptDetails($detailsCrypted);

        # Carrega o projeto referente ao TRACKER
        $projectToken = $cacheMemoryApi->get('TID' . $data['details']->tokenId);
        $data['project'] = new ProjectInfo(json_decode($cacheMemoryApi->get($projectToken)));

        # Retorna PhoneNumber, TrackerDetails e ProjectInfo
        return (object)$data;
    }
}