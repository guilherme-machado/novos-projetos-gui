<?php

namespace Api\Util;

class Convert
{
    /**
     * @param $a
     * @return array|object
     */
    public static function arrayToObject($a)
    {
        if (is_array($a)) {
            foreach ($a as $k => $v) {
                if (is_integer($k)) {
                    $a['index'][$k] = self::arrayToObject($v);
                } else {
                    $a[$k] = self::arrayToObject($v);
                }
            }

            return (object)$a;
        }

        return $a;
    }
}