<?php
namespace Api\Util;

class Pincode
{
    public static function create()
    {
        return sprintf("%06d", mt_rand(1, 999999));
    }
}