<?php
namespace Api\Middleware;

use Api\Util\BearerToken;

class AdminBearer
{
    private $validToken = 'FS-1985f52a-3c19-41b5-a85e-790a42dd4b29';

    /**
     * Bearer middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        $header = $request->getHeader("Authorization")[0];

        $token = BearerToken::getToken($header);
        if($token === false){
            return $response->withJson(['status' => false, 'data' => 'Token não encontrado'], 401);
        } else if ($token !== $this->validToken){
            return $response->withJson(['status' => false, 'data' => 'Token inválido'], 401);
        } else {
            return $next($request, $response);
        }
    }
}