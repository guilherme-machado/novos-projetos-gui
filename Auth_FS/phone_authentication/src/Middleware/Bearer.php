<?php
namespace Api\Middleware;

use Api\Model\CacheConfig;
use Api\Util\BearerToken;

class Bearer
{
    /**
     * Bearer middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {
        $header = $request->getHeader("Authorization")[0];
        $token = BearerToken::getToken($header);
        if ($token === false){
            return $response->withJson(['status' => false, 'data' => 'Token não encontrado'], 401);
        }

        $cacheConfig = new CacheConfig();
        $projectConfig = $cacheConfig->get($token);
        if (!isset($projectConfig) || empty($projectConfig)){
            $cacheConfig->setConfigs();
            $projectConfig = $cacheConfig->get($token);
            if (!isset($projectConfig) || empty($projectConfig)){
                return $response->withJson(['status' => false, 'data' => 'Authorization Token inválido'], 401);
            }
        }

        $request = $request->withAttribute('projectInfo', json_decode($projectConfig));
        return $next($request, $response);
    }
}