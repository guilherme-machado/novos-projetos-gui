<?php

namespace Api\Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use Api\Model\SMSMarketCallback;
use Api\Model\InfobipCallback;
use Api\Model\TwilioCallback;
use Api\Model\CommissioningLog;
use Api\Model\AppLog;
use Api\Util\Convert;

class CallbackController
{
    private $logger;
    private $commissioningLog;
    private $startTime;
    private $action;

    /**
     * CallbackController constructor.
     */
    public function __construct()
    {
        $this->logger = new AppLog('callback', 'callback');
        $this->commissioningLog = new CommissioningLog();
        $this->startTime = microtime(true);
        $this->action = 'callback_pincode';
    }

    public function providers(Request $request, Response $response, array $args)
    {
        $this->logger->debug("Callback::Providers: " . json_encode([
                'args' => $args,
                'params' => $request->getParams()
            ]));

        switch ($args['provider']) {
            case 'twilio':
                $messageStatus = $request->getParam('MessageStatus');
                $callback = (new TwilioCallback())
                    ->setId($request->getParam('SmsSid'))
                    ->setPhoneNumber($request->getParam('To'))
                    ->setTracker($args['tracker'])
                    ->setStatusCode($messageStatus)
                    ->setStatusMessage($messageStatus)
                    ->setResponseStatus($messageStatus)
                    ->setResponseDescription($messageStatus);
                break;
            case 'sms-market':
                $statusCode = $request->getParam('status');
                $callback = (new SMSMarketCallback())
                    ->setId($request->getParam('id'))
                    ->setPhoneNumber($request->getParam('number'))
                    ->setTracker($request->getParam('campaign_id'))
                    ->setCarrier($request->getParam('carrier'))
                    ->setStatusCode($statusCode)
                    ->setStatusMessage($request->getParam('content'))
                    ->setStatusDatetime($request->getParam('date'))
                    ->setResponseStatus($statusCode)
                    ->setResponseDescription($statusCode);
                break;
            case 'infobip':
                $results = Convert::arrayToObject($request->getParam('results')[0]);
                $callback = (new InfobipCallback())
                    ->setId($results->messageId)
                    ->setPhoneNumber($results->to)
                    ->setTracker($args['tracker'])
                    ->setStatusCode($results->status->id)
                    ->setStatusMessage($results->status->name)
                    ->setStatusDatetime($results->doneAt)
                    ->setResponseStatus($results->status->groupId)
                    ->setResponseDescription($results->status->description);
                break;
            default:
                return $response->withJson(['status' => false, 'data' => 'Provider Invalid'], 400);
        }

        # Add LOG de COMISSIONAMENTO
        $logStartTime = \DateTime::createFromFormat('U.u', number_format($this->startTime, 6, '.', ''));
        $this->commissioningLog
            ->setAction($this->action)
            ->setCarrier($callback->getCarrier())
            ->setVendorName($callback->getVendorName())
            ->setVendorLocale($callback->getVendorLocale())
            ->setMsisdn($callback->getPhoneNumber())
            ->setTrackerId($callback->getTracker())
            ->setStartTime($logStartTime->format('Y-m-d H:i:s.u'))
            ->setGatewayName($callback->getBrokerName())
            ->setGatewayStatus($callback->getResponseStatus())
            ->setGatewayResponseCode($callback->getStatusCode())
            ->setGatewayResponseDescription($callback->getResponseDescription())
            ->setGatewayTracker($callback->getId())
            ->setExecutionTime(microtime(true) - $this->startTime)
            ->setEventStatus(true)
            ->setEventDescription($callback->getStatusMessage())
            ->setHttpCode(200)
            ->record();

        return $response->withJson(['status' => true, 'data' => 'success'], 200);
    }
}