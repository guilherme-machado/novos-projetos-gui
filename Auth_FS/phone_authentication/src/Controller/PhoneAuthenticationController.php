<?php

namespace Api\Controller;

use Api\Model\Entity\SMSMessage;
use Slim\Http\Request;
use Slim\Http\Response;
use Api\Model\Entity\PhoneNumber;
use Api\Model\CommissioningLog;
use Api\Model\Aprovisionador;
use Api\Model\CacheWhitelist;
use Api\Model\ProjectInfo;
use Api\Model\CacheMemory;
use Api\Model\BrokerFactory;
use Api\Model\Queuing;
use Api\Model\AppLog;
use Api\Util\Pincode;
use Api\Util\Tracker;

const EVENT_DESCRIPTION_INVALID_TRACKER = "Tracker inválido ou expirado";

class PhoneAuthenticationController
{
    private $commissioningLog;
    private $startTime;
    private $projectAttribute;

    public function __construct()
    {
        $this->commissioningLog = new CommissioningLog();
        $this->startTime = microtime(true);
        $this->projectAttribute = 'projectInfo';
    }

    /**
     * Send first message with pin code
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response Json
     */
    public function send(Request $request, Response $response, array $args)
    {
        $projectInfo = new ProjectInfo($request->getAttribute($this->projectAttribute));

        # Valida body MSISDN
        $phoneNumber = new PhoneNumber($request->getParam('msisdn'));
        if (empty($phoneNumber->getPhoneNumberInternational())) {
            return $response->withJson(['status' => false, 'data' => 'Número de telefone (MSISDN) inválido'], 400);
        }

        # Valida body LOCALE e o define (false = DEFAULT)
        $locale = $request->getParam('locale');
        if (empty($locale) || empty($projectInfo->getPhraseologies($locale))) {
            $locale = $projectInfo->getLanguageDefault();
        }
        $phraseologyOriginal = $projectInfo->getPhraseologies($locale);

        # Valida se existe TRACKER
        $cacheMemory = new CacheMemory();
        $searchTracker = $cacheMemory->find(Tracker::encryptPhoneNumber($phoneNumber) . "*");
        if (!empty($searchTracker)) {
            # Recupera informações
            list(, $detailsCrypted) = explode('.', $searchTracker[0]);
            $details = Tracker::decryptDetails($detailsCrypted);
            $tokenRecovery = $cacheMemory->get('TID' . $details->tokenId);

            # Valida Project
            if ($projectInfo->getToken() === $tokenRecovery) {
                # Valida TIMER (Reduz 25% do tempo da expiração)
                $timer = (microtime(true) + ((PINCODE_EXPIRE_SECONDS / 100) * 25)) - $details->microtime;
                if ($timer < PINCODE_EXPIRE_SECONDS) {
                    $data = [
                        'transaction_id' => $searchTracker[0],
                        'message_delivered' => $projectInfo->getPhraseologies($details->locale)
                    ];
                    return $response->withJson(['status' => true, 'data' => $data], 200);
                }

                # Remove da MEMORIA e segue o FLUXO
                $cacheMemory->del($searchTracker[0]);
            }
        }

        # Consultar WHITELIST
        $cacheWhitelist = new CacheWhitelist();
        $whitelistConfig = $cacheWhitelist->getWhitelist($phoneNumber->getPhoneNumberInternational());
        if (isset($whitelistConfig) && !empty($whitelistConfig)) {
            $whitelistConfig = json_decode($whitelistConfig);
        }

        # Gera PINCODE
        if (isset($whitelistConfig->pincode) && !empty($whitelistConfig->pincode)) {
            $pincode = $whitelistConfig->pincode;
        } else {
            $pincode = Pincode::create();
        }

        # Gera TRACKER
        $tracker = Tracker::create($phoneNumber, $projectInfo->getId(), $pincode, $locale);

        # Valida criacao do TRACKER
        if ($tracker === false) {
            $responseError = json_encode([
                'projectInfo' => $projectInfo->getObject(),
                'phoneNumber' => $phoneNumber,
                'pincode' => $pincode
            ]);
            return $response->withJson(['status' => false, 'data' => "Erro Tracker.create(): " . $responseError], 500);
        }

        # Grava TRACKER na MEMORIA
        $cacheMemorySetResponse = $cacheMemory->set($tracker, true, PINCODE_EXPIRE_SECONDS);
        if (!$cacheMemorySetResponse) {
            return $response->withJson(['status' => false, 'data' => $cacheMemorySetResponse], 500);
        }

        # Gera MENSAGEM
        $phraseologyMask = ['#PINCODE#', '#PROJECT#'];
        $phraseologyReplace = [$pincode, $projectInfo->getProject()];
        $phraseology = str_replace($phraseologyMask, $phraseologyReplace, $phraseologyOriginal);
        $message = new SMSMessage();
        $message->setPhoneNumber($phoneNumber->getPhoneNumberInternational());
        $message->setMessage($phraseology);
        $message->setTracker($tracker);

        # [Whitelist] VERIFICA se ENVIA
        if (!isset($whitelistConfig->sendSMS) || $whitelistConfig->sendSMS === true) {
            # ENFILEIRA a MENSAGEM
            $queuingApi = new Queuing();
            $queuingPublishResponse = $queuingApi
                ->setQueue(RABBITMQ_QUEUE_PREFIX . ":" . $projectInfo->getCarrier() . ":send")
                ->setExchange(RABBITMQ_QUEUE_PREFIX . "_" . $projectInfo->getCarrier() . "_exchange")
                ->setMessage($message)
                ->publish();

            # Valida ENFILEIRAMENTO
            if ($queuingPublishResponse !== true) {
                return $response->withJson(['status' => false, 'data' => $queuingPublishResponse], 500);
            }
        }

        # Add LOG de COMISSIONAMENTO
        $logStartTime = \DateTime::createFromFormat('U.u', number_format($this->startTime, 6, '.', ''));
        $this->commissioningLog
            ->setAction('send_pincode')
            ->setCarrier($projectInfo->getCarrier())
            ->setVendorName($projectInfo->getProject() . " " . $projectInfo->getPlataform())
            ->setVendorLocale($locale)
            ->setMsisdn($phoneNumber->getPhoneNumberInternational(false))
            ->setTrackerId($tracker)
            ->setStartTime($logStartTime->format('Y-m-d H:i:s.u'))
            ->setExecutionTime(microtime(true) - $this->startTime)
            ->setEventStatus(true)
            ->setEventDescription("Mensagem enfileirada com sucesso!")
            ->setHttpCode(202)
            ->record();

        # Prepara o payload de RETORNO
        $data = ['transaction_id' => $tracker, 'message_delivered' => $phraseologyOriginal];
        return $response->withJson(['status' => true, 'data' => $data], 202);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response Json
     */
    public function resend(Request $request, Response $response, array $args)
    {
        $logger = new AppLog(__METHOD__);

        # Recebe PARAMETROS
        $tracker = $request->getParam('transaction_id');

        # Define PARAMETROS de RETORNO
        $httpcode = 200;
        $status = true;

        # Consulta TRACKER
        $trackerInfo = Tracker::getTracker($tracker);
        $logger->debug("getTracker Response: " . json_encode($trackerInfo));

        # Valida TRACKER
        if ($trackerInfo === false) {
            # Define PARAMETROS de RETORNO
            $httpcode = 403;
            $status = false;

            # Add LOG de COMISSIONAMENTO
            $logStartTime = \DateTime::createFromFormat('U.u', number_format($this->startTime, 6, '.', ''));
            $this->commissioningLog
                ->setAction(__FUNCTION__)
                ->setTrackerId($tracker)
                ->setStartTime($logStartTime->format('Y-m-d H:i:s.u'))
                ->setEventStatus($status)
                ->setEventDescription(EVENT_DESCRIPTION_INVALID_TRACKER)
                ->setExecutionTime(microtime(true) - $this->startTime)
                ->setHttpCode($httpcode)
                ->record();

            $logger->error("Error getTracker: '" . EVENT_DESCRIPTION_INVALID_TRACKER . "'");
            return $response->withJson(['status' => $status, 'data' => EVENT_DESCRIPTION_INVALID_TRACKER], $httpcode);
        }

        # Gera MENSAGEM
        $phraseologyMask = ['#PINCODE#', '#PROJECT#'];
        $phraseologyReplace = [$trackerInfo->details->pincode, $trackerInfo->project->getProject()];
        $phraseologyOriginal = $trackerInfo->project->getPhraseologies($trackerInfo->details->locale);
        $phraseology = str_replace($phraseologyMask, $phraseologyReplace, $phraseologyOriginal);
        $logger->debug("{$trackerInfo->phoneNumber->getMsisdn()} - Phraseology: {$phraseologyOriginal}");

        # Envia MENSAGEM via BROKERS
        $brokerFactory = new BrokerFactory($logger);
        foreach ($trackerInfo->project->getProviders() as $broker) {
            $logger->info("{$trackerInfo->phoneNumber->getMsisdn()} - BrokerFactory: {$broker}");

            # Gera MENSAGEM PADRÃO
            $message = new SMSMessage();
            $message->setMessage($phraseology);
            $message->setPhoneNumber($trackerInfo->phoneNumber->getPhoneNumberInternational());
            $message->setTracker($tracker);

            # Envia SMS via BROKER
            $brokerResponse = $brokerFactory->sendSMS($broker, $message);
            $logger->debug("{$trackerInfo->phoneNumber->getMsisdn()} - BrokerFactory.sendSMS Response: " . json_encode($brokerResponse));

            # Add LOG de COMISSIONAMENTO
            $logStartTime = \DateTime::createFromFormat('U.u', number_format($this->startTime, 6, '.', ''));
            $this->commissioningLog
                ->setAction('resend_pincode')
                ->setCarrier($trackerInfo->project->getCarrier())
                ->setVendorName($trackerInfo->project->getProject() . " " . $trackerInfo->project->getPlataform())
                ->setVendorLocale($trackerInfo->details->locale)
                ->setMsisdn($trackerInfo->phoneNumber->getPhoneNumberInternational(false))
                ->setTrackerId($tracker)
                ->setStartTime($logStartTime->format('Y-m-d H:i:s.u'))
                ->setGatewayName($broker)
                ->setGatewayStatus($brokerResponse->data->status)
                ->setGatewayResponseCode($brokerResponse->data->responseCode)
                ->setGatewayResponseDescription($brokerResponse->data->responseDescription)
                ->setGatewayTracker($brokerResponse->data->tracker)
                ->setGatewayBalance($brokerResponse->data->balance)
                ->setGatewayCredit($brokerResponse->data->credit)
                ->setEventStatus($brokerResponse->status)
                ->setEventDescription(($brokerResponse->status === false) ? "Erro ao eviar SMS" : "SMS enviado com sucesso")
                ->setExecutionTime(microtime(true) - $this->startTime)
                ->setHttpCode(($brokerResponse->status === false) ? 500 : 200)
                ->record();

            if ($brokerResponse->status === false) {
                $logger->error("{$trackerInfo->phoneNumber->getMsisdn()} - BrokerFactory.{$broker}.sendSMS Error: {$brokerResponse->data->responseCode}: {$brokerResponse->data->responseDescription}");
                $status = false;
            } else {
                $logger->info("{$trackerInfo->phoneNumber->getMsisdn()} - BrokerFactory.{$broker}.sendSMS Success!");
                break;
            }
        }

        # Prepara o payload de RETORNO
        return $response->withJson([
            'status' => $status,
            'data' => [
                'transaction_id' => $tracker,
                'message_delivered' => $phraseologyOriginal
            ]
        ], $httpcode);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response Json
     */
    public function validate(Request $request, Response $response, array $args)
    {
        $projectInfo = new ProjectInfo($request->getAttribute($this->projectAttribute));

        # Recebe PARAMETROS
        $tracker = $request->getParam('transaction_id');
        $pincode = $request->getParam('pincode');

        # Valida PARAMETROS
        if (empty($tracker) || empty($pincode)) {
            return $response->withJson(['status' => false, 'data' => "'transaction_id' ou 'pincode' não informado."],
                400);
        }

        # Consulta TRACKER
        $trackerInfo = Tracker::getTracker($tracker);

        # Valida TRACKER
        if ($trackerInfo === false) {
            return $response->withJson(['status' => false, 'data' => 'Tracker inválido ou expirado'], 403);
        }

        # Add LOG de COMISSIONAMENTO
        $logStartTime = \DateTime::createFromFormat('U.u', number_format($this->startTime, 6, '.', ''));
        $this->commissioningLog
            ->setAction('validate_pincode')
            ->setCarrier($trackerInfo->project->getCarrier())
            ->setVendorName($trackerInfo->project->getProject() . " " . $trackerInfo->project->getPlataform())
            ->setVendorLocale($trackerInfo->details->locale)
            ->setMsisdn($trackerInfo->phoneNumber->getPhoneNumberInternational(false))
            ->setTrackerId($tracker)
            ->setStartTime($logStartTime->format('Y-m-d H:i:s.u'));

        # Valida PINCODE
        if ($trackerInfo->details->pincode !== $pincode) {
            # Add LOG de COMISSIONAMENTO = FAIL
            $this->commissioningLog
                ->setExecutionTime(microtime(true) - $this->startTime)
                ->setEventStatus(false)
                ->setEventDescription("Erro ao validar o PINCODE")
                ->setHttpCode(401)
                ->record();

            return $response->withJson(['status' => false, 'data' => 'Pincode inválido'], 401);
        }

        # Gera CUSTOM TOKEN (Firebase)
        $aprovisionadorApi = new Aprovisionador(APROVISIONADOR_HOST, APROVISIONADOR_TIMEOUT);
        $customTokenResponse = $aprovisionadorApi->getCustomToken($trackerInfo->phoneNumber,
            $projectInfo->getCarrier());
        $customToken = json_decode($customTokenResponse['body']);

        # Add LOG de COMISSIONAMENTO = GATEWAY INFORMATIONS
        $this->commissioningLog
            ->setGatewayName('Aprovisionador')
            ->setGatewayResponseCode($customTokenResponse['httpcode'])
            ->setGatewayResponseDescription($customTokenResponse['body']);

        # Validar CUSTOM TOKEN REQUEST
        if ($customTokenResponse['httpcode'] !== 200 || empty($customToken->data)) {
            # Add LOG de COMISSIONAMENTO = FAIL
            $this->commissioningLog
                ->setGatewayStatus(false)
                ->setExecutionTime(microtime(true) - $this->startTime)
                ->setEventStatus(false)
                ->setEventDescription("Erro ao gerar CUSTOM_TOKEN")
                ->setHttpCode(500)
                ->record();

            return $response->withJson(['status' => false, 'data' => 'Cliente não encontrado.'], 500);
        }

        # Add LOG de COMISSIONAMENTO = SUCCESS
        $this->commissioningLog
            ->setGatewayStatus(true)
            ->setExecutionTime(microtime(true) - $this->startTime)
            ->setEventStatus(true)
            ->setEventDescription("PINCODE validado com sucesso")
            ->setHttpCode(200)
            ->record();

        # Prepara o payload de RETORNO
        $data = ['custom_token' => $customToken->data];
        return $response->withJson(['status' => true, 'data' => $data], 200);
    }
}