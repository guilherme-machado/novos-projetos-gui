<?php

namespace Api\Controller;

use Api\Util\Tracker;
use Slim\Http\Request;
use Slim\Http\Response;
use Api\Model\CacheConfig;
use Api\Model\CacheWhitelist;
use Api\Model\Entity\PhoneNumber;

class AdminController
{
    const METHOD_SET = 'set';
    const METHOD_CLEAR = 'clear';
    const METHOD_FLUSH = 'flush';
    const FIELD_STATUS = 'status';
    const FIELD_CONTENT = 'data';

    /**
     * @param Request $request
     * @param Response $response
     * @return Response Json
     */
    public function updateConfig(Request $request, Response $response)
    {
        $method = $request->getParam('method');
        $cache = new CacheConfig();

        $body = [];
        if (empty($method) || $method == self::METHOD_SET) {
            $body['setConfigsConsult'] = $cache->getConfigs();
            $body['setConfigs'] = $cache->setConfigs();
            $body['setConfigsResult'] = $cache->getConfigs();
        }

        if (empty($method) || $method == self::METHOD_CLEAR) {
            $body['clearConfigsConsult'] = $cache->getConfigs();
            $body['clearConfig'] = $cache->clearConfig();
            $body['clearConfigsResult'] = $cache->getConfigs();
        }

        if (empty($method) || $method == 'flush') {
            $body['flushConfigsConsult'] = $cache->getConfigs();
            $body['flushConfig'] = $cache->flushConfig();
            $body['flushConfigsResult'] = $cache->getConfigs();
        }

        return $response->withJson([self::FIELD_STATUS => true, self::FIELD_CONTENT => $body], 200);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response Json
     */
    public function updateWhitelist(Request $request, Response $response)
    {
        $body = [];

        $method = $request->getParam('method');
        $msisdn = $request->getParam('msisdn');

        $cache = new CacheWhitelist();
        $phoneNumber = new PhoneNumber($msisdn);
        $body['phoneNumber'] = $phoneNumber->getPhoneNumberObject();

        if (empty($method) || $method == self::METHOD_SET) {
            $body[self::METHOD_SET]['consult'] = $cache->getWhitelist();
            $body[self::METHOD_SET]['run'] = $cache->setWhitelist();
            $body[self::METHOD_SET]['result'] = $cache->getWhitelist();
        }

        if (empty($method) || $method == self::METHOD_CLEAR) {
            $body[self::METHOD_CLEAR]['consult'] = $cache->getWhitelist();
            $body[self::METHOD_CLEAR]['run'] = $cache->clearWhitelist($phoneNumber->getPhoneNumberInternational());
            $body[self::METHOD_CLEAR]['result'] = $cache->getWhitelist();
        }

        if (empty($method) || $method == self::METHOD_FLUSH) {
            $body[self::METHOD_FLUSH]['consult'] = $cache->getWhitelist();
            $body[self::METHOD_FLUSH]['run'] = $cache->flushWhitelist();
            $body[self::METHOD_FLUSH]['result'] = $cache->getWhitelist();
        }

        return $response->withJson([self::FIELD_STATUS => true, self::FIELD_CONTENT => $body], 200);
    }

    /**
     * Get tracker information
     *
     * @param Request $request
     * @param Response $response
     * @return Response Json
     */
    public function getTracker(Request $request, Response $response)
    {
        $tracker = $request->getParam('transaction_id');

        list($phoneCrypted, $detailsCrypted) = explode('.', $tracker);
        $data['phoneNumber'] = (Tracker::decryptPhoneNumber($phoneCrypted))->getPhoneNumberObject();
        $data['details'] = Tracker::decryptDetails($detailsCrypted);
        $data['trackerValid'] = true;

        $trackerInfo = Tracker::getTracker($tracker);
        if (empty($trackerInfo)) {
            $data['trackerValid'] = false;
        }

        return $response->withJson([self::FIELD_STATUS => true, self::FIELD_CONTENT => $data], 200);
    }
}
