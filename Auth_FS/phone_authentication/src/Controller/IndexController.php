<?php

namespace Api\Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use Api\Model\AppLog;

class IndexController
{
    private $logger;

    public function __construct()
    {
        $this->logger = new AppLog('phone-authentication', __CLASS__);
    }

    public function index(Request $request, Response $response)
    {
        $this->logger->info("Attributes: ". json_encode($request->getParams()));
        return $response->withJson(['status' => 'OK'], 200);
    }
}
