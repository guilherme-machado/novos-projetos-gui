<?php

use Api\Middleware\Bearer;
use Api\Middleware\AdminBearer;
use Api\Controller\TestController;
use Api\Controller\AdminController;
use Api\Controller\IndexController;
use Api\Controller\CallbackController;
use Api\Controller\IntegracaoController;
use Api\Controller\PhoneAuthenticationController;

// Routes
$app->get('/', IndexController::class . ":index");

// API Phone Authentication
$app->group('/phone-auth', function () {
    $this->post('/send', PhoneAuthenticationController::class . ":send");
    $this->post('/resend', PhoneAuthenticationController::class . ":resend");
    $this->post('/validate', PhoneAuthenticationController::class . ":validate");
})->add(new Bearer());

// API Admin
$app->group('/admin', function () {
    $this->post('/update-settings', AdminController::class . ":updateConfig");
    $this->post('/update-whitelist', AdminController::class . ":updateWhitelist");
    $this->get('/get-tracker', AdminController::class . ":getTracker");
})->add(new AdminBearer());

// API Callback
$app->any('/callback/{provider}[/{tracker}]', CallbackController::class . ":providers");