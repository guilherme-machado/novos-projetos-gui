<?php
# APLICACAO
define('PROJECTS_CONFIG', __DIR__ . "/../configs/projects.json");
define('PROJECTS_WHITELIST', __DIR__ . "/../configs/whitelist.json");
define('PHONE_AUTH_URL', getenv('PHONE_AUTH_URL'));
define('PHONE_AUTH_DEBUG', getenv('PHONE_AUTH_DEBUG'));
define('PINCODE_EXPIRE_SECONDS', getenv('PINCODE_EXPIRE_SECONDS'));

# SMS MARKET
define('SMS_MARKET_HOST', getenv('SMS_MARKET_HOST'));
define('SMS_MARKET_USERNAME', getenv('SMS_MARKET_USERNAME'));
define('SMS_MARKET_PASSWORD', getenv('SMS_MARKET_PASSWORD'));
define('SMS_MARKET_TIMEOUT', getenv('SMS_MARKET_TIMEOUT'));

# TWILIO
define('TWILIO_SID', getenv('TWILIO_SID'));
define('TWILIO_TOKEN', getenv('TWILIO_TOKEN'));
define('TWILIO_SERVICE', getenv('TWILIO_SERVICE'));
define('TWILIO_TIMEOUT', getenv('TWILIO_TIMEOUT'));

# INFOBIP
define('INFOBIP_HOST', getenv('INFOBIP_HOST'));
define('INFOBIP_USERNAME', getenv('INFOBIP_USERNAME'));
define('INFOBIP_PASSWORD', getenv('INFOBIP_PASSWORD'));

# APROVISIONADOR
define('APROVISIONADOR_HOST', getenv('APROVISIONADOR_HOST'));
define('APROVISIONADOR_TIMEOUT', getenv('APROVISIONADOR_TIMEOUT'));

# REDIS
define('REDIS_HOST', getenv('REDIS_HOST'));
define('REDIS_PORT', getenv('REDIS_PORT'));

# RABBITMQ
define('RABBITMQ_HOST', filter_var(getenv('RABBITMQ_HOST'), FILTER_SANITIZE_STRING));
define('RABBITMQ_PORT', filter_var(getenv('RABBITMQ_PORT'), FILTER_SANITIZE_STRING));
define('RABBITMQ_USER', filter_var(getenv('RABBITMQ_USER'), FILTER_SANITIZE_STRING));
define('RABBITMQ_PASSWORD', filter_var(getenv('RABBITMQ_PASSWORD'), FILTER_SANITIZE_STRING));
define('RABBITMQ_VHOST', filter_var(getenv('RABBITMQ_VHOST'), FILTER_SANITIZE_STRING));
define('RABBITMQ_INSIST', filter_var(getenv('RABBITMQ_INSIST'), FILTER_VALIDATE_BOOLEAN));
define('RABBITMQ_LOGIN_METHOD', filter_var(getenv('RABBITMQ_LOGIN_METHOD'), FILTER_SANITIZE_STRING));

$rabbitmqLoginResponse = getenv('RABBITMQ_LOGIN_RESPONSE');
if (empty($rabbitmqLoginResponse) || $rabbitmqLoginResponse == 'null') {
    $rabbitmqLoginResponse = null;
}
define('RABBITMQ_LOGIN_RESPONSE', $rabbitmqLoginResponse);
define('RABBITMQ_LOCALE', filter_var(getenv('RABBITMQ_LOCALE'), FILTER_SANITIZE_STRING));
define('RABBITMQ_CONNECTION_TIMEOUT', filter_var(getenv('RABBITMQ_CONNECTION_TIMEOUT'), FILTER_SANITIZE_NUMBER_FLOAT));
define('RABBITMQ_READ_WRITE_TIMEOUT', filter_var(getenv('RABBITMQ_READ_WRITE_TIMEOUT'), FILTER_SANITIZE_NUMBER_FLOAT));

$rabbitmqContext = getenv('RABBITMQ_CONTEXT');
if (empty($rabbitmqContext) || $rabbitmqContext == "null") {
    $rabbitmqContext = null;
}
define('RABBITMQ_CONTEXT', $rabbitmqContext);
define('RABBITMQ_KEEPALIVE', filter_var(getenv('RABBITMQ_KEEPALIVE'), FILTER_VALIDATE_BOOLEAN));
define('RABBITMQ_HEARTBEAT', filter_var(getenv('RABBITMQ_HEARTBEAT'), FILTER_VALIDATE_INT));
define('RABBITMQ_TTL_RETRY', getenv('RABBITMQ_TTL_RETRY'));
define('RABBITMQ_QUEUE_PREFIX', getenv('RABBITMQ_QUEUE_PREFIX'));
