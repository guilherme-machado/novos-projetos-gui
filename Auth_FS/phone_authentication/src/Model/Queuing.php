<?php

namespace Api\Model;

use Api\Model\Entity\SMSMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Queuing
{
    private $host;
    private $port;
    private $username;
    private $password;
    private $vhost;
    private $queue;
    private $queue_retry;
    private $exchange;
    private $exchange_retry;
    private $message;
    private $timeout;

    public function __construct(
        $host = RABBITMQ_HOST,
        $port = RABBITMQ_PORT,
        $username = RABBITMQ_USER,
        $password = RABBITMQ_PASSWORD,
        $vhost = RABBITMQ_VHOST
    ) {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->vhost = $vhost;
    }

    /**
     * @param $queue
     * @return $this
     */
    public function setQueue($queue)
    {
        $this->queue = $queue;
        $this->queue_retry = $queue . "_retry";
        return $this;
    }

    /**
     * @param $exchange
     * @return $this
     */
    public function setExchange($exchange)
    {
        $this->exchange = $exchange;
        $this->exchange_retry = $exchange . "_retry";
        return $this;
    }

    /**
     * @param SMSMessage $message
     * @return $this
     */
    public function setMessage(SMSMessage $message)
    {
        $this->message = new AMQPMessage($message);
        return $this;
    }

    /**
     * @param int $timeout
     */
    public function setTimeout($timeout = RABBITMQ_TTL_RETRY)
    {
        $this->timeout = (int)$timeout;
    }

    /**
     * @return bool|string
     */
    public function publish()
    {
        $validateError = null;
        foreach (['queue', 'exchange', 'message'] as $field) {
            if (empty($this->$field)) {
                $validateError .= "Erro ao identificar '{$field}'";
            }
        }

        if ($validateError !== null) {
            return $validateError;
        }

        if (empty($this->timeout)) {
            $this->setTimeout();
        }

        try {
            $connection = new AMQPStreamConnection(
                $this->host,
                $this->port,
                $this->username,
                $this->password,
                $this->vhost,
                RABBITMQ_INSIST,
                RABBITMQ_LOGIN_METHOD,
                RABBITMQ_LOGIN_RESPONSE,
                RABBITMQ_LOCALE,
                RABBITMQ_CONNECTION_TIMEOUT,
                RABBITMQ_READ_WRITE_TIMEOUT,
                RABBITMQ_CONTEXT,
                RABBITMQ_KEEPALIVE,
                RABBITMQ_HEARTBEAT
            );
            $channel = $connection->channel();
            $channel->exchange_declare($this->exchange, 'direct');
            $channel->exchange_declare($this->exchange_retry, 'direct');
            $channel->queue_declare(
                $this->queue,
                false,
                true,
                false,
                false,
                false,
                ['x-dead-letter-exchange' => ['S', $this->exchange_retry]]
            );
            $channel->queue_declare(
                $this->queue_retry,
                false,
                true,
                false,
                false,
                false,
                ['x-message-ttl' => ['I', $this->timeout], 'x-dead-letter-exchange' => ['S', $this->exchange]]
            );
            $channel->queue_bind($this->queue, $this->exchange);
            $channel->queue_bind($this->queue_retry, $this->exchange_retry);
            $channel->basic_publish($this->message, $this->exchange);
        } catch (\Exception $exception) {
            return 'Erro ao publicar no RabbitMQ: ' . $exception->getMessage();
        }
        return true;
    }
}