<?php
namespace Api\Model;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Formatter\LineFormatter;
use Api\Model\Entity\ComissioningLogEntity;


class CommissioningLog extends ComissioningLogEntity
{
    private $logger;

    public function __construct()
    {
        // Create some handlers
        $stream = new StreamHandler('php://stdout', Logger::DEBUG);
        $firephp = new FirePHPHandler();

        // Create the main logger of the app
        $this->logger = new Logger('comissionamento');
        $this->logger->pushHandler($stream);
        $this->logger->pushHandler($firephp);

        // Format output
        $format = "%message%\n";
        $formatter = new LineFormatter($format);
        $stream->setFormatter($formatter);
    }

    /**
     * @return bool|string
     */
    public function record()
    {
        try {
            $payload = json_encode(self::getObject());
            $this->logger->info($payload);
            return true;
        } catch (\Exception $exception) {
            return "Erro ao registrar 'Log de Comissionamento': {$exception->getMessage()}";
        }
    }
}