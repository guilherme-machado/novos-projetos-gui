<?php

namespace Api\Model;

use Api\Model\Entity\Callback;

class InfobipCallback extends Callback
{
    /**
     * InfobipCallback constructor.
     */
    public function __construct()
    {
        parent::__construct();
        parent::setBrokerName("Infobip");
    }

    public function setResponseStatus($groupId)
    {
        switch ($groupId) {
            case "1":
            case "3":
                parent::setResponseStatus(true);
                break;

            case "2":
            case "4":
            case "5":
            default:
                parent::setResponseStatus(false);
        }
        return $this;
    }
}