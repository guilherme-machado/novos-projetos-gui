<?php
namespace Api\Model;

class CurlAbstract
{
    private $method;
    private $endpoint;
    private $uri_params;
    private $header;
    private $timeout;
    private $response_httpcode;
    private $response_header;
    private $response_body;

    /**
     * CurlAbstract constructor.
     */
    public function __construct()
    {
        $this->method  = 'GET';
        $this->timeout = 50;
    }

    /**
     * @param $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param $endpoint
     * @return $this
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
        return $this;
    }

    /**
     * @param $uri_params
     * @return $this
     */
    public function setUriParams($uri_params)
    {
        $this->uri_params = $uri_params;
        return $this;
    }

    /**
     * @param $header
     * @return $this
     */
    public function setHeader($header)
    {
        $this->header = $header;
        return $this;
    }

    /**
     * @param $timeout
     * @return $this
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @return array|bool
     */
    public function run()
    {
        $url = $this->endpoint;

        if(!empty($this->uri_params)){
            $url .= "?". http_build_query($this->uri_params);
        }

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, TRUE);

            if($this->method == 'POST'){
                curl_setopt($ch, CURLOPT_POST, TRUE);
            }

            if(!empty($this->header)){
                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->header);
            }

            curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
            $output = curl_exec($ch);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $this->response_httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $this->response_header = substr($output, 0, $header_size);
            $this->response_body = substr($output, $header_size);
            curl_close($ch);
        } catch (\Exception $exception) {
            $this->response_httpcode = 500;
            $this->response_header = false;
            $this->response_body = $exception->getMessage();
            return false;
        }

        return [
            'httpcode' => $this->response_httpcode,
            'header' => $this->response_header,
            'body'     => $this->response_body
        ];
    }
}