<?php

namespace Api\Model;

use Api\Model\Entity\SMSMessage;
use Api\Util\Convert;
use Monolog\Logger;

class BrokerFactory
{
    private $logger;

    /**
     * BrokerFactory constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $broker
     * @param SMSMessage $message
     * @return bool|object
     */
    public function sendSMS($broker, SMSMessage $message)
    {
        $return = [
            'status' => false,
            'data' => [
                'name' => $broker,
                'tracker' => null,
                'credit' => null,
                'balance' => null,
                'status' => null,
                'responseCode' => null,
                'responseDescription' => null
            ]
        ];

        $this->logger->debug("{$message->getPhoneNumber()} - Definindo broker \"{$broker}\" e SMSMessage: {$message}");
        $brokerApi = $this->setBroker($broker);
        if ($brokerApi === false) {
            $this->logger->error("{$message->getPhoneNumber()} - Erro ao definir broker: \"{$broker}\"");
            return false;
        }

        try {
            $responseBrokerApi = $brokerApi->sendSMS($message);
            $this->logger->debug("{$message->getPhoneNumber()} - {$broker}.sendSMS Response: " . json_encode($responseBrokerApi));

            $return['status'] = $responseBrokerApi->sent;
            $return['data']['tracker'] = $responseBrokerApi->id;
            $return['data']['credit'] = $responseBrokerApi->credit;
            $return['data']['balance'] = $responseBrokerApi->balance;
            $return['data']['status'] = $responseBrokerApi->status;
            $return['data']['responseCode'] = $responseBrokerApi->code;
            $return['data']['responseDescription'] = $responseBrokerApi->message;
        } catch (\Exception $exception) {
            $this->logger->error("{$message->getPhoneNumber()} - {$broker}.sendSMS Exception: {$exception->getMessage()}");
            $return['data']['status'] = false;
            $return['data']['responseCode'] = $exception->getCode();
            $return['data']['responseDescription'] = $exception->getMessage();
        }

        return Convert::arrayToObject($return);
    }

    /**
     * @param $broker
     * @return Infobip|SMSMarket|Twilio|bool
     */
    private function setBroker($broker)
    {
        switch (strtolower($broker)) {
            case 'sms market':
                $brokerApi = new SMSMarket($this->logger);
                break;
            case 'twilio':
                $brokerApi = new Twilio($this->logger);
                break;
            case 'infobip':
                $brokerApi = new Infobip($this->logger);
                break;
            default:
                $brokerApi = false;
        }
        return $brokerApi;
    }
}