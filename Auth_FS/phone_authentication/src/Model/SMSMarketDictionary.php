<?php

namespace Api\Model;

const SMSMARKET_DICTIONARY_REQUEST_STATUS = __DIR__ . "/../../dictionary/smsmarket-requeststatus.json";
const SMSMARKET_DICTIONARY_CALLBACK_STATUS = __DIR__ . "/../../dictionary/smsmarket-callbackstatus.json";

class SMSMarketDictionary
{
    /**
     * @param $dictionary
     * @param null $code
     * @return bool|object
     */
    public static function getStatusInformation($dictionary, $code = null)
    {
        switch ($dictionary) {
            case 'request': $file = file_get_contents(SMSMARKET_DICTIONARY_REQUEST_STATUS); break;
            case 'callback': $file = file_get_contents(SMSMARKET_DICTIONARY_CALLBACK_STATUS); break;
            default: return false;
        }

        $dictionary = json_decode($file, true);
        if (!isset($dictionary[$code])) {
            return (object)['message' => 'Invalid Code', 'status' => false];
        }

        return (object)$dictionary[$code];
    }
}