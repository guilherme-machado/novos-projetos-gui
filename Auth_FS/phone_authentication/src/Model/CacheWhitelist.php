<?php

namespace Api\Model;

const WHITELIST_PREFIX = 'TEL:';

class CacheWhitelist extends CacheMemory
{

    private $logger;

    /**
     * CacheWhitelist constructor.
     */
    public function __construct()
    {
        $this->logger = new AppLog('cacheWhitelist', 'cache-memory');
        parent::__construct();
    }

    /**
     * Set all whitelist to memory
     * @param string $filename
     * @return bool
     */
    public function setWhitelist($filename = PROJECTS_WHITELIST)
    {
        $this->logger->info("Start setting all whitelist to memory");

        try {
            $projectsWhitelist = json_decode(file_get_contents($filename));
            foreach ($projectsWhitelist as $phoneNumberInternational => $whitelistConfig) {
                $this->logger->debug("new key: " . WHITELIST_PREFIX . $phoneNumberInternational . " - Content: " .
                    json_encode($whitelistConfig));
                parent::set(WHITELIST_PREFIX . $phoneNumberInternational, json_encode($whitelistConfig));
            }

            $this->logger->info("All whitelist in memory");
            return true;
        } catch (\Exception $exception) {
            $this->logger->error("error set whitelist to memory: {$exception->getMessage()}");
            return false;
        }
    }

    /**
     * Consult whitelist in memory
     * @param string $find
     * @return array|bool|string
     */
    public function getWhitelist($find = '*')
    {
        $find = (empty($find)) ? '*' : $find;
        $this->logger->debug("Find whitelistConfig: {$find}");

        if ($find !== '*') {
            $found = parent::get(WHITELIST_PREFIX . $find);
            $this->logger->debug("Return get: " . json_encode($found));
            return $found;
        }

        $found = parent::find(WHITELIST_PREFIX . $find);
        $this->logger->debug("Return find: " . json_encode($found));

        $configs = [];
        foreach ($found as $key => $config) {
            $configs[$key] = $config;
        }
        $this->logger->debug("Return whitelistConfig: " . json_encode($configs));
        return $configs;
    }

    /**
     * Delete whitelist in memory
     * @param string $find
     * @return array
     */
    public function clearWhitelist($find = '*')
    {
        $this->logger->info("Start delete whitelist in memory");

        $find = (empty($find)) ? '*' : $find;
        $this->logger->debug("Delete whitelistConfig: {$find}");

        $found = parent::find(WHITELIST_PREFIX . $find);
        $this->logger->debug("Return find: " . json_encode($found));

        $configs = [];
        foreach ($found as $key) {
            $configs[$key] = parent::del($key);
        }

        $this->logger->debug("Return whitelistConfig removed: " . json_encode($configs));
        return $configs;
    }

    /**
     * Reset all whitelist in memory
     * @return string
     */
    public function flushWhitelist()
    {
        $this->logger->info("Start flush all whitelistConfigs in memory");
        self::clearWhitelist();
        self::setWhitelist();
        $this->logger->info("WhitelistConfigs updated to memory");
        return true;
    }
}