<?php

namespace Api\Model;

use Api\Model\Entity\PhoneNumber;
use Api\Model\Entity\SMSMessage;
use Monolog\Logger;

const SMSMARKET_TYPE_SMS = "0";

class SMSMarket extends BrokerResponse implements BrokerInterface
{
    private $logger;

    /**
     * SMSMarket constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param SMSMessage $message
     * @return array|object
     */
    public function sendSMS(SMSMessage $message)
    {
        try {
            $phoneNumber = new PhoneNumber($message->getPhoneNumber());
            $uriParams = [
                'user' => SMS_MARKET_USERNAME,
                'password' => SMS_MARKET_PASSWORD,
                'country_code' => $phoneNumber->getDdi(),
                'number' => $phoneNumber->getMsisdn(),
                'campaing_id' => $message->getTracker(),
                'content' => $message->getMessage(),
                'type' => SMSMARKET_TYPE_SMS,
                'timezone' => date("P")
            ];
            $smsMarketResponse = $this->run("/webservice-rest/send-single", $uriParams);
            $this->logger->debug("{$message->getPhoneNumber()} - Response: " . json_encode($smsMarketResponse));

            $bodyResponse = json_decode($smsMarketResponse['body']);

            parent::setId($bodyResponse->id);
            parent::setStatus($bodyResponse->success);
            parent::setCode($bodyResponse->responseCode);
            parent::setMessage($bodyResponse->responseDescription);
            parent::setCredit($bodyResponse->credit);
            parent::setBalance($bodyResponse->balance);

            # Verifica status de sucesso conforme tabela de status
            # https://smsmarket.docs.apiary.io/#introduction/tabela-de-status
            if ($bodyResponse->success && in_array($bodyResponse->responseCode, ['000', '001', '002'])) {
                parent::setSent(true);
            }
        } catch (\Exception $exception) {
            $this->logger->error("{$message->getPhoneNumber()} - Error to sendSMS: [{$exception->getCode()}] {$exception->getMessage()}");
            parent::setSent(false);
            parent::setCode($exception->getCode());
            parent::setMessage($exception->getMessage());
        }

        return parent::getCollection();
    }

    /**
     * @param $uri
     * @param array $uriParams
     * @return array|bool
     */
    private function run($uri, $uriParams = array())
    {
        return (new CurlAbstract())
            ->setEndpoint(SMS_MARKET_HOST . $uri)
            ->setHeader(["Content-Type: application/x-www-form-urlencoded"])
            ->setTimeout(SMS_MARKET_TIMEOUT)
            ->setUriParams($uriParams)
            ->run();
    }
}