<?php

namespace Api\Model;

use Api\Model\Entity\SMSMessage;
use Twilio\Rest\Client;
use Monolog\Logger;

class Twilio extends BrokerResponse implements BrokerInterface
{
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param SMSMessage $message
     * @return array|object
     */
    public function sendSMS(SMSMessage $message)
    {
        try {
            $httpClient = new TwilioHttpClient();
            $twilio = new Client(TWILIO_SID, TWILIO_TOKEN, null, null, $httpClient);

            $twilioMessage = $twilio->messages->create(
                $message->getPhoneNumber(),
                [
                    "body" => $message->getMessage(),
                    "messagingServiceSid" => TWILIO_SERVICE,
                    "statusCallback" => PHONE_AUTH_URL . "/callback/twilio/" . $message->getTracker()
                ]
            );
            $this->logger->debug("{$message->getPhoneNumber()} - Response: " . json_encode($twilioMessage));


            parent::setId($twilioMessage->sid);
            parent::setStatus($twilioMessage->status);
            parent::setCredit($twilioMessage->price);

            if (!in_array($twilioMessage->status, ['undelivered', 'failed'])) {
                parent::setSent(true);
                parent::setCode("OK");
                parent::setMessage("Success");
            } else {
                parent::setCode($twilioMessage->errorCode);
                parent::setMessage($twilioMessage->errorMessage);
                parent::setSent(false);
            }
        } catch (\Exception $exception) {
            $this->logger->error("{$message->getPhoneNumber()} - Error to sendSMS: [{$exception->getCode()}] {$exception->getMessage()}");
            parent::setSent(false);
            parent::setCode($exception->getCode());
            parent::setMessage($exception->getMessage());
        }

        return parent::getCollection();
    }
}