<?php

namespace Api\Model;

class CacheConfig extends CacheMemory
{
    private $tokenIdPrefix;
    private $tokenFormat;
    private $logger;

    /**
     * CacheConfig constructor.
     */
    public function __construct()
    {
        $this->tokenIdPrefix = 'TID';
        $this->tokenFormat = '*-*-*-*-*';
        $this->logger = new AppLog('cacheConfig', 'cache-memory');

        parent::__construct();
    }

    /**
     * Set all configs to memory
     * @return bool
     */
    public function setConfigs()
    {
        $this->logger->info("Start setting all configs to memory");
        $projectsConfig = json_decode(file_get_contents(PROJECTS_CONFIG));

        try {
            foreach ($projectsConfig as $projectConfig) {
                $tokenIDKey = $this->tokenIdPrefix . $projectConfig->id;
                $this->logger->debug("new Token ID Key: {$tokenIDKey}");
                parent::set($tokenIDKey, $projectConfig->token);

                $this->logger->debug("new Token Key: {$projectConfig->token}");
                parent::set($projectConfig->token, json_encode($projectConfig));
            }
        } catch (\Exception $exception) {
            $this->logger->error("error set configs to memory: {$exception->getMessage()}");
            return false;
        }

        $this->logger->info("All configs in memory");
        return true;
    }

    /**
     * Consult all configs in memory
     * @return array
     */
    public function getConfigs()
    {
        $this->logger->info("Start get all configs in memory");
        $configs = [];
        foreach ($this->find($this->tokenFormat) as $key) {
            $configs[$key] = $this->get($key);
        }
        $this->logger->debug("Return configs: " . json_encode($configs));
        return $configs;
    }

    /**
     * Delete all configs in memory
     * @return array
     */
    public function clearConfig()
    {
        $this->logger->info("Start clear all configs in memory");
        $configs = [];
        $findTokenPrefix = parent::find($this->tokenIdPrefix . '*');
        $this->logger->debug("Return find by Token Prefix: " . json_encode($findTokenPrefix));

        $findTokenFormat = parent::find($this->tokenFormat);
        $this->logger->debug("Return find by Token Format: " . json_encode($findTokenFormat));

        foreach (array_merge($findTokenPrefix, $findTokenFormat) as $key) {
            $configs[$key] = parent::del($key);
        }

        $this->logger->debug("Return configs removed: " . json_encode($configs));
        return $configs;
    }

    /**
     * Reset all configs in memory
     *
     * @return string
     */
    public function flushConfig()
    {
        $this->logger->info("Start flush all configs in memory");
        self::clearConfig();
        self::setConfigs();
        $this->logger->info("Configs updated to memory");
        return true;
    }
}