<?php

namespace Api\Model;

use Api\Model\Entity\Callback;

class SMSMarketCallback extends Callback
{
    /**
     * SMSMarketCallback constructor.
     */
    public function __construct()
    {
        parent::__construct();
        parent::setBrokerName("SMS Market");
    }

    /**
     * @param string $carrierName
     * @return $this
     */
    public function setCarrier($carrierName)
    {
        list($carrier) = explode('-', $carrierName);
        $carrier = strtolower(str_replace(' ', '', $carrier));
        parent::setCarrier($carrier);
        return $this;
    }

    public function setResponseStatus($responseStatus)
    {
        return parent::setResponseStatus(SMSMarketDictionary::getStatusInformation('callback', $responseStatus)->status);
    }

    public function setResponseDescription($responseDescription)
    {
        return parent::setResponseDescription(SMSMarketDictionary::getStatusInformation('callback', $responseDescription)->message);
    }
}