<?php

namespace Api\Model;

use Api\Model\Entity\SMSMessage;
use Monolog\Logger;

interface BrokerInterface
{
    public function __construct(Logger $logger);
    public function sendSMS(SMSMessage $message);
}