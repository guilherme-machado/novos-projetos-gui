<?php

namespace Api\Model;


use infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest;
use infobip\api\client\SendMultipleTextualSmsAdvanced;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\sms\mt\send\Message;
use infobip\api\model\Destination;

use Api\Model\Entity\SMSMessage;
use Monolog\Logger;


class Infobip extends BrokerResponse implements BrokerInterface
{
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function sendSMS(SMSMessage $message)
    {
        try {
            $client = new SendMultipleTextualSmsAdvanced(new BasicAuthConfiguration(INFOBIP_USERNAME,
                INFOBIP_PASSWORD));


            // Creating request body
            $destination = new Destination();
            $destination->setTo($message->getPhoneNumber());

            $infobipMessage = new Message();
            $infobipMessage->setFrom("PhoneAuthentication");
            $infobipMessage->setDestinations([$destination]);
            $infobipMessage->setText($message->getMessage());
            $infobipMessage->setNotifyUrl(PHONE_AUTH_URL . "/callback/infobip/" . $message->getTracker());

            $requestBody = new SMSAdvancedTextualRequest();
            $requestBody->setMessages([$infobipMessage]);

            // Executing request
            $response = $client->execute($requestBody);
            $this->logger->debug("{$message->getPhoneNumber()} - Response: " . json_encode($response));

            $sentMessageInfo = $response->getMessages()[0];

            parent::setId($sentMessageInfo->getMessageId());
            parent::setCode($sentMessageInfo->getStatus()->getName());
            parent::setMessage($sentMessageInfo->getStatus()->getDescription());
            parent::setStatus($sentMessageInfo->getStatus()->getGroupName());

            # Verifica o GroupID para validar se foi enviado com sucesso conforme documentação:
            # https://dev.infobip.com/getting-started/response-status-and-error-codes#section-status-object-example
            if (in_array(intval($sentMessageInfo->getStatus()->getGroupId()), [1, 3])) {
                parent::setSent(true);
            } else {
                parent::setSent(false);
            }
        } catch (\Exception $exception) {
            $this->logger->error("{$message->getPhoneNumber()} - Error to sendSMS: [{$exception->getCode()}] {$exception->getMessage()}");
            parent::setSent(false);
            parent::setCode($exception->getCode());
            parent::setMessage($exception->getMessage());
        }

        return parent::getCollection();
    }
}
