<?php

namespace Api\Model;

class CacheMemory
{
    private $conn;
    private $logger;

    /**
     * CacheMemory constructor.
     */
    public function __construct()
    {
        $this->logger = new AppLog('cache-memory');

        $this->conn = new \Redis();
        $this->conn->connect(REDIS_HOST, REDIS_PORT);
        $this->conn->select(15);
    }

    /**
     * Creates key with values in memory
     *
     * @param $key
     * @param $value
     * @param int $expireSeconds
     * @return bool
     */
    public function set($key, $value, $expireSeconds = 0)
    {
        $this->logger->debug("Redis.Set: " . json_encode([
                'KEY' => $key,
                'VALUE' => $value,
                'EXPIRE' => $expireSeconds
            ]));

        try {
            $this->conn->set($key, $value);
            if ($expireSeconds !== 0) {
                $this->conn->expire($key, $expireSeconds);
            }
            return true;
        } catch (\Exception $exception) {
            $this->logger->debug("Redis.Set error {$exception->getCode()} - {$exception->getMessage()}");
            return false;
        }
    }

    /**
     * Find key in memory
     *
     * @param string $find
     * @return array
     */
    public function find($find = '*')
    {
        $this->logger->debug("Redis.Find: " . json_encode(['FIND' => $find]));
        try {
            return $this->conn->keys($find);
        } catch (\Exception $exception) {
            $this->logger->error("Redis.Find error {$exception->getCode()} - {$exception->getMessage()}");
            return [];
        }
    }

    /**
     * Consult key in memory
     *
     * @param $key
     * @return bool|null|string
     */
    public function get($key)
    {
        $this->logger->debug("Redis.Get: " . json_encode(['KEY' => $key]));
        try {
            if ($this->conn->exists($key)) {
                return $this->conn->get($key);
            }
        } catch (\Exception $exception) {
            $this->logger->error("Redis.Get error {$exception->getCode()} - {$exception->getMessage()}");
        }
        return null;
    }

    /**
     * Delete key in memory
     *
     * @param $key
     * @return bool
     */
    public function del($key)
    {
        $this->logger->debug("Redis.Del: " . json_encode(['KEY' => $key]));
        try {
            if ($this->conn->exists($key) && $this->conn->del($key) > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->logger->error("Redis.Del error {$exception->getCode()} - {$exception->getMessage()}");
        }
        return false;
    }
}