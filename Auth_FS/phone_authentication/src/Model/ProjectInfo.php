<?php
namespace Api\Model;

class ProjectInfo
{
    private $id;
    private $token;
    private $project;
    private $plataform;
    private $carrier;
    private $language_default;
    private $providers;
    private $phraseologies;

    /**
     * ProjectInfo constructor.
     * @param bool|object $setObject
     */
    public function __construct($setObject=false)
    {
        if(is_object($setObject) && !empty($setObject)){
            $this->setObject($setObject);
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param string $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * @return mixed
     */
    public function getPlataform()
    {
        return $this->plataform;
    }

    /**
     * @param mixed $plataform
     */
    public function setPlataform($plataform)
    {
        $this->plataform = $plataform;
    }

    /**
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param string $carrier
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;
    }

    /**
     * @return string
     */
    public function getLanguageDefault()
    {
        return $this->language_default;
    }

    /**
     * @param string $language_default
     */
    public function setLanguageDefault($language_default)
    {
        $this->language_default = $language_default;
    }

    /**
     * @return array
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * @param array $providers
     */
    public function setProviders($providers)
    {
        $this->providers = $providers;
    }

    /**
     * @param string $language
     * @return mixed
     */
    public function getPhraseologies($language='')
    {
        $idiom = substr($language, 0, 2);
        if(!empty($idiom)){
            return $this->phraseologies->$idiom;
        }

        return $this->phraseologies;
    }

    /**
     * @param array $phraseologies
     */
    public function setPhraseologies($phraseologies)
    {
        $this->phraseologies = (object) $phraseologies;
    }

    public function getObject()
    {
        return (object) [
            'id'               => (int) $this->id,
            'token'            => (string) $this->token,
            'project'          => (string) $this->project,
            'plataform'        => (string) $this->plataform,
            'carrier'          => (string) $this->carrier,
            'language_default' => (string) $this->language_default,
            'providers'        => (object) $this->providers,
            'phraseologies'    => (object) $this->phraseologies
        ];
    }

    /**
     * @param object $projectInforObject
     */
    public function setObject($projectInforObject)
    {
        $this->setId($projectInforObject->id);
        $this->setToken($projectInforObject->token);
        $this->setProject($projectInforObject->project);
        $this->setPlataform($projectInforObject->plataform);
        $this->setCarrier($projectInforObject->carrier);
        $this->setLanguageDefault($projectInforObject->language_default);
        $this->setProviders($projectInforObject->providers);
        $this->setPhraseologies($projectInforObject->phraseologies);
    }
}