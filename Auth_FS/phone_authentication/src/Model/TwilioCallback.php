<?php

namespace Api\Model;

use Api\Model\Entity\Callback;

class TwilioCallback extends Callback
{
    /**
     * TwilioCallback constructor.
     */
    public function __construct()
    {
        parent::__construct();
        parent::setBrokerName("Twilio");
    }

    /**
     * @param $status
     * @return $this
     */
    public function setResponseStatus($status)
    {
        switch (strtolower($status)) {
            case 'delivered':
            case 'queued':
            case 'sent':
                parent::setResponseStatus(true);
                break;

            case 'undelivered':
            case 'failed':
            default:
                parent::setResponseStatus(false);
        }
        return $this;
    }
}