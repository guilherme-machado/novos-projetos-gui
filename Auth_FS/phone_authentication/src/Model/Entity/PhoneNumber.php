<?php
namespace Api\Model\Entity;

class PhoneNumber
{
    private $phoneNumberInternational; // +5511922334455
    private $msisdn;                   // 11922334455
    private $phoneNumber;              // 22334455
    private $ddd;                      // 11
    private $ddi;                      // 55
    private $symbol;                   // +

    /**
     * PhoneNumber constructor.
     * @param null $msisdn
     * @param string $default_ddi
     * @param string $default_symbol
     */
    public function __construct($msisdn=null, $default_ddi='55', $default_symbol='+')
    {
        $this->setSymbol($default_symbol);
        $this->setDdi($default_ddi);
        self::formatMsisdn($msisdn);
    }

    /**
     * @param bool $symbol
     * @return mixed
     */
    public function getPhoneNumberInternational($symbol=true)
    {
        if ($symbol === false){
            return preg_replace('/\D/', '', $this->phoneNumberInternational);
        }
        return $this->phoneNumberInternational;
    }

    /**
     * @param mixed $phoneNumberInternational
     */
    public function setPhoneNumberInternational($phoneNumberInternational)
    {
        $this->phoneNumberInternational = $phoneNumberInternational;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getMsisdn()
    {
        return $this->msisdn;
    }

    /**
     * @param mixed $msisdn
     */
    public function setMsisdn($msisdn)
    {
        $this->msisdn = $msisdn;
    }

    /**
     * @return mixed
     */
    public function getDdd()
    {
        return $this->ddd;
    }

    /**
     * @param mixed $ddd
     */
    public function setDdd($ddd)
    {
        $this->ddd = $ddd;
    }

    /**
     * @return mixed
     */
    public function getDdi()
    {
        return $this->ddi;
    }

    /**
     * @param mixed $ddi
     */
    public function setDdi($ddi)
    {
        $this->ddi = $ddi;
    }

    /**
     * @return mixed
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param mixed $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @param $msisdn
     * @return bool
     */
    public function formatMsisdn($msisdn)
    {
        $msisdn = preg_replace('/\D/', '', $msisdn);
        switch (strlen($msisdn)) {
            case 10: // 1122334455
            case 11: // 11922334455
                $this->setMsisdn($msisdn);
                $this->setDdd(substr($msisdn, 0, 2));
                $this->setPhoneNumber(substr($msisdn, 2));
                $this->setPhoneNumberInternational($this->symbol . $this->ddi . $msisdn);
                break;

            case 12: // 551122334455
            case 13: // 5511922334455
                $this->setMsisdn(substr($msisdn, 2));
                $this->setDdi(substr($msisdn, 0, 2));
                $this->setDdd(substr($msisdn, 2, 2));
                $this->setPhoneNumber(substr($msisdn, 4));
                $this->setPhoneNumberInternational($this->symbol . $msisdn);
                break;
            default: return false;
        }
        return true;
    }

    /**
     * @return object
     */
    public function getPhoneNumberObject()
    {
        $payload = [
            'phoneNumberInternational' => $this->phoneNumberInternational,
            'msisdn'                   => $this->msisdn,
            'phoneNumber'              => $this->phoneNumber,
            'ddd'                      => $this->ddd,
            'ddi'                      => $this->ddi,
            'symbol'                   => $this->symbol
        ];
        return (object) $payload;
    }
}