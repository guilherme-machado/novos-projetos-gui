<?php

namespace Api\Model\Entity;

use Api\Util\Tracker;

class Callback
{
    private $id;
    private $brokerName;
    private $carrier;
    private $tracker;
    private $phoneNumber;
    private $statusCode;
    private $statusMessage;
    private $statusDatetime;
    private $vendorName;
    private $vendorLocale;
    private $responseStatus;
    private $responseDescription;

    /**
     * Callback constructor.
     */
    public function __construct()
    {
        $date = new \DateTime();
        $this->statusDatetime = $date->format('Y-m-d H:i:s');
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getBrokerName()
    {
        return $this->brokerName;
    }

    /**
     * @param string $brokerName
     * @return $this
     */
    public function setBrokerName($brokerName)
    {
        $this->brokerName = $brokerName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param string $carrier
     * @return $this
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;
        return $this;
    }

    /**
     * @return string
     */
    public function getTracker()
    {
        return $this->tracker;
    }

    /**
     * @param string $tracker
     * @return $this
     */
    public function setTracker($tracker)
    {
        $this->tracker = $tracker;

        $trackerInfo = Tracker::getTracker($tracker, false);
        $this->setVendorName($trackerInfo->project->getProject() . " " . $trackerInfo->project->getPlataform());
        $this->setVendorLocale($trackerInfo->details->locale);
        $this->setCarrier($trackerInfo->project->getCarrier());

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return $this
     */
    public function setPhoneNumber($phoneNumber)
    {
        $phoneNumber = new PhoneNumber($phoneNumber);
        $this->phoneNumber = $phoneNumber->getPhoneNumberInternational(false);
        return $this;
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param string $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    /**
     * @param string $statusMessage
     * @return $this
     */
    public function setStatusMessage($statusMessage)
    {
        $this->statusMessage = $statusMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatusDatetime()
    {
        return $this->statusDatetime;
    }

    /**
     * @param string $statusDatetime
     * @return $this
     */
    public function setStatusDatetime($statusDatetime)
    {
        $this->statusDatetime = $statusDatetime;
        return $this;
    }

    /**
     * @return string
     */
    public function getVendorName()
    {
        return $this->vendorName;
    }

    /**
     * @param string $vendorName
     * @return $this
     */
    public function setVendorName($vendorName)
    {
        $this->vendorName = $vendorName;
        return $this;
    }

    /**
     * @return string
     */
    public function getVendorLocale()
    {
        return $this->vendorLocale;
    }

    /**
     * @param string $vendorLocale
     * @return $this
     */
    public function setVendorLocale($vendorLocale)
    {
        $this->vendorLocale = $vendorLocale;
        return $this;
    }

    /**
     * @return bool
     */
    public function getResponseStatus()
    {
        return $this->responseStatus;
    }

    /**
     * @param $responseStatus
     * @return $this
     */
    public function setResponseStatus($responseStatus)
    {
        $this->responseStatus = $responseStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getResponseDescription()
    {
        return $this->responseDescription;
    }

    /**
     * @param $responseDescription
     * @return $this
     */
    public function setResponseDescription($responseDescription)
    {
        $this->responseDescription = $responseDescription;
        return $this;
    }
}