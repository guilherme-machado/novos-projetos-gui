<?php
namespace Api\Model\Entity;

class ComissioningLogEntity
{
    private $action;
    private $carrier;
    private $vendorName;
    private $vendorLocale;
    private $msisdn;
    private $gatewayName;
    private $gatewayStatus;
    private $gatewayResponseCode;
    private $gatewayResponseDescription;
    private $gatewayTracker;
    private $gatewayBalance;
    private $gatewayCredit;
    private $eventStatus;
    private $eventDescription;
    private $trackerId;
    private $startTime;
    private $executionTime;
    private $httpCode;

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param $action
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param $carrier
     * @return $this
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;
        return $this;
    }

    /**
     * @return string
     */
    public function getVendorName()
    {
        return $this->vendorName;
    }

    /**
     * @param $vendorName
     * @return $this
     */
    public function setVendorName($vendorName)
    {
        $this->vendorName = $vendorName;
        return $this;
    }

    /**
     * @return string
     */
    public function getVendorLocale()
    {
        return $this->vendorLocale;
    }

    /**
     * @param $vendorLocale
     * @return $this
     */
    public function setVendorLocale($vendorLocale)
    {
        $this->vendorLocale = $vendorLocale;
        return $this;
    }

    /**
     * @return string
     */
    public function getMsisdn()
    {
        return $this->msisdn;
    }

    /**
     * @param $msisdn
     * @return $this
     */
    public function setMsisdn($msisdn)
    {
        $this->msisdn = $msisdn;
        return $this;
    }

    /**
     * @return string
     */
    public function getGatewayName()
    {
        return $this->gatewayName;
    }

    /**
     * @param $gatewayName
     * @return $this
     */
    public function setGatewayName($gatewayName)
    {
        $this->gatewayName = $gatewayName;
        return $this;
    }

    /**
     * @return bool
     */
    public function getGatewayStatus()
    {
        return $this->gatewayStatus;
    }

    /**
     * @param $gatewayStatus
     * @return $this
     */
    public function setGatewayStatus($gatewayStatus)
    {
        $this->gatewayStatus = $gatewayStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getGatewayResponseCode()
    {
        return $this->gatewayResponseCode;
    }

    /**
     * @param $gatewayResponseCode
     * @return $this
     */
    public function setGatewayResponseCode($gatewayResponseCode)
    {
        $this->gatewayResponseCode = $gatewayResponseCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getGatewayResponseDescription()
    {
        return $this->gatewayResponseDescription;
    }

    /**
     * @param $gatewayResponseDescription
     * @return $this
     */
    public function setGatewayResponseDescription($gatewayResponseDescription)
    {
        $this->gatewayResponseDescription = $gatewayResponseDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getGatewayTracker()
    {
        return $this->gatewayTracker;
    }

    /**
     * @param $gatewayTracker
     * @return $this
     */
    public function setGatewayTracker($gatewayTracker)
    {
        $this->gatewayTracker = $gatewayTracker;
        return $this;
    }

    /**
     * @return string
     */
    public function getGatewayBalance()
    {
        return $this->gatewayBalance;
    }

    /**
     * @param $gatewayBalance
     * @return $this
     */
    public function setGatewayBalance($gatewayBalance)
    {
        $this->gatewayBalance = $gatewayBalance;
        return $this;
    }

    /**
     * @return string
     */
    public function getGatewayCredit()
    {
        return $this->gatewayCredit;
    }

    /**
     * @param $gatewayCredit
     * @return $this
     */
    public function setGatewayCredit($gatewayCredit)
    {
        $this->gatewayCredit = $gatewayCredit;
        return $this;
    }

    /**
     * @return bool
     */
    public function getEventStatus()
    {
        return $this->eventStatus;
    }

    /**
     * @param $eventStatus
     * @return $this
     */
    public function setEventStatus($eventStatus)
    {
        $this->eventStatus = $eventStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getEventDescription()
    {
        return $this->eventDescription;
    }

    /**
     * @param $eventDescription
     * @return $this
     */
    public function setEventDescription($eventDescription)
    {
        $this->eventDescription = $eventDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getTrackerId()
    {
        return $this->trackerId;
    }

    /**
     * @param $trackerId
     * @return $this
     */
    public function setTrackerId($trackerId)
    {
        $this->trackerId = $trackerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param $startTime
     * @return $this
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getExecutionTime()
    {
        return $this->executionTime;
    }

    /**
     * @param $executionTime
     * @return $this
     */
    public function setExecutionTime($executionTime)
    {
        $this->executionTime = $executionTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getHttpCode()
    {
        return (empty($this->httpCode)) ? 0 : $this->httpCode;
    }

    /**
     * @param $httpCode
     * @return $this
     */
    public function setHttpCode($httpCode)
    {
        $this->httpCode = $httpCode;
        return $this;
    }

    /**
     * @return object
     */
    public function getObject()
    {
        $data = [
            'action' => $this->getAction(),
            'carrier' => $this->getCarrier(),
            'vendor_name' => $this->getVendorName(),
            'vendor_locale' => $this->getVendorLocale(),
            'msisdn' => $this->getMsisdn(),
            'gateway_name' => $this->getGatewayName(),
            'gateway_status' => $this->getGatewayStatus(),
            'gateway_response_code' => $this->getGatewayResponseCode(),
            'gateway_response_description' => $this->getGatewayResponseDescription(),
            'gateway_tracker' => $this->getGatewayTracker(),
            'gateway_balance' => $this->getGatewayBalance(),
            'gateway_credit' => $this->getGatewayCredit(),
            'event_status' => $this->getEventStatus(),
            'event_description' => $this->getEventDescription(),
            'tracker_id' => $this->getTrackerId(),
            'start_time' => $this->getStartTime(),
            'execution_time' => $this->getExecutionTime(),
            'http_code' => $this->getHttpCode()
        ];
        return (object) $data;
    }
}