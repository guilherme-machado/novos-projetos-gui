<?php

namespace Api\Model\Entity;

use Api\Util\Text;

class SMSMessage
{
    private $message;
    private $tracker;
    private $phoneNumber;

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @param bool $clearText
     * @return $this
     */
    public function setMessage($message, $clearText = true)
    {
        if ($clearText) {
            $message = Text::clear($message);
        }
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getTracker()
    {
        return $this->tracker;
    }

    /**
     * @param string $tracker
     */
    public function setTracker($tracker)
    {
        $this->tracker = $tracker;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return $this
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = preg_replace('/\D/', '', $phoneNumber);
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode([
            'message' => $this->getMessage(),
            'phoneNumber' => $this->getPhoneNumber(),
            'tracker' => $this->getTracker()
        ]);
    }
}