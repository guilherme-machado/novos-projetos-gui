<?php
namespace Api\Model;

use Api\Model\Entity\PhoneNumber;

class Aprovisionador
{
    private $resource;
    private $timeout;

    /**
     * Aprovisionador constructor.
     * @param $resource
     * @param int $timeout
     */
    public function __construct($resource, $timeout=30)
    {
        $this->resource = $resource;
        $this->timeout = $timeout;
    }

    /**
     * @param PhoneNumber $phoneNumber
     * @param $carrier
     * @return array|bool
     */
    public function getCustomToken(PhoneNumber $phoneNumber, $carrier)
    {
        return (new CurlAbstract())
            ->setMethod("POST")
            ->setTimeout($this->timeout)
            ->setEndpoint($this->resource . "/v2/firebase/{$carrier}/user")
            ->setUriParams(['msisdn' => $phoneNumber->getPhoneNumberInternational(false)])
            ->run();
    }
}