class AppSms
    def initialize
        #Criando uma nova mensagem
        $driver.wait_true(10){ $driver.exists { $driver.find_element :id, 'com.google.android.apps.messaging:id/start_new_conversation_button' } }
        new_conversation = $driver.find_element :id, 'com.google.android.apps.messaging:id/start_new_conversation_button'
        new_conversation.click
    end

    def new_message
        #Criando uma nova mensagem
        $driver.wait_true(10){ $driver.exists { $driver.find_element :id, 'com.google.android.apps.messaging:id/start_new_conversation_button' } }
        new_conversation = $driver.find_element :id, 'com.google.android.apps.messaging:id/start_new_conversation_button'
        new_conversation.click
    end

    def insert_recipient(la)
        #Inserindo remente
        $driver.wait_true(10){ $driver.exists { $driver.find_element :id, 'com.google.android.apps.messaging:id/recipient_text_view' } }
        recipient = $driver.find_element (:id), 'com.google.android.apps.messaging:id/recipient_text_view'
        recipient.send_keys la
        $driver.press_keycode(66)
    end

    def insert_message(keyword)
        #Inserindo conteúdo da mensagem
        $driver.wait_true(5){ $driver.exists { $driver.find_element :id, 'com.google.android.apps.messaging:id/compose_message_text' } }
        message = $driver.find_element (:id), 'com.google.android.apps.messaging:id/compose_message_text'
        message.send_keys keyword
    end

    def send_message
        #Enviar mensagem
        $driver.wait_true(10){ $driver.exists { $driver.find_element :id, 'com.google.android.apps.messaging:id/send_message_button_icon' } }
        send_message = $driver.find_element (:id), 'com.google.android.apps.messaging:id/send_message_button_icon'
        send_message.click
    end

    # def validate_message
    #     #Aguardar resposta
    #     # sleep 25
    #     $driver.wait_true(10){ $driver.exists { $driver.find_elements (:id), 'com.google.android.apps.messaging:id/message_text' } }
    #     validator = $driver.find_elements (:id), 'com.google.android.apps.messaging:id/message_text'
    #     extent = validator.size
    #     message = validator[extent - 1].attribute('text')
    #     message
    # end

    def validate_message(text)
        #Aguardar resposta
        validate = $driver.wait_true(50) { $driver.exists { $driver.find_element :xpath, "//*[@text[contains(., '#{text}')]]" } }
        #puts "Wait True: #{validate}"
        #validator = $driver.find_element(:xpath, "//*[@text='#{text}*']")
        validator = $driver.find_element :xpath, "//*[@text[contains(., '#{text}')]]"
        message = validator.attribute('text')
        puts "Message: #{message}"
        message
    end

    def cancel(la)
        insert_recipient(la)
        insert_message("SAIR")
        send_message
        insert_message("SAIR")
        send_message
    end

    def cancel_all(validate)
        puts validate
        if validate.nil?
            puts "Sem produtos assinados"
        else
            cancel("876")
            $driver.back
            $driver.back
            initialize

            cancel("877")
            $driver.back
            $driver.back
            initialize

            cancel("878")
            $driver.back
            $driver.back
            initialize

            cancel("881")
            $driver.back
            $driver.back
            initialize
            puts "Produtos cancelados"
        end
    end
end