class BaseScreen
    
    def msisdn
        msisdn = "11964908102"
        msisdn
    end

    def wait(id, timeout = 10)
        begin
            $driver.wait_true(timeout) { $driver.exists { $driver.find_element :id, id } }
        rescue
            raise "Elemento de id: #{id} nao encontrado"
        end
    end

    def wait_text(text, timeout = 10)
        begin
            $driver.wait_true(timeout) { $driver.exists { $driver.find_element :xpath, "//*[@text='#{text}']" } }
        rescue
            raise "Elemento de texto: #{text} nao encontrado"
        end
    end

    def exists(id)
        begin
            $driver.exists { $driver.find_element :id, id }
        rescue
            raise "Elemento de id: #{id} nao encontrado"
        end
    end

    def exists_text(text)
        validator = $driver.exists { $driver.find_element :xpath, "//*[@text='#{text}']" }
        puts validator
        validator
    end

    def find(id)
        element = $driver.find_element :id, id
        element
    end

    def find_text(text)
        element = $driver.find_element :xpath, "//*[@text='#{text}']"
        element
    end    

    def touch_button(id)
        button = $driver.find_element :id, id
        button.click
    end

    def touch_button_text(text)
        button = $driver.find_element :xpath, "//*[@text='#{text}']"
        button.click
    end

    def fill(id, text)
        field = $driver.find_element :id, id
        field.send_key "#{text}"
    end
end