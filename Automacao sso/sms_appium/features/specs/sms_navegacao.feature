# language: pt
@nav
Funcionalidade: Fluxos de Interacao SMS dos comando de navegação
    Usuario Vivo que interage com comandos de navegação

    Contexto:
        Dado que usuario Vivo interaja via SMS
@ajuda_sem_produto
    Esquema do Cenario: FLuxo de envio do comando AJUDA sem produto assinado
        Quando usuario, que nao possui produto assinado, envia a keyword "AJUDA" para o LA <la>
        Entao recebe a mensagem <mensagem>

        Exemplos: Envio da keyword AJUDA sem produto assinado
            | la    | mensagem                                                                                                                                                                |
            | "881" | "Voce nao possui este servico. Use o Vivo Protege e tenha internet segura e 32GB de memoria online, por RS11,99/mes com 7 dias gratis. Para assinar, responda OK"       |
            | "878" | "Voce ainda nao possui o Vivo Sync. Aproveite 32 Giga de memoria online por so RS 5,99/mes com primeiros 2 dias gratis! Para assinar, responda com o numero 2"          |
            | "877" | "Voce ainda nao possui o Vivo Filhos Online. Monitore ate 3 aparelhos por apenas RS 5,99/mes com primeiros 15 dias gratis! Para assinar, responda com o numero 3"       |
            | "876" | "O Seguranca Online agora faz parte do Vivo Protege! Tenha mais beneficios com 7 dias GRATIS. Aguarde um SMS do numero 881 ou acesse protecao.vivo.com.br/vivo-protege" |
            
@invalido
    Esquema do Cenario: FLuxo de envio de comandos INVALIDOS com ou sem produtos assinados 
        Quando usuario, com ou sem produto assinado, envia uma keyword "INVALIDA" para o LA <la>
        Entao recebe a mensagem <mensagem>

        Exemplos: Envio de keyword INVALIDA com ou sem produto assinado
            | la    | mensagem                                                                                                                                                         |
            | "881" | "Voce enviou um comando invalido. Precisa de ajuda com o seu Vivo Protege? Acesse o nosso chat: protecao.vivo.com.br/chat. Para cancelar, envie SAIR"           |
            | "878" | "Comando invalido. Guarde fotos e videos no Vivo Sync e tenha 32 Giga por so RS 5,99/mes com primeiros 2 dias gratis! Para assinar, responda com o numero 2"     |
            | "877" | "Comando invalido. Use Vivo Filhos Online e monitore ate 3 aparelhos por apenas RS 5,99/mes com primeiros 15 dias gratis! Para assinar, responda com o numero 3" |
            | "876" | "Comando invalido! O Seguranca Online faz parte do Vivo Protege. Aproveite mais beneficios com 7 dias gratis, responda PROTEGE ou acesse protecao.vivo.com.br/"  |


#@ajuda_com_produto
#    Esquema do Cenario: FLuxo de envio do comando AJUDA com produto assinado
#        Quando usuario, que possui produto assinado, envia a keyword "AJUDA" para o LA <la>
#        Entao recebe a mensagem <mensagem>
#
#        Exemplos: Envio da keyword AJUDA com produto assinado
#            | la    | mensagem                                                                                                                                                                |
#            | "881" | "Vivo Protege: Qual sua duvida? Envie um SMS gratis com a letra: A)Instalacao B)Senha  C)Chat D)Cancelar Servico E)Tarifacao"                                           |
#            | "878" | "Vivo Sync: Qual sua duvida? Responda gratis com a letra: A)Instalacao B)Senha C)Servico D)Compatibilidade E)Cancelamento F)Aumentar armazenamento T)Tarifacao"         |
#            | "877" | "Vivo Filhos Online: Qual sua duvida? Responda gratis com a letra: A)Instalacao B)Senha C)Servico D)Compatibilidade E)Cancelar F)Mais licencas T)Tarifacao"             |
#            | "876" | "O Seguranca Online agora faz parte do Vivo Protege! Tenha mais beneficios com 7 dias GRATIS. Aguarde um SMS do numero 881 ou acesse protecao.vivo.com.br/vivo-protege" |