# language: pt

Funcionalidade: Fluxos SMS de assinatura do Vivo Protege
    Usuario Vivo que envia keyword de assinatura do Vivo Protege 

    Contexto:
        Dado que usuario Vivo interaja via SMS
@protege
    Esquema do Cenario: FLuxo de assinatura dos pacotes Vivo Protege
        Quando usuario, que nao possui produto assinado no "881", inicia o fluxo de assinatura
        Entao usuario envia a keyword de assinatura <keyword> para "881"
        E recebe a mensagem <message_one> que solicita confirmacao
        E envia "SIM" para "881" e recebe a mensagem de boas vindas <message_two>

        Exemplos: FLuxo de assinatura dos pacotes Vivo Protege
            | keyword | message_one                                                                                                                     | message_two                                                                                                      |
            | "2"     | "Assine o Vivo Protege por RS 11,99/mes (renovacao automatica). Primeiros 7 dias gratis. Para confirmar, responda SIM"          | "Bem-vindo ao Vivo Protege! Aproveite por RS 11.99/mes com 7 dias gratis. Instale o APP em http://su.fsvas.com/" |
            | "3"     | "Assine o Vivo Protege Mais por RS 15,99/mes (renovacao automatica). Primeiros 30 dias gratis. Para confirmar, responda SIM"    |"Bem-vindo ao Vivo Protege Mais! Aproveite por RS 15.99/mes com 7 dias gratis. Instale o APP em http://su.fsvas/"                                   |
            | "4"     | "Assine o Vivo Protege Familia por RS 29,99/mes (renovacao automatica). Primeiros 30 dias gratis. Para confirmar, responda SIM" |"Bem-vindo ao Vivo Protege! Voce assinou o Vivo Protege Familia e tem 30 dias gratis para usar, depois so RS 29.99/mes. Para cancelar envie SAIR"   |