#encoding: utf-8
#require_relative '../screens/database.rb'

Dado("que usuario Vivo interaja via SMS") do
  @msisdn2 = "11996124602"
  @page = AppSms.new
  @db = Database.new
  puts @msisdn2
end

Quando("usuario, que nao possui produto assinado, envia a keyword {string} para o LA {string}") do |keyword, la|
  signed = @db.validate_db(@msisdn2)
  @page.cancel_all(signed)

  @page.insert_recipient(la)
  @page.insert_message(keyword)
  @page.send_message
end

Entao("recebe a mensagem {string}") do |mt|
  message_text = @page.validate_message(mt)
  puts "teste: #{message_text}"
  expect(message_text).to include mt
end

Quando("usuario, que possui produto assinado, envia a keyword {string} para o LA {string}") do |keyword, la|
  @page.insert_recipient(la)
  @page.insert_message(keyword)
  @page.send_message
end

Quando("usuario, com ou sem produto assinado, envia uma keyword {string} para o LA {string}") do |keyword, la|
  signed = @db.validate_db(@msisdn2)
  @page.cancel_all(signed)

  @page.insert_recipient(la)
  @page.insert_message(keyword)
  @page.send_message
end

# Quando("usuario, que nao possui produto assinado no {string}, inicia o fluxo de assinatura") do |la|
#   @page.insert_recipient(la)
#   @page.insert_message("SAIR")
#   @page.send_message
# end

# Entao("usuario envia a keyword de assinatura {string} para {string}") do |keyword, la|
#   @page.insert_message(keyword)
#   @page.send_message
# end

# Entao("recebe a mensagem {string} que solicita confirmacao") do |message_one|
#   result = @page.validate_message(message_one)
#   puts result.eql? message_one
#   expect(result).to include message_one
# end

# Entao("envia {string} para {string} e recebe a mensagem de boas vindas {string}") do |keyword, la, message_two|
#   @page.insert_message(keyword)
#   @page.send_message(message_two)
#   result = @page.validate_message
#   puts result.eql? message_two
#   expect(result).to include message_two
# end