#encoding: utf-8

# Dado("que usuario Vivo interaja via SMS") do
#   @page = AppSms.new
# end

Quando("usuario, que nao possui produto assinado no {string}, inicia o fluxo de assinatura") do |la|
  signed = @db.validate_db(@msisdn2)
  @page.cancel_all(signed)
end

Entao("usuario envia a keyword de assinatura {string} para {string}") do |keyword, la|
  @page.insert_recipient(la)
  @page.insert_message(keyword)
  @page.send_message
end

Entao("recebe a mensagem {string} que solicita confirmacao") do |message_one|
  result = @page.validate_message(message_one)
  puts result.eql? message_one
  expect(result).to include message_one
end

Entao("envia {string} para {string} e recebe a mensagem de boas vindas {string}") do |keyword, la, message_two|
  @page.insert_message(keyword)
  @page.send_message
  result = @page.validate_message(message_two)
  puts result.eql? message_two
  expect(result).to include message_two
end