
class Login
    set_url "#{DATA['cloud_by_hero']['portal']}"
    element :tela_login, '#phone'
    element :entrar, ".btn-login"
    element :pincode, "#pincode"
    element :valida_pincode, ".btn-valida-pincode"
    
    
    def solicita_pin 
   
        body = {
            "msisdn": "11951480432"
            }
             HTTParty.post("#{DATA['cloud_by_hero']['api']}/phone-auth/send",
            :body => body.to_json,
            :headers => {"Content-Type" => 'application/json', "Authorization" => "Bearer 8afffd27-2adf-483e-8222-a49cd0cdfb8f"  #tokem hero Webporal
             })
             
        transaction_id = solicita_pin.response.body
        data = transaction_id.parsed_response["data"]["transaction_id"]
        @transaction_id2 = data    
    end  


     def captura_pin 
        HTTParty.get("#{DATA['cloud_by_hero']['api']}/phone-auth/extra/get-tracker-info?transaction_id=#{@transaction_id2}" ,
         
       :headers => {"Content-Type" => 'application/json', "Authorization" => "Bearer 8afffd27-2adf-483e-8222-a49cd0cdfb8f" 
       })
       
       response = captura_pin.response.body
       @data_pin  = response.parsed_response["data"]["details"]["pincode"]
       puts @data_pin
     end 
end

class Logged_area

    element :galeria, "#moduleHeaderTitle"
    element :sanswich, ".material-icons"
    element :profile, "profile-background-overlay"
    element :foto, "profilePhoto0"
    element :trocafoto, "#editProfileUploadPicture0"
    element :savefoto, "#savePicBtn0"
    element :editprofile, ".editProfile"
    element :nome, "#zpProfile0FirstName"
    element :sobrenome, "#lastname"
    element :email, "#zpProfile0Email"
    element :save, "#zpWin455Buttons > div:nth-child(1)"
    element :MeusDispositivos,  "#zpProfileleftButtons > span:nth-child(2) > a"
    element :TextoMeusDisposivos, ".accountSubTitle"
    element :dispositivos, ".accountTitleBottom"
    element :MinhasConexões, "#zpProfileleftButtons > span.impersonate.active > a"
    element :facebook, "#zpArgoProfileContainer > div > div.profileArea > div > div:nth-child(1) > div.connectionTexts > div.connectionTitle > label > span.mdl-checkbox__ripple-container.mdl-js-ripple-effect.mdl-ripple--center"
    element :confirm, "#zpWin485Buttons > div:nth-child(1)"
    element :LoginFacebook, "#email"
    element :PasswordFacebook, "#pass"
    element :ConfirmLogin, "#loginbutton"
    element :dropbox, "#zpArgoProfileContainer > div > div.profileArea > div > div:nth-child(2) > div.connectionTexts > div.connectionTitle > label > span.mdl-checkbox__ripple-container.mdl-js-ripple-effect.mdl-ripple--center"
    element :ConfirmDropbox, "#zpWin499Buttons > div:nth-child(1)"
    element :LoginDropbox, "[name = 'login_email']"    
    element :PasswordDropbox, "[name = 'login_password']"
    element :ConfirmPassword, "#regular-login-forms > div > form > div:nth-child(7) > button > div.sign-in-text"
    element :MeuArmazanamento, "#zpArgoProfileContainer"
    element :voltar, "#topbarContainer > div.mdl-layout__header-main-button.mdl-layout__header-main-button--previous-view > i"
    element :uploadgeneral, "#uploadActionButton"
    element :ChargeUpload, "<span class='accountinfo'>Carregar a partir desse computador</span>"
    element :FilesArchive, "#zpLoadfilesTip > div"
    element :musics, "#zpLoadmusicTip"
    element :family, "#zpLoadfamilyTip > div"
    element :NoFamily, "#noFamilyButtonCreateFamily"
    element :InviteEmail, "#familySendInvitationToContacts"
    element :SendEmailFamilyMember, "#zpFamily224introWindowButtonInv"
    element :compartilhando, "#zpLoadcollaborationTip"
    element :AllFiles, "#zpLoadallfilesTip > div"
    element :contacts, "#zpLoadcontactsTip > div"
    element :GoToTrash, "#dotsMenu77 > i"
    element :trash, "#viewActionsContainer77 > div > ul > li:nth-child(1) > span.action-name"
    element :EmpityTrash, "#emptyTrashAction77"
end
