#language:pt

@cloudhero


Funcionalidade: Validação do Cloud By Hero Webportal

O Webportal do Cloud by Hero, é o nosso cloud B2C vendido pela fs 
Ele possui alguns tipos de tamanho e configurações. O intuito dessa automação é
certificar que todas as funcionalidades manuais, que por mais que simplicitas e oneram uma infinidade de tempo
sejam testadas com maior qualidade cobertura e eficácia.

Esquema do Cenário: login

Dado que eu como um Q.A irei fazer os testes iniciais do cloud by Hero
Quando eu logar no cloud farei as validações necessárias
Então farei os testes necessáraios como <nome> <senha> <cota> <profile> <Versão_de_Pathce> para assegurar que estou logado corretamente


Exemplos:

|Usuário|Senha|Cota|Profile|Versão_de_Pathce|
|   ""  |  "" | "" |   ""  |       ""       |
|   ""  |  "" | "" |   ""  |       ""       |
|   ""  |  "" | "" |   ""  |       ""       |
|   ""  |  "" | "" |   ""  |       ""       |
|   ""  |  "" | "" |   ""  |       ""       |

Esquema do Cenário: Area logada

Dado que eu como Q.A certifiquei-me que estou logado
Quando eu estiver logado vou para meu profile e inserir meus dados na alterações e confirmar
Então farei os testes necessários com os dados <Nome> <Sobrenome> <Email> <Texto_rodapé>

Exemplos: 

|Nome|Sobrenome|Email|Texto_rodapé|
| "" |    ""   |  "" |     ""     |
| "" |    ""   |  "" |     ""     |
| "" |    ""   |  "" |     ""     |
| "" |    ""   |  "" |     ""     |
| "" |    ""   |  "" |     ""     |


Esquema do Cenário: Meus dispositivos

Dado que eu como QA certifiquei-me que estou logado 
Quando eu estiver logado vou para meu profile
Então certifico-me que os <Textos> e layout da página estão ok

Exemplos:

|Textos|
|  ""  |
|  ""  |
|  ""  |
|  ""  |
|  ""  |

Esquema do Cenário: Minhas conexões 

Dado eu com Q.A certifico me que estou logado e clico em minhas conexões 
Quando eu fizer as conexões com as redes sociais 
Então eu farei as validações necessárias para conectar com os dados <Login> <Senha> <Serviço> <Texto> para entrar nas contas com sucesso.

Exemplos: 

|login|Senha|Serviço|Texto|
|  "" |  "" |   ""  |  "' |
|  "" |  "" |   ""  |  "' |
|  "" |  "" |   ""  |  "' |
|  "" |  "" |   ""  |  "' |

Esquema do Cenário: Meu Armazenamento

Dado que eu como Q.A Certifico me que estou logado e clico em meu Armazenamento
Quando eu identificar os tamanhos e quota irei exibilos e tela
Então farei 


Cenário: Galeria 

Dado que eu como Q.A estou com o Portal Logado vou para galeria de imagens
Quando Eu estou na galeria eu irei realizar os uploads
Então  eu farei as validações para identificar se o upload de imagem foi feito com sucesso.

Cenário: Download

Dado que eu como Q.A fiz o upload completo no cenário acima
Quando a foto estiver sendo exibida no Portal 
Então eu faço o download e valido as ações necessárias para isso.

Cenário: Arquivos 

Dado que eu como Q.A estou com o Portal Logado vou para os Aquivos
Quando eu estou em Arquivos eu irei realizar os uploads necessários
Então  eu farei as validações para identificar se o upload de arquivos foi feito com sucesso. 

Cenário: Músicas

Dado que eu como Q.A estou como Portal Logado e vou para Músicas 
Quando eu estou em Músicas eu irei realizar os uploads necessários
Então  eu farei as validações para identificar se o upload de arquivos foi feito com sucesso. 


Cenário: Famiília
 
Dado que eu como Q.A estou com o Portal Logado e clico em Famiília
Quando eu estou em Familia irei realizar os uploads necessários
Então eu farei as validações necessárias para compartilhar com sucesso

Cenário: Logout 

Dado que eu como Q.A estou com o Portal logado e clico em meu perfil
Quando eu estou em perfil clico em Logout
Então eu farei as valiações necessárias para garantir que o logout foi sucesso.

