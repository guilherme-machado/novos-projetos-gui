#language:pt

Funcionalidade: Área Logada

    Para gerenciar minha conta no site Hero
    Sendo um usuário que possui o plano assinado
    Posso fazer meu login no site e acessar a área logada
    
@DM_page @area_logada
Esquema do Cenário: Download Manager 
    
    Dado que eu efetuei assinatura e login
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    Quando exibida a área logada visualizarei a opção Download do menu
    E quando acionar o botão Faça o Download
    Entao sou direcionado para a tela de Download
    E efetuarei o cancelamento do plano

    Exemplos: 
        | msisdn         | plano     | la   | keyword     |
        | (11)98816-2602 | Basico    | 5513 | HERO199GEM1 |


@MeusApps_page @area_logada
Esquema do Cenário: Meus Aplicativos
    
    Dado que eu efetuei assinatura e login
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    Quando exibida a área logada visualizarei a opção Meus Aplicativos no menu
    E quando acionar o botão Meus Aplicativos
    Entao sou direcionado para a tela contendo os aplicativos
    E os aplicativos exibidos deverão corresponder ao plano assinado
    E efetuarei o cancelamento do plano

      Exemplos: 
        | msisdn        | plano     | la   | keyword     |
        # | 5511988162602 | Essencial | 5513 | HEROAD1     |
        | (11)98816-2602 | Basico    | 5513 | HERO199GEM1 |
        | (11)98816-2602 | Premium   | 5513 | HERO299GEMP |


