#language:pt

Funcionalidade: Login

    Para gerenciar minha conta no site Hero
    Sendo um usuário que possui o plano assinado
    Posso fazer meu login no site
    
@loginValido @login
Esquema do Cenário: Login Valido 
    
    Dado que eu acessei a pagina de login
    Quando faço login com o seguinte msisdn que possui uma assinatura:
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    E digito um PINCODE válido
    Entao sou autenticado com sucesso e vejo a pagina de area logada

    Exemplos: 
      | msisdn         | plano     | la   | keyword     |
      | (11)98816-2602 | Basico    | 5513 | HERO199GEM1 |


@loginReenvioPincode @login
Esquema do Cenário: Login Validando Reenvio de Pincode 
    
    Dado que eu acessei a pagina de login
    Quando faço login com o seguinte msisdn que possui uma assinatura:
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    E quero receber um novo PINCODE através da funcionalidade reenviar 
    Entao digito o novo pincode recebido
    E sou autenticado com sucesso e vejo a pagina de area logada

    Exemplos: 
    | msisdn         | plano     | la   | keyword     |
    | (11)98816-2602 | Basico    | 5513 | HERO199GEM1 |


@loginInvalido @login
Esquema do Cenário: Login Invalido

    Dado que eu acessei a pagina de login
    Quando faço login com o seguinte msisdn que não possui assinatura:
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    Então devo ver uma "<mensagem>" de negação

    Exemplos: 
      | msisdn | plano     | la   | keyword     | mensagem                            |
      | (10    | Basico    | 5513 | HERO199GEM1 | Por favor, digite um número válido! |


     