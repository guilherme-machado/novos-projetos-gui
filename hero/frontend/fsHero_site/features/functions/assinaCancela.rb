def assina_cancela_Hero(msisdn, la, keyword)

    endpoint = $api['assinaCancelaHero']
    
    body = {
        "msisdn": msisdn,
        "la": la,
	    "text": keyword,
	    "partner": "claro",
	    "debug": true,
	    "transaction_id": Faker::Number.number(21),
	    "auth_code": Faker::Number.number(4),
	    "auth_code_status": true,
        "transaction_id_auth": Faker::Number.number(15)
    }    
       
    @result = HTTParty.post(
        endpoint,
        headers: {"Content-Type" => 'application/json'},
        body: body.to_json       
    )
  
end