class LoginPage < SitePrism::Page

    set_url 'https://www.fshero.com.br/'

    element :botao_login, '#btn-logar'
    element :input_msisdn, '#phone'    
    element :botao_entrar, 'button[class*=btn-login]'
    element :input_pincode, '#pincode' 
    element :botao_continuar, 'button[class*=btn-valida-pincode]'
    element :botao_reenviarPincode, '.btn-reenvia-pincode'
    element :text_reenviandoPincode, '.aguarde'
    element :mensagem_erro, '.error'


    def logar(msisdn)
        botao_login.click
        input_msisdn.set msisdn    
        botao_entrar.click
    end


    def validarPincode(pincode)
        input_pincode.set pincode  
        botao_continuar.click
    end

    
    def reenviarPincode      
        botao_reenviarPincode.click
    end

    
end
