class MeusApps_Page < SitePrism::Page
    #Menu Principal
    element :botao_meusApps, 'a[title="Meus aplicativos"]'

    #Sub-menu Planos
    element :icone_seguranca, 'a[class*="app-seguranca"]'
    element :icone_cloud, 'a[class*="app-cloud"]'
    element :icone_suporteDigital, 'a[class*="app-suporte-digital"]'
    element :icone_wifi, 'a[class*="app-wi-fi"]'
    element :icone_familia, 'a[class*="app-familia"]'


    #Conteúdo dos planos
    element :nome_plano, 'div .vh-content > h2'
    element :qtd_licencas, 'div .vh-content > h3'

    
 
    def ver_meusAplicativos()
        botao_meusApps.click
    end

    def clicar_icone_seguranca()
        icone_seguranca.click
    end
    
    def clicar_icone_cloud()
        icone_cloud.click
    end

    def clicar_icone_familia()
        icone_familia.click
    end

    def clicar_icone_suporteDigital()
        icone_suporteDigital.click
    end

    def clicar_icone_wifi()
        icone_wifi.click
    end
    
 end