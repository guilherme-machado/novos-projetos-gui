  Dado("que eu acessei a pagina de login") do
    @login = LoginPage.new
    @login.load    
  end
  
  Quando("faço login com o seguinte msisdn que possui uma assinatura:") do |table|
    @dadosLogin = table.rows_hash
    @msisdn = ((@dadosLogin['msisdn']).tr(' ()-', ''))

    # Começa testes cancelando plano assinado
    assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), "cancelar")
    
    # Assina plano
    assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), (@dadosLogin['keyword'])) 

    #logar
    @login.logar(@dadosLogin['msisdn'])    
  end
  
  Quando("digito um PINCODE válido") do
    solicita_pincode(@dadosLogin['msisdn'])    
    pincode = captura_pincode
    @login.validarPincode(pincode)
  end
  
  Entao("sou autenticado com sucesso e vejo a pagina de area logada") do
    @home = HomePage.new    
    expect(@home.text_bemVindo.text).to eql 'BEM-VINDO AO HERO!'    
  end
  
# ----------------------------Login validando reenvio de pincode----------------------------

  Quando("quero receber um novo PINCODE através da funcionalidade reenviar") do
    solicita_pincode(@dadosLogin['msisdn'])    
    pincode = captura_pincode
    @login.reenviarPincode
    
    # Chama função que tira print do teste
    screenshot(nome_cenario = "ReenvioPincode")
  end
  
  Entao("digito o novo pincode recebido") do
    solicita_pincode(@dadosLogin['msisdn'])    
    pincode = captura_pincode
    @login.validarPincode(pincode)
  end

 
  # --------------------------------------Login inválido--------------------------------------

  Quando("faço login com o seguinte msisdn que não possui assinatura:") do |table|
    @dadosLogin = table.rows_hash
    @msisdn = ((@dadosLogin['msisdn']).tr(' ()-', ''))

    # Começa testes cancelando plano assinado
    assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), "cancelar")

    #logar
    @login.logar(@dadosLogin['msisdn'])
  end
  
  Então("devo ver uma {string} de negação") do |mensagem|
    sleep 1
    expect(@login.mensagem_erro.text).to eql mensagem
  end
  
  