# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado(/^que a claro envie um cancelamento para o backend do hero$/) do

end

Quando(/^o hero receber os dados do msisdn$/) do
  
  @createbody = {
  "id_cliente": recuperar_msisdn_hero,
  "id_chave": recuperar_idchave_hero,
  "motivo": "Claro pediu através do #{Faker::Name.name}",
  "canal": Faker::Name.name
}.to_json

    puts @createbody

    @create = HTTParty.post($api['post_cancelamento_hero'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody)
    puts ($api['post_cancelamento_hero'])
end

Então(/^precisa realizar o cancelamento do cliente hero$/) do
    puts @create.body
    case @create.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@create.code}"
    end

  expect(@create.code).to eq 200
  #response = JSON.parse(@create.body, object_class: OpenStruct)
  #expect(response.status).to eq("OK").or eq("Chave ja cancelada.")

  expect(@create['status']).to eq("OK").or eq("Chave ja cancelada.")
end