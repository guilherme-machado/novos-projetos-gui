# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado(/^que eu chame o endpoint de integração.$/) do

end

Quando(/^for um cancelamento da claro cancelar claro, quando for hero cancelar hero.$/) do
  
  @createbody = {
 "msisdn": recuperar_msisdn_hero_billing,
 "auth_transactionid": recuperar_transaction_id_auth_hero_billing,
 "auth_code": recuperar_auth_code_hero_billing,
 "special_number": "X",
 "share_code": recuperar_share_code_hero_billing,
 "auth_request_status": "CANCL",
 "auth_status_date": "20171031095338",
 "auth_request_status_reason_id": 106,
 "auth_profile_id": "FSVAS",
 "charging_profile_id": "123456"
}.to_json

    puts @createbody

    @create = HTTParty.post($api['post_cancelamento_hero_integrador'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody)
    puts ($api['post_cancelamento_hero_integrador'])
end

Então(/^cancelamento ok.$/) do
    puts @create.body
    case @create.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@create.code}"
    end

  expect(@create.code).to eq 200
end