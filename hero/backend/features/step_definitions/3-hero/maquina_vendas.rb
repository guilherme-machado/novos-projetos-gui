Dado("que eu queria um produto da maquina de vendas") do

  puts "\n\nINICIANDO ASSINATURA DO PRODUTO MAQUINA DE VENDAS\n\n"
  @msisdns          = "5540#{Faker::Number.number(9)}"

end

Quando("passar o {int} do plano correto para a contratação de um plano da maquina de vendas") do |code|

  maquina_vendas(code)

  puts "\nENDPOINT\n"
  puts $api['maquina_vendas']
  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @payload_maquina_vendas

  @post_maquina_vendas = HTTParty.post($api['maquina_vendas'],:headers => {"Content-Type" => 'application/json'}, :body => @payload_maquina_vendas)

  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo(@msisdns)
  expect(@getProduto.success).to eq true
  
  # puts "CONSULTANDO NUMERO DA SORTE"
  # luck_number

end

Então("a contratação será realizada com sucesso para os planos solicitados da maquina de vendas") do

  puts "\n\nIMPRIMINDO RESULTADO DO TESTE\n\n"
  puts @post_maquina_vendas.body
    case @post_maquina_vendas.code
      when 200
        puts "\n\nRetornou 200, ok\n\n"
      when 404
        puts "\n\nRetornou 404, não existe\n\n"
      when 400
        puts "\n\nRetornou 400, problema de negócio\n\n"
      when 500...600
        puts "\n\nops #{@post_maquina_vendas.code}\n\n"
    end

  expect(@post_maquina_vendas.code).to eq 200

  # @get_ns = JSON.parse(@numero_sorte.body, object_class: OpenStruct)
  # expect(@get_ns['normal']).to eq 1
  # expect(@get_ns['especial']).to eq 1
  # puts @numero_sorte.body

end