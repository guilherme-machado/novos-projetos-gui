# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
E("tenha uma assinatura ativa") do

  puts "\n\nVALIDANDO ASSINATURA\n\n"
  algar("I")
  puts ($api['algar_compra_canc'])
  puts @createbody_algar

  @create_algar = HTTParty.post($api['algar_compra_canc'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody_algar)

  puts @create_algar.body
  case @create_algar.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@create_algar.code}"
  end

  expect(@create_algar.code).to eq 200

  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo(@msisdns)
  expect(@getProduto.success).to eq true

end

Quando("eu passar o body com a intensão de cancelamento") do

  puts "\n\nINICIANDO CANCELAMENTO\n\n"
  algar("C")
  puts ($api['algar_compra_canc'])
  puts @createbody_algar

  @cancel_algar = HTTParty.post($api['algar_compra_canc'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody_algar)


  @waitingTimeOut=1
  sleep(1) until !consultar_produto_ativo(@msisdns)
  expect(@getProduto.success).to eq false

end

Então("o cancelamento será realizada com sucesso") do
  puts @cancel_algar.body
  case @cancel_algar.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@cancel_algar.code}"
  end

  expect(@cancel_algar.code).to eq 200

end