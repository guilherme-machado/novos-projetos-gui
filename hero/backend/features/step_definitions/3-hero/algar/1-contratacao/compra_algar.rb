# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado(/^que eu acesse o endpoint \/algar\/algar-api.php$/) do

  @msisdns = "5550900#{Faker::Number.number(6)}"

end

Quando(/^eu passar o body com a intensão de compra$/) do

  algar("I")
  puts ($api['algar_compra_canc'])
  puts @createbody_algar

  @create_algar = HTTParty.post($api['algar_compra_canc'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody_algar)


  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo(@msisdns)
  expect(@getProduto.success).to eq true

end

Então(/^a compra será realizada com sucesso$/) do
  puts @create_algar.body
  case @create_algar.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@create_algar.code}"
  end

  expect(@create_algar.code).to eq 200
  response = JSON.parse(@create_algar.body, object_class: OpenStruct)
  expect(response.codigo).to eq "01"
end