# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado("que tenha um msisdn para cada {string}") do |plano|

    @msisdn                = "5540#{Faker::Number.number(9)}"
    @plano                 = plano 

end



Quando("vier a {string}") do |campanha|

    @campanha              = campanha 

    endpoint               = $api['campanha']
    mount                  = endpoint.gsub("<msisdn>", @msisdn)
    mount                  = mount.gsub("<plano>", @plano)
    mount                  = mount.gsub("<campanha>", @campanha)

    @create = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json'})
    puts (mount)


end



Então(/^okss$/) do

  puts @create.body
  case @create.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...900
      puts "ops #{@create.code}"
  end

  expect(@create.code).to eq 200
  response = JSON.parse(@create.body, object_class: OpenStruct)
  expect(response.success).to eq true

end
