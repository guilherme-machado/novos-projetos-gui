Dado("que eu queria um produto fast") do

  puts "\n\nINICIANDO ASSINATURA DO PRODUTO FAST\n\n"

end

Quando("passar o {string} e o {string} corretos para a contratação de um plano fast") do |plano, codigoServico|

  assina_fast(plano, codigoServico)

  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo(@msisdns)
  expect(@getProduto.success).to eq true

  puts "CONSULTANDO NUMERO DA SORTE"
  luck_number

end

Então("a contratação será realizada com sucesso para os planos") do

  puts "\n\nIMPRIMINDO RESULTADO DO TESTE\n\n"
  puts @post_fast.body
    case @post_fast.code
      when 200
        puts "\n\nRetornou 200, ok\n\n"
      when 404
        puts "\n\nRetornou 404, não existe\n\n"
      when 400
        puts "\n\nRetornou 400, problema de negócio\n\n"
      when 500...600
        puts "\n\nops #{@post_fast.code}\n\n"
    end

  expect(@post_fast.code).to eq 200

  # Descomentar para rodar em produção, em homol não funciona
  # @get_ns = JSON.parse(@numero_sorte.body, object_class: OpenStruct)
  # expect(@get_ns['normal']).to eq 1
  # expect(@get_ns['especial']).to eq 1
  # puts @numero_sorte.body
  
end