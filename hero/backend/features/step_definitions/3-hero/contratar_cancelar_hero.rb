# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Dado("que eu acesse o endpoint ws mo index.php") do

  puts "INICIANDO FLUXO DE ASSINATURA\n\n"

end

Quando("eu passar o body com um msisdn valido e o payload valido") do |table|

  contratar_hero(table)

  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo(@parametros['msisdns'])
  expect(@getProduto.success).to eq true

  # puts "\n\nCONSULTANDO NUMERO DA SORTE\n\n"
  # luck_number
  
end

Então("ok") do

  puts @create_sim_hero.body
  case @create_sim_hero.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@create_sim_hero.code}"
  end

  # @get_ns = JSON.parse(@numero_sorte.body, object_class: OpenStruct)
  # expect(@get_ns['normal']).to eq 1
  # expect(@get_ns['especial']).to eq 1
  # puts @numero_sorte.body

  puts "\n\nASSINADO COM SUCESSO\n\n"
  
end

E("mandar o cancalamento com sucesso") do

  puts "\n\nCANCELANDO\n\n"

  @parametros['text']              = "cancelar"
  hero
  puts ($api['contratar_hero'])
  puts @payload_hero 
    
  @cancela_hero = HTTParty.post($api['contratar_hero'],:headers => {"Content-Type" => 'application/json'}, :body => @payload_hero)

  puts @cancela_hero.body
  case @cancela_hero.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@cancela_hero.code}"
  end

  expect(@cancela_hero.code).to eq 200

  @waitingTimeOut=1
  sleep(1) until !consultar_produto_ativo(@parametros['msisdns'])
  expect(@getProduto.success).to eq false
  puts "\n\nCANCELADO COM SUCESSO\n\n"

end