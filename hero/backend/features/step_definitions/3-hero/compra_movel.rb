# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado("que um cliente contrate um plano movel") do

  puts "INICIANDO FLUXO DE ASSINATURA\n\n"

end

Quando("os dados forem preenchidos corretamente para a contratação de um movel") do

  movel
  @msisdns = msisdn_compra_movel    
  puts ($api['movel_compra'])
  puts @payload_movel
  
  @create_movel = HTTParty.post($api['movel_compra'],:headers => {"Content-Type" => 'application/json'}, :body => @payload_movel)

  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo(@msisdns)
  expect(@getProduto.success).to eq true

  puts "\n\nCONSULTANDO NUMERO DA SORTE\n\n"
  luck_number
  
end

  Então("o pedido precisa ser criado e o produto entrar no banco com a chave de um movel") do
    puts @create_movel.body
    case @create_movel.code
      when 200
        puts "Retornou 200, ok"
      when 404
        puts "Retornou 404, não existe"
      when 400
        puts "Retornou 400, problema de negócio"
      when 500...600
        puts "ops #{@create_movel.code}"
    end

  expect(@create_movel.code).to eq 200
  response = JSON.parse(@create_movel.body, object_class: OpenStruct)
  expect(response.sucesso).to eq true

  @get_ns = JSON.parse(@numero_sorte.body, object_class: OpenStruct)
  expect(@get_ns['normal']).to eq 1
  expect(@get_ns['especial']).to eq 1
  puts @numero_sorte.body

end