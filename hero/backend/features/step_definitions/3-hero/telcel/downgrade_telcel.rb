# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado(/^que eu tenha o plano PREMIUM ativo$/) do
  @msisdn = "52401225#{Faker::Number.number(5)}"

  @createbody = {
  "partner": "telcel",
  "msisdn": @msisdn,
  "la": "5519",
  "text": "PREMIUM",
  "fs_request_id": "",
  "partner_request_id": "",
  "order_id": ""
}.to_json
    
    puts @createbody
    @create = HTTParty.post($api['fake_mo_telcel'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody)
    puts ($api['fake_mo_telcel'])
end

Quando(/^eu solicitar um downgrade do plano PREMIUM para o plano BASICO e as informações estiverem corretas$/) do

  @createbody = {
  "partner": "telcel",
  "msisdn": @msisdn,
  "la": "5519",
  "text": "BASICO",
  "fs_request_id": "",
  "partner_request_id": "",
  "order_id": ""
}.to_json
    
    puts @createbody
    @create = HTTParty.post($api['fake_mo_telcel'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody)
    puts ($api['fake_mo_telcel'])
end

Então(/^o downgrade do plano será realizado com sucesso, cancelando o PREMIUM e ativando o BASICO$/) do
  puts @create.body
  case @create.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@create.code}"
  end

  expect(@create.code).to eq 200
  response = JSON.parse(@create.body, object_class: OpenStruct)
  expect(response.success).to eq true
end

