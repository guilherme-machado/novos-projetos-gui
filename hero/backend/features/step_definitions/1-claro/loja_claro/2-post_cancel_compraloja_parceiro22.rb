# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Quando(/^for passado o nome parceiro diferente de claro$/) do
  
  @createbody = {
  "cancel" => {
    "product" => {
      "name" => produto_hero
    },
    "partner" => {
      "name" => parceiro_hero_22
    },
    "customer" => {
      "msisdn" => msisdn_hero
    },
    "reason" => "Sem Razao",
    "canceler" => "Fulano #{Faker::Name.name} pediu"
  }
}.to_json

    puts @createbody

    @create = HTTParty.post($api['post_cancel_compra'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody)
    puts ($api['post_cancel_compra'])
end

Então(/^o plano não pode ser cancelado porque o unico que pode ser cancelado é o parceiro claro$/) do
    puts @create.body
    case @create.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@create.code}"
    end

  expect(@create.code).to eq 200
  response = JSON.parse(@create.body, object_class: OpenStruct)
  expect(response.success).to eq false
end