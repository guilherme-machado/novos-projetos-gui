# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

E(/^passar o nome do parceiro inexistente "([^"]*)"$/) do |partner|
@namepartner = partner;

end


Quando(/^apenas o nome parceiro for inexistente$/) do

  @createbody = {
  "buy" => {
    "keyword" => {
      "name" => @namekw
    },
    "partner" => {
      "name" => @namepartner
    },
    "customer" => {
      "name" => Faker::Name.name,
      "email" => Faker::Internet.email,
      "msisdn" => @msisdn,
      "cpf" => Faker::CPF.numeric
    }
  }
}.to_json
  
    puts @createbody 
    
    @create = HTTParty.post($api['post_compra'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody)
    puts ($api['post_compra'])
end

Então(/^a contratação não será efetuada porque o nome do parceiro é obrigatório e precisa ser existente.$/) do
  puts @create.body
  case @create.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@create.code}"
  end

  expect(@create.code).to eq 200
  response = JSON.parse(@create.body, object_class: OpenStruct)
  expect(response.success).to eq false
  puts "RETORNO DO TESTE"
  puts response.success
end