# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado(/^que eu queira contratar os planos Hero Básico, Hero Premium, Hero Essencial, Hero Super, Hero Familia, Hero Top, Ensina e Club Criar.$/) do

end

E(/^passar o nome do produto "([^"]*)"$/) do |names|
@namekw = names;

end

E(/^passar o nome do parceiro "([^"]*)"$/) do |partner|
@namepartner = partner;

end

E(/^passar o msisdn$/) do
@msisdn = "554090#{Faker::Number.number(7)}";

end

Quando(/^os dados estiverem corretos$/) do

  @createbody = {
  "buy" => {
    "keyword" => {
      "name" => @namekw
    },
    "partner" => {
      "name" => @namepartner
    },
    "customer" => {
      "name" => Faker::Name.name,
      "email" => Faker::Internet.email,
      "msisdn" => @msisdn,
      "cpf" => Faker::CPF.numeric
    }
  }
}.to_json
  
    puts @createbody 
    
    @create = HTTParty.post($api['post_compra'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody)
    puts ($api['post_compra'])

end

Então(/^o post de solicitação de compra sera feito$/) do
  puts @create.body
  case @create.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@create.code}"
  end

  expect(@create.code).to eq 200
  response = JSON.parse(@create.body, object_class: OpenStruct)
  expect(response.success).to eq true
  puts "RETORNO DO TESTE"
  puts response.success
end