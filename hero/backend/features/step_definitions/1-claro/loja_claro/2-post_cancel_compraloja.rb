# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado(/^que seja solicitado o cancelamento de um plano$/) do

end

Quando(/^os dados dos planos forem passados corretamente com nome do parceiro claro e produto dentre 199, 251, 83, 89, 95, 101$/) do
  
  parceiro_produto_msisdn_hero.each  do |row|
    @createbody = {
      "cancel" => {
        "product" => {
          "name" => row['nome_produto_comercial']
        },
        "partner" => {
          "name" => row['nome']
        },
        "customer" => {
          "msisdn" => row['msisdn']
        },
        "reason" => "Sem Razao",
        "canceler" => "Fulano #{Faker::Name.name} pediu"
      }
    }.to_json

    puts @createbody

    @create = HTTParty.post($api['post_cancel_compra'],:headers => {"Content-Type" => 'application/json'}, :body => @createbody)
    puts ($api['post_cancel_compra'])
  end
end

Então(/^o último pedido que entrou na base que atenda os requisitos acima será cancelado corretamente$/) do
    puts @create.body
    case @create.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@create.code}"
    end

  expect(@create.code).to eq 200
  response = JSON.parse(@create.body, object_class: OpenStruct)
  expect(response.success).to eq true
end