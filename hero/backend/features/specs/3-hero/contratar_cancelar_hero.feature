#language: pt
@contratar_hero @back @extensao_promo_backend
Funcionalidade: Criação via fake mo

Esquema do Cenario: Criação via fake mo

 	Dado que eu acesse o endpoint ws mo index.php
  	Quando eu passar o body com um msisdn valido e o payload valido
  	| msisdn                   | <msisdns>	    		|
  	| la	                   | <la>		    		|
  	| text	                   | <text>		    		|
  	| partner                  | <partner>	    		|
  	| debug                    | <debug>	    		|
  	| transaction_id           | <transaction_id>		|
  	| auth_code                | <auth_code>	    	|
  	| auth_code_status         | <auth_code_status>	    |
  	| transaction_id_auth      | <transaction_id_auth>	|
	Então ok
	Entao mandar o cancalamento com sucesso
	Exemplos:
	|msisdn       	 |la           |text    					 |partner	 |debug		|transaction_id		|auth_code		|auth_code_status	|transaction_id_auth|
    # |      			 |5513         |quero         			  	 |claro      |true		|					|				|true    			|					|
    # |      			 |5513         |basico       				 |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |premium        				 |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |familia      				 |claro  	 |true		|					|				|true				|					|
    |      			 |5513         |es             				 |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |top          				 |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |HEROAVANCADOLOJA			 |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |super          				 |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |HEROINTERMEDIARIOLOJA        |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |HEROPCTADICIONAL150GBLOJA    |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |HEROPCTADICIONAL500GBLOJA    |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |ESSENCIALLOJASCLARO          |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |BASICOLOJASCLARO             |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |PREMIUMLOJASCLARO            |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |AJUDALOJASCLARO              |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |AJUDAHERP	                 |claro  	 |true		|					|				|true				|					|
    # |      			 |5513         |HEROBASICOLOJA	             |claro  	 |true		|					|				|true				|					|