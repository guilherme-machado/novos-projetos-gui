#language: pt
@compra_movel @back @extensao_promo
Funcionalidade: Contratação Movel

Cenario: Contratação de um plano Movel

 	Dado que um cliente contrate um plano movel
  Quando os dados forem preenchidos corretamente para a contratação de um movel
	Então o pedido precisa ser criado e o produto entrar no banco com a chave de um movel