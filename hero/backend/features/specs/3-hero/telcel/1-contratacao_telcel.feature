#language: pt
@contratacao_telcel @back @telcel @extensao_promo
Funcionalidade: Criação via fake mo telccel

Esquema do Cenario: Criação via fake mo telcel

 	Dado que eu acesse o endpoint /hero/api/v1/sms/mo
  	Quando eu passar o nome do <plano> e as informações corretas para a contratação de um plano
	Então a contratação será realizada com sucesso para os planos BASICO, ESENCIAL, FAMILIA, PREMIUM

	Exemplos:
    |plano		|
    |"BASICO"	|
    |"ESENCIAL"	|
    |"FAMILIA"	|
    |"PREMIUM"	|