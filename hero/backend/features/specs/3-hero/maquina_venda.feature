#language: pt
@maquina_venda @extensao_promo
Funcionalidade: Maquina Vendas

Esquema do Cenario: Maquina Vendas

 	Dado que eu queria um produto da maquina de vendas
  	Quando passar o <code> do plano correto para a contratação de um plano da maquina de vendas
	Então a contratação será realizada com sucesso para os planos solicitados da maquina de vendas

	Exemplos:
    |itens			    |code		|
    |"BASICO"   	  |297315	|
    |"ESPECIAL"		  |302076	|
    |"PLUS"			    |302075	|
    |"BASICO PROMO" |305857	|
