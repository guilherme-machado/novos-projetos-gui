#language: pt
@contratacao_campanha
Funcionalidade: Campanha

Esquema do Cenario: Campanha

 	Dado que tenha um msisdn para cada <plano>
  	Quando vier a <campanha>
	Então okss

	Exemplos:
    |plano		   |campanha								   |
    |"ESSENCIAL"   |"novitech_bcef1d40b27611e68b160aba1582df3d"|
    |"HERO PREMIUM"|"novitech_16d1b2abac3a4abee6635968fb0c1c41"|
    |"BASICO"	   |"novitech_2e3e511baabc3364c3049e7a08218d10"|