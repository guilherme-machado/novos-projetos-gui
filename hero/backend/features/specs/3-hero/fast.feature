#language: pt
@contratacao_fast @extensao_promo_backend
Funcionalidade: Fast

Esquema do Cenario: Fast

 	Dado que eu queria um produto fast
  	Quando passar o <plano> e o <codigoServico> corretos para a contratação de um plano fast
	Então a contratação será realizada com sucesso para os planos

	Exemplos:
    |itens		|plano		|codigoServico	|
    |"BASICO"   |"BASICO"	|"GYBASICO92N"	|
    |"ESPECIAL"	|"ESPECIAL"	|"GYESPECIAL92"	|
    |"PLUS"		|"PLUS"		|"GYPLUS92"		|
    |"MASTER"   |"MASTER"	|"GYMASTER92"	|
    |"HERO"		|"HERO"		|"GYMASTER"		|