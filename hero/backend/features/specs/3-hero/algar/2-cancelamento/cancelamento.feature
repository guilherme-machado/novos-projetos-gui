#language: pt
@cancelamento_algar @back @algar
Funcionalidade: Cancelamento Algar

Cenario: Cancelamento de plano Algar

  Dado que eu acesse o endpoint /algar/algar-api.php
  E tenha uma assinatura ativa
  Quando eu passar o body com a intensão de cancelamento
  Então o cancelamento será realizada com sucesso