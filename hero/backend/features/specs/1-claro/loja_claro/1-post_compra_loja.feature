#language: pt
@post_compra @back @lojaclaro
Funcionalidade: (Interface de Lojas Claro) (HERO) Fazer a criação de API de venda

Esquema do Cenario: (Interface de Lojas Claro) Fazer a criação de API de venda de todos os planos

 	Dado que eu queira contratar os planos Hero Básico, Hero Premium, Hero Essencial, Hero Super, Hero Familia, Hero Top, Ensina e Club Criar.
  	E passar o nome do produto <names>
	E passar o nome do parceiro <partner>
	E passar o msisdn
	Quando os dados estiverem corretos
	Entao o post de solicitação de compra sera feito

    Exemplos:
    |names 		|partner|
	|"BASICO"	|"claro"|
    |"PREMIUM"	|"claro"|
    |"ESSENCIAL"|"claro"|
    |"SUPER"	|"claro"|
    |"FAMILIA"	|"claro"|
    |"TOP"		|"claro"|

Esquema do Cenario: (Interface de Lojas Claro) Fazer a criação de API de venda com o nome do parceiro inexistente

	Dado que eu queira contratar os planos Hero Básico, Hero Premium, Hero Essencial, Hero Super, Hero Familia, Hero Top, Ensina e Club Criar.
	E passar o nome do produto <names>
	E passar o nome do parceiro inexistente <partner>
	E passar o msisdn
	Quando apenas o nome parceiro for inexistente
	Entao a contratação não será efetuada porque o nome do parceiro é obrigatório e precisa ser existente.

	Exemplos:
    |names 		|partner|
    |"BASICO"	|"vivo"	|