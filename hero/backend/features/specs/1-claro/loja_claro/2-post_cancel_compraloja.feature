#language: pt
@post_cancelamento_loja @back @lojaclaro
Funcionalidade: (Interface de Lojas Claro) Fazer a criação de API de Cancelamento

Cenario: (Interface de Lojas Claro) Fazer a criação de API de Cancelamento


 	Dado que seja solicitado o cancelamento de um plano
  Quando os dados dos planos forem passados corretamente com nome do parceiro claro e produto dentre 199, 251, 83, 89, 95, 101
	Então o último pedido que entrou na base que atenda os requisitos acima será cancelado corretamente


Cenario: (Interface de Lojas Claro) Fazer a criação de API de Cancelamento com nome parceiro diferente de claro


 	Dado que seja solicitado o cancelamento de um plano
  Quando for passado o nome parceiro diferente de claro
	Então o plano não pode ser cancelado porque o unico que pode ser cancelado é o parceiro claro


Cenario: (Interface de Lojas Claro) Fazer a criação de API de Cancelamento com nome produto diferente de 199, 251, 83, 89, 95, 101


 	Dado que seja solicitado o cancelamento de um plano
  Quando for passado o nome do produto diferente dos que são aceitos 199, 251, 83, 89, 95, 101
	Então o plano não pode ser cancelado porque o produto não está no range dos permitidos