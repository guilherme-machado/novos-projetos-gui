#language: pt
@post_cancelamento_claro @back @mse
Funcionalidade: Integração MSE - [global] Criar api de recebimento de cancelamentos da claro para o backend claro

Cenario: Integração MSE - [global] Criar api de recebimento de cancelamentos da claro para o backend claro conforme documentação. A URL da claro recebera um post json com os campos descritos na documentação.


 	Dado que a claro envie um cancelamento para o backend da claro
  Quando o backend da claro receber os dados do msisdn
	Então precisa realizar o cancelamento do cliente claro