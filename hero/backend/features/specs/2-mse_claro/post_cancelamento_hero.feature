#language: pt
@post_cancelamento_hero @back @mse
Funcionalidade: Integração MSE - [global] Criar api de recebimento de cancelamentos da claro para o backend hero

Cenario: Integração MSE - [global] Criar api de recebimento de cancelamentos da claro para o backend hero conforme documentação. A URL do hero recebera um post json com os campos descritos na documentação.


 	Dado que a claro envie um cancelamento para o backend do hero
  Quando o hero receber os dados do msisdn
	Então precisa realizar o cancelamento do cliente hero