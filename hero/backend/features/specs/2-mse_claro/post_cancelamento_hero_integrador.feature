#language: pt
@post_cancelamento_hero_claro_integrador @back @mse
Funcionalidade: Integração MSE - [global] Criar api de de integração para cancelamentos da claro e do hero

Cenario: Integração MSE - [global] Criar api de de integração para a claro e para o hero conforme documentação. A URL envia os dados ou para o hero ou para a claro.


 	Dado que eu chame o endpoint de integração.
  Quando for um cancelamento da claro cancelar claro, quando for hero cancelar hero.
	Então cancelamento ok.