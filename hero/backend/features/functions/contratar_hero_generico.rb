def contratar_hero(table)
  @parametros                       = table.rows_hash
  @parametros['msisdns']            = "5540#{Faker::Number.number(9)}"
  @parametros['transaction_id']     = Faker::Number.number(21)
  @parametros['auth_code']          = Faker::Number.number(4)
  @parametros['transaction_id_auth']= Faker::Number.number(15)

  puts "\n\nPRIMEIRO OPTIM COM KEYWORD DE CONTRATAÇÃO\n\n"

  hero
  puts ($api['contratar_hero'])
  puts @payload_hero 
    
  @create_hero = HTTParty.post($api['contratar_hero'],:headers => {"Content-Type" => 'application/json'}, :body => @payload_hero)

  puts @create_hero.body
  expect(@create_hero.code).to eq 200

  sleep 3
  contratar_hero_confirme

end

def contratar_hero_confirme()
  puts "\n\nSEGUNDO OPTIM COM KEYWORD SIM\n\n"

  @parametros['text']              = "sim"
  hero
  puts ($api['contratar_hero'])
  puts @payload_hero 
    
  @create_sim_hero = HTTParty.post($api['contratar_hero'],:headers => {"Content-Type" => 'application/json'}, :body => @payload_hero)

  puts @create_sim_hero.body
  expect(@create_sim_hero.code).to eq 200

end