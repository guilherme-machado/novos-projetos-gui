def consultar_produto_ativo(msisdn)

	endpoint               = $api['get_verificaCliente_produtoHero_porMsisdn']
  mount                  = endpoint.gsub("<msisdn>", @msisdns ? @msisdns : @parametros['msisdns'])

  @consulta_produto = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json'})
  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN CONTRATADO\n\n"
  puts (mount)

  puts "\n\nIMPRIMINDO RESULTADO DA CONSULTA\n\n"
 	puts @consulta_produto.body
  	case @consulta_produto.code
    		when 200
      	puts "Retornou 200, ok"
    		when 404
      	puts "Retornou 404, não existe"
    		when 400
      	puts "Retornou 400, problema de negócio"
    		when 500...600
      	puts "ops #{@consulta_produto.code}"
  	end

  @getProduto = JSON.parse(@consulta_produto.body, object_class: OpenStruct)

  if @waitingTimeOut > 7
    return true
  end

  @waitingTimeOut+=1
  @getProduto.success

end