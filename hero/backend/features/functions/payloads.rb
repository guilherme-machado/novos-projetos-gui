def fnac()
  
  @payload_fnac               = {
    "name"                    => Faker::GameOfThrones.character,
    "email"                   => Faker::Internet.email,
    "personalId"              => Faker::CPF.cpf,
    "installationPhone"       => @msisdns,
    "heroPlan"                => "basico",
    "purchaseDate"            => "20170717 11:33:00",
    "invoiceId"               => 5945,
    "purchaseId"              => "000#{Faker::Number.number(9)}",
    "addressType"             => "Principal",
    "address"                 => Faker::Address.street_name,
    "addressNumber"           => Faker::Number.number(4),
    "address2"                => "CASA A",
    "neighborhood"            => "RONDINHA",
    "zipCode"                 => "83607310",
    "city"                    => "CAMPO LARGO",
    "state"                   => "PR",
    "storeId"                 => "7",
    "sellerId"                => "GUSTAVOGS"
  }.to_json

end

def fixa()

  @payload_fixa               = {
    "data"                    => Faker::Time.between(DateTime.now - 1, DateTime.now),
    "numero"                  => Faker::Number.number(10),
    "canal"                   => Faker::DragonBall.character,
    "agente"                  => Faker::GameOfThrones.character,
    "vendedor"                => Faker::Beer.name,
    "telefone"                => Faker::PhoneNumber.phone_number,
    "celular"                 => @msisdns,
    "cpf_cnpj"                => Faker::CPF.numeric,
    "nome"                    => "TESTE TRES ALGAR HERO",
    "email"                   => Faker::Internet.email,
    "produto"                 => "HERO",
    "pacote"                  => "SIM",
    "endereco"                => Faker::Address.street_name,
    "status"                  => "I",
    "dsname"                  => "TEST_PROD_11JAN2017"
  }.to_json

end

def degustacao()

  @createbody_degustacao       = {
    "msisdn"                   => @msisdns
  }

end

def movel()
  
  @payload_movel                = {
    "msisdn"                    => msisdn_compra_movel,
    "productid"                 => "3789"
  }.to_json

end

def hero()

  @payload_hero                 = {
    "msisdn"                    => @parametros['msisdns'],
    "la"                        => @parametros['la'],
    "text"                      => @parametros['text'],
    "partner"                   => @parametros['partner'],
    "debug"                     => @parametros['debug'],
    "transaction_id"            => @parametros['transaction_id'],
    "auth_code"                 => @parametros['auth_code'],
    "auth_code_status"          => @parametros['auth_code_status'],
    "transaction_id_auth"       => @parametros['transaction_id_auth']
  }.to_json

end

def fast(plano, codigoServico)

  @payload_fast               = {
    "file"                    => "fs01092016_151515.env",
    "identificador"           => "00",
    "numeroNSU"               => "90000000000000000001",
    "nomeCliente"             => Faker::Name.name,
    "plano"                   => plano,
    "endereco"                => Faker::Address.street_name,
    "uf"                      => Faker::Address.state_abbr,
    "cidade"                  => Faker::Address.city,
    "cep"                     => "98550000",
    "telefone"                => @msisdns,
    "dataInicioServico"       => Date.today.to_s.gsub("-", ""),
    "dataTerminoServico"      => (Date.today + 180).to_s.gsub("-", ""),
    "status"                  => "I",
    "cpfCnpj"                 => Faker::CPF.numeric,
    "email"                   => Faker::Internet.email,
    "celular"                 => @msisdns,
    "marca"                   => "",
    "modelo"                  => "DEI145448C25B",
    "serieProduto"            => "",
    "serieNF"                 => "UA",
    "dataCompra"              => Date.today.to_s.gsub("-", ""),
    "numeroNF"                => "477357",
    "codigoCliente"           => "Z285075",
    "tabelaVenda"             => "92",
    "codigoServico"           =>  codigoServico,
    "certificadoServico"      => "GY94657489",
    "filialVenda"             => "92",
    "codigoVendedor"          => "V000986",
    "linhaProduto"            => "US - BANHO",
    "creationDate"            => Date.today.to_s.gsub("-", "")
  }.to_json

end

def maquina_vendas(code)
    @radon = (code).to_s+(Faker::Number.number(12)).to_s

  @payload_maquina_vendas   = {
    "numeroPedido"          => 5945,
    "tipoVenda"             => "CARTAO DE CREDITO",
    "codigoLoja"            => 9032,
    "numeroEmpresa"         => 1,
    "nomeLoja"              => "F9032  CARPINA I  PE",
    "cidadeLoja"            => "CARPINA",
    "ufLoja"                => "PE",
    "regionalLoja"          => "R24 PARAIBA E PE",
    "codigoVendedor"        => "39009",
    "nomeVendedor"          => "ERINALDO DOS SANTOS JUNIOR",
    "numeroServico"         => 209514,
    "descricaoServico"      => "SERVICO ANTIVIRUS MOBILE NORTON",
    "quantidade"            => 1,
    "valor"                 => 29.9,
    "dataVenda"             => Date.today.to_s.gsub("-", "/"),
    "nomeCliente"           => Faker::Name.name,
    "cpf"                   => Faker::CPF.numeric,
    "email"                 => Faker::Internet.email,
    "tipo"                  => "R",
    "logradouro"            => Faker::Address.street_name,
    "numero"                => Faker::Number.number(12),
    "bairro"                => "SP",
    "cidade"                => Faker::Address.city,
    "estado"                => Faker::Address.state_abbr,
    "cep"                   => "98550000",
    "celular"               => @msisdns,
    "chaveDownload"         => @radon,
    "status"                => "I"
  }.to_json

end

def algar(parametro)

  @createbody_algar         = {
    "assetNumber"           => Faker::Number.number(6).to_i,
    "saleDate"              => Time.now.strftime("%Y-%m-%d"),
    "saleStatus"            => "ABERTO",
    "productName"           => "Hero Basico",
    "planSVA"               => "Hero Basico",
    "priceSVA"              => 4.99,
    "cellPhoneNumber"       => @msisdns,
    "nameClient"            => Faker::GameOfThrones.character,
    "emailClient"           => Faker::Internet.email,
    "cpfCnpjClient"         => Faker::CPF.numeric,
    "addressClient"         => Faker::Address.street_name,
    "operationType"         => parametro
  }.to_json

end
