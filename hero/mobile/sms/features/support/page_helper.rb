#encoding: utf-8

#O comando abaixo estou dando um require neste arquivo em todos os arquivos que termina em ".rb".

Dir[File.join(File.dirname(__FILE__),'../functions/*.rb')].each { |file| require file }

# Modulo para verificar se as classes foram instanciadas.
module Screens
    def base
        @base ||= BaseAppium.new
    end

    # def home
    #     @page ||= PageElements.new
    # end

    def app
        @app ||= AppFunctions.new
    end

    def db
        @db ||= Database.new
    end
end