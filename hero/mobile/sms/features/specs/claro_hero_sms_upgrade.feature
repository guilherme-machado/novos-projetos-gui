# #language:pt

# Funcionalidade: Upgrade de planos

# Esquema do Cenário: Upgrade basico para essencial 
# Dado que eu como QA possua o plano "BASICO" então eu farei o necessário para realizar o Upgrade
# Quando eu tiver com os dados como : <planos><msisdn><la><kw><duplo_optin><fraseologia1><fraseologia2> e <fraseologia3>
# Então o upgrade deve ser realizado com sucesso, e eu farei as validações necessárias.

#  Exemplos: 

# |   planos          |   msisdn         |   la  |   kw           |duplo_optin |fraseologia1|fraseologia2|fraseologia3|
# |"Hero Essencial"   |   "11932269929"  |"5513" |  "up"          |   "sim"    |""          |""          |""          |


# Esquema do Cenário: Upgrade essencial para premium 
# Dado que eu como QA possua o plano "ESSENCIAL" então eu farei o necessário para realizar o Upgrade
# Quando eu tiver com os dados como : <planos><msisdn><la><kw><duplo_optin><fraseologia1><fraseologia2> e <fraseologia3>
# Então o upgrade deve ser realizado com sucesso, e eu farei as validações necessárias.

#  Exemplos: 

# |   planos          |   msisdn         |   la  |   kw           |duplo_optin |fraseologia1|fraseologia2|fraseologia3|
# |"Hero premium"     |   "11932269929"  |"5513" |  "up"          |   "sim"    |""          |""          |""          |
