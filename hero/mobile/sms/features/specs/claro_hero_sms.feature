#language:pt

@claro_sms
Funcionalidade: Compras
Esse teste é baseado na compra via sms, ultilizando o Appium para fazer as ações de sms.
Assim ultilizando todas as ações disponiveis para os fluxos de compra e cancelamento dos produtos.

@compra_hero_Basico
Esquema do Cenário: Comprar plano Hero Básico

Dado que eu como Q.A tenho que assinar os planos factiveis pertencentes a operadora Claro.
Quando eu possuo os dados como <planos>, <msisdn>, <la> e <kw> e <duplo_optin> <fraseologia1> <fraseologia2> <fraseologia3> para o la 5513
Então eu faço a assinatura dos mesmos e verifico de está tudo ok com seu funcionamento e aprovisionamento.

Exemplos: 

|   planos          |   msisdn         |   la  |   kw             |duplo_optin |fraseologia1                                                                                                               |fraseologia2                                                                                                                              | fraseologia3                                                                                                                                                   |
| "HERO BASICO"     |   "11932269929"  |"5513" |  "HERO199GEM1"   |   ""       |"Voce contratou o servico Hero BASICO. Valor: RS 2.99/semana. Para cancelar responda esta mensagem com a palavra SAIR."    |"Voce comprou o BASICO esta concorrendo a 25MIL REAIS na Promocao Manda Premios" |"Baixe agora o HERO em"|                                               

@banco_basico
Esquema do Cenário: validação no banco Hero Basico

Dado que eu Q.A fiz todo o cenario de compra acima.
Quando eu tiver em mãos a contatação dos plano <hero_basico> 
Então eu farei as validações necessárias para os planos estejam devidamente contatados.

Exemplos:

|         hero_basico           |
|"Protect - 3"                  |
|"Cloud - 10GB"                 |
|"Filho Protegido - 1"          |
|"WiFi Seguro - 1"              |
|"SOS Dispositivos - Sem Visita"|



@compra_hero_essencial
Esquema do Cenário: Comprar plano Hero Essencial

Dado que eu como Q.A tenho que assinar o plano ESSENCIAL 
Quando eu possuo os dados como <planos>, <msisdn>, <la> e <kw> e <duplo_optin> <fraseologia1> <fraseologia2> <fraseologia3> para o la 5513.
Então eu faço a assinatura dos mesmos e verifico de está tudo ok com o aprovisionamento do plano "ESSENCIAL".

Exemplos: 

|   planos       |msisdn         |   la  | kw |duplo_optin |fraseologia1                                                                                                               |fraseologia2                                                                                                                | fraseologia3                                                                                                                                                   |
|"ESSENCIAL"     |"11932269929"  |"5513" |"ES"|   "SIM"    |"Para confirmar a contratacao do servico Seguranca HERO por R$ 6,99 por mes com tarifacao automatica, responda com SIM."   |"Voce contratou o servico Hero ESSENCIAL. Valor: RS 6.99/mes. Para cancelar responda esta mensagem com a palavra SAIR." |"Parabens! Voce comprou o ESSENCIAL esta concorrendo a 25MIL REAIS na Promocao Manda Premios. Para mais infos acesse: www.promocaomandapremios.com.br"|

@banco_essencial

Esquema do Cenário: validação no banco Hero Essencial

Dado que eu Q.A fiz todo o cenario de compra do plano ESSENCIAL
Quando eu tiver em mãos a contatação do plano <hero_essencial> 
Então eu farei as validações necessárias para que o Plano "ESSENCIAL" esteja contratado com os planos corretos no banco

Exemplos:

|       hero_essencial          |
|"Protect - 3"                  |
|"Cloud - 10GB"                 |
|"WiFi Seguro - 3"              |
|"SOS Dispositivos - Sem Visita"|


@compra_hero_premium
Esquema do Cenário: Comprar plano Hero Premium

Dado que eu como Q.A tenho que assinar o plano Hero Premium
Quando eu possuo os dados como <planos>, <msisdn>, <la> e <kw> e <duplo_optin> <fraseologia1> <fraseologia2> <fraseologia3>
Então eu faço a assinatura dos do plano "PREMIUM"

Exemplos: 

|   planos     |   msisdn         |   la  |   kw             |duplo_optin |fraseologia1                                                                                                            |fraseologia2                                                                                                                                                  | fraseologia3                                                                                                                                                   |
|"PREMIUM"     |   "11932269929"  |"5513" |  "HERO299GEMP"   |   ""       |"Voce contratou o servico HERO PREMIUM. Valor: RS 3.99/semana. Para cancelar responda esta mensagem com a palavra SAIR."|"Bem vindo ao HERO PREMIUM! Voce ja pode navegar livre de ameacas. Acesse: http://su.fsvas.com/L7n8T8 e instale seus apps. Se precisar use a senha: hero5511" |"Baixe agora o HERO em http://su.fsvas.com/L7n8T8, instale seus aplicativos para proteger o celular, e navegue livre de ameacas. Duvidas? Envie AJUDA para 5513,"|

@valida_banco_premium
Esquema do Cenário: validação no banco Hero Premiun

Dado que eu Q.A fiz o cenário de compra do produto Hero Premium
Quando eu tiver em mãos a contatação dos planos <hero_premium> 
Então eu farei as validações em banco necessárias para que o Hero "HERO PREMIUM" seja contratado com sucesso.

Exemplos:

|           hero_premium        |
|"Protect - 3"                  |
|"Cloud - 30GB"                 |
|"Filho Protegido - 3"          |
|"WiFi Seguro - 3"              |
|"SOS Dispositivos - Sem Visita"|



