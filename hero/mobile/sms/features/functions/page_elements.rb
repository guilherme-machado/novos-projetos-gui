#encoding: utf-8

class PageElements
    def message_elements
        {
            #home_screen
            app_package: "com.google.android.apps.messaging",
            new_conversation: "#{DATA['message_id']}fslogin_hero_security_spinner_text_country_code",
            conversation: "#{DATA['message_id']}conversation_snippet",
            search: "#{DATA['message_id']}action_zero_state_search",
            options: "Mais opções",
            options_menu: "#{DATA['message_id']}title",
            delete_button: "android:id/button1",
            switch_widget: "android:id/switchWidget"
        }
    end
end