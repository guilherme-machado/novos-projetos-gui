class AppFunctions
    def initialize
        @base = BaseAppium.new
        @page = PageElements.new
        @db = Database.new
    end

    def subscribe(planos, la, kw, duplo_optin, fraseologia1, fraseologia2, fraseologia3)
        cancel_conversation
        @base.click_text('Iniciar bate-papo')
        unsubscribe(la)
        @base.wait_text('Cancelamento' ||'Voce nao e assinante')
        $driver.press_keycode(66)
        @base.send_keys_id('compose_message_text', "#{kw}")
        @base.click_id('send_message_button_container')
        @base.send_keys_id('compose_message_text', "#{duplo_optin}")
        @base.click_id('send_message_button_container')
        @base.find_text("#{planos}")
        $driver.back
    end 


    def unsubscribe(la)
        @base.send_keys_id('recipient_text_view' , "#{la}")
        $driver.press_keycode(66)
        @base.send_keys_id('compose_message_text', "SAIR")
        @base.click_id('send_message_button_container')
    end

    def cancel_conversation
        @base.wait_id(@page.message_elements[:conversation])
        while @base.exists_id(@page.message_elements[:conversation])
            @base.click_id(@page.message_elements[:conversation])
            sleep 3
            @base.click_desc("Mais opções")
            exclude = @base.find_elements_id(@page.message_elements[:options_menu])
            for i in (0..exclude.size-1)
                if exclude[i].attribute("text") == "Excluir"
                    exclude[i].click
                    @base.click_id(@page.message_elements[:delete_button])
                    sleep 2
                    break
                end
            end
        end
     end


    def sms_validate(sms, kw, fraseologia1, fraseologia2, fraseologia3 )
        for i in 0..sms.size-1
            if sms[i] == kw
              next
            end
        
            if sms[i].include? 'protocolo'
              next
            end
        
            if sms[i] == ("#{fraseologia1}" or "#{fraseologia2}" or "#{fraseologia3}" ) 
              @validate = true
            elsif
              @validate = false
            end
            puts "SMS: #{sms[i]}"
            puts "VALIDATE: #{@validate}"
        end 
    
    end 



end





 # for i in 0..sms.size-1 
    #     if sms[i] == fraseologia1 
    #     puts true
    #     else 
    #     puts false
    #     end
#  end