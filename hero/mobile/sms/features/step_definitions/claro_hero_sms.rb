#encoding:utf-8


#Compra Plano Básico -------------------------------------------------------------------------------------------------------------------------------------------
Dado("que eu como Q.A tenho que assinar os planos factiveis pertencentes a operadora Claro.") do
  # @app = AppFunctions.new
  # @base = BaseAppium.new
end
  
Quando("eu possuo os dados como {string}, {string}, {string} e {string} e {string} {string} {string} {string} para o la {int}")  do |planos, msisdn, la, kw, duplo_optin, fraseologia1, fraseologia2, fraseologia3, int|
  @planos = planos
  @app.subscribe(planos, la, kw, duplo_optin, fraseologia1,fraseologia2, fraseologia3)
  sms = @base.return_strings_id('message_text')
  @app.sms_validate(sms, kw, fraseologia1, fraseologia2, fraseologia3)
end
 
Então("eu faço a assinatura dos mesmos e verifico de está tudo ok com seu funcionamento e aprovisionamento.") do
 puts Compra: "#{@planos}"
end

#Validação em banco --------------------------------------------------------------------------------------------------------------------------------------------

Dado("que eu Q.A fiz todo o cenario de compra acima.") do
  # @base = BaseAppium.new
  # # @page = PageElements.new
  # @app = AppFunctions.new
  # @db = Database.new
end

Quando("eu tiver em mãos a contatação dos plano {string}") do |basico|
  @vp = @db.product_search(DATA['msisdn'])
  for i in 0..@vp.size-1
    @s = @vp.to_s
      expect(@s).to include basico
  end
    @basico = basico
end

Então("eu farei as validações necessárias para os planos estejam devidamente contatados.") do
  puts  "Nome do produto no banco: #{@vp[0]['product_name']}"
  puts @produto
  expect(@vp[0]['product_name']).to include "BASICO"
end

#compra essencial ----------------------------------------------------------------------------------------------------------------------------------------------

Dado("que eu como Q.A tenho que assinar o plano ESSENCIAL") do
  # @app = Claro.new
  # @base = BasePage.new
end

Quando("eu possuo os dados como {string}, {string}, {string} e {string} e {string} {string} {string} {string} para o la {int}.") do |planos, msisdn, la, kw, duplo_optin, fraseologia1, fraseologia2, fraseologia3, int|
  @planos = planos
  @app.subscribe(planos, la, kw, duplo_optin, fraseologia1,fraseologia2, fraseologia3)
  sms = @base.return_strings_id('message_text')
  @app.sms_validate(sms, kw, fraseologia1, fraseologia2, fraseologia3)
end

Então("eu faço a assinatura dos mesmos e verifico de está tudo ok com o aprovisionamento do plano {string}.") do |meu_plano|
  puts Compra: "#{@planos}"
end


#Validação no banco plano Essencial------------------------------------------------------------------------------------------------------------------------------
 
Dado("que eu Q.A fiz todo o cenario de compra do plano ESSENCIAL") do
  @db = Database.new
end

Quando("eu tiver em mãos a contatação do plano {string}") do |planos_contratados|
  @vp = @db.product_search(DATA['msisdn'])
    for i in 0..@vp.size-1
      @s = @vp.to_s
      expect(@s).to include planos_contratados
    end

end

Então("eu farei as validações necessárias para que o Plano {string} esteja contratado com os planos corretos no banco") do |essencial|
  puts  "Nome do produto no banco: #{@vp[0]['product_name']}"
  puts @produto
  expect(@vp[0]['product_name']).to include essencial
end


#Compra Hero Premium---------------------------------------------------------------------------------------------------------------------------------------------

Dado("que eu como Q.A tenho que assinar o plano Hero Premium") do
  # @app = Claro.new
  # @base = BasePage.new
end

Quando("eu possuo os dados como {string}, {string}, {string} e {string} e {string} {string} {string} {string}") do |planos, msisdn, la, kw, duplo_optin, fraseologia1, fraseologia2, fraseologia3|
  @planos = planos
  @app.subscribe(planos, la, kw, duplo_optin, fraseologia1,fraseologia2, fraseologia3)
  sms = @base.return_strings_id('message_text')
  @app.sms_validate(sms, kw, fraseologia1, fraseologia2, fraseologia3)
end

Então("eu faço a assinatura dos do plano {string}") do |string|
puts Compra: "#{@planos}"
end

#Validação de banco Hero Premium----------------------------------------------------------------------------------------------------------------------------------

Dado("que eu Q.A fiz o cenário de compra do produto Hero Premium") do
  # @db = Database.new
end

Quando("eu tiver em mãos a contatação dos planos {string}") do |premium|
  @vp = @db.product_search(DATA['msisdn'])
  for i in 0..@vp.size-1
    @s = @vp.to_s
      expect(@s).to include premium
  end
end

Então("eu farei as validações em banco necessárias para que o Hero {string} seja contratado com sucesso.") do |premium|
  puts  "Nome do produto no banco: #{@vp[0]['product_name']}"
  puts @produto
  expect(@vp[0]['product_name']).to include premium
end
