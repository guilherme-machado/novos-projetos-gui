#1feature
#contexto
Dado("que o usuario efetue o login") do
  @login_page.logar
end

#1scenario
Quando("o usuario estiver na Home do Hero") do
  @home_page.clicar_popup
end

Então("valido os menus do app") do
  @home_page.validacao_app
end

#2scenario
Então("deve ser apresentadas as informações corretas") do
  @home_page.validar_campos1
  swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)
  @home_page.validar_campos2
end

#3scenario
Quando("o usuario tocar no Otimizador no menu inferior") do
  @home_page.clicar_popup
  @base_page.touch_text('Otimizador')
end

Então("deve ser direcionado para a tela do Otimizador") do
  otimizador = @base_page.exists("#{DATA['url']}/lyt_percent_text")
  expect(otimizador).to eql true
end

#4scenario
Quando("o usuario tocar no Cloud no menu inferior") do
  @home_page.clicar_popup
  @base_page.touch_text('Cloud')
end

Então("deve ser direcionado para a tela do Cloud by Hero") do
  cloud = @base_page.exists_text('CLOUD BY HERO')
  expect(cloud).to eql true
end

#5scenario
Quando("o usuario tocar no Família no menu inferior") do
  @home_page.clicar_popup
  @base_page.touch_text('Família')
end

Então("deve ser direcionado para a tela do Família by Hero") do
  familia = @base_page.exists_text('FAMÍLIA BY HERO')
  expect(familia).to eql true
end

#6scenario
Quando("o usuario tocar no Help Desk") do
  @home_page.clicar_popup
  @base_page.touch_text('Help Desk')
end

Então("deve ser direcionado para a tela Help Desk by Hero") do
  hp = @base_page.exists_text('HELP DESK BY HERO')
  expect(hp).to eql true
end

#7scenario
Quando("o usuario tocar na engrenagem") do
  @home_page.clicar_popup
  @base_page.touch_button("#{DATA['url']}/img_settings")
end

Então("deve ser apresentado a lista de menus") do
  config = @base_page.exists_text('Configurações')
  expect(config).to eql true
end