#3feature 
#1scenario
Quando("ele tocar no escudo") do
  @home_page.clicar_popup
  @home_page.clicar_escudo
end

Então("a verificação deve ser feita") do
  @base_page.wait_text('Procurando ameaças...')
end

Então("após o termino apresentar os textos corretos nas tela de Ameaças") do |table|
  @base_page.wait("#{DATA['url']}/toolbar_title_prim", 150)
  text = []
  for i in 0..table.cells_rows.count
    text[i] = @base_page.exists_text(table.rows_hash["texto#{i}"])
    puts "#{table.rows_hash["texto#{i}"]}: #{text[i]}"
    expect(text[i]).to eql true
  end
end

Então("na tela de Suspeitos") do |table|
  @base_page.touch_text('SUSPEITOS')
  text = []
  for i in 0..table.cells_rows.count
    text[i] = @base_page.exists_text(table.rows_hash["texto#{i}"])
    puts "#{table.rows_hash["texto#{i}"]}: #{text[i]}"
    expect(text[i]).to eql true
  end
end
  
#@2scenario
Quando("o usuario scrollar a tela e tocar na opção Transfer") do
  @home_page.clicar_popup
  swipe(start_x: 250, start_y: 1500, end_x: 0, end_y: 0, duration: 500)
  @base_page.wait_text('TRANSFER BY HERO')
  @base_page.touch_text('TRANSFER BY HERO')
end

Então("deve apresentar os textos do Transfer corretamente") do |table|
  @base_page.wait("#{DATA['url']}/button_feature_action")
  swipe(start_x: 250, start_y: 1500, end_x: 0, end_y: 0, duration: 500)
  text = []
  for i in 0..table.cells_rows.count
    text[i] = @base_page.exists_text(table.rows_hash["texto#{i}"])
    puts "#{table.rows_hash["texto#{i}"]}: #{text[i]}"
    expect(text[i]).to eql true
  end
  swipe(start_x: 250, start_y: 100, end_x: 0, end_y: 0, duration: 500)
end

Então("devo tocar no botão Instalar app") do
  @base_page.touch_button("#{DATA['url']}/button_feature_action")
end

Então("validar que sou direcionado para a loja do Transfer") do
  @base_page.exists_text('Transfer by Hero')
end

#@3scenario
Quando("habilitar o Wi-Fi Seguro") do
  @home_page.clicar_popup
  swipe(start_x: 250, start_y: 1500, end_x: 0, end_y: 0, duration: 500)
  @base_page.wait("#{DATA['url']}/switch_secure_vpn")
  $driver.press_keycode(3)
  @function_page.acessar_site("www.dropbox.com")
  @base_page.exists_text('Não é possível acessar esse site')
  $driver.press_keycode(3)
  $driver.press_keycode(187)
  @base_page.touch_text('Hero')
  @home_page.wifi_seguro
end

Então("devo garantir o acesso a qualquer site antes bloqueado") do
  $driver.press_keycode(3)
  @base_page.touch_button('Chrome')
  @base_page.touch_button('reload-button')
  @base_page.wait_text('Acessar conta')
  validacao = @base_page.exists_text('Acessar conta')
  expect(validacao).to eql true
end


# #@4scenario
# Quando("habilitar o Navegação Segura") do
#   @home_page.clicar_popup
#   @home_page.nav_seguro
# end

# Então("devo garantir que os sites da categoria selecionada estão bloqueados") do
#   @home_page.incluir_bloq
#   $driver.press_keycode(3)
#   $driver.press_keycode(3)
#   @function_page.acessar_site('www.americanas.com.br')

# end


#@5scenario
Quando("o usuario estiver na Home do Cloud e tocar no botão Gerenciar") do
  @home_page.clicar_popup
  @base_page.touch_text('Segurança')
  @base_page.touch_text('Cloud')
  @base_page.touch_text('Gerenciar')
end
  
Então("deve apresentar os textos do Cloud corretamente") do |table|
  @base_page.wait("#{DATA['url']}/button_feature_action")
  swipe(start_x: 250, start_y: 1500, end_x: 0, end_y: 0, duration: 500)
  text = []
  for i in 0..table.cells_rows.count
    text[i] = @base_page.exists_text(table.rows_hash["texto#{i}"])
    puts "#{table.rows_hash["texto#{i}"]}: #{text[i]}"
    expect(text[i]).to eql true
  end
  swipe(start_x: 250, start_y: 100, end_x: 0, end_y: 0, duration: 500)
end

Então("validar que sou direcionado para a loja do Cloud") do
    @base_page.exists_text('Cloud by Hero')
end

#@6scenario
Quando("o usuario estiver na Home do Família e tocar no botão Gerenciar") do
    @home_page.clicar_popup
    @base_page.touch_text('Família')
    @base_page.touch_text('Gerenciar')
end

Então("deve apresentar os textos do Família corretamente") do |table|
    @base_page.wait("#{DATA['url']}/button_feature_action")
    swipe(start_x: 250, start_y: 1500, end_x: 0, end_y: 0, duration: 500)
    text = []
    for i in 0..table.cells_rows.count
      text[i] = @base_page.exists_text(table.rows_hash["texto#{i}"])
      puts "#{table.rows_hash["texto#{i}"]}: #{text[i]}"
      expect(text[i]).to eql true
    end
    swipe(start_x: 250, start_y: 100, end_x: 0, end_y: 0, duration: 500)
end

Então("validar que sou direcionado para a loja do Família") do
    @base_page.exists_text('Família by Hero')
end


#@7scenario
Quando("o usuario estiver na Home do Help Desk e tocar no botão Solicitar Ajuda") do
  @home_page.clicar_popup
  @base_page.touch_text('Help Desk')
  @base_page.touch_text('SOLICITAR AJUDA')
end

Então("deve ser direcionado para tela de dados") do
  @base_page.fill('islpronto_object_3', 'Teste')
  @base_page.touch_button('islpronto_object_6')
end

Então("após informar os dados e clicar no botão Conversar abre o chat") do
  @base_page.exists('islpronto_object_29')
end


#@8scenario
Quando("efetuar o download de um app com virus") do
  @home_page.clicar_popup
  $driver.press_keycode(3)
  @function_page.baixar_app
end

Então("deve apresentar alerta de ameaça") do
  alerta = @base_page.wait_text('TIPO DE AMEAÇA')
  expect(alerta).to eql true
end


#@9scenario
Quando("efetuar a verificação de virus") do
  @home_page.clicar_popup
  $driver.press_keycode(3)
  @function_page.baixar_app
  @base_page.wait_text('TIPO DE AMEAÇA')
  @base_page.touch_button('btn_cancel')
  $driver.press_keycode(3)
  $driver.press_keycode(187)
  @base_page.touch_text('Hero')
  @base_page.touch_text('Segurança')
  @home_page.clicar_escudo
end

Então("após apresentar o virus remove-lo") do
  @base_page.wait_text('Procurando ameaças...')
  @base_page.wait("#{DATA['url']}/toolbar_title_prim", 150)
  if @base_page.exists("#{DATA['url']}/txt_threats_select_all")
    @base_page.touch_button("#{DATA['url']}/txt_threats_select_all")
  else
    @base_page.touch_button('cb_threat')
  end
  @base_page.touch_button('btn_remove_threats')
  @base_page.wait('android:id/title_template')
  @base_page.touch_button('button1')  
  @base_page.wait_text('Em uma escala de 0 a 10...')
  @base_page.touch_button("#{DATA['url']}/nps_close_button")
  @base_page.wait_text('TELEFONE PROTEGIDO')
end