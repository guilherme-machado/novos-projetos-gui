#2feature
#contexto
Dado("que o usuario esteja em Configurações") do
  @login_page.logar
  @home_page.clicar_popup
  @base_page.wait("#{DATA['url']}/img_settings")
  @base_page.touch_button("#{DATA['url']}/img_settings")
end

#1scenario
Quando("o usuario tocar no Perfil") do
  @base_page.touch_text('Perfil')
end

Então("deve ser direcionado para o Perfil") do
  @base_page.wait("#{DATA['url']}/toolbar_title_prim")
  perfil = @base_page.exists_text('Perfil')
  expect(perfil).to eql true
end

#2scenario
Quando("o usuario tocar no Gestor de Licenças") do
  @base_page.touch_text('Gestor de Licenças')
end

Então("deve ser direcionado para o Gestor de Licenças") do
  @base_page.wait("#{DATA['url']}/toolbar_title_prim")
  gestor = @base_page.exists_text('Gestor de Licenças')
  expect(gestor).to eql true
end

#3scenario
Quando("o usuario tocar no Sobre") do
  @base_page.touch_text('Sobre')
end

Então("deve ser direcionado para o Sobre") do
  @base_page.wait("#{DATA['url']}/toolbar_title_prim")
  sobre = @base_page.exists_text('Sobre')
  expect(sobre).to eql true
end

#4scenario
Quando("o usuario tocar em Política de Privacidade") do
  @base_page.touch_text('Política de Privacidade')
end

Então("deve ser direcionado para a Política de Privacidade") do
  @base_page.wait("#{DATA['url']}/toolbar_title_prim")
  politica = @base_page.exists_text('Política de Privacidade')
  expect(politica).to eql true
end

#5scenario
Quando("o usuario tocar em Termos de Uso") do
  @base_page.touch_text('Termos de Uso')
end

Então("deve ser direcionado para os Termos de Uso") do
  @base_page.wait("#{DATA['url']}/toolbar_title_prim")
  termos = @base_page.exists_text('Termos de Uso')
  expect(termos).to eql true
end

#6scenario
Quando("o usuario tocar nas Perguntas Frequentes") do
  @base_page.touch_text('Perguntas Frequentes')
end

Então("deve ser direcionado para as Perguntas Frequentes") do
  @base_page.wait("#{DATA['url']}/toolbar_title_prim")
  faq = @base_page.exists_text('Ajuda')
  expect(faq).to eql true
end