# language: pt

Funcionalidade: Usuario HERO que utiliza o App HERO
    Usuario HERO que inicia o app HERO

    Contexto:
        Dado que usuario HERO inicie o aplicativo HERO
            
            |login1|Proteja sua vida digital|
            |login2|A vida está cada vez mais digital. Mas você não precisa se preocupar com hackers, vírus ou roubo de dados|
            |login3|Insira o seu DDD + celular|
            |login4|Entrar|

@login
    Cenario: Usuario HERO que possui o HERO
        Quando usuario insere o msisdn e clica em "Entrar"
        Então recebe o pincode de validacao

            |confirma1|Confirmar seu número de telefone|
            |confirma2|Insira o código de 6 dígitos enviado para|
            |confirma3|Continuar|

        E exibe a home do app

            |home1|Segurança|
            |home2|Cloud|
            |home3|Família|
            |home4|Help Desk|
            # |home5|PROTEJA SEU CELULAR|
            # |home6|Verificar Vírus|
            # |home7|TRANSFER BY HERO|