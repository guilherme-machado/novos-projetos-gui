#language: pt
@2feature
Funcionalidade: Validar o menu Configurações


    Contexto: Menu Configurações
        Dado que o usuario esteja em Configurações

    @1scenario
    Cenario: Acessar o Perfil
        Quando o usuario tocar no Perfil
        Então deve ser direcionado para o Perfil

    @2scenario
    Cenario: Acessar o Gestor de Licenças
        Quando o usuario tocar no Gestor de Licenças
        Então deve ser direcionado para o Gestor de Licenças

    @3scenario
    Cenario: Acessar o Sobre
        Quando o usuario tocar no Sobre
        Então deve ser direcionado para o Sobre

    @4scenario
    Cenario: Acessar a Política de Privacidade
        Quando o usuario tocar em Política de Privacidade
        Então deve ser direcionado para a Política de Privacidade

    @5scenario
    Cenario: Acessar os Termos de Uso
        Quando o usuario tocar em Termos de Uso
        Então deve ser direcionado para os Termos de Uso

    @6scenario
    Cenario: Acessar as Perguntas Frequentes
        Quando o usuario tocar nas Perguntas Frequentes
        Então deve ser direcionado para as Perguntas Frequentes