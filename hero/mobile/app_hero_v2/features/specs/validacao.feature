#language: pt
@3feature
Funcionalidade: Validação das abas do aplicativo Hero

    Contexto: Abrir e logar no app
        Dado que o usuario efetue o login

    @1scenario
    Cenario: Validação do Segurança
        Quando ele tocar no escudo
        Então a verificação deve ser feita
        E após o termino apresentar os textos corretos nas tela de Ameaças
            |texto1|Seu aparelho está protegido! Após verificar minuciosamente seu aparelho não encontramos ameaças.|
        E na tela de Suspeitos
            |texto2|Você está seguro! Não encontramos nenhuma atividade suspeita durante a verificação.|
    
    @2scenario
    Cenario: Validação do Transfer
        Quando o usuario scrollar a tela e tocar na opção Transfer
        Então deve apresentar os textos do Transfer corretamente
            |texto1|Como parte do seu pacote, Transfer by Hero está disponível para ajudar a transferir conteúdos com segurança. Instale o aplicativo acima para desbloquear esse recurso.|
            |texto2|Transferir dados de um celular velho para um novo|
            |texto3|Selecionar o que vai ser transferido|
            |texto4|Transferência segura via Wi-Fi|
            |texto5|Previsão de tempo da transferência|
            |texto6|Prévia de espaço de dados ocupado|
        E devo tocar no botão Instalar app
        E validar que sou direcionado para a loja do Transfer

    @3scenario
    Cenario: Validação do Wi-Fi Seguro
        Quando habilitar o Wi-Fi Seguro
        Então devo garantir o acesso a qualquer site antes bloqueado

    # @4scenario
    # Cenario: Validação do Navegação Segura
    #     Quando habilitar o Navegação Segura
    #     Então devo garantir que os sites da categoria selecionada estão bloqueados

    @5scenario
    Cenario: Validação do Cloud
        Quando o usuario estiver na Home do Cloud e tocar no botão Gerenciar
        Então deve apresentar os textos do Cloud corretamente
            |texto1|Como parte de seu plano, o Cloud by Hero está disponível para ajudar a proteger seu telefone. Instale o aplicativo acima para desbloquear esse recurso.|
            |texto2|Armazenamento online na nuvem|
            |texto3|Sincronizar com suas contas das redes sociais|
            |texto4|Gerenciamento de conteúdo|
            |texto5|Criação de grupos para compartilhar arquivos|
            |texto6|Acesso por múltiplos dispositivos|
        E devo tocar no botão Instalar app
        E validar que sou direcionado para a loja do Cloud

    @6scenario
    Cenario: Validação do Família
        Quando o usuario estiver na Home do Família e tocar no botão Gerenciar
        Então deve apresentar os textos do Família corretamente
            |texto1|Família by Hero é uma ferramenta completa para monitorar e proteger seus familiares das ameaças do mundo digital.|
            |texto2|Filtrar conteúdo de sites maliciosos|
            |texto3|Administrar funcionalidades pelo portal dos pais|
            |texto4|Acesso por múltiplos dispositivos|
            |texto5|Limitar o tempo de uso das crianças|
            |texto6|Modo protect para mecanismos de busca|
        E devo tocar no botão Instalar app
        E validar que sou direcionado para a loja do Família

    @7scenario
    Cenario: Validação Help Desk
        Quando o usuario estiver na Home do Help Desk e tocar no botão Solicitar Ajuda
        Então deve ser direcionado para tela de dados
        E após informar os dados e clicar no botão Conversar abre o chat

    @8scenario
    Cenario: Validando alerta de virus
        Quando efetuar o download de um app com virus
        Então deve apresentar alerta de ameaça

    @9scenario
    Cenario: Removendo virus
        Quando efetuar a verificação de virus
        Então após apresentar o virus remove-lo