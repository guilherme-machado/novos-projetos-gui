#language:pt

@heronps

Funcionalidade: Hero Android com NPS

REGRA 1
A pesquisa deverá ser exibida após o usuário realizar COM SUCESSO pela 1a vez uma das interações abaixo:

Scan de vírus: Pesquisa deverá ser exibida após o scan ser finalizado com sucesso - exibindo ou não ameaças detectadas - e o usuário retornar para a página principal do APP. Caso o scan seja interrompido manualmente pelo usuário, a pesquisa não deverá ser exibida. (Somente para ANDROID)
Ativação do Wi-Fi Seguro: Pesquisa deverá ser exibida após a conclusão da ativação do Wi-Fi e após o usuário clicar no pop-up de confirmação da ativação.
Acesso à privacidade de aplicativos: Pesquisa deverá ser exibida após o usuário finalizar a consulta de privacidade e retornar para a página principal do APP. É considerada como interação com sucesso a consulta do resultado da privacidade de aplicativos independente do usuário fazer alguma ação dentro da feature. (Somente para ANDROID)
Ativação do navegação segura: Pesquisa deverá ser exibida após a ativação ser finalizada e o usuário retornar para a página principal do APP.
Compartilhamento de licença: Pesquisa deverá ser exibida após a exibição do pop-up de confirmação do compartilhamento de licença.

REGRA 2
A exibição de nova pesquisa somente acontecerá 60 dias após a última exibição da pesquisa, vinculada à realização de uma das ações descritas na REGRA 1.

REGRA 3
Se o usuário fechar a pesquisa sem respondê-la (clicar no "x" da tela de votação), deverá ser registrada e armazenada a nota -1.
Caso o usuário realize a votação e nas telas posteriores interrompa o fluxo (clique no "x", feche o APP, etc), ainda assim a nota deverá ser registrada e armazenada.

REGRA 4 - Backend
Ao votar em uma nota detratora, automaticamente, deverá ser disparado um e-mail para hero@fs.com.br com as seguintes informações do usuário: MSISDN, nota atribuída, e-mail do cliente, campo observação, nome do APP, versão do APP, device (android/IOS), modelo do aparelho, sistema operacional, data, horário e ação que disparou a pesquisa (fluxo atual já existente em produção), conforme layout anexo.

REGRA 5
Na tela de canais de atendimento, o cliente pode clicar em EMAIL, WHATSAPP e CHAT.

Email: ao clicar em e-mail, deverá ser aberto o aplicativo padrão de E-mail com uma nova mensagem e o campo destinatário já preenchido com o endereço hero@fs.com.br (fluxo de envio de e-mail permanece como o atual).
WhatsAPP: ao clicar em WhatsApp, deverá ser aberto o app WhatsAPP para o cliente falar com o número 11 94342-8056. Caso o aplicativo WhatsApp não esteja instalado, usuário deverá ser direcionado para a loja de apps para fazer o download do WhatsApp (fluxo permanece como o atual).
Chat: ao clicar em Chat deverá abrir a webview de Chat (fluxo e chat permanecem como o atual).

REGRA 6
Para os clientes PROMOTORES (notas 9 e 10), após dar a nota, deverá ser exibida a tela que incentiva o cliente a avaliar o APP no Google Play ou Apple Store.

REGRA 7
Os relatórios deverão conter as seguintes informações:

MSISDN - Formato: 5511987654321 (DDI + DDD + MSISDN)
Nome do APP: Hero Único
•	Ação do cliente que disparou a pesquisa: scan de virus, ativação do wi-fi, privacidade de app, navegação segura e compartilhamento de licença.
•	Nota atribuída (score): 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 e 10. Se o cliente fechar a pesquisa sem respondê-la, deve ser registrada a nota -1.
•	E-mail do cliente: campo não é obrigatório. O formato é o padrão: xxxxx@xxxx.com ou @xxxx.com.br. O domínio do e-mail pode conter números ou letras.
•	Descrição do comentário: campo não é obrigatório. É um campo de texto livre para o cliente e deve ser exibido na base de dados com formato de texto.
•	Device (Android ou IOS): Android, IOS
•	Versão do Sistema Operacional: Se Android informar número. Ex: Android 6, Android 7, Android 8. Se IOS informar IOS 8, IOS 9 etc.
•	Modelo do aparelho: informar a marca e o modelo. Ex: Motorola G5.
•	Canal de atendimento que o cliente clicou: Email, WhatsApp ou Chat.
•	Versão do APP: número da versão do aplicativo: Ex: 2.0.
•	Pacote/Plano do cliente: qual o plano do produto da FS. EX: Hero Essencial, Hero Básico, Hero Premium etc.
•	Data da pesquisa: data que o cliente respondeu a pesquisa: AAAA/MM/DD = 2018/08/31
•	Hora da pesquisa: horário que o cliente respondeu a pesquisa: HH:MM:SS = 11:21:45
•	Caso PROMOTOR (notas 9 e 10) informar se clicou no link para avaliar a loja de APP Google ou Apple: dado exibido nesta base de dados deve ser Sim ou Não. Em caso de notas neutras ou detratoras o dado será sempre Não.

REGRA 8
A pesquisa está vinculada a instalação e ao login - ou seja - caso o mesmo MSISDN seja utilizado em mais de um device, as contagens serão diferentes em cada um deles;
Deslogar e logar com o mesmo número não deve zerar a regra de exibição;
Deslogar e logar com um número diferente deve zerar a regra de exibição;

REGRA 9
Caso o usuário force o fechamento do app durante a exibição do NPS não será gravada nenhuma informação da pesquisa. A nova exibição seguirá a REGRA 2
@scan
Cenario: Regra 1 - Escaneamento

Dado que eu como Q.A esteja logado no app unico HERO.
Quando eu estiver na home, entao eu irei ativar o escanemento e aguardá-lo terminar , então o nps ira aparecer
Então eu contruirei com a minha alteração e farei as validações necessárias
@wi-fi
Cenário: Regra 1 - Ativação Wifi Seguro

Dado que eu como Q.A esteja logado no app unico HERO.
Quando eu estiver na home , então eu irei ativar Wi-fi e assim que a ação ser concluída 
Então farei as validações necessárias e o NPS deverá abrir com sucesso
@nav
Cenário: Regra 1 - Navegação segura 

Dado que eu como Q.A esteja logado no app unico HERO.
Quando eu estiver na home , então eu irei ativar Navegação Segura e assim que a ação ser concluída 
Então farei as validações necessárias e o NPS deverá abrir com sucesso

@priv
Cenário: Regra 1 - Gerenciador de licenças - Politica de privacidade e sobre
Dado que eu como Q.A esteja logado no app unico HERO.
Quando eu estiver na home , então eu irei clicar nas atividades do  menu e assim que a ação ser concluída 
Então farei as validações necessárias e o NPS deverá abrir com sucesso

@d60
Cenário: Regra 2 - D+60 

Dado que eu como Q.A estou logado, realizo uma interação então meu NPS abre.
Quando isso acontece eu fecho a pesquisa NPS, Altero a data do meu dispositivo para 60 dias a frente, abro novamente meu aplicativos
Então eu farei as validações necessárias para que o nps possa ser revalidado em D60.

@nota-1
Cenário: Regra 3 - nota -1

Dado  que eu como Q.A estou no logado , realizo uma interação no NPS
Quando o NPS é aberto eu fecho ele , sem gerar uma nota
Então Eu farei as validações necessárias para garantir que a nota menos um foi gerada no board
@notadet
Cenário: Regra 4 - Nota detratora

Dado que eu como Q.A estou logado, realizo uma intração no NPS
Quando o NPS é aberto então eu qualifico com uma nota de 1 a 5
Então eu farei as validaçoes necessárias para meu report ser enviado para "hero@fs.com.br"
@clique
Cenário: Regra 5 - Clique nas opções 

Dado que eu como Q.A estou logado, realizo uma interação no nps
Quando eu realizo uma interação detratora 
Então me certifico de que os seviçõs EMAIL Whatsapp e Chat estão clicáveis com seus links certos.
@play
Cenário: Regra 6 - Play Store

Dado eu como Q.A estou logado , realizo uma interação com NPS 
Quando eu qualifico com uma nota "9" ou "10" e prossigo
Então eu recebo uma notificação para avaliação na loja

# @bi
# Esquema do Cenário: Regra 7 - Relatórios de B.I

# Dado que eu como Q.A Fiz algumas interações no NPS
# Quando essas interações de <nota> <email> <descrição>
# Então farei as validações necessárias para testar o mesmo

# Exemplos:

# |nota       |email                 |descrição                       |
# |"one"      |"testesqa1@gamail.com"|"Não Curti"                     |
# |"two"      |"testesqa2@gamail.com"|"Aplicatrivo para muito!"       |
# |"three"    |"testesqa3@gamail.com"|"tá travando"                   |
# |"for"      |"testesqa4@gamail.com"|"Achei meio lento"              |
# |"five"     |"testesqa5@gamail.com"|"È bom mais pode melhorar"      |
# |"six"      |"testesqa6@gamail.com"|"dei 6 se for bom dou mais"     |
# |"seven"    |"testesqa7@gamail.com"|"legalzinho"                    |
# |"eight"    |"testesqa8@gamail.com"|"bom"                           |
# |"nine"     |"testesqa9@gamail.com"|"Maravilhoso mas não perfeito"  |
# |"ten"      |"testesqa9@gamail.com"|"App ótimo"                     |

@memo
Cenário: Regra 8 - Ação Memorizada 

Dado que eu como Q.A estou logado na home e faço uma interação
Quando eu faço a interação , o NPS abre, eu ignoro e fecho o aplicativo
Então eu abro o mesmo novamente para me certificar que o mesmo não abrirá

# Cenário: Regra 9 - Ação não contabilizada

# Dado que eu estou logdo no app, e então aparece para mim o NPS 
# Quando o mesmo abre , eu fecho o aplicativo e mudo a data para d+60
# Então abro o aplicativo e o mesmo exibe o nps novamente