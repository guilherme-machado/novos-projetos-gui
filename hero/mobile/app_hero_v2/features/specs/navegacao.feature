#language: pt
@1feature
Funcionalidade:  Validar a navegação no aplicativo do HERO


    Contexto: Abrir e logar no app
        Dado que o usuario efetue o login

    @1scenario
    Cenario: Validação total to app
        Quando o usuario estiver na Home do Hero
        Então valido os menus do app

    @2scenario
    Cenario: Validação da Home do Hero
        Quando o usuario estiver na Home do Hero
        Então deve ser apresentadas as informações corretas

    @3scenario 
    Cenario: Validação do Otimizador
        Quando o usuario tocar no Otimizador no menu inferior
        Então deve ser direcionado para a tela do Otimizador

    @4scenario 
    Cenario: Validação do Cloud
        Quando o usuario tocar no Cloud no menu inferior
        Então deve ser direcionado para a tela do Cloud by Hero

    @5scenario
    Cenario: Validação do Família
        Quando o usuario tocar no Família no menu inferior
        Então deve ser direcionado para a tela do Família by Hero

    @6scenario
    Cenario: Validação do Help Desk
        Quando o usuario tocar no Help Desk
        Então deve ser direcionado para a tela Help Desk by Hero

    @7scenario
    Cenario: Acessar Configurações
        Quando o usuario tocar na engrenagem
        Então deve ser apresentado a lista de menus