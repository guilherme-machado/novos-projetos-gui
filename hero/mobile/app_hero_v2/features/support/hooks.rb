Before do
    @base_page = BasePage.new
    @login_page = LoginPage.new
    @home_page = HomePage.new
    @function_page = FunctionPage.new
end

After('@evidencia') do |scenario|
    if scenario.failed?
        nome_cenario = scenario.name.gsub(/[^A-Za-z0-9 ]/, '')
        nome_cenario = nome_cenario.gsub(' ', '-').downcase!            
        screenshot = "log/screenshot/falha/#{nome_cenario} #{Time.now}.png"
        $driver.screenshot(screenshot)
        embed(screenshot, 'image/png', 'Evidencia')
    else
        nome_cenario = scenario.name.gsub(/[^A-Za-z0-9 ]/, '')
        nome_cenario = nome_cenario.gsub(' ', '-').downcase!            
        screenshot = "log/screenshot/sucesso/#{nome_cenario} #{Time.now}.png"
        $driver.screenshot(screenshot)
        embed(screenshot, 'image/png', 'Evidencia')
    end
end