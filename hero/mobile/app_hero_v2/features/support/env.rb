# This file provides setup and common functionality across all features.  It's
# included first before every test run, and the methods provided here can be 
# used in any of the step definitions used in a test.  This is a great place to
# put shared data like the location of your app, the capabilities you want to
# test with, and the setup of selenium.

require 'rspec/expectations'
require 'appium_lib'
require 'appium_console'
require 'rspec'
require 'httparty'
require 'pry'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'faker'
require 'cpf_faker'

World(Capybara::DSL)
World(Capybara::RSpecMatchers)

# Create a custom World class so we don't pollute `Object` with Appium methods
class AppiumWorld
end

# Load the desired configuration from appium.txt, create a driver then
# Add the methods to the world
caps_path = File.join(File.dirname(__FILE__), '..', '..', 'caps', 'appium_hero.txt')
caps = Appium.load_appium_txt file: caps_path, verbose: true
Appium::Driver.new(caps, true)
Appium.promote_appium_methods AppiumWorld

World do
  AppiumWorld.new
end

Before { $driver.start_driver }
After { $driver.driver_quit }

ENV_TYPE = ENV['ENV_TYPE']
DATA = YAML.load_file(File.dirname(__FILE__) + "/#{ENV_TYPE}.yaml") 