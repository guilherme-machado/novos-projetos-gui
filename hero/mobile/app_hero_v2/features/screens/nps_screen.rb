class Nps < BasePage
    
    def wait_pin
        @timeout = 120
        sentPin = 0
        wait("#{DATA['end_point']}/submit_confirmation_code")
          
          @start_time = Time.now
                    
    while   @timeout > sentPin
            @end_time = Time.now
            
    if      return_text("#{DATA['end_point']}/confirmation_code") != "- - - - - -"
            sentPin = timer(@start_time, @end_time)
            found = "sucesso"
            break
    elsif    exists("#{DATA['end_point']}/resend_code")
            touch_button("#{DATA['end_point']}/resend_code")
                
    else
            sentPin = timer(@start_time, @end_time)
            found = "falha"
            end
        end
     end
    
     def login_hero
        fill("#{DATA['end_point']}/fslogin_hero_security_edt_phone_number", msisdn)
        $driver.back
        touch_button("#{DATA['end_point']}/fslogin_hero_security_btn_login")
        wait_pin
    end
    

    def escaneamento_completo_nps(email,text_nota)
        touch_button("#{DATA['end_point']}/iv_shield")
        wait("#{DATA['end_point']}/toolbar_title_prim", 150 )
        $driver.back
        touch_button("#{DATA['end_point']}/nps_radio_eight")
        touch_button("#{DATA['end_point']}/nps_vote_button")
        email = find("#{DATA['end_point']}/nps_form_email_edit_text")
        email.send_key email 
        $driver.back
        text_message = find("#{DATA['end_point']}/nps_form_comment_edit_text")
        text_message.send_key text_nota
        $driver.back
        texto =  exists_text('votar')
        touch_button("#{DATA['end_point']}/nps_form_send_button")
    end 

    def wifi_seguro(email, text_nota)
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)
        sleep 3
        touch_button("#{DATA['end_point']}/switch_secure_vpn")
        wait_text('OK')
        touch_button("#{DATA['end_point']}/txt_positive")
        # wait_text('OK')
        # touch_text('OK')
        wait("#{DATA['end_point']}/nps_vote_button")
        touch_button("#{DATA['end_point']}/nps_radio_seven")
        texto1 =  exists_text('votar')
        touch_button("#{DATA['end_point']}/nps_vote_button")
        email = find("#{DATA['end_point']}/nps_form_email_edit_text")
        email.type email 
        $driver.back
        text_message = find("#{DATA['end_point']}/nps_form_comment_edit_text")
        text_message.type text_nota
        $driver.back
        texto2 =  exists_text('enviar')
        touch_button("#{DATA['end_point']}/nps_form_send_button")
        texto3 =  exists_text('E-MAIL')
        texto4 =  exists_text('WHATSAPP')
        texto5 =  exists_text('CHAT')
        touch_button("#{DATA['end_point']}/nps_close_button")
    end

    

    def navegacao_segura
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500) 
        sleep 3
        touch_button("#{DATA['end_point']}/switch_setting_opt")
        texto1= exists_text('Para monitorar e alertar você dos perigos da internet, é preciso ativar o Modo de Acessibilidade para o Hero na próxima tela.')
        touch_button("#{DATA['end_point']}/txt_positive")
        wait_text('Hero')
        touch_text('Hero')
        touch_button("com.android.settings:id/switch_widget")
        touch_button("android:id/button1")
        $driver.back
        $driver.back
        touch_button("#{DATA['end_point']}/nps_radio_ten")
        touch_button("#{DATA['end_point']}/nps_vote_button")
        touch_button("#{DATA['end_point']}/nps_google_play_button")
        $driver.back
    end

    def privacidade_de_aplicativos(email, text_nota) 
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)  
        sleep 3
        touch_text('Privacidade De Aplicativos')
        wait_text('Classificações de Permissão')
        $driver.back
        touch_button("#{DATA['end_point']}/nps_radio_six")
        touch_button("#{DATA['end_point']}/nps_vote_button")
        email = find("#{DATA['end_point']}/nps_form_email_edit_text")
        email.send_key email 
        $driver.back
        text_message = find("#{DATA['end_point']}/nps_form_comment_edit_text")
        text_message.send_key text_nota
        $driver.back
        texto =  exists_text('votar')
        touch_button("#{DATA['end_point']}/nps_form_send_button")
    end

    def d60 
        $driver.back
        touch_button('Apps')
        exists_text('Configurar')
        touch_text('Configurar')
        sleep 1
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)
        exists_text('Data e hora')
        touch_text('Data e hora')
        touch_text('Data e hora automáticas')
        touch_text('Definir data')
        touch_button('android:id/next')
        touch_button('android:id/next')
        touch_button('android:id/next')
        touch_text('1')
        touch_text('OK')
        $driver.back
        $driver.back
        $driver.swipe(start_x: 250, start_y: 750, end_x: 250, end_y: 5, duration: 500) 
        touch_button('Hero')
    end

    def nota_1
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)  
        sleep 3
        touch_text('Privacidade De Aplicativos')
        wait_text('Classificações de Permissão')
        $driver.back
        touch_button("#{DATA['end_point']}/nps_close_button")
        $driver.back
    end

        # def valida_nota
        #     $driver.back
        #     touch_button('Apps')
        #     touch_text('Chrome')     
        #     # fill('com.android.chrome:id/url_bar', "https://datastudio.google.com/u/0/reporting/1U_0nnW8mLeXpRHNkhYBBXztwOt2EbgUS/page/HBzM" )      
        #     # find('#identifierId').set "guilherme.machado@fs.com.br"
        #     # find('#identifierId').native.send_keys
        #     # find('#password').set "RED&code11"
        #     # find('#password').native.send_keys
        #     # find('#body > div > div > div.lego-reporting-view.activity-view.no-licensed > div.page > div > div:nth-child(2)').click
        # end


    def nota_det(email, text_nota)
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)  
        sleep 3
        touch_text('Privacidade De Aplicativos')
        wait_text('Classificações de Permissão')
        $driver.back
        touch_button("#{DATA['end_point']}/nps_radio_three")
        touch_button("#{DATA['end_point']}/nps_vote_button")
        email = find("#{DATA['end_point']}/nps_form_email_edit_text")
        email.type email 
        $driver.back
        text_message = find("#{DATA['end_point']}/nps_form_comment_edit_text")
        text_message.type text_nota
        $driver.back
        texto2 =  exists_text('enviar')
        touch_button("#{DATA['end_point']}/nps_form_send_button")
        texto3 =  exists_text('E-MAIL')
        texto4 =  exists_text('WHATSAPP')
        texto5 =  exists_text('CHAT')
        touch_text('E-MAIL')
        touch_text('Gmail')
        # texto6 = find('com.google.android.gm:id/to').text
        # $driver.expect(texto6).to eql ("<#{remetente}>,")
        fill('com.google.android.gm:id/composearea_tap_trap_bottom', "#{text_nota}") 
        $driver.back
        touch_button('com.google.android.gm:id/send')
        
    end

    def play_store
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)  
        sleep 3
        touch_text('Privacidade De Aplicativos')
        wait_text('Classificações de Permissão')
        $driver.back
        touch_button("#{DATA['end_point']}/nps_radio_nine")
        touch_button("#{DATA['end_point']}/nps_vote_button")
        exists_text('Estamos felizes com sua satisfação e contamos com seu apoio para compartilhar sua experiência em nossa loja.')
        touch_button('nps_google_play_button')
        exists_text('FS SECURITY SERVIÇOS DE TECNOLOGIA')
    end
        

        # def nota_one_to_ten(nota, email, descrição )
        #     $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)  
        #     sleep 3
        #     touch_text('Privacidade De Aplicativos')
        #     wait_text('Classificações de Permissão')
        #     $driver.back
        #     if  find_text("#{nota},[one..eigth]") 
        #         touch_button("nps_radio_#{nota}")
        #         touch_button("#{DATA['end_point']}/nps_vote_button")
        #         email = find("#{DATA['end_point']}/nps_form_email_edit_text")
        #         email.send_key email 
        #         $driver.back
        #         text_message = find("#{DATA['end_point']}/nps_form_comment_edit_text")
        #         text_message.send_key descrição
        #         $driver.back
        #         texto =  exists_text('votar')
        #         touch_button("#{DATA['end_point']}/nps_form_send_button")
        #     else find_text("#{nota}, [nine..ten]")
        #         touch_button('nps_radio_nine')
        #         touch_button('nps_vote_button')
        #         exists_text('Estamos felizes com sua satisfação e contamos com seu apoio para compartilhar sua experiência em nossa loja.')
        #         touch_button('nps_google_play_button')
        #         exists_text('FS SECURITY SERVIÇOS DE TECNOLOGIA')
        #     end
       
       
    def nota_memorizada
        $driver.back
        touch_button('Apps')
        $driver.swipe(start_x: 250, start_y: 750, end_x: 250, end_y: 5, duration: 500) 
        touch_button('Hero')
    end
  
    
end    
