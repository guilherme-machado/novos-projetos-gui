class BasePage 
    def msisdn
        msisdn = "11957141535"
        # msisdn = "11968926522"
        msisdn
    end

    def fill(id, text)
        field = $driver.find_element :id, id 
        field.click
        field.send_key "#{text}"
    end

    def find(id)
        element = $driver.find_element :id, id
        element
    end

    def find_elements(id)
        element = $driver.find_elements :id, id
        element
    end

    def wait(id, timeout = 60)
        $driver.wait_true(timeout) { $driver.exists { $driver.find_element :id, id } }               
    end

    def wait_text(text, timeout = 60)
        $driver.wait_true(timeout) { $driver.exists { $driver.find_element :xpath, "//*[@text='#{text}']" } }
    end

    def timer(start_time, end_time)
        if end_time.min > start_time.min
            count = 60 * (end_time.min - start_time.min)
            result = (end_time.sec + count) - start_time.sec
        elsif
            result = end_time.sec - start_time.sec
        end
        result
    end

    def touch_button(id)
        button = $driver.find_element :id, id
        button.click
    end

    def touch_text(text)
        button = $driver.find_element(:xpath, "//*[@text='#{text}']")
        button.click
    end

    def return_text(id)
        element = find(id)
        text = element.attribute('text')
        text
    end

    def exists(id)
        $driver.exists { $driver.find_element :id, id }
    end

    def clicar_popup
        wait("#{DATA['end_point']}/txt_action")
        touch_button("#{DATA['end_point']}/txt_action")
    end

    def exists_text(text)
        validator = $driver.exists { $driver.find_element :xpath, "//*[@text='#{text}']" }
        validator
    end
   
    def touch_text(text)
        button = $driver.find_element(:xpath, "//*[@text='#{text}']")
        button.click
    end

    def find_text(text)
        find = $driver.find_element(:xpath, "//*[@text='#{text}']")
        find 
    end
end




 

