class LoginPage < BasePage
    def wait_pin
        @timeout = 120
        sentPin = 0
        wait("#{DATA['url']}/submit_confirmation_code")
          
        @start_time = Time.now
                    
        while @timeout > sentPin
          @end_time = Time.now
        
          if return_text("#{DATA['url']}/confirmation_code") != "- - - - - -"
            sentPin = timer(@start_time, @end_time)
            found = "sucesso"
            break
          elsif exists("#{DATA['url']}/resend_code")
            touch_button("#{DATA['url']}/resend_code")
          else
            sentPin = timer(@start_time, @end_time)
            found = "falha"
          end
        end
    end

    def logar      
        fill("#{DATA['url']}/fslogin_hero_security_edt_phone_number", msisdn)
        $driver.back
        touch_button("#{DATA['url']}/fslogin_hero_security_btn_login")
        wait_pin
    end
end