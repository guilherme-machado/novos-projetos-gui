class HomePage < BasePage
    def clicar_popup
        wait_text('Bem-vindo ao Hero')
        touch_button("#{DATA['url']}/txt_action")
    end

    def validar_campos1
        wait("#{DATA['url']}/lyt_scan")
        exists("#{DATA['url']}/lyt_scan")
        exists("#{DATA['url']}/button_feature_action")
    end

    def validar_campos2
        exists_text('TRANSFER BY HERO')
        exists_text('Conexão Wi-Fi')
        exists_text('WI-FI SEGURO BY HERO')
        exists_text('NAVEGAÇÃO SEGURA')
        exists_text('Privacidade De Aplicativos')
    end

    def validacao_app
        validar_campos1
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)
        validar_campos2
        touch_text('Otimizador')
        exists("#{DATA['url']}/lyt_percent_text")
        touch_text('Cloud')
        exists_text('CLOUD BY HERO')
        touch_text('Família')
        exists_text('FAMÍLIA BY HERO')
        touch_text('Help Desk')
        exists_text('HELP DESK BY HERO')
        touch_button("#{DATA['url']}/img_settings")
        exists_text('Configurações')
    end

    def clicar_escudo
        wait("#{DATA['url']}/lyt_scan", '150')  
        touch_button("#{DATA['url']}/lyt_scan")
    end

    def nav_seguro
        @function_page = FunctionPage.new
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 0, end_y: 0, duration: 500)
        wait("#{DATA['url']}/switch_setting_opt")
        touch_button("#{DATA['url']}/switch_setting_opt")
        touch_button("#{DATA['url']}/txt_positive")
        @function_page.acessibilidade('Hero', 'Usar Hero?')
        wait_text('Em uma escala de 0 a 10...')
        touch_button("#{DATA['url']}/nps_close_button")
        exists_text('ATIVAR')
    end

    def incluir_bloq
        touch_text('NAVEGAÇÃO SEGURA')
        touch_button("#{DATA['url']}/lyt_blockcategory")
        categoria = find_elements("#{DATA['url']}/switch_block_status")
        categoria[5].click
    end

    def wifi_seguro
        wait("#{DATA['url']}/switch_secure_vpn")
        touch_button("#{DATA['url']}/switch_secure_vpn")
        wait("#{DATA['url']}/txt_dialog_header")
        touch_button("#{DATA['url']}/txt_positive")
        wait_text('Solicitação de conexão')
        touch_button('android:id/button1')
        wait_text('Em uma escala de 0 a 10...')
        touch_button("#{DATA['url']}/nps_close_button")
        exists_text('ATIVAR')
    end
end