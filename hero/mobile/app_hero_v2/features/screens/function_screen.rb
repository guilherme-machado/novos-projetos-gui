class FunctionPage < BasePage
    def baixar_app
        touch_button('Play Store')
        wait('com.android.vending:id/search_box_idle_text')
        touch_button('com.android.vending:id/search_box_idle_text')
        fill('com.android.vending:id/search_box_text_input', 'Test Virus')  
        $driver.press_keycode(66)
        touch_text('Test Virus')
        touch_text('INSTALAR')
        wait('com.android.vending:id/continue_button')
        touch_button('com.android.vending:id/continue_button')
        wait_text('ABRIR') 
    end

    def acessar_site(site)
        touch_button('Chrome')
        touch_button('com.android.chrome:id/search_box_text')
        fill('com.android.chrome:id/url_bar', site)
        $driver.press_keycode(66)
    end

    def acessibilidade(app, text)
        wait_text(app)
        touch_text(app)
        wait_text(app)
        touch_button('com.android.settings:id/switch_widget')
        wait_text(text)
        touch_button('android:id/button1')
        $driver.back
        $driver.back
    end
end