#language:pt

@monitoria_mobile_android_prod


Funcionalidade: Monitoria Mobile

Nós como um time mobile temos alguns serviços que precisam ser checados antes uma buid ser gerada ou mesmo para assegurar que podemos prosseguir com um desenvolvimento.
Por isso tivemos a iniciativa de criar uma automação na qual iremos iniciar as monitorias mobile , que serão separadas por IOS e Android.
Essas monitorias irão ser excutadas diariamente, antes e depois de um desenvolvimento mobile , ou antes e deplois de um deploy ou build mobile.


Esquema do Cenario: Apis Android

Dado Eu como qa , irei reunir as informações necessárias para o ambiente funcionar corretamente.
Quando a build for gerada, irei executar os testes para assegurar que o ambiente Android ou iOS está ok
Então eu farei as validações necessárias para que tudo ocorra nem ou não

Exemplos:   

|Operadora  |                 Custon_token                      |     MSISDN        |LA    |
|"vivo_prod"|"AIzaSyDyYWGcNbjWHv8SqbxQ5Qa6HGIDOkHHWQk"          |"5511951480432"    |"881" |
|"hero_prod"|"AIzaSyCtYde_Yz6b3-KYhFOpp6pdJCkyWI_54jY"          |"5511951480432"    |"5513"|
|"tim_prod" |"AIzaSyAWN1stNgS2GQ71SUb3ASd8BUs0ROYVquM"          |"5511951480432"    |"5513"|
|"oi_prod"  |"AIzaSyDGTy3PBqCbf6UBRluK5_Wa9aXepV4aH9c"          |"5511951480432"    |"3990"|
