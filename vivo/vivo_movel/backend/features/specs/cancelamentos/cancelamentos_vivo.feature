#language: pt

Funcionalidade: Cancelamento Vivo Móvel

    Como FS
    Quero que seja implementado o fluxo de cancelamento para clientes VIVO Móvel 
    Para quando recebermos uma notificação de cancelamento, seja gravado no nosso backend o cancelamento do produto


@cancelamento @cancelamentoProtege
Esquema do Cenário: Cancelamento - VIVO PROTEGE
    
    Dado que eu tenha contratado um plano VIVO PROTEGE
    Quando que eu faço uma solicitação para o serviço de cancelamento contendo os seguintes dados
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |    
    Então o código de resposta HTTP deve ser igual a "<status_code>"    
      
    Exemplos:
        | msisdn      | la  | keyword | status_code | 
        | 11972893010 | 881 | sair    | 200         |
               

@cancelamento @cancelamentoFilhosHomol 
Esquema do Cenário: Cancelamento - VIVO FILHOS ONLINE - HOMOL
    
    Dado que eu tenha contratado um plano VIVO FILHOS ONLINE
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    Quando que eu faço uma solicitação para o serviço de cancelamento
    Então o código de resposta HTTP deve ser igual a "<status_code>"
    E o produto QUSTODIO deverá ser cancelado com sucesso no nosso backend
    E o extref deverá ser cancelado com sucesso na QUSTODIO     
      
    Exemplos:
        | msisdn      | Tipo     | la   | keyword | status_code | numero_licencas | Plano                | ServiceID  | ProductID  |
        | 11972893001 | Pré      | 1418 | 3       | 200         | 3               | Vivo Filhos Online 3 | 0100315800 | 0055002332 |
        | 11972893001 | Pré      | 1418 | 5       | 200         | 5               | Vivo Filhos Online 5 | 0100315800 | 0055002333 |
        | 11972894001 | Pos      | 1418 | 3       | 200         | 3               | Vivo Filhos Online 3 | 0100315800 | 0055002332 |
        | 11972894001 | Pos      | 1418 | 5       | 200         | 5               | Vivo Filhos Online 5 | 0100315800 | 0055002333 |        
        | 11972893021 | Controle | 1418 | 3       | 200         | 3               | Vivo Filhos Online 3 | 0100315800 | 0055002332 |
        | 11972893021 | Controle | 1418 | 5       | 200         | 5               | Vivo Filhos Online 5 | 0100315800 | 0055002333 |


@cancelamento @cancelamentoFilhos
Esquema do Cenário: Cancelamento - VIVO FILHOS ONLINE
    
    Dado que eu tenha contratado um plano VIVO FILHOS ONLINE
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    Quando que eu faço uma solicitação para o serviço de cancelamento
    Então o código de resposta HTTP deve ser igual a "<status_code>"
    E o produto QUSTODIO deverá ser cancelado com sucesso no nosso backend
    E o extref deverá ser cancelado com sucesso na QUSTODIO     
      
    Exemplos:
        | msisdn      | Tipo     | la  | keyword | status_code | numero_licencas | Plano                | ServiceID  | ProductID  |
        | 11972893000 | Pré      | 877 | 3       | 200         | 3               | Vivo Filhos Online 3 | 0101048500 | 0055011029 |
        | 11972893000 | Pré      | 877 | 5       | 200         | 5               | Vivo Filhos Online 5 | 0101048500 | 0055011030 |