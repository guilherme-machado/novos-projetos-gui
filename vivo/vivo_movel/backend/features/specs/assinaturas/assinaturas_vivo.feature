#language: pt

Funcionalidade: Assinatura Vivo Móvel

    Como FS
    Quero que seja implementado o fluxo de assinatura para clientes VIVO Móvel 
    Para quando recebermos uma notificação de contratação, seja gravado no nosso backend a assinatura do produto


@assinatura @assinaturaProtege
Esquema do Cenário: Nova Assinatura - VIVO PROTEGE
    
    Dado que eu informe os seguintes dados:
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    Quando que eu faço uma solicitação para o serviço de assinaturas do vivo móvel
    Então o código de resposta HTTP deve ser igual a "<status_code>"
    E o <numero_licencas> do VIVO PROTEGE deve ser correspondente ao plano assinado
          
    Exemplos:
        | msisdn      | la  | keyword | status_code | numero_licencas | Plano                  | ServiceID  | ProductID  |
        | 11972893010 | 881 | 1       | 200         | 3               | Vivo Protege Voce Mais | 0100979200 | 0055008251 |
        | 11972893010 | 881 | 2       | 200         | 3               | Vivo Protege           | 0100979300 | 0055008252 |
        | 11972893010 | 881 | 3       | 200         | 5               | Vivo Protege Mais      | 0100979400 | 0055008253 |
        | 11972893010 | 881 | 4       | 200         | 10              | Vivo Protege Familia   | 0100979500 | 0055008254 |
        | 11972893010 | 881 | 3       | 200         | 5               | Vivo Protege Mais      | 0100979400 | 0055008253 |
        | 11972893010 | 881 | 2       | 200         | 3               | Vivo Protege           | 0100979300 | 0055008252 |
        | 11972893010 | 881 | 1       | 200         | 3               | Vivo Protege Voce Mais | 0100979200 | 0055008251 |
        

@assinatura @assinaturaFilhosHomol
Esquema do Cenário: Nova Assinatura - VIVO FILHOS ONLINE - HOMOL
    
    Dado que eu informe os seguintes dados:
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    Quando que eu faço uma solicitação para o serviço de assinaturas do vivo móvel
    Então o código de resposta HTTP deve ser igual a "<status_code>"
    E o produto QUSTODIO deverá ser assinado com sucesso no nosso backend    
    E o extref deverá ser provisionado com sucesso na QUSTODIO
    E o <numero_licencas> do VIVO FILHOS ONLINE deve ser correspondente ao plano assinado
      
    Exemplos:
        | msisdn      | Tipo     | la   | keyword | status_code | numero_licencas | Plano                | ServiceID  | ProductID  |
        | 11963977692 | Controle | 1418 | f3      | 200         | 3               | Vivo Filhos Online 3 | 0100315800 | 0055002332 |
        | 11963977692 | Controle | 1418 | f5      | 200         | 5               | Vivo Filhos Online 5 | 0100315800 | 0055002333 |
        | 11996401178 | Pré      | 1418 | filhos3 | 200         | 3               | Vivo Filhos Online 3 | 0100315800 | 0055002332 |
        | 11996401178 | Pré      | 1418 | filhos5 | 200         | 5               | Vivo Filhos Online 5 | 0100315800 | 0055002333 |
        | 11972893002 | Pos      | 1418 | filhos3 | 200         | 3               | Vivo Filhos Online 3 | 0100315800 | 0055002332 |
        | 11972893002 | Pos      | 1418 | filhos5 | 200         | 5               | Vivo Filhos Online 5 | 0100315800 | 0055002333 |        
        | 11963977692 | Controle | 1418 | filhos3 | 200         | 3               | Vivo Filhos Online 3 | 0100315800 | 0055002332 |
        | 11963977692 | Controle | 1418 | filhos5 | 200         | 5               | Vivo Filhos Online 5 | 0100315800 | 0055002333 |     
        

@assinatura @assinaturaFilhos
Esquema do Cenário: Nova Assinatura - VIVO FILHOS ONLINE
    
    Dado que eu informe os seguintes dados:
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    Quando que eu faço uma solicitação para o serviço de assinaturas do vivo móvel
    Então o código de resposta HTTP deve ser igual a "<status_code>"
    E o produto QUSTODIO deverá ser assinado com sucesso no nosso backend
    E o extref deverá ser provisionado com sucesso na QUSTODIO
    E o <numero_licencas> do VIVO FILHOS ONLINE deve ser correspondente ao plano assinado
      
    Exemplos:
        | msisdn      | Tipo     | la  | keyword | status_code | numero_licencas | Plano                | ServiceID  | ProductID  |
        | 11972893000 | Pré      | 877 | 3       | 200         | 3               | Vivo Filhos Online 3 | 0101048500 | 0055011029 |
        | 11972893000 | Pré      | 877 | 5       | 200         | 5               | Vivo Filhos Online 5 | 0101048500 | 0055011030 |