
def consultaPlanosAtivosVivoMovel(msisdn)

    endpoint = $api['consultaPlanoAtivo_vivo']

    HTTParty.get(
        "#{endpoint}#{msisdn}"
    )

end


def consultaPlanosCanceladosVivoMovel(msisdn)

    endpoint = $api['consultaPlanoCancelado_vivo']

    HTTParty.get(
        "#{endpoint}#{msisdn}"
    )

end


def consultaFirebase(msisdn)

    endpoint = $api['consultaFirebase']

    HTTParty.get(
        "#{endpoint}55#{msisdn}"
    )

end


def consultaExtrefQUSTODIO(extref)

    endpoint = $api['consultaExtrefQUSTODIO_vivo']

    HTTParty.get(
        "#{endpoint}#{extref}"
    )

end