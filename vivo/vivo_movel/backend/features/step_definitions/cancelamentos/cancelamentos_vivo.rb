Dado("que eu tenha contratado um plano VIVO FILHOS ONLINE") do |table|
  # assina
  @assinatura = table.rows_hash
  @resultAPI = assina_vivoMovel(@assinatura['msisdn'], @assinatura['la'], @assinatura['keyword'])
  expect(@resultAPI.response.code).to eql "200"
  @planoAtivo = consultaPlanosAtivosVivoMovel(@assinatura['msisdn'])
  # provisiona
  provisiona((@planoAtivo['data']['QUSTODIO'][0]['user_id']), (@planoAtivo['data']['QUSTODIO'][0]['produto_parceiro_id']))
  @planoAtivo = consultaPlanosAtivosVivoMovel(@assinatura['msisdn'])
   # consulta no provisionador o extref
  @resultQUSTODIO = consultaExtrefQUSTODIO(@planoAtivo['data']['QUSTODIO'][0]['chaves_extref'])
  expect(@resultQUSTODIO.response.code).to eql "200"
end

Quando("que eu faço uma solicitação para o serviço de cancelamento") do
  cancela_vivoMovel(@assinatura['msisdn'], @assinatura['la'], 'sair')
end

Então("o produto QUSTODIO deverá ser cancelado com sucesso no nosso backend") do
  # consulta plano cancelado
  @planoCancelado = consultaPlanosCanceladosVivoMovel(@assinatura['msisdn'])
  expect(@planoCancelado['data']['QUSTODIO'][0]['chaves_data_cancelamento']).not_to be_empty  
  
  puts " \n Produto cancelado com sucesso: #{@planoCancelado['data']['QUSTODIO'][0]['produto_parceiro_nome']}"
  puts " \n Data da compra: #{@planoCancelado['data']['QUSTODIO'][0]['chaves_datacompra']}"
  puts " \n Data do cancelamento: #{@planoCancelado['data']['QUSTODIO'][0]['chaves_data_cancelamento']}"
end

Então("o extref deverá ser cancelado com sucesso na QUSTODIO") do
  # consulta no provisionador o extref foi cancelado
  @resultQUSTODIO = consultaExtrefQUSTODIO(@planoCancelado['data']['QUSTODIO'][0]['chaves_extref'])
  expect(@resultQUSTODIO.response.code).to eql "200"
  expect(@resultQUSTODIO['data']['enabled']).to eql false
  puts " \n Status do Extref QUSTODIO no provisionador: enabled #{@resultQUSTODIO['data']['enabled']}"
end

