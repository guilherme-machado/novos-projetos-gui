  Dado("que eu informe os seguintes dados:") do |table|
    @assinatura = table.rows_hash
  end
  
  Quando("que eu faço uma solicitação para o serviço de assinaturas do vivo móvel") do
    @resultAPI = assina_vivoMovel(@assinatura['msisdn'], @assinatura['la'], @assinatura['keyword'])    
  end
  
  Então("o código de resposta HTTP deve ser igual a {string}") do |status_code|
    expect(@resultAPI.response.code).to eql status_code
    # consulta plano assinado
    sleep 3
    @planoAtivo = consultaPlanosAtivosVivoMovel(@assinatura['msisdn'])
  end
  

  # ******************************************FS VENDOR******************************************

  Então("o {int} do VIVO PROTEGE deve ser correspondente ao plano assinado") do |numero_licencas|
    expect(@planoAtivo['data']['FSVENDOR'][0]['produto_parceiro_tamanho']).to eql numero_licencas
    # cancela
    cancela_vivoMovel(@assinatura['msisdn'], @assinatura['la'], 'sair')
  end
    

  # ******************************************QUSTODIO*******************************************


  Então("o produto QUSTODIO deverá ser assinado com sucesso no nosso backend") do    
    expect(@planoAtivo['data']['QUSTODIO'][0]['chaves_datacompra']).not_to be_empty      
    puts " \n Produto assinado com sucesso: #{@planoAtivo['data']['QUSTODIO'][0]['produto_parceiro_nome']}"
    puts " \n Data da compra: #{@planoAtivo['data']['QUSTODIO'][0]['chaves_datacompra']}"    
  end   

  Então("o extref deverá ser provisionado com sucesso na QUSTODIO") do
    # provisiona
    provisiona((@planoAtivo['data']['QUSTODIO'][0]['user_id']), (@planoAtivo['data']['QUSTODIO'][0]['produto_parceiro_id']))
    @planoAtivo = consultaPlanosAtivosVivoMovel(@assinatura['msisdn'])
    puts " \n Extref QUSTODIO: #{@planoAtivo['data']['QUSTODIO'][0]['chaves_extref']}"
    # consulta no provisionador o extref
    @resultQUSTODIO = consultaExtrefQUSTODIO(@planoAtivo['data']['QUSTODIO'][0]['chaves_extref'])
    expect(@resultQUSTODIO.response.code).to eql "200"
    expect(@resultQUSTODIO['data']['enabled']).to eql true
    puts " \n Status do Extref QUSTODIO no provisionador: enabled #{@resultQUSTODIO['data']['enabled']}, return #{@resultQUSTODIO['return']}"    
  end

  Então("o {int} do VIVO FILHOS ONLINE deve ser correspondente ao plano assinado") do |numero_licencas|
    expect(@planoAtivo['data']['QUSTODIO'][0]['produto_parceiro_tamanho']).to eql numero_licencas
    puts " \n Numero de Licenças: #{@planoAtivo['data']['QUSTODIO'][0]['produto_parceiro_tamanho']}"
    # cancela
    cancela_vivoMovel(@assinatura['msisdn'], @assinatura['la'], 'sair') 
  end

  
  
  

  