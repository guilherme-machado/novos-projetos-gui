require 'httparty'
require 'capybara/cucumber'
require 'rspec'
require 'capybara'

require "faker"
require 'selenium-webdriver'
require 'securerandom'



$profile = ENV['PROFILE']
api_configs = YAML.load_file('./features/support/api.yml')
$api = api_configs[$profile]