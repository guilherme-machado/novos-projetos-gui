#language:pt

@callcenter

Funcionalidade: Login no callceter 
O Callcenter é uma ferramenta poderosa, de consulta para as operadoras com ele o operador pode facilmente consultar o histórico de compras 
de um cliente , a função deste teste é garantir a integridade do serviço prestado pela Fs para as operadoras, estar em pleno funionamento.

@logged

Esquema do Cenário: Àrea logada

Dado que eu esteja logado
Quando eu estiver logado irei validar a área logada com minha base de <msisdn>
Então irei validar a área logada e seus conteudos 

Exemplos:

|    msisdn   |
|"11942592333"|
|"11998289443"|
|"11974939531"|
|"11956516148"|
|"11971465207"|
|"11941179314"|
|"11969043678"|
|"11963124653"|
|"11989908903"|
|"11993449611"|
|"11965802537"|
|"11988257255"|
|"11989106144"|
|"11969995870"|
|"11962303076"|
|"11980340047"|
|"11967638131"|
|"11962544109"|
|"11970192127"|
|"11977977085"|
|"11963436854"|

@invalid_login
Esquema do Cenário: Login_vivo invalido

Dado que eu como Q.A preciso testar login do Callcenter.
Quando eu possuo os dados de <user> e a <senha> e os <textos> que serão usados na validação.
Entao sendo assim eu farei as validações necessárias validar o teste de longin invalido  

Exemplos: 

|    user   |                                        senha                                       |                   textos                   |
|  "churle" |                                     "lovedogsz"                                    | "Erro! O login churle não foi encontrado." |
|   "vivo"  |                                   "   cwdfw    "                                   |          "Erro! Senha incorreta."          |
|"Chales_QA"|                            "   edfgsr&@#*&#&*%!@!1    "                            |"Erro! O login Chales_QA não foi encontrado"|
|   "vivo"  |                                "   @*$*&$@#%87    "                                |          "Erro! Senha incorreta."          |
