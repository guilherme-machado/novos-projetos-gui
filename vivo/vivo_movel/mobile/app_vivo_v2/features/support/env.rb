require 'rspec/expectations'
require 'appium_lib'
require 'appium_console'
require 'rspec'
require 'httparty'
require 'pry'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'faker'
require 'cpf_faker'



Capybara.default_driver = :selenium
Capybara.default_max_wait_time = 60

World(Capybara::DSL)
World(Capybara::RSpecMatchers)


# Create a custom World class so we don't pollute `Object` with Appium methods
class AppiumWorld
end

# Load the desired configuration from appium.txt, create a driver then
# Add the methods to the world
caps_path = File.join(File.dirname(__FILE__), '..', '..', 'caps', 'appium_vivo.txt')
caps = Appium.load_appium_txt file: caps_path, verbose: true
Appium::Driver.new(caps, true)
Appium.promote_appium_methods AppiumWorld

World do
  AppiumWorld.new
end

Before { $driver.start_driver }
After { $driver.driver_quit }




ENV_TYPE = ENV['ENV_TYPE']
BROWSER = ENV['BROWSER']
DATA = YAML.load_file(File.dirname(__FILE__) + "/#{ENV_TYPE}.yaml") 