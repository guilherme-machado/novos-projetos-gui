#encoding:utf-8

Dado("que eu como Q.A esteja logado no app unico Vivo.") do
   @email = Faker::Internet.email
   @text_nota = Faker::Lorem.paragraph(2)
   @page= Nps.new
   @base= BasePage.new
   @msisdn = "11957141535"
  end
  
  Quando("eu estiver na home, entao eu irei ativar o escanemento e aguardá-lo terminar , então o nps ira aparecer") do
    @page.login_vivo
  end
  
  Então("eu contruirei com a minha alteração e farei as validações necessárias") do
    @page.escaneamento_completo_nps(@email, @text_nota)
  end
  
  Quando("eu estiver na home , então eu irei ativar Wi-fi e assim que a ação ser concluída") do
    @page.login_vivo
    @page.wifi_seguro(@email, @text_nota)
    

  end
  
  Então("farei as validações necessárias e o NPS deverá abrir com sucesso") do
    puts @email
    puts @text_nota
  end

  
  Quando("eu estiver na home , então eu irei ativar Navegação Segura e assim que a ação ser concluída") do
    @page.login_vivo
    @page.navegacao_segura
  end


  # Quando("eu estiver na home , então eu irei clicar nas atividades do  menu e assim que a ação ser concluída") do
  #   @page.login_vivo
  #   @page.privacidade_de_aplicativos(@email, @text_nota)
  # end
  
  
  Dado("que eu como Q.A estou no logado , realizo uma interação no NPS") do
    @page= Nps.new
    @base= BasePage.new
    
  end
  
  Quando("o NPS é aberto eu fecho ele , sem gerar uma nota") do
    @page.login_vivo
  end
  
  Então("Eu farei as validações necessárias para garantir que a nota menos um foi gerada no board") do
    @page.nota_1
    
  end

  Dado("que eu como Q.A estou logado, realizo uma interação então meu NPS abre.") do
    @page= Nps.new
    @base= BasePage.new
  end
    
  Quando("isso acontece eu fecho a pesquisa NPS, Altero a data do meu dispositivo para {int} dias a frente, abro novamente meu aplicativos") do |int|
    @page.login_vivo
    @page.navegacao_segura
     end
  
  Então("eu farei as validações necessárias para que o nps possa ser revalidado em D{int}.") do |int|
    @page.d60
  end  
  
  Dado("que eu como Q.A estou logado, realizo uma intração no NPS") do
    @page= Nps.new
    @base= BasePage.new
    @email = Faker::Internet.email
    @text_nota = Faker::Lorem.paragraph(2)
  end
  
  Quando("o NPS é aberto então eu qualifico com uma nota de {int} a {int}") do |int, int2|
    @page.login_vivo
  
  end
  
  Então("eu farei as validaçoes necessárias para meu report ser enviado para {string}") do |remetente|
    @page.nota_det(@email, @text_nota)
  end
  
  Dado("que eu como Q.A estou logado, realizo uma interação no nps") do
    @page= Nps.new
    @base= BasePage.new
    @email = Faker::Internet.email
    @text_nota = Faker::Lorem.paragraph(2)
  end
  
  Quando("eu realizo uma interação detratora") do
    @page.login_vivo
   
  end
  
  Então("me certifico de que os seviçõs EMAIL Whatsapp e Chat estão clicáveis com seus links certos.") do 
    @page.clique(@email, @text_nota)
  end
  
  Dado("eu como Q.A estou logado , realizo uma interação com NPS") do
    @page= Nps.new
    @base= BasePage.new
    @text_nota = Faker::Lorem.paragraph(3)
  end
  
  Quando("eu qualifico com uma nota {string} ou {string} e prossigo") do |string, string2|
    @page.login_vivo
  end
  
  Então("eu recebo uma notificação para avaliação na loja") do
   @page.avaliação_playstore(@text_nota)
  end
  
  # # Dado("que eu como Q.A Fiz algumas interações no NPS") do
  # #   @page= Nps.new
  # #   @base= BasePage.new
  # # end
  
  # # Quando("essas interações de {string} {string} {string}") do |nota, email, descrição|
  # #   @nota = nota
  # #   @email = email
  # #   @descrição = descrição
  # #   @page.login_vivo
  # #   @base.clicar_popup
  # #   @page.nota_one_to_ten(@nota, @email, @descrição)
  # # end
  
  # # Então("farei as validações necessárias para testar o mesmo") do
  # #  puts @nota
  # #  puts @email
  # #  puts @descrição
  # # end
  
  Dado("que eu como Q.A estou logado na home e faço uma interação") do
    @page= Nps.new
    @base= BasePage.new
  end
  
  Quando("eu faço a interação , o NPS abre, eu ignoro e fecho o aplicativo") do
    @page.login_vivo
    
  end
  
  Então("eu abro o mesmo novamente para me certificar que o mesmo não abrirá") do
   
    @page.acao_memorizada
  end

  