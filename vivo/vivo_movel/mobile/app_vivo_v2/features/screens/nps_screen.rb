class Nps < BasePage
    
    def wait_pin
        @timeout = 120
        sentPin = 0
        wait("#{DATA['end_point']}/submit_confirmation_code")
          
          @start_time = Time.now
                    
    while   @timeout > sentPin
            @end_time = Time.now
            
    if      return_text("#{DATA['end_point']}/confirmation_code") != "- - - - - -"
            sentPin = timer(@start_time, @end_time)
            found = "sucesso"
            break
    elsif    exists("#{DATA['end_point']}/resend_code")
            touch_button("#{DATA['end_point']}/resend_code")
                
    else
            sentPin = timer(@start_time, @end_time)
            found = "falha"
            end
        end
     end
    
     def login_vivo
        touch_button("#{DATA['end_point']}/btn_mobile_login")
        fill("#{DATA['end_point']}/et_phone_number", msisdn)
        $driver.back
        touch_button("#{DATA['end_point']}/btn_login")
        wait_pin
    end
    

    def escaneamento_completo_nps(email,text_nota)
        touch_text('VERIFICAR VÍRUS')
        wait("#{DATA['end_point']}/txt_threat_title", 150 )
        $driver.back
        touch_button("#{DATA['end_point']}/nps_radio_eight")
        touch_button("#{DATA['end_point']}/nps_vote_button")
        email = find("#{DATA['end_point']}/nps_form_email_edit_text")
        email.send_key email 
        $driver.back
        text_message = find("#{DATA['end_point']}/nps_form_comment_edit_text")
        text_message.send_key text_nota
        $driver.back
        texto =  exists_text('votar')
        touch_button("#{DATA['end_point']}/nps_form_send_button")
    end 

    def wifi_seguro(email, text_nota)
        sleep 3
        touch_button("#{DATA['end_point']}/switch_secure_vpn")
        wait_text('OK')
        touch_text('OK')
        wait("#{DATA['end_point']}/nps_vote_button")
        touch_button("#{DATA['end_point']}/nps_radio_seven")
        texto1 =  exists_text('votar')
        touch_button("#{DATA['end_point']}/nps_vote_button")
        email = find("#{DATA['end_point']}/nps_form_email_edit_text")
        email.send_key email 
        $driver.back
        text_message = find("#{DATA['end_point']}/nps_form_comment_edit_text")
        text_message.type text_nota
        $driver.back
        texto2 =  exists_text('enviar')
        touch_button("#{DATA['end_point']}/nps_form_send_button")
        texto3 =  exists_text('E-MAIL')
        texto4 =  exists_text('WHATSAPP')
        texto5 =  exists_text('CHAT')
        touch_button("#{DATA['end_point']}/nps_close_button")
    end

    

    def navegacao_segura
        sleep 3
        touch_button("#{DATA['end_point']}/switch_safe_browsing")
        texto1= exists_text('Selecione "Continuar" para abrir as configurações do seu aparelho. Depois, ative o "Vivo Protege" em "Modo de Acessibilidade".')
        touch_button("#{DATA['end_point']}/txt_positive")
        wait_text('Vivo Protege')
        touch_text('Vivo Protege')
        touch_button("com.android.settings:id/switch_widget")
        touch_button("android:id/button1")
        $driver.back
        $driver.back
        touch_button("#{DATA['end_point']}/nps_radio_ten")
        touch_button("#{DATA['end_point']}/nps_vote_button")
        touch_button("#{DATA['end_point']}/nps_google_play_button")
        $driver.back
    end

    def nota_1
        sleep 3
        touch_button("#{DATA['end_point']}/switch_safe_browsing")
        texto1= exists_text('Selecione "Continuar" para abrir as configurações do seu aparelho. Depois, ative o "Vivo Protege" em "Modo de Acessibilidade".')
        touch_button("#{DATA['end_point']}/txt_positive")
        wait_text('Vivo Protege')
        touch_text('Vivo Protege')
        touch_button("com.android.settings:id/switch_widget")
        touch_button("android:id/button1")
        $driver.back
        $driver.back
        touch_button("#{DATA['end_point']}/nps_close_button")

    end

    def nota_det(email, text_nota)
        sleep 3
        touch_button("#{DATA['end_point']}/switch_safe_browsing")
        texto1= exists_text('Selecione "Continuar" para abrir as configurações do seu aparelho. Depois, ative o "Vivo Protege" em "Modo de Acessibilidade".')
        touch_button("#{DATA['end_point']}/txt_positive")
        wait_text('Vivo Protege')
        touch_text('Vivo Protege')
        touch_button("com.android.settings:id/switch_widget")
        touch_button("android:id/button1")
        $driver.back
        $driver.back
        touch_button("#{DATA['end_point']}/nps_radio_three")
        touch_button("#{DATA['end_point']}/nps_vote_button")
        email = find("#{DATA['end_point']}/nps_form_email_edit_text")
        email.send_key email 
        $driver.back
        text_message = find("#{DATA['end_point']}/nps_form_comment_edit_text")
        text_message.send_key text_nota
        $driver.back
        texto2 =  exists_text('enviar')
        touch_button("#{DATA['end_point']}/nps_form_send_button")
        texto3 =  exists_text('E-MAIL')
        texto4 =  exists_text('WHATSAPP')
        texto5 =  exists_text('CHAT')
        touch_text('E-MAIL')
        touch_text('Gmail')
        fill('com.google.android.gm:id/composearea_tap_trap_bottom', "#{text_nota}") 
        $driver.back
        touch_button('com.google.android.gm:id/send')
    end 



    def clique(email, text_nota)
        sleep 3
        touch_button("#{DATA['end_point']}/switch_safe_browsing")
        texto1= exists_text('Selecione "Continuar" para abrir as configurações do seu aparelho. Depois, ative o "Vivo Protege" em "Modo de Acessibilidade".')
        touch_button("#{DATA['end_point']}/txt_positive")
        wait_text('Vivo Protege')
        touch_text('Vivo Protege')
        touch_button("com.android.settings:id/switch_widget")
        touch_button("android:id/button1")
        $driver.back
        $driver.back
        wait("#{DATA['end_point']}/nps_vote_button")
        touch_button("#{DATA['end_point']}/nps_radio_seven")
        texto1 =  exists_text('votar')
        touch_button("#{DATA['end_point']}/nps_vote_button")
        email = find("#{DATA['end_point']}/nps_form_email_edit_text")
        email.send_key email 
        $driver.back
        text_message = find("#{DATA['end_point']}/nps_form_comment_edit_text")
        text_message.send_key text_nota
        $driver.back
        texto2 =  exists_text('enviar')
        touch_button("#{DATA['end_point']}/nps_form_send_button")
        texto3 =  exists_text('E-MAIL')
        texto4 =  exists_text('WHATSAPP')
        texto5 =  exists_text('CHAT')
        touch_button("#{DATA['end_point']}/nps_close_button")
    end 
    

    def avaliação_playstore(text_nota)
        sleep 3
        touch_button("#{DATA['end_point']}/switch_safe_browsing")
        texto1= exists_text('Selecione "Continuar" para abrir as configurações do seu aparelho. Depois, ative o "Vivo Protege" em "Modo de Acessibilidade".')
        touch_button("#{DATA['end_point']}/txt_positive")
        wait_text('Vivo Protege')
        touch_text('Vivo Protege')
        touch_button("com.android.settings:id/switch_widget")
        touch_button("android:id/button1")
        $driver.back
        $driver.back
        touch_button("#{DATA['end_point']}/nps_radio_ten")
        touch_button("#{DATA['end_point']}/nps_vote_button")
        touch_button("#{DATA['end_point']}/nps_google_play_button")
        wait_text('Vivo Protege')
        exists_text('Vivo Protege')
       end

    def d60
        $driver.press_keycode(3)
        touch_button('Apps')
        exists_text('Configurar')
        touch_text('Configurar')
        sleep 1
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)
        exists_text('Data e hora')
        sleep 1
        touch_text('Data e hora')
        touch_text('Data e hora automáticas')
        touch_text('Definir data')
        touch_button('android:id/next')
        touch_button('android:id/next')
        touch_button('android:id/next')
        touch_text('1')
        touch_text('OK')
        $driver.back
        $driver.back
        $driver.swipe(start_x: 250, start_y: 750, end_x: 250, end_y: 5, duration: 500) 
        touch_text('Vivo Protege')
        exists_text('Qual a chance de recomendar o VIVO PROTEGE para um amigo ou colega?')
        $driver.press_keycode(3)
        touch_button('Apps')
        exists_text('Configurar')
        touch_text('Configurar')
        sleep 1
        $driver.swipe(start_x: 250, start_y: 1500, end_x: 250, end_y: 5, duration: 500)
        exists_text('Data e hora')
        sleep 1
        exists_text('Data e hora')
        touch_text('Data e hora')
        touch_text('Data e hora automáticas')
        $driver.back
        $driver.back
        $driver.back
    end

    def acao_memorizada
        $driver.press_keycode(3)
        wait_text('Vivo Protege')
        touch_text('Vivo Protege')
    end
  
    
end    
