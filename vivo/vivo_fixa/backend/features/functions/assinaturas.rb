def assina_vivoFixa(assinatura)

    endpoint = $api['assina']
    
    HTTParty.post(
        endpoint,
        headers: {'Content-Type' => 'application/json'},
        body: assinatura.to_json
    )
end


def assinaUMproduto_vivoFixa()

    assinatura = { 
        "msisdn" => "11972893010", 
        "cpf" => "72812727748", 
        "email" => "testesqa4@gmail.com", 
        "produto" => "GVT Protege EMPRESAS", 
        "purchaseOrderNumber" => "1234", 
        "serviceId" => "1234" 
    }

    HTTParty.post(
        endpoint,
        headers: {'Content-Type' => 'application/json'},
        body: assinatura.to_json
    )
end