def consultaPlanoAssinado(cpf)

    endpoint = $api['consultaPlanoAssinado']

    @result = HTTParty.get(
        "#{endpoint}#{cpf}?XDEBUG_SESSION_START=GVTBACKEND"
    )
    
end


def consultaPlanoCancelado(cpf)

    endpoint = $api['consultaPlanoCancelado']
    
    HTTParty.get(
        "#{endpoint}#{cpf}?XDEBUG_SESSION_START=GVTBACKEND"
    )

end


def consultaFirebase(msisdn)
    
    endpoint = $api['consultaFirebase']

    HTTParty.get(
        "#{endpoint}55#{msisdn}"
    )

end