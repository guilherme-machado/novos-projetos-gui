def cancela_vivoFixa(purchaseOrderNumber, serviceId)
    
    endpoint = $api['cancela']
    @dadosParaCancelar = { "purchaseOrderNumber" => purchaseOrderNumber, "serviceId" => serviceId }

    @result = HTTParty.post(
        endpoint,
        headers: {'Content-Type' => 'application/json'},
        body: @dadosParaCancelar.to_json
    )

end