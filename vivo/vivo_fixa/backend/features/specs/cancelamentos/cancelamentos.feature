#language: pt

Funcionalidade: Cancelamento Vivo Fixa

    Sendo uma aplicação
    Posso solicitar o endpoint de cancelamento da vivo_fixa
    Para cancelar um produto


@cancela
Esquema do Cenário: Cancelar Assinatura

    Dado que eu tenha um plano contratado no vivo fixa:
        | msisdn              | <msisdn>              |
        | cpf                 | <cpf>                 |
        | email               | <email>               |
        | produto             | <produto>             |
        | purchaseOrderNumber | <purchaseOrderNumber> |
        | serviceId           | <serviceId>           |           
    Quando que eu faço uma solicitação para o serviço de cancelamento do vivo fixa
    Então o código de resposta HTTP deve ser igual a "<status_code>"
    E o produto FSVENDOR deverá ser cancelado com sucesso no nosso backend
    
    Exemplos:
        | msisdn      | cpf         | email               | produto              | purchaseOrderNumber | serviceId       | status_code | numero_licencas |
        | 11972893010 | 72812727748 | testesqa4@gmail.com | GVT Protege EMPRESAS | 1234                | 1234            | 200         | 10              |