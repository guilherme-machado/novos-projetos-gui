#language: pt


Funcionalidade: Assinatura Vivo Fixa

    Sendo uma aplicação
    Posso solicitar o endpoint de assinaturas da vivo_fixa
    Para contratar um produto

@assina @gvt
Esquema do Cenário: Nova Assinatura
    
    Dado que eu informe os seguintes dados:
        | msisdn              | <msisdn>              |
        | cpf                 | <cpf>                 |
        | email               | <email>               |
        | produto             | <produto>             |
        | purchaseOrderNumber | <purchaseOrderNumber> |
        | serviceId           | <serviceId>           |        
    Quando que eu faço uma solicitação para o serviço de assinaturas do vivo fixa
    Então o código de resposta HTTP deve ser igual a "<status_code>"
    E o produto FSVENDOR deverá ser assinado com sucesso no nosso backend
   
    Exemplos:
        | msisdn      | cpf         | email               | produto                | purchaseOrderNumber | serviceId       | status_code | numero_licencas |
        | 11972893010 | 72812727748 | testesqa4@gmail.com | Vivo Protege Light B2B | 1234                | 1234            | 200         | 1               |
        | 11972893010 | 72812727748 | testesqa4@gmail.com | GVT Protege Voce       | 1234                | 1234            | 200         | 1               |
        | 11972893010 | 72812727748 | testesqa4@gmail.com | GVT Protege VOCE +     | 1234                | 1234            | 200         | 3               |
        | 11972893010 | 72812727748 | testesqa4@gmail.com | GVT Protege FAMILIA +  | 1234                | 1234            | 200         | 5               |
        | 11972893010 | 72812727748 | testesqa4@gmail.com | GVT Protege EMPRESAS   | 1234                | 1234            | 200         | 10              |
