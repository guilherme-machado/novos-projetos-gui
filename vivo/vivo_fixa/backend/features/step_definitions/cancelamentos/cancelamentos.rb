  Dado("que eu tenha um plano contratado no vivo fixa:") do |table|
    @assinatura = table.rows_hash
    assina_vivoFixa(@assinatura)
  end

  Quando("que eu faço uma solicitação para o serviço de cancelamento do vivo fixa") do
    cancela_vivoFixa(@assinatura['purchaseOrderNumber'], @assinatura['serviceId'])
  end

  Então("o produto FSVENDOR deverá ser cancelado com sucesso no nosso backend") do
    @consultaPlano = consultaPlanoCancelado(@assinatura['cpf'])
    expect(@consultaPlano['data']['FSVENDOR'][0]['chaves_data_cancelamento']).not_to be_empty 

    puts " \n Produto cancelado com sucesso: #{@consultaPlano['data']['FSVENDOR'][0]['produto_parceiro_nome']}"
    puts " \n Data da compra: #{@consultaPlano['data']['FSVENDOR'][0]['chaves_datacompra']}"
    puts " \n Data do cancelamento: #{@consultaPlano['data']['FSVENDOR'][0]['chaves_data_cancelamento']}"
  end