  Dado("que eu informe os seguintes dados:") do |table|
    @assinatura = table.rows_hash    
  end

  Quando("que eu faço uma solicitação para o serviço de assinaturas do vivo fixa") do
    cancela_vivoFixa(@assinatura['purchaseOrderNumber'], @assinatura['serviceId'])    
    @result = assina_vivoFixa(@assinatura)
  end

  Então("o código de resposta HTTP deve ser igual a {string}") do |status_code|    
    expect(@result.response.code).to eql status_code    
  end

  Então("o produto FSVENDOR deverá ser assinado com sucesso no nosso backend") do
    @consultaPlano = consultaPlanoAssinado(@assinatura['cpf'])
    expect(@consultaPlano['data']['FSVENDOR'][0]['chaves_datacompra']).not_to be_empty      
    puts " \n Produto assinado com sucesso: #{@consultaPlano['data']['FSVENDOR'][0]['produto_parceiro_nome']}"
    puts " \n Data da compra: #{@consultaPlano['data']['FSVENDOR'][0]['chaves_datacompra']}"
  end