require 'httparty'
require 'selenium-webdriver'
require 'capybara'
require 'capybara/cucumber'
require 'securerandom'
require "faker"



$profile = ENV['PROFILE']
api_configs = YAML.load_file('./features/support/api.yml')
$api = api_configs[$profile]