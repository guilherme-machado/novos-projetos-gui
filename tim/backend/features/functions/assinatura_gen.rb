def assinatura_ensina(txId, service_id, kw, msisdns, subscription_id)

    body_mensal = {
        "SubscriptionNotificationRequest"       =>{   
            "txId"                               => txId,
            "serviceId"                          => service_id,
            "msisdn"                             => msisdns,
            "subscriptionId"                     => subscription_id,
            "applicationId"                      => kw, 
            "origin"                             => "ok"
        }
    }.to_json

    # puts "\n\nImprimindo Api\n\n"
    # puts ($api['assina_tim_ensina'])

    @post_tim = HTTParty.post($api['assina_tim_ensina'],:headers => {"Content-Type" => 'application/json'}, :body => body_mensal)
  
 	# puts @post_tim.body
  	case @post_tim.code
    		when 200
      	puts "Retornou 200, ok"
    		when 404
      	puts "Retornou 404, não existe contratado com os parametros corretos"
    		when 400
      	puts "Retornou 400, problema de negócio"
    		when 500...600
      	puts "ops #{@post_tim.code}"
    end

end
