
def cancela_Tim(assinatura)
    
    endpoint = $api['assinaEcancela_tim']

    body = {"UnsubscriptionNotificationRequest": assinatura}
    puts "\nPAYLOAD DE CANCELAMENTO\n #{body}"
  
    HTTParty.post(
        endpoint,
        headers: {'Content-Type' => 'application/json'},
        body: body.to_json
    )

end