  Dado("que eu tenha contratado um plano Tim") do |table|
    @assinatura = table.rows_hash
    @assinatura['txId'] = "#{@assinatura['txId']}#{SecureRandom.uuid}"
    @assinatura['subscriptionId'] = "#{SecureRandom.uuid}"
    assina_Tim(@assinatura)
    sleep 2
  end
  
  Quando("que eu faço uma solicitação para o serviço de cancelamento") do
    @result = cancela_Tim(@assinatura)
    sleep 2
    puts "\n\nRESULTADO DO CANCELAMENTO\n #{@result}"
  end
  