# encoding: utf-8
require 'mysql2'
require_relative '../functions/consulta_assinatura'

#Assinatura do plano Tim Ensina Mensal
Dado("que tenho os parametros unicos {string} e {string}.") do |service_id, kw|
    puts "\n\nINICIANDO PROCESSO DE ASSINATURA DO TIM ENSINA MENSAL\n\n"
    @db              = Consulta.new
    @msisdns         = Faker::Base.numerify('55119########')
    @subscription_id = Faker::Base.numerify('########-####-####-####-############')
    @txId            = Faker::Base.numerify('########-####-####-####-############')
    @kw              = kw
    @service_id      = service_id
    
end

Quando("reunido as informacoes corretas, será efetuada a assinatura do plano mensal") do
    assinatura_ensina(@txId, @service_id, @kw, @msisdns, @subscription_id)
    expect(@post_tim.code).to eq 200
    puts "\n\nImprimindo Api\n\n"
    puts ($api['assina_tim_ensina'])

    puts "\n\n Dados da Contratação \n\n"
    puts "Txt ID: #{@txId} \n"
    puts "Service ID: #{@service_id} \n"
    puts "Msisdn: #{@msisdns} \n"
    puts "Subscription ID: #{@subscription_id} \n"
    puts "Aplication ID: #{@kw} \n"
    
end
  
Então("validamos a contratacao da assinatura mensal com os dados {string}, {string} e {string}") do |id_produto, nome_produto, valor_produto|
    
    puts "\n\nValidação da Contrataçao Mensal\n\n"
    result = @db.mysql(@db.select(@msisdns[2..@msisdns.size])).to_a
    iProduto = "ID do Produto: #{result[0][0]} \n"
    nProduto = "Nome do Produto: #{result[0][1]} \n"
    vProduto = "Valor do Produto: #{result[0][2]} \n"

    expect(id_produto).to eq result[0][0].to_s
    expect(nome_produto).to eq result[0][1].to_s
    expect(valor_produto).to eq result[0][2].to_s

    puts iProduto
    puts nProduto
    puts vProduto
    puts "\n"

end


#Assinatura do plano Tim Ensina Semanal
Dado("que tenho os parametros unicos {string} e {string}") do |service_id_s, kw_s|
    puts "\n\nINICIANDO PROCESSO DE ASSINATURA DO TIM ENSINA SEMANAL\n\n"
    @db              = Consulta.new
    @msisdns         = Faker::Base.numerify('55119########')
    @subscription_id = Faker::Base.numerify('########-####-####-####-############')
    @txId            = Faker::Base.numerify('########-####-####-####-############')
    @kw              = kw_s
    @service_id      = service_id_s
    
end

Quando("reunido as informacoes corretas, será efetuada a assinatura do plano semanal") do
    assinatura_ensina(@txId, @service_id, @kw, @msisdns, @subscription_id)
    expect(@post_tim.code).to eq 200
    puts "\n\nImprimindo Api\n\n"
    puts ($api['assina_tim_ensina'])

    puts "\n\n Dados da Contratação \n\n"
    puts "Txt ID: #{@txId} \n"
    puts "Service ID: #{@service_id} \n"
    puts "Msisdn: #{@msisdns} \n"
    puts "Subscription ID: #{@subscription_id} \n"
    puts "Aplication ID: #{@kw} \n"
    
end
  
Então("validamos a contratacao da assinatura semanal com os dados {string}, {string} e {string}") do |id_produto_s, nome_produto_s, valor_produto_s|
    
    puts "\n\nValidação da Contrataçao Semanal\n\n"
    result_s = @db.mysql(@db.select(@msisdns[2..@msisdns.size])).to_a
    iProduto_s = "ID do Produto: #{result_s[0][0]} \n"
    nProduto_s = "Nome do Produto: #{result_s[0][1]} \n"
    vProduto_s = "Valor do Produto: #{result_s[0][2]} \n"

    expect(id_produto_s).to eq result_s[0][0].to_s
    expect(nome_produto_s).to eq result_s[0][1].to_s
    expect(valor_produto_s).to eq result_s[0][2].to_s

    puts iProduto_s
    puts nProduto_s
    puts vProduto_s
    puts "\n"

end
