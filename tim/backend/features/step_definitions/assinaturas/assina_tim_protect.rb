# encoding: utf-8
require 'mysql2'
require_relative '../functions/consulta_assinatura_prot'

#Assinatura do plano Tim Protect Essencial
Dado("que tenho os parametros unicos {string} e {string} do plano essencial.") do |service_id_e, kw_e|
    puts "\n\nINICIANDO PROCESSO DE ASSINATURA DO TIM PROTECT ESSENCIAL\n\n"
    @db              = Consulta.new
    @msisdns         = Faker::Base.numerify('55119########')
    # @msisdns         = '5511943618888'
    @subscription_id = Faker::Base.numerify('########-####-####-####-############')
    @txId            = Faker::Base.numerify('########-####-####-####-############')
    @kw              = kw_e
    @service_id      = service_id_e
    
end

Quando("reunido as informacoes corretas, sera efetuada a assinatura do plano essencial.") do
    assinatura_protect(@txId, @service_id, @kw, @msisdns, @subscription_id)
    expect(@post_tim_protect.code).to eq 200
    puts "\n\nImprimindo Api\n\n"
    puts ($api['assina_tim_protect'])

    puts "\n\n Dados da Contratação \n\n"
    puts "Txt ID: #{@txId} \n"
    puts "Service ID: #{@service_id} \n"
    puts "Msisdn: #{@msisdns} \n"
    puts "Subscription ID: #{@subscription_id} \n"
    puts "Aplication ID: #{@kw} \n"
end

Então("validamos a contratacao da assinatura do Tim Protect Essencial, com os dados {string}, {string} e {string}.") do |id_produto_e, nome_produto_e, valor_produto_e|
    puts "\n\nValidação da Contrataçao Tim Protect Essencial\n\n"
    result_e = @db.mysql(@db.select(@msisdns[2..@msisdns.size])).to_a
    
    for i in (0..(result_e.size-1))
        # puts result_e[i][0].class
        # puts id_produto_e.class
            
        if result_e[i][0] == id_produto_e.to_i
            iProduto_e = "ID do Produto: #{result_e[i][0]} \n"
            nProduto_e = "Nome do Produto: #{result_e[i][1]} \n"
            vProduto_e = "Valor do Produto: #{result_e[i][2]} \n"

            expect(id_produto_e).to eq result_e[i][0].to_s
            expect(nome_produto_e).to eq result_e[i][1].to_s
            expect(valor_produto_e).to eq result_e[i][2].to_s

            puts iProduto_e
            puts nProduto_e
            puts vProduto_e
            puts "\n"
        end 
    end
end


#Assinatura do plano Tim Protect Familia
Dado("que tenho os parametros unicos {string} e {string} do plano familia.") do |service_id_f, kw_f|
    puts "\n\nINICIANDO PROCESSO DE ASSINATURA DO TIM PROTECT FAMILIA\n\n"
    @db              = Consulta.new
    @msisdns         = Faker::Base.numerify('55119########')
    @subscription_id = Faker::Base.numerify('########-####-####-####-############')
    @txId            = Faker::Base.numerify('########-####-####-####-############')
    @kw              = kw_f
    @service_id      = service_id_f
    
end

Quando("reunido as informacoes corretas, sera efetuada a assinatura do plano familia.") do
    assinatura_protect(@txId, @service_id, @kw, @msisdns, @subscription_id)
    expect(@post_tim_protect.code).to eq 200
    puts "\n\nImprimindo Api\n\n"
    puts ($api['assina_tim_protect'])

    puts "\n\n Dados da Contratação \n\n"
    puts "Txt ID: #{@txId} \n"
    puts "Service ID: #{@service_id} \n"
    puts "Msisdn: #{@msisdns} \n"
    puts "Subscription ID: #{@subscription_id} \n"
    puts "Aplication ID: #{@kw} \n"
end

Então("validamos a contratacao da assinatura do Tim Protect Familia, com os dados {string}, {string} e {string}.") do |id_produto_f, nome_produto_f, valor_produto_f|
    puts "\n\nValidação da Contrataçao Tim Protect Familia\n\n"
    result_f = @db.mysql(@db.select(@msisdns[2..@msisdns.size])).to_a
    
    for i in (0..(result_f.size-1))
        # puts result_f[i][0].class
        # puts id_produto_f.class
            
        if result_f[i][0] == id_produto_f.to_i
            iProduto_f = "ID do Produto: #{result_f[i][0]} \n"
            nProduto_f = "Nome do Produto: #{result_f[i][1]} \n"
            vProduto_f = "Valor do Produto: #{result_f[i][2]} \n"

            expect(id_produto_f).to eq result_f[i][0].to_s
            expect(nome_produto_f).to eq result_f[i][1].to_s
            expect(valor_produto_f).to eq result_f[i][2].to_s

            puts iProduto_f
            puts nProduto_f
            puts vProduto_f
            puts "\n"
        end 
    end

end


#Assinatura do plano Tim Protect Premium
Dado("que tenho os parametros unicos {string} e {string} do plano premium.") do |service_id_p, kw_p|
    puts "\n\nINICIANDO PROCESSO DE ASSINATURA DO TIM PROTECT PREMIUM\n\n"
    @db              = Consulta.new
    @msisdns         = Faker::Base.numerify('55119########')
    @subscription_id = Faker::Base.numerify('########-####-####-####-############')
    @txId            = Faker::Base.numerify('########-####-####-####-############')
    @kw              = kw_p
    @service_id      = service_id_p
    
end

Quando("reunido as informacoes corretas, sera efetuada a assinatura do plano premium.") do
    assinatura_protect(@txId, @service_id, @kw, @msisdns, @subscription_id)
    expect(@post_tim_protect.code).to eq 200
    puts "\n\nImprimindo Api\n\n"
    puts ($api['assina_tim_protect'])

    puts "\n\n Dados da Contratação \n\n"
    puts "Txt ID: #{@txId} \n"
    puts "Service ID: #{@service_id} \n"
    puts "Msisdn: #{@msisdns} \n"
    puts "Subscription ID: #{@subscription_id} \n"
    puts "Aplication ID: #{@kw} \n"
end

Então("validamos a contratacao da assinatura do Tim Protect Premium, com os dados {string}, {string} e {string}.") do |id_produto_p, nome_produto_p, valor_produto_p|
    puts "\n\nValidação da Contrataçao Tim Protect Premium\n\n"
    result_p = @db.mysql(@db.select(@msisdns[2..@msisdns.size])).to_a

    for i in (0..(result_p.size-1))
        # puts result_f[i][0].class
        # puts id_produto_f.class
            
        if result_p[i][0] == id_produto_p.to_i
            iProduto_p = "ID do Produto: #{result_p[i][0]} \n"
            nProduto_p = "Nome do Produto: #{result_p[i][1]} \n"
            vProduto_p = "Valor do Produto: #{result_p[i][2]} \n"

            expect(id_produto_p).to eq result_p[i][0].to_s
            expect(nome_produto_p).to eq result_p[i][1].to_s
            expect(valor_produto_p).to eq result_p[i][2].to_s

            puts iProduto_p
            puts nProduto_p
            puts vProduto_p
            puts "\n"
        end 
    end

end


#Assinatura do plano Tim Protect Familia Premium
Dado("que tenho os parametros unicos {string} e {string} do plano familia premium.") do |service_id_fpremium, kw_fpremium|
    puts "\n\nINICIANDO PROCESSO DE ASSINATURA DO TIM PROTECT FAMILIA PREMIUM\n\n"
    @db              = Consulta.new
    @msisdns         = Faker::Base.numerify('55119########')
    @subscription_id = Faker::Base.numerify('########-####-####-####-############')
    @txId            = Faker::Base.numerify('########-####-####-####-############')
    @kw              = kw_fpremium
    @service_id      = service_id_fpremium
    
end

Quando("reunido as informacoes corretas, sera efetuada a assinatura do plano familia premium.") do
    assinatura_protect(@txId, @service_id, @kw, @msisdns, @subscription_id)
    expect(@post_tim_protect.code).to eq 200
    puts "\n\nImprimindo Api\n\n"
    puts ($api['assina_tim_protect'])

    puts "\n\n Dados da Contratação \n\n"
    puts "Txt ID: #{@txId} \n"
    puts "Service ID: #{@service_id} \n"
    puts "Msisdn: #{@msisdns} \n"
    puts "Subscription ID: #{@subscription_id} \n"
    puts "Aplication ID: #{@kw} \n"
end

Então("validamos a contratacao da assinatura do Tim Protect Familia Premium, com os dados {string}, {string} e {string}.") do |id_produto_fpremium, nome_produto_fpremium, valor_produto_fpremium|
    puts "\n\nValidação da Contrataçao Tim Protect Familia Premium\n\n"
    result_fpremium = @db.mysql(@db.select(@msisdns[2..@msisdns.size])).to_a

    for i in (0..(result_fpremium.size-1))
        # puts result_f[i][0].class
        # puts id_produto_f.class
            
        if result_fpremium[i][0] == id_produto_fpremium.to_i
            iProduto_fpremium = "ID do Produto: #{result_fpremium[i][0]} \n"
            nProduto_fpremium = "Nome do Produto: #{result_fpremium[i][1]} \n"
            vProduto_fpremium = "Valor do Produto: #{result_fpremium[i][2]} \n"

            expect(id_produto_fpremium).to eq result_fpremium[i][0].to_s
            expect(nome_produto_fpremium).to eq result_fpremium[i][1].to_s
            expect(valor_produto_fpremium).to eq result_fpremium[i][2].to_s

            puts iProduto_fpremium
            puts nProduto_fpremium
            puts vProduto_fpremium
            puts "\n"
        end 
    end

end