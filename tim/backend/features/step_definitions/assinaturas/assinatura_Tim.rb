  Dado("que eu informe os seguintes dados:") do |table|    
    @assinatura = table.rows_hash
    @assinatura['txId'] = "#{@assinatura['txId']}#{SecureRandom.uuid}"
    @assinatura['subscriptionId'] = "#{SecureRandom.uuid}"
  end
 
  Quando("que eu faço uma solicitação para o serviço de assinaturas da TIM") do
    @result = assina_Tim(@assinatura)
    puts "\n\nRESULTADO DA ASSINATURA\n #{@result}"
  end
  
  Então("o código de resposta HTTP deve ser igual a {string}") do |status_code|
    expect(@result.response.code).to eql status_code
  end
  