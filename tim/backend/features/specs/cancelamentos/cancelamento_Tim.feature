#language: pt

Funcionalidade: Cancelamento Tim

    Como FS
    Quero que seja implementado o fluxo de cancelamento para clientes Tim 
    Para quando recebermos uma notificação de cancelamento, seja gravado no nosso backend o cancelamento do produto

@cancelamento
Esquema do Cenário: Cancela Assinatura
    
    Dado que eu tenha contratado um plano Tim
        | txId           | <txId>           |
        | serviceId      | <serviceId>      |
        | msisdn         | <msisdn>         |
        | subscriptionId | <subscriptionId> |
        | applicationId  | <applicationId>  |
        | origin         | <origin>         |
    Quando que eu faço uma solicitação para o serviço de cancelamento        
    Então o código de resposta HTTP deve ser igual a "<status_code>"
   
Exemplos:
    | Produto                        | txId | serviceId                          | msisdn      | subscriptionId | applicationId | origin | status_code |
    | TIM CASA                       |      | TIM_PROT_COMBOS_BUNDLE             | 11954215729 |                | 13907         | HUB    | 200         |
    | TIM PROTECT COMBO LIGHT AVULSO |      | TIM_PROT_COMBO_LIGHT_BUNDLE_AVULSO | 11954215729 |                | 14054         | HUB    | 200         |
    | TIM PROTECT COMBO LIGHT BUNDLE |      | TIM_PROT_COMBO_LIGHT_BUNDLE        | 11954215729 |                | 14053         | HUB    | 200         |