#language: pt


Funcionalidade: Assinar produtos do TIM ENSINA
Como usuário, quero contratar os produtos do Tim ensina, seguindo os parâmetros para a contratação dos planos mensal/semanal.
Após reunido as informações necessárias, será realizada a assinatura. 
Logo, consultado na base da Tim e efetuada a validação dos dados da contratação.


@assinatura_mensal
Esquema do Cenario: Assinatura e validacao do Tim Ensina Mensal
    Dado que tenho os parametros unicos <service_id> e <kw>.
    Quando reunido as informacoes corretas, será efetuada a assinatura do plano mensal
    Então validamos a contratacao da assinatura mensal com os dados <id_produto>, <nome_produto> e <valor_produto>

Exemplos:
| service_id          | kw      | id_produto | nome_produto        | valor_produto |
| "TIM_ENSINA_MENSAL" | "11973" | "284"      | "TIM ENSINA MENSAL" | "13.29"       |



@assinatura_semanal
Esquema do Cenario: Assinatura e validacao do Tim Ensina Semanal
    Dado que tenho os parametros unicos <service_id_s> e <kw_s>
    Quando reunido as informacoes corretas, será efetuada a assinatura do plano semanal
    Então validamos a contratacao da assinatura semanal com os dados <id_produto_s>, <nome_produto_s> e <valor_produto_s>

Exemplos:
| service_id_s         | kw_s    | id_produto_s | nome_produto_s       | valor_produto_s |
| "TIM_ENSINA_SEMANAL" | "11974" | "283"        | "TIM ENSINA SEMANAL" | "4.19"          |