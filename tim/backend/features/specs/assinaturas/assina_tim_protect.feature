#language: pt


Funcionalidade: Assinar produtos do TIM PROTECT
Como usuário, quero contratar os produtos do TIM PROTECT, seguindo os parâmetros para a contratação dos planos 
Tim Protect Essencial/Família/Premium/Família Premium
Após reunido as informações necessárias, será realizada a assinatura
Logo, consultado na base da Tim e efetuada a validação dos dados da contratação.


@assinatura_tim_protect_essencial
Esquema do Cenario: Assinatura e validacao do Tim Protect Essencial
    Dado que tenho os parametros unicos <service_id_e> e <kw_e> do plano essencial.
    Quando reunido as informacoes corretas, sera efetuada a assinatura do plano essencial.
    Então validamos a contratacao da assinatura do Tim Protect Essencial, com os dados <id_produto_e>, <nome_produto_e> e <valor_produto_e>.

Exemplos:
| service_id_e         | kw_e    | id_produto_e | nome_produto_e          | valor_produto_e |
| "TIM_PROT_ESSENCIAL" | "13559" | "172"        | "TIM PROTECT ESSENCIAL" | "6.09"          |


@assinatura_tim_protect_familia
Esquema do Cenario: Assinatura e validacao do Tim Protect Família
    Dado que tenho os parametros unicos <service_id_f> e <kw_f> do plano familia.
    Quando reunido as informacoes corretas, sera efetuada a assinatura do plano familia.
    Então validamos a contratacao da assinatura do Tim Protect Familia, com os dados <id_produto_f>, <nome_produto_f> e <valor_produto_f>.

Exemplos:
| service_id_f       | kw_f    | id_produto_f | nome_produto_f        | valor_produto_f |
| "TIM_PROT_FAMILIA" | "13561" | "174"        | "TIM PROTECT FAMILIA" | "10.29"         |


@assinatura_tim_protect_premium
Esquema do Cenario: Assinatura e validacao do Tim Protect Premium
    Dado que tenho os parametros unicos <service_id_p> e <kw_p> do plano premium.
    Quando reunido as informacoes corretas, sera efetuada a assinatura do plano premium.
    Então validamos a contratacao da assinatura do Tim Protect Premium, com os dados <id_produto_p>, <nome_produto_p> e <valor_produto_p>.

Exemplos:
| service_id_p       | kw_p    | id_produto_p | nome_produto_p        | valor_produto_p |
| "TIM_PROT_PREMIUM" | "13560" | "173"        | "TIM PROTECT PREMIUM" | "8.19"          |


@assinatura_tim_protect_familia_premium
Esquema do Cenario: Assinatura e validacao do Tim Protect Família Premium
    Dado que tenho os parametros unicos <service_id_fpremium> e <kw_fpremium> do plano familia premium.
    Quando reunido as informacoes corretas, sera efetuada a assinatura do plano familia premium.
    Então validamos a contratacao da assinatura do Tim Protect Familia Premium, com os dados <id_produto_fpremium>, <nome_produto_fpremium> e <valor_produto_fpremium>.

Exemplos:
| service_id_fpremium  | kw_fpremium  | id_produto_fpremium | nome_produto_fpremium         | valor_produto_fpremium |
| "TIM_PROT_FAMIL_PRM" | "13562"      | "175"               | "TIM PROTECT FAMILIA PREMIUM" | "15.39"                |