#language: pt

Funcionalidade: Assinatura Tim

    Como FS
    Quero que seja implementado o fluxo de assinatura para clientes Tim 
    Para quando recebermos uma notificação de contratação, seja gravado no nosso backend a assinatura do produto

@assinatura
Esquema do Cenário: Nova Assinatura
    
    Dado que eu informe os seguintes dados:
        | txId           | <txId>           |
        | serviceId      | <serviceId>      |
        | msisdn         | <msisdn>         |
        | subscriptionId | <subscriptionId> |
        | applicationId  | <applicationId>  |
        | origin         | <origin>         |        
    Quando que eu faço uma solicitação para o serviço de assinaturas da TIM
    Então o código de resposta HTTP deve ser igual a "<status_code>"
   
Exemplos:
    | Produto                        | txId | serviceId                          | msisdn      | subscriptionId | applicationId | origin | status_code |
    | TIM CASA                       |      | TIM_PROT_COMBOS_BUNDLE             | 11954215729 |                | 13907         | HUB    | 200         |
    | TIM PROTECT COMBO LIGHT AVULSO |      | TIM_PROT_COMBO_LIGHT_BUNDLE_AVULSO | 11954215729 |                | 14054         | HUB    | 200         |
    | TIM PROTECT COMBO LIGHT BUNDLE |      | TIM_PROT_COMBO_LIGHT_BUNDLE        | 11954215729 |                | 14053         | HUB    | 200         |