  # -----------------------------------Login Válido---------------------------------------------
  # --------------------------------------------------------------------------------------------
  
  Dado("que eu acessei a pagina de login") do
    @login = LoginPage.new
    @login.load    
  end
  
  Quando("faço login com o seguinte msisdn que possui uma assinatura e senha:") do |table|
    @dadosLogin = table.rows_hash

    # # Começa testes cancelando plano assinado
    # assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), "cancelar")
    
    # # Assina plano
    # assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), (@dadosLogin['keyword'])) 

    #logar
    @login.logar(@dadosLogin['msisdn'], @dadosLogin['senha'])
  end
  
  
  Entao("sou autenticado com sucesso e vejo a pagina de area logada") do
    @home = HomePage.new    
    expect(@home.text_minhaConta.text).to eql 'MINHA CONTA'    
  end
  

  # -----------------------------------MSISDN Sem Assinatura------------------------------------
  # --------------------------------------------------------------------------------------------
  Quando("faço login com o seguinte msisdn que não possui assinatura:") do |table|
    @dadosLogin = table.rows_hash
   
    # Começa testes cancelando plano assinado
    # assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), "cancelar")

    #logar
    @login.logar(@dadosLogin['msisdn'], @dadosLogin['senha'])
  end
  
  Então("deve ser exibida uma pop-up com a mensagem {string}") do |mensagem|
    sleep 2
    expect(@login.mensagem_retorno.text).to eql mensagem
  end

  # -----------------------------------Semja Inválida-------------------------------------------
  # --------------------------------------------------------------------------------------------
  Quando("faço login com senha inválida") do |table|
    @dadosLogin = table.rows_hash
   
    # Começa testes cancelando plano assinado
    # assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), "cancelar")

    #logar
    @login.logar(@dadosLogin['msisdn'], @dadosLogin['senha'])
  end

  # -----------------------------------Msisdn Inválido------------------------------------------
  # --------------------------------------------------------------------------------------------
  Quando("faço login com msisdn inválido") do |table|
    @dadosLogin = table.rows_hash
   
    # Começa testes cancelando plano assinado
    # assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), "cancelar")

    #logar
    @login.logar(@dadosLogin['msisdn'], @dadosLogin['senha'])
  end