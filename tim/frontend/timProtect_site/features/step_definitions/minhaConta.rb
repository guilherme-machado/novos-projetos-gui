Dado("que eu efetuei assinatura e login") do |table|  
  @dadosLogin = table.rows_hash
  @msisdn = ((@dadosLogin['msisdn']).tr(' ()-', ''))

  # Começa testes cancelando plano assinado
  assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), "cancelar")
  
  # Assina plano
  assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), (@dadosLogin['keyword'])) 
    
  # Realiza login
  @login = LoginPage.new
  @login.load  
  @login.logar(@dadosLogin['msisdn'])
  solicita_pincode(@dadosLogin['msisdn'])
  pincode = captura_pincode
  @login.validarPincode(pincode)    
end

#----------------------------Meus Aplicativos----------------------------
Quando("exibida a área logada visualizarei a opção Meus Aplicativos no menu") do
  @apps = MeusApps_Page.new
end

Quando("quando acionar o botão Meus Aplicativos") do
  @apps.ver_meusAplicativos
end

Entao("sou direcionado para a tela contendo os aplicativos") do
  expect(page.current_url).to eql "https://www.fshero.com.br/cliente/meus-aplicativos"
end

Entao("os aplicativos exibidos deverão corresponder ao plano assinado") do
  #Função consulta dados no banco
  @db = Consulta.new
  result_select = @db.mysql(@db.select("55#{@msisdn}")).to_a
  sleep 1
  produtos_assinados = result_select.map{|s| {idProduto: s[0], nomeProduto: s[1].to_s, QtdLicencas: s[2].to_i} }
  produtos_assinados = produtos_assinados.to_json
  produtos_assinados = JSON.parse(produtos_assinados, object_class: OpenStruct)
 
  # -----------------------Meus aplicativos - Licenças - ESSENCIAL-----------------------
  if (produtos_assinados[0]["nomeProduto"].to_s) == "ESSENCIAL"
  
    @apps.clicar_icone_seguranca
    expect(@apps.qtd_licencas.text).to include (produtos_assinados[1]["QtdLicencas"].to_s)     
    # Chama função que tira print do teste
    screenshot(nome_cenario = "IconeSeguranca")

    @apps.clicar_icone_cloud  
    expect(@apps.qtd_licencas.text).to include (produtos_assinados[2]["QtdLicencas"].to_s)
    # Chama função que tira print do teste
    screenshot(nome_cenario = "IconeCloud")

    @apps.clicar_icone_wifi     
    expect(@apps.qtd_licencas.text).to include (produtos_assinados[4]["QtdLicencas"].to_s)
    # Chama função que tira print do teste
    screenshot(nome_cenario = "IconeWifi")

  # -----------------------Meus aplicativos - Licenças - BASICO-----------------------
  elsif (produtos_assinados[0]["nomeProduto"].to_s) == "BASICO"
    
    @apps.clicar_icone_seguranca    
    expect(@apps.qtd_licencas.text).to include (produtos_assinados[1]["QtdLicencas"].to_s)
    # Chama função que tira print do teste
    screenshot(nome_cenario = "IconeSeguranca")

    @apps.clicar_icone_cloud    
    expect(@apps.qtd_licencas.text).to include (produtos_assinados[2]["QtdLicencas"].to_s)
    # Chama função que tira print do teste
    screenshot(nome_cenario = "IconeCloud")

    @apps.clicar_icone_familia    
    expect(@apps.qtd_licencas.text).to include (produtos_assinados[5]["QtdLicencas"].to_s)
    # Chama função que tira print do teste
    screenshot(nome_cenario = "IconeFamilia")

    # @apps.clicar_icone_wifi
    # expect(@apps.qtd_licencas.text).to include (produtos_assinados[6]["QtdLicencas"].to_s)
    # # Chama função que tira print do teste
    # screenshot(nome_cenario = "IconeWifi")

  # -----------------------Meus aplicativos - Licenças - PREMIUM-----------------------
  elsif (produtos_assinados[0]["nomeProduto"].to_s) == "PREMIUM"
    
    @apps.clicar_icone_cloud    
    expect(@apps.qtd_licencas.text).to include (produtos_assinados[1]["QtdLicencas"].to_s)
    # Chama função que tira print do teste
    screenshot(nome_cenario = "IconeCloud")

    @apps.clicar_icone_seguranca    
    expect(@apps.qtd_licencas.text).to include (produtos_assinados[2]["QtdLicencas"].to_s)
    # Chama função que tira print do teste
    screenshot(nome_cenario = "IconeSeguranca")

    @apps.clicar_icone_familia    
    expect(@apps.qtd_licencas.text).to include (produtos_assinados[5]["QtdLicencas"].to_s)
    # Chama função que tira print do teste
    screenshot(nome_cenario = "IconeFamilia")

    @apps.clicar_icone_wifi    
    expect(@apps.qtd_licencas.text).to include (produtos_assinados[6]["QtdLicencas"].to_s)
    # Chama função que tira print do teste
    screenshot(nome_cenario = "IconeWifi")
  end
end

Entao("efetuarei o cancelamento do plano") do
  assina_cancela_Hero("55#{@msisdn}", (@dadosLogin['la']), "cancelar")
end
  
