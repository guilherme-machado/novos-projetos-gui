class LoginPage < SitePrism::Page

    set_url 'https://www.timprotect.com.br/'

    element :botao_login, '#login'
    element :input_msisdn, '#numero' 
    element :input_senha, '#senha'   
    element :botao_continuar, '#submit_protect'

    element :mensagem_retorno, '#msg-retorno'


    def logar(msisdn, senha)
        botao_login.click
        input_msisdn.set msisdn
        input_senha.set senha
        botao_continuar.click      
    end
end
