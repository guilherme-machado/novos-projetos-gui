require 'selenium-webdriver'
require 'capybara'
require 'capybara/cucumber'
require 'site_prism'
require 'httparty'
require "rspec"
require "json"
require "faker"

#configurando driver do capybara
Capybara.configure do |config|
    config.default_driver = :selenium_chrome # roda teste no chrome
    # config.default_driver = :selenium # roda teste no firefox
    config.default_max_wait_time = 30
end

$profile = ENV['PROFILE']
api_configs = YAML.load_file('./features/support/api.yml')
$api = api_configs[$profile]