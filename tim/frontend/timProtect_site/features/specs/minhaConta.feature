#language:pt

Funcionalidade: Minha Conta

    Para gerenciar minha conta no site Tim Protect
    Sendo um usuário que possui o plano assinado
    Posso fazer meu login no site e acessar a Home, validando os planos assinados
    

@Minha_Conta
Esquema do Cenário: Meus Aplicativos
    
    Dado que eu efetuei assinatura e login
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    Quando exibida a área logada visualizarei a opção Meus Aplicativos no menu
    E quando acionar o botão Meus Aplicativos
    Entao sou direcionado para a tela contendo os aplicativos
    E os aplicativos exibidos deverão corresponder ao plano assinado
    E efetuarei o cancelamento do plano

      Exemplos: 
        | msisdn        | plano     | la   | keyword     |
        # | 5511988162602 | Essencial | 5513 | HEROAD1     |
        | (11)98816-2602 | Basico    | 5513 | HERO199GEM1 |
        | (11)98816-2602 | Premium   | 5513 | HERO299GEMP |


