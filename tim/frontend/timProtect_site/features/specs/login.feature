#language:pt

Funcionalidade: Login

    Para acessar minha conta no site Tim Protect
    Sendo um usuário que possui o plano assinado
    Posso fazer meu login no site
    
@loginValido @login
Esquema do Cenário: Login Valido 
    
    Dado que eu acessei a pagina de login
    Quando faço login com o seguinte msisdn que possui uma assinatura e senha:
        | msisdn  | <msisdn>  |
        | senha   | <senha>   |
    Entao sou autenticado com sucesso e vejo a pagina de area logada

    Exemplos: 
    | msisdn      | senha  |
    | 11987824916 | 123456 |


@semAssinatura @login
Esquema do Cenário: Msisdn sem assinatura

    Dado que eu acessei a pagina de login
    Quando faço login com o seguinte msisdn que não possui assinatura:
        | msisdn   | <msisdn>   |
        | mensagem | <mensagem> |
    Então deve ser exibida uma pop-up com a mensagem "<mensagem>"

    Exemplos: 
      | msisdn      | mensagem                                                |
      | 11955662135 | Verificamos que não há produto neste número de celular. |


@senhaInvalida @login
Esquema do Cenário: Senha Inválida

    Dado que eu acessei a pagina de login
    Quando faço login com senha inválida
        | msisdn   | <msisdn>   |
        | senha    | <senha>    |
        | mensagem | <mensagem> |
    Então deve ser exibida uma pop-up com a mensagem "<mensagem>"

    Exemplos: 
      | msisdn      | mensagem                    | senha  |
      | 11987824916 | Usuário ou senha inválidos  | 1234   |

@msisdnInvalido @login
Esquema do Cenário: Msisdn Inválido

    Dado que eu acessei a pagina de login
    Quando faço login com msisdn inválido
        | msisdn   | <msisdn>   |
        | mensagem | <mensagem> |
    Então deve ser exibida uma pop-up com a mensagem "<mensagem>"

    Exemplos: 
      | msisdn      | mensagem                    |
      | 1094        | Número de telefone inválido |
