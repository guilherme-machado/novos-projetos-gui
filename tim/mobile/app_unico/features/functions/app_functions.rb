#enconding: utf-8

require_relative 'base_appium'
require_relative 'page_elements'
require_relative 'database'

class AppFunctions
    def initialize
        @base = BaseAppium.new
        @page = PageElements.new
        @db = Database.new
    end

    #Método para envio de notificação via SMS
    def send_mt(message)
        body = {
            "msisdn": "5511987190582",
            "la": 77000,
            "carrier": "vivo",
            "text": message,
            "priority": 0
            }
            response = HTTParty.post("#{DATA[url_global_sdp]}",
            :body => body.to_json,
            :headers => {"Content-Type" => 'application/json', #"Authorization" => "#{token}"
             })
        # transaction_id = response.parsed_response["data"]["transaction_id"]
        # transaction_id
        response.parsed_response
    end

    #Método para excluir todas as conversas de SMS
    def cancel_conversation
        @base.wait_id(@page.message_elements[:conversation])
        while @base.exists_id(@page.message_elements[:conversation])
            @base.click_id(@page.message_elements[:conversation])
            sleep 3
            @base.click_desc("Mais opções")
            exclude = @base.find_elements_id(@page.message_elements[:options_menu])
            for i in (0..exclude.size-1)
                if exclude[i].attribute("text") == "Excluir"
                    exclude[i].click
                    @base.click_id(@page.message_elements[:delete_button])
                    sleep 2
                    break
                end
            end
        end
    end

    def insert_msisdn
        @base.click_id(@page.tim_elements[:insert_phone])
        @base.send_keys_id(@page.tim_elements[:insert_phone], DATA['msisdn'])
        $driver.back
        @base.click_id(@page.tim_elements[:enter_button])
    end

    #Método que captura o pincode recebido via SMS
    def receive_pin
        # if @base.wait_id(@page.message_elements[:conversation], 60)
        if @base.wait_text(@page.message_elements[:message_pin], 60)
            text = @base.return_string_text(@page.message_elements[:message_pin])
            pincode = text[0..5]
        else
            pincode = false
        end
        pincode
    end

    #Método que valida se os textos informados pelo BDD estão corretos no app
    def validate_text_app(table_size, table_hash, tag)
        text = []
        for i in 1..table_size
            hash = table_hash["#{tag}#{i}"]
            text[i] = @base.exists_text(hash)
            if text[i]
                puts "#{hash} = OK"
                validate = true
            else
                puts "#{hash} = NOK"
                validate = false
                break
            end
        end
        validate
    end

    def insert_result_pincode(start_date, sentPin, found, timeout)
        if found == "sucesso"
            $driver.screenshot("#{DATA['screenshots']}pin/sucesso/success_pin#{Time.now}.png")
            @db.insert_tim(start_date, "Envio do pincode", "#{sentPin} segundos", found)
            # send_mt("tim: PIN recebido com sucesso em #{sentPin} segundos")
        else
            $driver.screenshot("#{DATA['screenshots']}pin/erro/error_pin#{Time.now}.png")
            if @base.exists_text('Ocorreu um erro desconhecido.')
                @db.insert_tim(start_date, "Envio do pincode" , "Msg do app: Ocorreu um erro desconhecido", found)
                # send_mt("tim: Msg do app: Ocorreu um erro desconhecido")
            else
                @db.insert_tim(start_date, "Envio do pincode", "Pin nao recebido (timeout #{timeout}s)", found)
                # send_mt("tim: PIN não recebido (timeout #{@timeout}s)")
            end
        end
    end

    def insert_result_license(start_date, plan, found)
        if found == "sucesso"
            $driver.screenshot("#{DATA['screenshots']}gestor_licenca/sucesso/success_license#{Time.now}.png")
            @db.insert_tim(start_date, "Gestor de Licencas", plan, found)
        else
            $driver.screenshot("#{DATA['screenshots']}gestor_licenca/erro/error_license#{Time.now}.png")
            @db.insert_tim(start_date, "Gestor de Licencas", plan, found)
        end
    end

    def validate_pin(start_date, start_time)
        timeout = 60
        sentPin = 0

        #inicia o app de SMS para capturar o pincode
        # $driver.activate_app(@page.message_elements[:app_package])
        # pincode = []
        # pincode = receive_pin

        while timeout > sentPin
            end_time = Time.now

            if @base.return_string_id(@page.tim_elements[:pincode]) != "- - - - - -"
                sentPin = @base.timer(start_time, end_time)
                found = "sucesso"
                break
            else
                sentPin = @base.timer(start_time, end_time)
                found = "falha"
            end
        end

        # if pincode == false
        #     $driver.activate_app(@page.tim_elements[:app_package])
        #     @base.wait_id(@page.tim_elements[:resend_code])
        #     @base.click_id(@page.tim_elements[:resend_code])
        #     $driver.activate_app(@page.message_elements[:app_package])
        #     pincode = receive_pin
        # end

        # while timeout > sentPin
        # end_time = Time.now
        #     if pincode != nil && pincode.size == 6
        #         sentPin = @base.timer(start_time, end_time)
        #         found = "sucesso"
        #         break
        #     else
        #         sentPin = @base.timer(start_time, end_time)
        #         found = "falha"
        #     end
        # end

        insert_result_pincode(start_date, sentPin, found, timeout)

        puts "TEMPO DE ESPERA DO PIN: #{sentPin}"
        puts "Fim: #{Time.now.strftime("%Y/%m/%d %H:%M:%S")}"
        # pincode

        # $driver.activate_app(@page.tim_elements[:app_package])
        # @base.send_keys_id(@page.tim_elements[:pincode], pincode)
        # @base.click_id(@page.tim_elements[:continue_button])
    end

    #Método que confirma a permissão da função de VPN do app
    def permission_vpn_message(id_msg, id_check, id_confirm)
        @base.wait_id(id_msg)
        permission = @base.exists_id(id_msg)
        if permission
            @base.click_id(id_check)
            @base.click_id(id_confirm)
        end
    end

    #Método que confirma as permissões necessárias do Android para o app
    def permission_welcome_message(id_msg, id_confirm)
        @base.wait_id(id_msg, 30)
        permission = @base.exists_id(id_msg)
        if permission
            @base.click_id(id_confirm)
        end
    end

    #Método que confirma a permissão de sobreposição do app
    def permission_overlap_message(id_msg, id_confirm)
        validator = false
        @base.wait_id(id_msg)
        if @base.exists_id(id_msg)
            validator = true
            @base.click_id(id_confirm)
            @base.wait_text('Permitir a sobreposição a outros apps')
            @base.click_id(@page.message_elements[:switch_widget])
            $driver.back
        end
        validator
    end

    def validate_home(start_date)
        permission_vpn_message(@page.tim_elements[:warning], @page.tim_elements[:check], @page.message_elements[:delete_button])
        permission_welcome_message(@page.tim_elements[:dialog_tilte], @page.tim_elements[:txt_action])
        permission_overlap_message(@page.tim_elements[:dialog_information], @page.tim_elements[:txt_positive])

        if @base.wait_id(@page.tim_elements[:activity_main])
            $driver.screenshot("#{DATA['screenshots']}home_app/sucesso/success_home_app#{Time.now}.png")
            @db.insert_tim(start_date, "Login Tim Protect" , "Login OK", "sucesso")
            # @app.send_mt("tim: Login OK")
        else
            $driver.screenshot("#{DATA['screenshots']}home_app/erro/failed_home_app#{Time.now}.png")
            # @app.send_mt("tim: Login NOK")
        end
    end

    def login_flow
        #Etapa I
        @base.wait_id(@page.tim_elements[:enter_button])
        @base.click_id(@page.tim_elements[:insert_phone])
        @base.send_keys_id(@page.tim_elements[:insert_phone], DATA['msisdn'])
        $driver.back
        @base.click_id(@page.tim_elements[:enter_button])
        @start_time = Time.now
        puts "Início: #{Time.now.strftime("%Y/%m/%d %H:%M:%S")}"

        #Etapa II
        @base.wait_id(@page.tim_elements[:continue_button])
        $driver.activate_app(@page.message_elements[:app_package])
        pincode = receive_pin

        if pincode == false
            $driver.activate_app(@page.tim_elements[:app_package])
            @base.wait_id(@page.tim_elements[:resend_code])
            @base.click_id(@page.tim_elements[:resend_code])
            $driver.activate_app(@page.message_elements[:app_package])
            pincode = receive_pin
        end
        
        #Retorna para o app tim
        $driver.activate_app(@page.tim_elements[:app_package])
        @base.send_keys_id(@page.tim_elements[:pincode], pincode)
        @base.click_id(@page.tim_elements[:continue_button])

        insert_name_app("QA FS")

        #Etapa III
        permission_vpn_message(@page.tim_elements[:warning], @page.tim_elements[:check], @page.message_elements[:delete_button])
        permission_welcome_message(@page.tim_elements[:dialog_tilte], @page.tim_elements[:txt_action])
        permission_overlap_message(@page.tim_elements[:dialog_information], @page.tim_elements[:txt_positive])
    end

    def insert_name_app(user)
        @base.wait_text("Digite seu nome", 30)

        if @base.exists_text("Digite seu nome")
            @base.send_keys_id(@page.tim_elements[:edit_user], user)
            @base.click_id(@page.tim_elements[:enter_user])
            @base.wait_text("Digite o Nome do Dispositivo", 30)
            @base.click_ids(@page.tim_elements[:enter_device], 1)
        end
    end

    # def insert_device_app(user)
    #     @base.wait_text("Digite o Nome do Dispositivo", 30)

    #     if @base.exists_text("Digite o Nome do Dispositivo")
    #         # @base.send_keys_id(@page.tim_elements[:edit_user], user)
    #         @base.click_ids(@page.tim_elements[:enter_device], 1)
    #     end
    # end

    def fake_mo(msisdn, la, keyword)
        endpoint = DATA['sign_tim']
        
        body = {
            "msisdn": msisdn,
            "la": la,
            "text": keyword,
            "partner": "claro",
            "debug": true,
            "transaction_id": Faker::Number.number(21),
            "auth_code": Faker::Number.number(4),
            "auth_code_status": true,
            "transaction_id_auth": Faker::Number.number(15)
        }    
           
        @result = HTTParty.post(
            endpoint,
            headers: {"Content-Type" => 'application/json'},
            body: body.to_json
        )
    end

    def sign_plan(plan)
        puts "cancelando plano"
        fake_mo(DATA['msisdn'], DATA['la'], "SAIR")
        sleep 5
        if plan == "BASICO"
            puts "Assinando plano BASICO"
            fake_mo(DATA['msisdn'], DATA['la'], "tim199GEM1")
        elsif plan == "ESSENCIAL"
            puts "Assinando plano ESSENCIAL"
            fake_mo(DATA['msisdn'], DATA['la'], "ES")
            sleep 5
            fake_mo(DATA['msisdn'], DATA['la'], "SIM")
        elsif plan == "PREMIUM"
            puts "Assinando plano PREMIUM"
            fake_mo(DATA['msisdn'], DATA['la'], "tim299GEMP")
        else
            puts "plan_error"
        end
    end
end