#encoding: utf-8

class BaseAppium
    #Método para esperar um elemento mapeado estar visível na tela por id durante 10s.
    def wait_id(id, timeout = 10)
        begin
            $driver.wait_true(timeout) { $driver.exists { $driver.find_element(:id, id) } }
        rescue
            puts "Elemento de id: #{id} nao encontrado"
        end
    end
    
    
    #Método para esperar um texto mapeado estar visível na tela por xpath durante 10s. 
    def wait_text(text, timeout = 10)
        begin
            $driver.wait_true(timeout) { $driver.exists { $driver.find_element :xpath, "//*[contains(@text, '#{text}')]" } }
        rescue
            puts "Elemento de texto: #{text} nao encontrado"
        end
    end


    #Método para clicar um elemento mapeado na tela por id.
    def click_id(id)
        $driver.find_element(:id, id).click
    end

    def click_ids(id, posicao)
        element = find_elements_id(id)
        element[posicao].click
    end

    #Método para clicar um elemento mapeado na tela por xpath.
    def click_text(text)
        $driver.find_element(:xpath, "//*[@text='#{text}']").click
    end


    #Método para clicar um elemento mapeado na tela por xpath.
    def click_desc(desc)
        $driver.find_element(:xpath, "//*[@content-desc[contains(., '#{desc}')]]").click
    end


    #Método para retornar se um elemento, por id, existe na tela.
    def exists_id(id)
        begin
            $driver.exists { $driver.find_element(:id, id) }
        rescue
            raise "Elemento de id: #{id} nao encontrado"
        end
    end


    #Método para retornar se um elemento, por texto, existe na tela.
    def exists_text(text)
        begin
            $driver.exists { $driver.find_element(:xpath, "//*[@text='#{text}']") }
        rescue
            raise "Elemento de texto: #{text} nao encontrado"
        end
    end
    

    #Método para retornar se um elemento, por desc, existe na tela.
    def exists_desc(desc)
        begin
            $driver.exists { $driver.find_element(:xpath, "//*[@content-desc[contains(., '#{desc}')]]") }
        rescue
            raise "Elemento de desc: #{desc} nao encontrado"
        end
    end


    #Método para preencher um campo mapeado na tela por id.
    def send_keys_id(id, text)
        $driver.find_element(:id, id).send_keys(text)
    end


    #Método para preencher um campo mapeado na tela por texto.
    def send_keys_text(xpath, text)
        $driver.find_element(:xpath, "//*[@text='#{xpath}']").send_keys(text)
    end
    
    #Método para localizar um elemento, por id, mapeado na tela.
    def find_id(id)
        begin
            $driver.find_element(:id, id)
        rescue
            raise "Elemento de id: #{id} nao encontrado"
        end
    end

    def find_elements_id(id)
        begin
            $driver.find_elements(:id, id)
        rescue
            raise "Elemento de id: #{id} nao encontrado"
        end
    end


    #Método para localizar um elemento, por texto, mapeado na tela.
    def find_text(text)
        begin
            # $driver.find_element :xpath, "//*[@text='#{text}']"
            $driver.find_element :xpath, "//*[contains(@text, '#{text}')]"
        rescue
            raise "Elemento de texto: #{text} nao encontrado" 
        end
    end

    #Método para localizar um elemento, por desc, mapeado na tela.
    def find_desc(desc)
        begin
            $driver.find_element :xpath, "//*[@content-desc[contains(., '#{desc}')]]"
        rescue
            raise "Elemento de desc: #{desc} nao encontrado" 
        end
    end


    #Método que retorna como String um elemento, por id, mapeado na tela.
    def return_string_id(id)
        element = find_id(id)
        element.attribute('text')
    end


    def return_strings_id(id)
        element = find_elements_id(id)
        elements = []
        for i in (0..element.size-1)
            elements << element[i].attribute('text')
        end
        elements
    end

    #Método que retorna como String um elemento, por texto, mapeado na tela.
    def return_string_text(text)
        element = find_text(text)
        element.attribute('text')
    end

    #Método que retorna o tempo corrido em segundos.
    def timer(start_time, end_time)
        if end_time.min > start_time.min
            count = 60 * (end_time.min - start_time.min)
            result = (end_time.sec + count) - start_time.sec
        elsif
            result = end_time.sec - start_time.sec
        end
        result
    end
end