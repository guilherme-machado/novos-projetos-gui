# language: pt

Funcionalidade: Usuario HERO que utiliza o App HERO
    Usuario HERO que inicia o app HERO

    Contexto:
        Dado que usuario HERO inicie o aplicativo HERO
            
            |login1|Proteja sua vida digital|
            |login2|A vida está cada vez mais digital. Mas você não precisa se preocupar com hackers, vírus ou roubo de dados|
            # |login3|Seus aplicativos juntos aqui|
            # |login4|Acesse com um toque todos os aplicativos Hero|
            # |login5|Mais rápido e com mais bateria|
            # |login6|Acelere o seu celular, remova arquivos desnecessários e armazene arquivos na nuvem ganhando espaço|
            # |login7|Navegue com segurança|
            # |login8|Utilize a internet sem medo de ameaças com a Navegação Segura|
            |login3|Insira o seu DDD + celular|
            |login4|Entrar|

@gestor_de_licenças @gestor
    Esquema do Cenario: Usuário Hero, já logado, acessa o Gerenciador de Licenças
        Quando usuario realiza o login no App com o plano <plano>
        E acessa a opcao "Gestor de Licenças" no menu Configuracoes
        Então será exibido o nome do Pacote ativo
        E os produtos, com suas respectivas licenças, disponíveis.

    Exemplos:
        | plano       |
        | "BASICO"    |
        | "ESSENCIAL" |
        | "PREMIUM"   |
