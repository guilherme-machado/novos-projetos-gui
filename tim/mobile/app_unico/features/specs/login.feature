# language: pt

Funcionalidade: Usuario Tim que utiliza o App Tim Protect
    Usuario Tim que inicia o app Tim Protect

    Contexto:
        Dado que usuario Tim inicie o aplicativo Tim Protect
            
            |login1|Proteja sua vida digital|
            |login2| A vida está cada vez mais digital. Mas você não precisa se preocupar com hackers, vírus ou roubo de dados|
            |login3|Insira o seu DDD + celular|
            |login4|Entrar|
            |login5|Dúvidas? Fale conosco.|

@login
    Cenario: Usuario que possui o Tim realiza o login
        Quando usuario insere o msisdn e clica em "Entrar"
        Então recebe o pincode de validacao

            |confirma1|Confirmar seu número de telefone|
            |confirma2|Insira o código de 6 dígitos enviado para|
            |confirma3|Continuar|

        E exibe a home do app

            |home1|Segurança|
            |home2|Backup|
            |home3|Filhos|
            |home4|Suporte Digital|
            # |home5|PROTEJA SEU CELULAR|