#encoding: utf-8

#Contexto
Dado("que usuario Tim inicie o aplicativo Tim Protect") do |table|
    @start_date = Time.now.strftime("%Y/%m/%d %H:%M:%S")
    @base.wait_id(@page.tim_elements[:enter_button])
    expect(@app.validate_text_app(table.cells_rows.count, table.rows_hash, "login")).to eql true
  end


Quando("usuario insere o msisdn e clica em {string}") do |button_entrar|
  #Inicia o app de SMS para limpar todas as conversas antes de solicitar o pincode
  $driver.activate_app(@page.message_elements[:app_package])
  @app.cancel_conversation

  #Retorna para o app tim
  $driver.activate_app(@page.tim_elements[:app_package])

  @base.wait_text(button_entrar)
  expect(@base.exists_text(button_entrar)).to eql true
  @app.insert_msisdn

  @start_time = Time.now
  puts "Início: #{Time.now.strftime("%Y/%m/%d %H:%M:%S")}"
end


Entao("recebe o pincode de validacao") do |table|
  @base.wait_id(@page.tim_elements[:continue_button])
  expect(@app.validate_text_app(table.cells_rows.count, table.rows_hash, "confirma")).to eql true
  @app.validate_pin(@start_date, @start_time)
  @app.insert_name_app("QA FS")
end


Entao("exibe a home do app") do |table|
  @app.validate_home(@start_date)
  expect(@app.validate_text_app(table.cells_rows.count, table.rows_hash, "home")).to eql true
end