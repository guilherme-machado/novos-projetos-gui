
class Login < SitePrism::Page 
    #login
    element :user, '#usuario'
    element :password, '#senha'
    element :entrar, '#btentrar'
    element :logo_oi, 'body > div > div > div.box-logos-topo > div > div > div > ul > li.logo-telefonica > img'
    element :logo_fs, 'body > div > div > div.box-logos-topo > div > div > div > ul > li.logo-fs > img'
    element :texts, '#boxerro > div'
    #Área Logada
    element :logged_area, '#formbusca > div > div:nth-child(1) > div > h2'
    element :nova_busca, 'body > div > div > div.menu-topo > div > div > div > div > ul > li.menu-padrao.ativo > a'
    element :bloquear_desbloquear, 'body > div > div > div.menu-topo > div > div > div > div > ul > li:nth-child(2) > a'
    element :promocoes, 'body > div > div > div.menu-topo > div > div > div > div > ul > li.menu-padrao.menu-sub-menu > a'
    element :sair, 'body > div > div > div.menu-topo > div > div > div > div > ul > li:nth-child(4) > a'
    element :insere_numero, '#numero'
    element :trinta_dias, '#formbusca > div > div:nth-child(2) > div > div > table > tbody > tr:nth-child(3) > td.txt2.bb > div > label'
    element :noventa_dias, '#formbusca > div > div:nth-child(2) > div > div > table > tbody > tr:nth-child(4) > td > div > label'
    element :sete_meses, '#formbusca > div > div:nth-child(2) > div > div > table > tbody > tr:nth-child(5) > td > div > label'
    element :pesquisar, 'body > div > div > div.corpo-bottom > div > div > div > div > div > a'
    #Busca
    element :resultado_busca, '#formblacklist > div > div:nth-child(1) > div > h2'
    element :busca_nula, '#formblacklist > div > div:nth-child(2) > div > div > table > tbody > tr:nth-child(2) > td > center > h3'
    element :linha_do_clinte, '#print-header > p:nth-child(1)'
    element :status, '#print-header > div'
    element :tabela_de_produtos, '#formblacklist > div > div:nth-child(2) > div > div.box-conteudo > table > tbody > tr:nth-child(2) > td > table'
    
    def login_off(user, senha)
        visit:"#{DATA['url']['site']}"
        self.user.set user
        self.password.set senha
        sleep(1)
        self.entrar.click 
        sleep(1)
        self.texts
        self.logo_oi
        self.logo_fs   
    end


    def log_in(user, senha)
        visit:"#{DATA['url']['site']}"
        self.user.set user
        self.password.set senha
        sleep(1)
        self.entrar.click 
        sleep(1)
        self.logged_area 
    end

        def new_search(msisdn)
            self.nova_busca
            self.bloquear_desbloquear
            self.promocoes
            self.sair
            self.insere_numero.click
            self.insere_numero.send_keys msisdn
            self.trinta_dias.click
            @find_number = self.pesquisar.click
            
        begin @find_number 
            self.busca_nula.text == "Esse usuário não possui interatividade/segurança."
            puts "MSISDN SEM PRODUTO!" 
        
        rescue =>  @find_number 
            self.resultado_busca.text == "RESULTADO DA BUSCA"
            puts "MSISDN COM PRODUTO!"
            puts "#{self.status.text}"
            @tbp = puts self.tabela_de_produtos.text
        end
            
    end

end
