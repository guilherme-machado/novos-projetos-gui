#language:pt

Funcionalidade: Área Logada

    Para gerenciar minha conta no site Oi Segurança
    Sendo um usuário que possui um plano assinado
    Posso fazer meu login no site e acessar a área logada
    
@download @area_logada
Esquema do Cenário: Download Manager 
    
    Dado que eu acessei a pagina de login
    Quando faço login com o seguinte msisdn que possui uma assinatura:
        | msisdn  | <msisdn>  |
        | la      | <la>      |
        | keyword | <keyword> |
    E digito um PINCODE válido
    Entao devo ter uma opção para fazer download do Gerenciador de Aplicativos
    E ser direcionado para tela de download

    Exemplos: 
        | msisdn         | plano           | la   | keyword |
        | (11)94662-9382 | Proteção Básica | 5990 | 1       |