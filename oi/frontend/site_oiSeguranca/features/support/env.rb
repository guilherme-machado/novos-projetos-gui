
require 'capybara'
require 'capybara/cucumber'
require "faker"
require 'httparty'
require "json"
require "mysql2"
require "rspec"
require 'selenium-webdriver'
require 'site_prism'


#configurando driver do capybara
Capybara.configure do |config|
    config.default_driver = :selenium_chrome # roda teste no chrome
    # config.default_driver = :selenium # roda teste no firefox
    config.default_max_wait_time = 30
end

$profile = ENV['PROFILE']
api_configs = YAML.load_file('./features/support/api.yml')
$api = api_configs[$profile]