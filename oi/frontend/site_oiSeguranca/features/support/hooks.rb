Before do
    @login = LoginPage.new
    @areaLogada = AreaLogadaPage.new
    @home = HomePage.new
end

After do |scenario|    
    nome_cenario = scenario.name.tr(' ,()#1', '')
    nome_cenario = nome_cenario.sub(/Exemplos/, '')
    
    # Chama função que tira print no final de cada cenário
    screenshot(nome_cenario)
end