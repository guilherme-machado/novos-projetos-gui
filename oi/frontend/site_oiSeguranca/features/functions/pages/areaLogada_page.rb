class AreaLogadaPage < SitePrism::Page

    element :text_suaConta, 'div[class="container"] > div[class="row"] > div[class="col-xs-12"] > h2'
    element :menu_meuMsisdn, 'div[id="menutopo"] > ul[class="nav navbar-nav navbar-right"] > li[class="box-tel"] > a[href="#"]'
    element :botao_download, 'a[class="bt-baixar"]'


    def downloadGerenciador
        botao_download.click
    end

end