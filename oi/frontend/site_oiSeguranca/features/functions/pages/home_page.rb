class HomePage < SitePrism::Page

    set_url 'https://www.oiseguranca.com.br/'

    element :botao_login, 'a[id="btn-logar"]'

    def abrirLogin
        botao_login.click
    end
    
end