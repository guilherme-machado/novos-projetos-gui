class LoginPage < SitePrism::Page

    element :input_msisdn, '#phone'    
    element :botao_entrar, 'button[class*=btn-login]'
    element :input_pincode, '#pincode' 
    element :botao_continuar, 'button[class*=btn-valida-pincode]'
    element :botao_reenviarPincode, 'p[class="reenvia-pincode"]'
    element :text_reenviandoPincode, 'p[class="aguarde"]'


    def logar(msisdn)
        input_msisdn.set msisdn    
        botao_entrar.click
    end


    def validarPincode(pincode)
        input_pincode.set pincode  
        botao_continuar.click
    end

    
    def reenviarPincode      
        botao_reenviarPincode.click
    end

    
end
