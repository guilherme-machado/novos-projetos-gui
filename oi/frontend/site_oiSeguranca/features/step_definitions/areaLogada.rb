Entao("devo ter uma opção para fazer download do Gerenciador de Aplicativos") do
  @areaLogada.downloadGerenciador
  sleep 10
end

Entao("ser direcionado para tela de download") do
  page.driver.browser.switch_to().window(page.driver.browser.window_handles.last)
  expect(page.current_url).to eql "http://oi.whitelabel.com.br/dm-war/o/passo1_c.jsp"
end
  