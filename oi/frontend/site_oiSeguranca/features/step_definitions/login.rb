Dado("que eu acessei a pagina de login") do
    @home.load
end

Quando("faço login com o seguinte msisdn que possui uma assinatura:") do |table|
    @dadosLogin = table.rows_hash
    # @msisdn = ((@dadosLogin['msisdn']).tr(' ()-', ''))

    # # Começa testes cancelando plano assinado
    # assina_cancela_Oi("55#{@msisdn}", (@dadosLogin['la']), "cancelar")
    
    # # Assina plano
    # assina_cancela_Oi("55#{@msisdn}", (@dadosLogin['la']), (@dadosLogin['keyword'])) 

    #logar
    @home.abrirLogin
    @login.logar(@dadosLogin['msisdn'])
end

Quando("digito um PINCODE válido") do
    solicita_pincode(@dadosLogin['msisdn'])    
    pincode = captura_pincode
    @login.validarPincode(pincode)
end

Entao("sou autenticado com sucesso e vejo a pagina de area logada") do
    expect(@areaLogada.text_suaConta.text).to eql 'SUA CONTA'
end

# ----------------------------Login validando reenvio de pincode----------------------------

Quando("quero receber um novo PINCODE através da funcionalidade reenviar") do
    solicita_pincode(@dadosLogin['msisdn'])    
    pincode = captura_pincode
    @login.reenviarPincode
    expect(@login.text_reenviandoPincode.text).to eql 'Enviando, aguarde um instante por favor...'
    # Chama função que tira print do teste
    screenshot(nome_cenario = "ReenvioPincode")
end

Entao("digito o novo pincode recebido") do
    solicita_pincode(@dadosLogin['msisdn'])    
    pincode = captura_pincode
    @login.validarPincode(pincode)
end

# --------------------------------------Login inválido--------------------------------------

Quando("faço login com o seguinte msisdn que não possui assinatura:") do |table|
    @dadosLogin = table.rows_hash
    @home.abrirLogin
    @login.logar(@dadosLogin['msisdn'])
  end
  
  Então("devo ver uma {string} de negação") do |mensagem|
    expect(@login.mensagem_erro.text).to eql mensagem
  end