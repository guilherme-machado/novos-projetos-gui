def renewed_backend()

    @post_action_renewed            = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => "RENEW",
    "digest"                        => "RENEWED",
    "channel"                       => @channel,
    "eventDate"                     => Time.now.strftime("%Y-%m-%d"),
    "nextRenewalDate"               => Date.today + 7,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => Faker::Number.number(9).to_s,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_renewed

  puts "\n\nSUBSCRIPTION ID\n\n"
  puts (@post_action_renewed[@subscriptionId])


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)


  @action_renewed = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_renewed)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_renewed.body
  expect(@action_renewed.code).to eq 200

end







def fault_backend()

    @post_action_fault              = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => "RENEW",
    "digest"                        => "RENEW_FAULT",
    "channel"                       => @channel,
    "eventDate"                     => Time.now.strftime("%Y-%m-%d"),
    "nextRenewalDate"               => Date.today,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => Faker::Number.number(9).to_s,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_fault

  puts "\n\nSUBSCRIPTION ID\n\n"
  puts (@post_action_fault[@subscriptionId])


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)


  @action_fault = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_fault)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_fault.body
  expect(@action_fault.code).to eq 200

end









def fail_backend()

    @post_action_fail               = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => "RENEW",
    "digest"                        => "RENEW_FAIL",
    "channel"                       => @channel,
    "eventDate"                     => Time.now.strftime("%Y-%m-%d"),
    "nextRenewalDate"               => Date.today,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => Faker::Number.number(9).to_s,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_fail

  puts "\n\nSUBSCRIPTION ID\n\n"
  puts (@post_action_fail[@subscriptionId])


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)


  @action_fail = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_fail)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_fail.body
  expect(@action_fail.code).to eq 200

end









def renewed_static()

    @post_action_renewed            = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => "RENEW",
    "digest"                        => "RENEWED",
    "channel"                       => @channel,
    "eventDate"                     => Time.now.strftime("%Y-%m-%d"),
    "nextRenewalDate"               => Date.today + 7,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => Faker::Number.number(9).to_i,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_renewed

  puts "\n\nSUBSCRIPTION ID\n\n"
  puts (@post_action_renewed[@subscriptionId])


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)


  @action_renewed = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_renewed)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_renewed.body
  expect(@action_renewed.code).to eq 200

end







def fault_static()

    @post_action_fault              = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => "RENEW",
    "digest"                        => "RENEW_FAULT",
    "channel"                       => @channel,
    "eventDate"                     => Time.now.strftime("%Y-%m-%d"),
    "nextRenewalDate"               => Date.today,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => Faker::Number.number(9).to_i,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_fault

  puts "\n\nSUBSCRIPTION ID\n\n"
  puts (@post_action_fault[@subscriptionId])


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)


  @action_fault = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_fault)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_fault.body
  expect(@action_fault.code).to eq 200

end









def fail_static()

    @post_action_fail               = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => "RENEW",
    "digest"                        => "RENEW_FAIL",
    "channel"                       => @channel,
    "eventDate"                     => Time.now.strftime("%Y-%m-%d"),
    "nextRenewalDate"               => Date.today,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => Faker::Number.number(9).to_i,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_fail

  puts "\n\nSUBSCRIPTION ID\n\n"
  puts (@post_action_fail[@subscriptionId])


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)


  @action_fail = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_fail)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_fail.body
  expect(@action_fail.code).to eq 200

end








def renewed_promo()

  @post_action_subscribe            = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => "RENEW",
    "digest"                        => "RENEWED",
    "channel"                       => @channel,
    "eventDate"                     => Time.now.strftime("%Y-%m-%d"),
    "nextRenewalDate"               => Date.today + 7,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => Faker::Number.number(9).to_i,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_subscribe

  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)

  @action_subscribe = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_subscribe)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_subscribe.body

end









def fault_promo()

  @post_action_subscribe            = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => "RENEW",
    "digest"                        => "RENEW_FAULT",
    "channel"                       => @channel,
    "eventDate"                     => Time.now.strftime("%Y-%m-%d"),
    "nextRenewalDate"               => Date.today,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => Faker::Number.number(9).to_i,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_subscribe

  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)

  @action_subscribe = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_subscribe)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_subscribe.body

end









def fail_promo()

  @post_action_subscribe            = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => "RENEW",
    "digest"                        => "RENEW_FAIL",
    "channel"                       => @channel,
    "eventDate"                     => Time.now.strftime("%Y-%m-%d"),
    "nextRenewalDate"               => Date.today,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => Faker::Number.number(9).to_i,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_subscribe

  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)

  @action_subscribe = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_subscribe)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_subscribe.body

end