require 'securerandom'

def post_service_provider_subscribe(table)

  @parametros                       = table.rows_hash  

  @parametros['userId']             = "#{@parametros['userId']}#{Faker::Number.number(8)}"
  @parametros['eventDate']          = Time.now.strftime("%Y-%m-%d")
  @parametros['nextRenewalDate']    = Time.now.strftime("%Y-%m-%d")
  @parametros['subscriptionKeyword']= [Faker::Name.name, 'OIPROTCOMPL1_DIALIN'].sample
  @parametros['protocol']           = Faker::Number.number(9)
  @parametros['subscriptionId']     = SecureRandom.uuid
  @guuid                            = SecureRandom.uuid

  @post_action_subscribe            = {
    "userId"                        => @parametros['userId'],
    "serviceId"                     => @parametros['serviceId'].to_i,
    "offerKey"                      => @parametros['offerKey'],
    "amountCharged"                 => @parametros['amountCharged'].to_i,
    "event"                         => @parametros['event'],
    "digest"                        => @parametros['digest'],
    "channel"                       => @parametros['channel'],
    "eventDate"                     => @parametros['eventDate'],
    "nextRenewalDate"               => @parametros['nextRenewalDate'],
    "tags"                          => {
      "subscriptionKeyword"         => @parametros['subscriptionKeyword']
    },
    "campaign"                      => @parametros['campaign'],
    "channelProviderId"             => @parametros['channelProviderId'].to_i,
    "protocol"                      => @parametros['protocol'].to_s,
    "subscriptionId"                => @parametros['subscriptionId']
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_subscribe

  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @parametros['subscriptionId'])

  @action_subscribe = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_subscribe)

  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_subscribe.body

end







def renew(table)

  @parametros                       = table.rows_hash  

  @parametros['eventDate']          = Time.now.strftime("%Y-%m-%d")
  @parametros['nextRenewalDate']    = Time.now.strftime("%Y-%m-%d")
  @parametros['property1']          = Faker::Name.name
  @parametros['property2']          = Faker::Name.name
  @parametros['protocol']           = Faker::Number.number(9)
  @guuid                            = SecureRandom.uuid

  @post_action_subscribe            = {
    "userId"                        => @parametros['userId'],
    "serviceId"                     => @parametros['serviceId'].to_i,
    "offerKey"                      => @parametros['offerKey'],
    "amountCharged"                 => @parametros['amountCharged'].to_i,
    "event"                         => @parametros['event'],
    "digest"                        => @parametros['digest'],
    "channel"                       => @parametros['channel'],
    "eventDate"                     => @parametros['eventDate'],
    "nextRenewalDate"               => @parametros['nextRenewalDate'],
    "tags"                          => {
      "keyword"                     => @parametros['property1']
    },
    "campaign"                      => @parametros['campaign'],
    "channelProviderId"             => @parametros['channelProviderId'].to_i,
    "protocol"                      => @parametros['protocol'].to_s,
    "subscriptionId"                => @parametros['subscriptionId']
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_subscribe

  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @parametros['subscriptionId'])

  @action_subscribe = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_subscribe)

  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_subscribe.body

end








def post_service_provider_subscribe_promo()

  @userId                           = "55309#{Faker::Number.number(8)}"
  @serviceId                        = 98772322
  @offerKey                         = "OFFER_KEY"
  @amountCharged                    = 399
  @event                            = "SUBSCRIBE"
  @digest                           = "SUBSCRIBED_BY_CHARGE"
  @channel                          = "SMS"
  @eventDate                        = Time.now.strftime("%Y-%m-%d")
  @nextRenewalDate                  = Time.now.strftime("%Y-%m-%d")
  @property1                        = Faker::Name.name
  @property2                        = Faker::Name.name
  @campaign                         = "HERO"
  @channelProviderId                = 1
  @protocol                         = Faker::Number.number(9).to_i
  @subscriptionId                   = SecureRandom.uuid
  @guuid                            = SecureRandom.uuid

  @post_action_subscribe            = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => @event,
    "digest"                        => @digest,
    "channel"                       => @channel,
    "eventDate"                     => @eventDate,
    "nextRenewalDate"               => @nextRenewalDate,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => @protocol,
    "subscriptionId"                => @subscriptionId,
    "X-TRANSACTION-ID"              => @guuid
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_subscribe

  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)

  @action_subscribe = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_subscribe)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_subscribe.body

end







def post_service_provider_subscribe_backend()

  puts "POM"

  @userId                           = "55309#{Faker::Number.number(8)}"
  @serviceId                        = 451
  @offerKey                         = ['FS-OS-PBM', 'FS-OS-PIM', 'FS-OS-PAM', 'FS-OS-PTM'].sample #ESSE TESTE É PARA O SERVICEID = 451
  @amountCharged                    = 399
  @event                            = "SUBSCRIBE"
  @digest                           = "SUBSCRIBED_BY_CHARGE"
  @channel                          = "SMS"
  @eventDate                        = Time.now.strftime("%Y-%m-%d")
  @nextRenewalDate                  = Time.now.strftime("%Y-%m-%d")
  @property1                        = Faker::Name.name
  @property2                        = Faker::Name.name
  @campaign                         = "PLATFORM_LAUNCH"
  @channelProviderId                = 1
  @protocol                         = Faker::Number.number(9).to_s
  @subscriptionId                   = SecureRandom.uuid
  @guuid                            = SecureRandom.uuid

  @post_action_subscribe            = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => @event,
    "digest"                        => @digest,
    "channel"                       => @channel,
    "eventDate"                     => @eventDate,
    "nextRenewalDate"               => @nextRenewalDate,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => @protocol,
    "subscriptionId"                => @subscriptionId,
    "X-TRANSACTION-ID"              => @guuid
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_subscribe


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)

  @action_subscribe = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_subscribe)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_subscribe.body

end







def post_service_provider_subscribe_backend_standalone()

  puts "STANDALONE"

  @userId                           = "55309#{Faker::Number.number(8)}"
  @serviceId                        = 467
  @offerKey                         = ['FS-PC-PC1M', 'FS-PC-PC3M', 'FS-PC-PC5M'].sample    #ESSE TESTE É PARA O SERVICEID = 467
  @amountCharged                    = 399
  @event                            = "SUBSCRIBE"
  @digest                           = "SUBSCRIBED_BY_CHARGE"
  @channel                          = "SMS"
  @eventDate                        = Time.now.strftime("%Y-%m-%d")
  @nextRenewalDate                  = Time.now.strftime("%Y-%m-%d")
  @property1                        = Faker::Name.name
  @property2                        = Faker::Name.name
  @campaign                         = "PLATFORM_LAUNCH"
  @channelProviderId                = 1
  @protocol                         = Faker::Number.number(9).to_s
  @subscriptionId                   = SecureRandom.uuid
  @guuid                            = SecureRandom.uuid

  @post_action_subscribe            = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => @event,
    "digest"                        => @digest,
    "channel"                       => @channel,
    "eventDate"                     => @eventDate,
    "nextRenewalDate"               => @nextRenewalDate,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => @protocol,
    "subscriptionId"                => @subscriptionId,
    "X-TRANSACTION-ID"              => @guuid
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_subscribe


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)

  @action_subscribe = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_subscribe)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_subscribe.body

end









def post_service_provider_subscribe_interactivity()

  @userId                           = "55309#{Faker::Number.number(8)}"
  @serviceId                        = 98772320
  @offerKey                         = "OFFER_KEY"
  @amountCharged                    = 399
  @event                            = "SUBSCRIBE"
  @digest                           = "SUBSCRIBED_BY_CHARGE"
  @channel                          = "SMS"
  @eventDate                        = Time.now.strftime("%Y-%m-%d")
  @nextRenewalDate                  = Time.now.strftime("%Y-%m-%d")
  @property1                        = Faker::Name.name
  @property2                        = Faker::Name.name
  @campaign                         = "PLATFORM_LAUNCH"
  @channelProviderId                = 1
  @protocol                         = Faker::Number.number(9).to_i
  @subscriptionId                   = SecureRandom.uuid
  @guuid                            = SecureRandom.uuid

  @post_action_subscribe            = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => @event,
    "digest"                        => @digest,
    "channel"                       => @channel,
    "eventDate"                     => @eventDate,
    "nextRenewalDate"               => @nextRenewalDate,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => @protocol,
    "subscriptionId"                => @subscriptionId,
    "X-TRANSACTION-ID"              => @guuid
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_subscribe


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)

  @action_subscribe = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_subscribe)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_subscribe.body

end







def post_service_provider_cancel

  @post_action_cancel               = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => @event,
    "digest"                        => @digest,
    "channel"                       => @channel,
    "eventDate"                     => @eventDate,
    "nextRenewalDate"               => @nextRenewalDate,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => @protocol,
    "subscriptionId"                => @subscriptionId,
    "X-TRANSACTION-ID"              => @guuid
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_cancel


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)

  @action_cancel = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_cancel)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_cancel.body

  sleep 20

end