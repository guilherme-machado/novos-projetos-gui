def consultar_produto_ativo_por_msisdn()

  endpoint               = $api['get_verificaCliente_produtoHero_porMsisdn']
  mount                  = endpoint.gsub("<msisdn>", @msisdns)

  @consulta_produto_ativo = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json'})
  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN CONTRATADO\n\n"
  puts (mount)

  puts "\n\nIMPRIMINDO RESULTADO DA CONSULTA\n\n"
  puts @consulta_produto_ativo.body
    case @consulta_produto_ativo.code
      when 200
        puts "Retornou 200, ok"
      when 404
        puts "Retornou 404, não existe"
      when 400
        puts "Retornou 400, problema de negócio"
      when 500...600
        puts "ops #{@consulta_produto_ativo.code}"
    end

  @getProduto = JSON.parse(@consulta_produto_ativo.body, object_class: OpenStruct)


  if @waitingTimeOut > 7
    puts "\n\nTENTANDO CONSULTAR ASSINATURA\n\n"
    return true
  end

  @waitingTimeOut+=1
  @getProduto.success

end







def consultar_produto_ativo_promo()

	endpoint               = $api['get_verificaCliente_produtoHero_porMsisdn']
  mount                  = endpoint.gsub("<msisdn>", @parametros['userId'])

  @consulta_produto_ativo = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json'})
  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN CONTRATADO\n\n"
  puts (mount)

  puts "\n\nIMPRIMINDO RESULTADO DA CONSULTA\n\n"
 	puts @consulta_produto_ativo.body
  	case @consulta_produto_ativo.code
    	when 200
        puts "Retornou 200, ok"
    	when 404
        puts "Retornou 404, não existe"
    	when 400
        puts "Retornou 400, problema de negócio"
    	when 500...600
        puts "ops #{@consulta_produto_ativo.code}"
  	end

  @getProduto = JSON.parse(@consulta_produto_ativo.body, object_class: OpenStruct)


  if @waitingTimeOut > 7
    puts "\n\nTENTANDO CONSULTAR ASSINATURA\n\n"
    return true
  end

  @waitingTimeOut+=1
  @getProduto.success

end









def consultar_produto_ativo_promo_user_id()

  endpoint               = $api['get_verificaCliente_produtoHero_porMsisdn']
  mount                  = endpoint.gsub("<msisdn>", @userId)

  @consulta_produto_ativo = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json'})
  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN CONTRATADO\n\n"
  puts (mount)

  puts "\n\nIMPRIMINDO RESULTADO DA CONSULTA\n\n"
  puts @consulta_produto_ativo.body
    case @consulta_produto_ativo.code
      when 200
        puts "Retornou 200, ok"
      when 404
        puts "Retornou 404, não existe"
      when 400
        puts "Retornou 400, problema de negócio"
      when 500...600
        puts "ops #{@consulta_produto_ativo.code}"
    end

  @getProduto = JSON.parse(@consulta_produto_ativo.body, object_class: OpenStruct)


  if @waitingTimeOut > 7
    puts "\n\nTENTANDO CONSULTAR ASSINATURA\n\n"
    return true
  end

  @waitingTimeOut+=1
  @getProduto.success

end








def consultar_produto_ativo_backend()

  sleep 3

  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN CONTRATADO\n\n"

  endpoint               = $api['get_verificaCliente_produtoHero_porMsisdn_oi']
  mount                  = endpoint.gsub("<msisdn>", @parametros['userId'])

  @consulta_produto_ativo = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json', "Authorization" => 'YXBpX2xvZ2luX3FhOkY0bDEwdVQ='})
  puts "\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN\n"
  puts (mount)

  puts "\nIMPRIMINDO RESULTADO DA CONSULTA\n"
  puts @consulta_produto_ativo.body
    case @consulta_produto_ativo.code
      when 200
        puts "Retornou 200, ok"
      when 404
        puts "Retornou 404, não existe"
      when 400
        puts "Retornou 400, problema de negócio"
      when 500...600
        puts "ops #{@consulta_produto_ativo.code}"
    end

  @getProduto = JSON.parse(@consulta_produto_ativo.body, object_class: OpenStruct)

end









def consultar_produto_ativo_backend_oi()

  sleep 3

  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN CONTRATADO\n\n"

  endpoint               = $api['get_verificaCliente_produtoHero_porMsisdn_oi']
  mount                  = endpoint.gsub("<msisdn>", @userId)

  
  @consulta_produto_ativo = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json', "Authorization" => 'YXBpX2xvZ2luX3FhOkY0bDEwdVQ='})
  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN\n\n"
  puts (mount)

  puts "\n\nIMPRIMINDO RESULTADO DA CONSULTA\n\n"
  puts @consulta_produto_ativo.body
    case @consulta_produto_ativo.code
      when 200
        puts "Retornou 200, ok"
      when 404
        puts "Retornou 404, não existe"
      when 400
        puts "Retornou 400, problema de negócio"
      when 500...600
        puts "ops #{@consulta_produto_ativo.code}"
    end

  @getProduto = JSON.parse(@consulta_produto_ativo.body, object_class: OpenStruct)


end









def consultar_produto_ativo_static()

  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN CONTRATADO\n\n"

  endpoint               = $api['get_verificaCliente_produto_porMsisdn_static']
  mount                  = endpoint.gsub("<msisdn>", @parametros['userId'])

  sleep 3

  @consulta_produto_ativo = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json', "Authorization" => 'YXBpX2xvZ2luX3FhOkY0bDEwdVQ='})
  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN\n\n"
  puts (mount)

  puts "\n\nIMPRIMINDO RESULTADO DA CONSULTA\n\n"
  puts @consulta_produto_ativo.body
    case @consulta_produto_ativo.code
      when 200
        puts "Retornou 200, ok"
      when 404
        puts "Retornou 404, não existe"
      when 400
        puts "Retornou 400, problema de negócio"
      when 500...600
        puts "ops #{@consulta_produto_ativo.code}"
    end

  @getProduto = JSON.parse(@consulta_produto_ativo.body, object_class: OpenStruct)

end









def consultar_produto_ativo_static_msisdn()

  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN CONTRATADO\n\n"

  endpoint               = $api['get_verificaCliente_produto_porMsisdn_static']
  mount                  = endpoint.gsub("<msisdn>", @userId)

  sleep 3

  @consulta_produto_ativo = HTTParty.get(mount,:headers => {"Content-Type" => 'application/json', "Authorization" => 'YXBpX2xvZ2luX3FhOkY0bDEwdVQ='})
  puts "\n\nIMPRIMINDO CONSULTA QUE FOI FEITA DO MSISDN\n\n"
  puts (mount)

  puts "\n\nIMPRIMINDO RESULTADO DA CONSULTA\n\n"
  puts @consulta_produto_ativo.body
    case @consulta_produto_ativo.code
      when 200
        puts "Retornou 200, ok"
      when 404
        puts "Retornou 404, não existe"
      when 400
        puts "Retornou 400, problema de negócio"
      when 500...600
        puts "ops #{@consulta_produto_ativo.code}"
    end

  @getProduto = JSON.parse(@consulta_produto_ativo.body, object_class: OpenStruct)

end