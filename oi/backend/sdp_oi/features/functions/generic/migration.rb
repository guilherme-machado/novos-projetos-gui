def post_service_provider_migration_promo()

  @userId                           = @userId
  @serviceId                        = @serviceId
  @offerKey                         = @offerKey
  @amountCharged                    = @amountCharged
  @event                            = @event
  @digest                           = "MIGRATION"
  @channel                          = @channel
  @eventDate                        = Time.now.strftime("%Y-%m-%d")
  @nextRenewalDate                  = Time.now.strftime("%Y-%m-%d")
  @property1                        = Faker::Name.name
  @property2                        = Faker::Name.name
  @campaign                         = "HERO"
  @channelProviderId                = 1
  @protocol                         = Faker::Number.number(9).to_i
  @subscription_Migracao            = SecureRandom.uuid
  @guuid                            = SecureRandom.uuid

  @post_action_migrate              = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => @event,
    "digest"                        => @digest,
    "channel"                       => @channel,
    "eventDate"                     => @eventDate,
    "nextRenewalDate"               => @nextRenewalDate,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => @protocol,
    "subscriptionId"                => @subscription_Migracao
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_migrate

  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscription_Migracao)

  @action_migrate = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_migrate)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_migrate.body

end







def post_service_provider_migration_backend()

  @userId                           = @userId
  @serviceId                        = @serviceId
  @offerKey                         = @offerKey
  @amountCharged                    = @amountCharged
  @event                            = @event
  @digest                           = "MIGRATION"
  @channel                          = @channel
  @eventDate                        = Time.now.strftime("%Y-%m-%d")
  @nextRenewalDate                  = Date.today + 7
  @property1                        = Faker::Name.name
  @property2                        = Faker::Name.name
  @campaign                         = "PLATFORM_LAUNCH"
  @channelProviderId                = 1
  @protocol                         = Faker::Number.number(9).to_s
  @subscription_Migracao            = SecureRandom.uuid
  @guuid                            = SecureRandom.uuid

  @post_action_migrate              = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => @event,
    "digest"                        => @digest,
    "channel"                       => @channel,
    "eventDate"                     => @eventDate,
    "nextRenewalDate"               => @nextRenewalDate,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => @protocol,
    "subscriptionId"                => @subscription_Migracao
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_migrate


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscription_Migracao)

  @action_migrate = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_migrate)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_migrate.body

end







def post_service_provider_migration_interactivity()

  @userId                           = @userId
  @serviceId                        = @serviceId
  @offerKey                         = @offerKey
  @amountCharged                    = @amountCharged
  @event                            = @event
  @digest                           = "MIGRATION"
  @channel                          = @channel
  @eventDate                        = Time.now.strftime("%Y-%m-%d")
  @nextRenewalDate                  = Time.now.strftime("%Y-%m-%d")
  @property1                        = Faker::Name.name
  @property2                        = Faker::Name.name
  @campaign                         = "PLATFORM_LAUNCH"
  @channelProviderId                = 1
  @protocol                         = Faker::Number.number(9).to_i
  @subscription_Migracao            = SecureRandom.uuid
  @guuid                            = SecureRandom.uuid

  @post_action_migrate              = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => @event,
    "digest"                        => @digest,
    "channel"                       => @channel,
    "eventDate"                     => @eventDate,
    "nextRenewalDate"               => @nextRenewalDate,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => @protocol,
    "subscriptionId"                => @subscription_Migracao
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_migrate


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscription_Migracao)

  @action_migrate = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_migrate)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_migrate.body

end