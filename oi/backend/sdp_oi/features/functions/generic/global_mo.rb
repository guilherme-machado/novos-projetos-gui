def mo_acao()

  @guuid                            = SecureRandom.uuid

  @post_action_mo                   = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "shortCode"                     => @shortCode,
    "text"                          => @text
}.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_mo


  @mo = HTTParty.post($api['mo_notify'],:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_mo)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts ($api['mo_notify'])

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @mo.body

end