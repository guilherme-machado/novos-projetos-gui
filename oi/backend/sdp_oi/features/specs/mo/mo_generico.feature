#language: pt
Funcionalidade: MO - OK
    Como uma API, quando eu chamar o endpoint de MO quero ser notificado das interações

@mo @mo_tarifa @promo @mo_promo_tarifa
Esquema do Cenario: ----- MO ASSINANTE TARIFA - PROMO
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de tarifa para um assinante será enviada atraves do backend da promo
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"PROMO"    |5511968150942   |98772322       |10910      |"tarifa"  |200         |
    |"PROMO"    |1               |98772322       |10910      |"tarifa"  |200         |
    |"PROMO"    |5511968150942   |1              |10910      |"tarifa"  |200         |
    |"PROMO"    |5511968150942   |98772322       |1          |"tarifa"  |200         |
    |"PROMO"    |5511968150942   |98772322       |10910      |"a"       |200         |


@mo @mo_tarifa @backend @mo_backend_tarifa
Esquema do Cenario: ----- MO ASSINANTE TARIFA - BACKEND
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de tarifa para um assinante será enviada atraves do backend
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"BACKEND"  |5511968150942   |98772321       |3990       |"tarifa"  |200         |
    |"BACKEND"  |2               |98772321       |3990       |"tarifa"  |200         |
    |"BACKEND"  |5511968150942   |2              |3990       |"tarifa"  |200         |
    |"BACKEND"  |5511968150942   |98772321       |2          |"tarifa"  |200         |
    |"BACKEND"  |5511968150942   |98772321       |3990       |"b"       |200         |


@mo @mo_tarifa @static @mo_static_tarifa
Esquema do Cenario: ----- MO ASSINANTE TARIFA - STATIC
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de tarifa para um assinante será enviada atraves do backend do static
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"STATIC"   |5511968150942   |98772320       |22020      |"tarifa"  |200         |
    |"STATIC"   |3               |98772320       |22020      |"tarifa"  |200         |
    |"STATIC"   |5511968150942   |3              |22020      |"tarifa"  |200         |
    |"STATIC"   |5511968150942   |98772320       |3          |"tarifa"  |200         |
    |"STATIC"   |5511968150942   |98772320       |22020      |"c"       |200         |


@mo @mo_saldo @promo @mo_promo_saldo
Esquema do Cenario: ----- MO ASSINANTE SALDO - PROMO
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de saldo para um assinante será enviada atraves do backend da promo
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"PROMO"    |5511968150942   |98772322       |10910      |"saldo"   |200         |


@mo @mo_saldo @backend @mo_backend_saldo
Esquema do Cenario: ----- MO ASSINANTE SALDO - BACKEND
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de saldo para um assinante será enviada atraves do backend
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"BACKEND"  |5511968150942   |98772321       |3990       |"saldo"   |200         |


@mo @mo_saldo @static @mo_static_saldo
Esquema do Cenario: ----- MO ASSINANTE SALDO - STATIC
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de saldo para um assinante será enviada atraves do backend do static
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"STATIC"   |5511968150942   |98772320       |22020      |"saldo"   |200         |


@mo @mo_ajuda @promo @mo_ajuda_promo
Esquema do Cenario: ----- MO ASSINANTE AJUDA - PROMO
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de ajuda para um assinante será enviada atraves do backend da promo
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"PROMO"    |5511968150942   |98772322       |10910      |"ajuda"   |200         |


@mo @mo_ajuda @backend @mo_ajuda_backend
Esquema do Cenario: ----- MO ASSINANTE AJUDA - BACKEND
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de ajuda para um assinante será enviada atraves do backend
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"BACKEND"  |5511968150942   |98772321       |3990       |"ajuda"   |200         |


@mo @mo_ajuda @static @mo_ajuda_static
Esquema do Cenario: ----- MO ASSINANTE AJUDA - STATIC
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de ajuda para um assinante será enviada atraves do backend do static
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"STATIC"   |5511968150942   |98772320       |22020      |"ajuda"   |200         |


@mo @mo_kw_invalida @promo @mo_kw_invalida_promo
Esquema do Cenario: ----- MO ASSINANTE KEYWORD INVALIDA - PROMO
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de erro para um assinante será enviada atraves do backend da promo
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"PROMO"    |5511968150942   |98772322       |10910      |"sim"     |200         |


@mo @mo_kw_invalida @backend @mo_kw_invalida_backend
Esquema do Cenario: ----- MO ASSINANTE KEYWORD INVALIDA - BACKEND
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de erro para um assinante será enviada atraves do backend
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"BACKEND"  |5511968150942   |98772321       |3990       |"erro"    |200         |


@mo @mo_kw_invalida @static @mo_kw_invalida_promo
Esquema do Cenario: ----- MO ASSINANTE KEYWORD INVALIDA - STATIC
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de erro para um assinante será enviada atraves do backend do static
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"STATIC"   |5511968150942   |98772320       |22020      |"erro"    |200         |


@mo @mo_tarifa_nao_assinante @promo @tarifa_nao_assinante_promo
Esquema do Cenario: ----- MO NAO ASSINANTE - PROMO
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de tarifa para um nao assinante será enviada atraves do backend da promo
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"PROMO"    |5530000000000   |98772322       |10910      |"tarifa"  |200         |


@mo @mo_tarifa_nao_assinante @backend @tarifa_nao_assinante_backend
Esquema do Cenario: ----- MO NAO ASSINANTE - BACKEND
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de tarifa para um nao assinante será enviada atraves do backend
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"BACKEND"  |5530000000000   |98772321       |3990       |"tarifa"  |200         |


@mo @mo_tarifa_nao_assinante @static @tarifa_nao_assinante_static
Esquema do Cenario: ----- MO NAO ASSINANTE - STATIC
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de tarifa para um nao assinante será enviada atraves do backend do static
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"STATIC"   |5530000000000   |98772320       |22020      |"tarifa"  |200         |


@mo @mo_saldo_nao_assinante @promo @saldo_nao_assinante_promo
Esquema do Cenario: ----- MO NAO ASSINANTE - PROMO
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de saldo para um nao assinante será enviada atraves do backend da promo
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"PROMO"    |5530000000000   |98772322       |10910      |"saldo"   |200         |


@mo @mo_saldo_nao_assinante @backend @saldo_nao_assinante_backend
Esquema do Cenario: ----- MO NAO ASSINANTE - BACKEND
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de saldo para um nao assinante será enviada atraves do backend
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"BACKEND"  |5530000000000   |98772321       |3990       |"saldo"   |200         |


@mo @mo_saldo_nao_assinante @static @saldo_nao_assinante_static
Esquema do Cenario: ----- MO NAO ASSINANTE - STATIC
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de saldo para um nao assinante será enviada atraves do backend do static
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"STATIC"   |5530000000000   |98772320       |22020      |"saldo"   |200         |



@mo @mo_ajuda_nao_assinante @promo @ajuda_nao_assinante_promo
Esquema do Cenario: ----- MO NAO ASSINANTE - PROMO
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de ajuda para um nao assinante será enviada atraves do backend da promo
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"PROMO"    |5530000000000   |98772322       |10910      |"ajuda"   |200         |


@mo @mo_ajuda_nao_assinante @backend @ajuda_nao_assinante_backkend
Esquema do Cenario: ----- MO NAO ASSINANTE - BACKEND
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de ajuda para um nao assinante será enviada atraves do backend
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"BACKEND"  |5530000000000   |98772321       |3990       |"ajuda"   |200         |


@mo @mo_ajuda_nao_assinante @static @ajuda_nao_assinante_static
Esquema do Cenario: ----- MO NAO ASSINANTE - STATIC
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de ajuda para um nao assinante será enviada atraves do backend do static
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"STATIC"   |5530000000000   |98772320       |22020      |"ajuda"   |200         |


@mo @mo_kw_invalida_nao_assinante @promo @kw_invalida_promo
Esquema do Cenario: ----- MO NAO ASSINANTE - PROMO
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de erro para nao assinante será enviada atraves do backend da promo
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"PROMO"    |5530000000000   |98772322       |10910      |"erro"    |200         |


@mo @mo_kw_invalida_nao_assinante @backend @kw_invalida_backend
Esquema do Cenario: ----- MO NAO ASSINANTE - BACKEND
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de erro para nao assinante será enviada atraves do backend
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"BACKEND"  |5530000000000   |98772321       |3990       |"erro"    |200         |


@mo @mo_kw_invalida_nao_assinante @static @kw_invalida_static
Esquema do Cenario: ----- MO NAO ASSINANTE - STATIC
    
    Dado que eu consulte um plano ativo
    Quando vier uma requisicao mo com os parametros <userId>, <serviceId>, <shortCode> e <text>
    Então o feedback devera ser enviado para o cliente retornando o <status_code>
    E a mensagem de erro para nao assinante será enviada atraves do backend do static
    Exemplos:
    |nome       |userId          |serviceId      |shortCode  |text      |status_code |
    |"STATIC"   |5530000000000   |98772320       |22020      |"erro"    |200         |