#language: pt
Funcionalidade: Migration
    Como uma API, quando eu chamar o endpoint de migration, quando o cliente tiver uma assinatura, entao o mesmo devera ser migrado.

@migration @migration_static @cenarios_positivos
Cenario: Migration - STATIC
    
    Dado que eu tenha um plano ativo do INTERACTIVITY
    Quando vier uma requisicao de migration do Global para o backend static
    Então meu plano devera ser migrado com sucesso para o static


@migration @migration_static_assina @cenarios_positivos
Cenario: Migration Sem Assinatura- STATIC
    
    Dado que eu queira realizar uma migração no static
    Quando eu ainda não tiver assinatura no static
    Então espero que o plano do static seja assinado do lado FS