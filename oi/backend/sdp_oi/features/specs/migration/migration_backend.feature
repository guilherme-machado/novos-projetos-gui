#language: pt
Funcionalidade: Migration
    Como uma API, quando eu chamar o endpoint de migration, quando o cliente tiver uma assinatura, entao o mesmo devera ser migrado.

@migration @migration_b @migration_backend @cenarios_positivos @backend_oi @rodar
Cenario: Migration - BACKEND
    
    Dado que eu tenha um plano ativo do tipo POM do BACKEND
    Quando vier uma requisicao de migration do Global para o backend
    Então meu plano devera ser migrado com sucesso para o backend


@migration @migration_b @migration_backend_assina @cenarios_positivos @backend_oi @rodar
Cenario: Migration Sem Assinatura- BACKEND
    
    Dado que eu queira realizar uma migração no backend
    Quando eu ainda não tiver assinatura no backend
    Então espero que o plano do backend seja assinado do lado FS