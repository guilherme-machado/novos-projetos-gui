#language: pt
Funcionalidade: Migration
    Como uma API, quando eu chamar o endpoint de migration, quando o cliente tiver uma assinatura, entao o mesmo devera ser migrado.

@migration @migration_promo @migration_promo @cenarios_positivos
Cenario: Migration - PROMO
    
    Dado que eu tenha um plano ativo da PROMO
    Quando vier uma requisicao de migration do Global para o backend promo
    Então meu plano devera ser migrado com sucesso para a promo


@migration @migration_promo_assina @migration_promo @cenarios_positivos
Cenario: Migration Sem Assinatura- PROMO
    
    Dado que eu queira realizar uma migração para a promo
    Quando eu ainda não tiver assinatura na promo
    Então espero que o plano da promo seja assinado do lado FS