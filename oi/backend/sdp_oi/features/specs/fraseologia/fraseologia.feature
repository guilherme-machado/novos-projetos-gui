#language: pt
Funcionalidade: Fraseologias OI
Como uma API, quando eu chamar o endpoint do dispacher desejo saber qual a mensagem foi enviada

@fraseologia_oi
Esquema do Cenario:Fraseologia Variada
	Dado que eu queira saber qual a mensagem será envia ao usuario
	Quando eu chamar o endpoint do dispatcher passando <type>, <carrier> e <shortnumber>
	Então devo receber a mensagem correspondente ao tipo de comunicação enviada <message>

  Exemplos:
  |type       									 |carrier|shortnumber|message|
  |"ACTIVATION_MESSAGE"          |"oi"   |5990       |"Oi Seguranca: Aproveite o Protecao Completa e proteja suas informacoes, use o PIN: ${pincode}. Guarde-o em seguranca. Duvidas? http://oiseguranca.com.br/chat"|
  |"CANCEL_NO_PRODUCTS"          |"oi"   |5990       |"Oi Seguranca: Voce ainda nao tem um plano de seguranca digital. Fique protegido!Envie INFO e conheca os planos para navegar com tranquilidade ou acesse oiseguranca.com.br/chat"|
  |"CANCELLING_PRODUCT"          |"oi"   |5990       |"Oi Seguranca: Seu pacote ${produto} foi cancelado por falta de creditos. Garanta total protecao na internet! Responda com a palavra INFO."|  
  |"DOWNGRADE"                   |"oi"   |5990       |"Oi Seguranca: Agora voce tem a ${produto} por RS ${preco}/mes. Acesse ${link_download} e instale. Duvidas? oiseguranca.com.br/chat"|         
  |"HAS_MAXIMUM_ALLOWED_PRODUCTS"|"oi"   |5990       |"Oi Seguranca: Voce ja possui a ${produto}. Quer ficar ainda mais protegido? Envie AJUDA para 3990 ou acesse http://oiseguranca.com.br/chat."|       
  |"HELP_NO_PRODUCTS"            |"oi"   |5990       |"Oi Seguranca: Envie: INFO para Descricao USAR para Utilizacao PLANOS para Assinatura PRECO para valores CANCELAR para cancelamento"|   
  |"HELP_WITH_PRODUCTS"          |"oi"   |5990       |"Oi Seguranca: Envie: INFO para Descricao USAR para Utilizacao PLANOS para Assinatura PRECO para valores CANCELAR para cancelamento"|     
  |"HOW_TO_CANCEL"               |"oi"   |5990       |"Para cancelar o Oi Seguranca, envie a palavra SAIR para o numero 5990."|   
  |"HOW_TO_USE"                  |"oi"   |5990       |"Utilize seu Oi Seguranca baixando o app em ${link_download}. Depois acesse http://oiseguranca.com.br/minha-protecao com senha ${senha}."|      
  |"INFO"                        |"oi"   |5990       |"Oi Seguranca: Envie o numero e assine a Protecao: 1 - Basica: RS3,90/mes 2 - Intermediaria: RS5,90/mes 3 - Avancada: RS9,90/mes"|      
  |"INFO_NO_PRODUCTS"            |"oi"   |5990       |"O Oi Seguranca e um combo de seguranca digital para voce e sua familia. Proteja-se contra as ameacas da internet e ineficiencia de seus dispositivos"|
  |"INFO_PRODUCTS"               |"oi"   |5990       |"O Oi Seguranca e um combo de seguranca digital para voce e sua familia. Proteja-se contra as ameacas da internet e ineficiencia de seus dispositivos"|
  |"INSUFFICIENT_FUNDS"          |"oi"   |5990       |"Oi Seguranca: Nao foi possivel realizar a cobranca de RS${preco}. Mesmo assim vc pode usar a Protecao Basica e sera tarifado quando recarregar. Duvidas? Envie AJUDA"|   
  |"KEY_WITH_PRODUCTS"           |"oi"   |5990       |"Oi Seguranca: Pra instalar sua ${produto} acesse ${link_download}. Duvidas? Acesse oiseguranca.com.br/chat"|   
  |"NOT_FOUND_NO_PRODUCTS"       |"oi"   |5990       |"Oi Seguranca: Voce enviou um comando invalido. Esta com duvidas sobre o Protecao Completa? Envie AJUDA."|    
  |"NOT_FOUND_WITH_PRODUCTS"     |"oi"   |5990       |"Oi Seguranca: Voce enviou um comando invalido. Esta com duvidas ? Envie AJUDA para 5990 ou acesse: oiseguranca.com.br/chat"|   
  |"RENEW_MESSAGE_WITH_SAIR"     |"oi"   |5990       |"Oi Seguranca: Sua assinatura da ${produto} foi renovada com sucesso por RS ${preco}/mes. Se quiser cancelar, envie SAIR. Duvidas? oiseguranca.com.br/chat"|
  |"RENEW_MESSAGE_WITHOUT_SAIR"  |"oi"   |5990       |"Oi Seguranca: Sua assinatura da ${produto} foi renovada com sucesso por apenas RS ${preco}/mes. Duvidas? Envie AJUDA. Se quiser cancelar, envie SAIR"|
  |"UPGRADE"                     |"oi"   |5990       |"Oi Seguranca: Otima escolha! Agora voce tem a ${produto} por RS ${preco}/mes. Acesse ${link_download} e instale. Duvidas? Envie AJUDA."|         




@fraseologia_oi
Cenario:Fraseologia de boas vindas
  Dado que eu queira saber quais as mensagens enviadas ao usuario
  Quando eu chamar o endpoint do dispatcher passando WELCOME
  Então devo receber 2 mensagens correspondente ao tipo de comunicação enviada

