#language: pt
Funcionalidade: Cancelamento do Static Para o Global
	Cancelar via CallCenter


@cancela_static_global_internal @cancel_callcenter @cenarios_positivos
Cenario: Cancelar operadoras do Static para o Global - OI
    
	Dado que eu envie uma solicitação de cancelamento do static
  	Quando a requisição for feita corretamente para o global
	Então a assinatura devera ser cancelada