#language: pt
Funcionalidade: Cancelamento do subscription Para o DataSync
	Cancelar via CallCenter


@cancela_subscription_datasync_oi @cancel_callcenter @cenarios_positivos
Esquema do Cenario: Cancelar operadoras do Subscription para o Datasync - OI
    
	Dado que eu envie uma solicitação de cancelamento para a <operadora> do subscriber para o datasync
  	Quando a requisição for feita corretamente para a oi
	Então o subscription vai mandar uma solicitação para o Datasync realizar o cancelamento para a oi

	Exemplos:
	|operadora|
	|"oi"	  |