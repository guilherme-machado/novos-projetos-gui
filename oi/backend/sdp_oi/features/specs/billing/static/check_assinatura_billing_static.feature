#language: pt
Funcionalidade: Billing
    Billing

@assina_static_billing @billing @cenarios_positivos
Cenario: Integração assinatura Global e Billing para o Static
    
    Dado que eu tenha um plano ativo assinado no static
    Quando eu fizer o check credit
    E rodar o datasync para o static
    Entao a assinatura vai ser retornada com sucesso mostrando o status do billing da fatura paga e recorrencia aguardando check credit
    E se o cancelamento for solicitado
    Entao o cancelamento deve ser feito no backend e no billing