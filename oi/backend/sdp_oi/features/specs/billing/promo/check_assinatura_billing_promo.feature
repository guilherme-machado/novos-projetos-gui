#language: pt
Funcionalidade: Billing
    Billing

@assina_promo_billing @billing @cenarios_positivos
Cenario: Integração assinatura Global e Billing para a PROMO
    
    Dado que eu tenha um plano ativo assinado na promo
    Quando eu fizer o check credit
    E rodar o datasync para a promo
    Entao a assinatura vai ser retornada com sucesso mostrando o status do billing da fatura paga e recorrencia aguardando check credit
    E se o cancelamento for solicitado
    Entao o cancelamento deve ser feito no backend e no billing
   