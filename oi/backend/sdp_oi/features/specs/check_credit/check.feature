#language: pt
Funcionalidade: Check Credit
	Cancelar via CallCenter

@check @cenarios_positivos @backend_oi @cenarios_positivos
Esquema do Cenario: Check Credit
    
    Dado que seja feita uma requisição de assinatura vindo do global com os parametros mocados
    Quando os parametros forem enviados com o método post no ambiente de homol
    | userId            | <userId>           |
    | serviceId         | <serviceId>        |
    | offerKey          | <offerKey>         |
    | amountCharged     | <amountCharged>    |
    | event             | <event>            |
    | digest            | <digest>           |
    | channel           | <channel>          |
    | eventDate         | <eventDate>        |
    | nextRenewalDate   | <nextRenewalDate>  |
    | property1         | <property1>        |
    | property2         | <property2>        |
    | campaign          | <campaign>         |
    | channelProviderId | <channelProviderId>|
    | protocol          | <protocol>         |
    | subscriptionId    | <subscriptionId>   |
    Então a assinatura devera retornar o status <status_code>
    Exemplos:
    |DETALHE  				|userId   		|serviceId   |offerKey      |amountCharged |event       |digest                  |channel         |eventDate | nextRenewalDate | property1 | property2 | campaign         | channelProviderId | protocol | subscriptionId | status_code |
    |MSISDN_PRE_SALDO_0		|5521111111111  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521111111111   |200          |
    |MSISDN_PRE_SALDO_0		|5521111111111  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_TRIAL     |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521111111111   |200          |
    |MSISDN_PRE_SALDO_0		|5521111111111  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAULT     |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521111111111   |200          |
    |MSISDN_PRE_SALDO_0		|5521111111111  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAIL	     |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521111111111   |200          |
    |MSISDN_PRE_SALDO_1		|5521222222222  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SIMCARD         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521222222222   |200          |
    |MSISDN_PRE_SALDO_1		|5521222222222  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_TRIAL     |SIMCARD         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521222222222   |200          |
    |MSISDN_PRE_SALDO_1		|5521222222222  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAULT     |SIMCARD         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521222222222   |200          |
    |MSISDN_PRE_SALDO_1		|5521222222222  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAIL      |SIMCARD         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521222222222   |200          |
    |MSISDN_POS				|5521444444444  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521444444444   |200          |
    |MSISDN_POS				|5521444444444  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_TRIAL     |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521444444444   |200          |
    |MSISDN_POS				|5521444444444  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAULT     |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521444444444   |200          |
    |MSISDN_POS				|5521444444444  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAIL      |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5521444444444   |200          | 
    |MSISDN_TIMEOUT			|5511111111111  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511111111111   |200          |
    |MSISDN_TIMEOUT			|5511111111111  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_TRIAL     |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511111111111   |200          |
    |MSISDN_TIMEOUT			|5511111111111  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAULT     |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511111111111   |200          |
    |MSISDN_TIMEOUT			|5511111111111  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAIL      |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511111111111   |200          |
    |MSISDN_BAD_REQUEST		|5511222222222  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |WAP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511222222222   |200          |
    |MSISDN_BAD_REQUEST		|5511222222222  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_TRIAL     |WAP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511222222222   |200          |
    |MSISDN_BAD_REQUEST		|5511222222222  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAULT     |WAP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511222222222   |200          |
    |MSISDN_BAD_REQUEST		|5511222222222  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAIL      |WAP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511222222222   |200          |
    |MSISDN_INTERNAL_ERROR	|5511333333333  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |APP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511333333333   |200          |
    |MSISDN_INTERNAL_ERROR	|5511333333333  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_TRIAL     |APP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511333333333   |200          |
    |MSISDN_INTERNAL_ERROR	|5511333333333  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAULT     |APP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511333333333   |200          |
    |MSISDN_INTERNAL_ERROR	|5511333333333  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAIL      |APP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511333333333   |200          |
    |MSISDN_PARSE_ERROR		|5511444444444  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |IVR             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511444444444   |200          |
    |MSISDN_PARSE_ERROR		|5511444444444  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_TRIAL     |IVR             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511444444444   |200          |
    |MSISDN_PARSE_ERROR		|5511444444444  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAULT     |IVR             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511444444444   |200          |
    |MSISDN_PARSE_ERROR		|5511444444444  |98772321    |INTERMEDIARIA |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAIL      |IVR             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |5511444444444   |200          |
    