#language: pt
Funcionalidade: Cancel
    Como uma API, quando eu chamar o endpoint de cancel quando vier o parametro Event setado com CANCEL espero ter uma assinatura ativa.

@cancel @cancel_promo @cenarios_positivos
Esquema do Cenario: Cancel - PROMO - POSITIVOS
    
    Dado que eu tenha um plano ativo da PROMO
    Quando vier uma requisicao <event> do Global
    E os parametros forem enviados com o método post <digest> e Event de CANCEL
    Então a solicitação devera ser executada obedecendo o status da requisição <status_code> ok para a promo
    Exemplos:
    |event       	 |digest					    |status_code|
    |"CANCEL"	     |"UNSUBSCRIBED_VOLUNTARY"  	|200		|


@erro_cancel @erro_cancel_promo
Esquema do Cenario: Cancel - PROMO - NEGATIVOS
    
    Dado que eu tenha um plano ativo da PROMO
    Quando vier uma requisicao <event> do Global
    E os parametros forem enviados com o método post <digest> e Event de CANCEL
    Então a solicitação devera ser executada obedecendo o status da requisição <status_code> falha para a promo
    Exemplos:
    |event           |digest                        |status_code|
    |""              |"UNSUBSCRIBED_VOLUNTARY"      |400        |
    |"CANCEL"        |""                            |400        |
    |"RENEW"         |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"SUBSCRIBE"     |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"ewrtyuuyt"     |"UNSUBSCRIBED_VOLUNTARY"      |400        |



@cancel @cancel_backend @cenarios_positivos @backend_oi @rodar @cancel_pom
Esquema do Cenario: Cancel - BACKEND - POSITIVOS - POM
    
    Dado que eu tenha um plano ativo do tipo POM do BACKEND
    Quando vier uma requisicao <event> do Global
    E os parametros forem enviados com o método post <digest> e Event de CANCEL
    Então a solicitação devera ser executada obedecendo o status da requisição <status_code> ok para o backend - POM
    Exemplos:
    |event           |digest                        |status_code|
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |


@cancel @cancel_backend @cenarios_positivos @backend_oi @rodar @cancel_standalone
Esquema do Cenario: Cancel - BACKEND - POSITIVOS - STANDALONE
    
    Dado que eu tenha um plano ativo do tipo STANDALONE do BACKEND
    Quando vier uma requisicao <event> do Global
    E os parametros forem enviados com o método post <digest> e Event de CANCEL
    Então a solicitação devera ser executada obedecendo o status da requisição <status_code> ok para o backend - STANDALONE
    Exemplos:
    |event           |digest                        |status_code|
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |


@erro_cancel @erro_cancel_backend
Esquema do Cenario: Cancel - BACKEND - NEGATIVOS
    
    Dado que eu tenha um plano ativo do BACKEND
    Quando vier uma requisicao <event> do Global
    E os parametros forem enviados com o método post <digest> e Event de CANCEL
    Então a solicitação devera ser executada obedecendo o status da requisição <status_code> falha para o backend
    Exemplos:
    |event           |digest                        |status_code|
    |""              |"UNSUBSCRIBED_VOLUNTARY"      |400        |
    |"CANCEL"        |""                            |400        |
    |"RENEW"         |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"SUBSCRIBE"     |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"ewrtyuuyt"     |"UNSUBSCRIBED_VOLUNTARY"      |400        |


@cancel @cancel_static @cenarios_positivos
Esquema do Cenario: Cancel - INTERACTIVITY - POSITIVOS
    
    Dado que eu tenha um plano ativo do INTERACTIVITY
    Quando vier uma requisicao <event> do Global
    E os parametros forem enviados com o método post <digest> e Event de CANCEL
    Então a solicitação devera ser executada obedecendo o status da requisição <status_code> ok para o static
    Exemplos:
    |event           |digest                        |status_code|
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |


@erro_cancel @erro_cancel_static
Esquema do Cenario: Cancel - INTERACTIVITY - NEGATIVOS
    
    Dado que eu tenha um plano ativo do INTERACTIVITY
    Quando vier uma requisicao <event> do Global
    E os parametros forem enviados com o método post <digest> e Event de CANCEL
    Então a solicitação devera ser executada obedecendo o status da requisição <status_code> falha para o static
    Exemplos:
    |event           |digest                        |status_code|
    |""              |"UNSUBSCRIBED_VOLUNTARY"      |400        |
    |"CANCEL"        |""                            |400        |
    |"RENEW"         |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"SUBSCRIBE"     |"UNSUBSCRIBED_VOLUNTARY"      |200        |
    |"ewrtyuuyt"     |"UNSUBSCRIBED_VOLUNTARY"      |400        |