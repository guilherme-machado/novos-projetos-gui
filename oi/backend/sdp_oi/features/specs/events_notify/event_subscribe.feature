#language: pt
Funcionalidade: Subscribe
	Como uma API, quando eu chamar o endpoint de subscribe quando vier o parametro Event setado com SUBSCRIBE espero ter uma assinatura ativa.

@subscribe_promo @subscribe @cenarios_positivos
Esquema do Cenario: Subscribe - PROMO - POSITIVOS
    
 	Dado que seja feita uma requisição de assinatura vindo do global
  	Quando os parametros forem enviados com o método post com Event de SUBSCRIBE na PROMO
    | userId     		| <userId>   		 |
    | serviceId  		| <serviceId>		 |
    | offerKey   		| <offerKey> 		 |
    | amountCharged   	| <amountCharged> 	 |
    | event   			| <event> 			 |
    | digest   			| <digest> 			 |
    | channel   		| <channel> 		 |
    | eventDate   		| <eventDate> 		 |
    | nextRenewalDate   | <nextRenewalDate>  |
    | property1   		| <property1> 		 |
    | property2   		| <property2> 		 |
    | campaign   		| <campaign> 		 |
    | channelProviderId | <channelProviderId>|
    | protocol   		| <protocol> 		 |
    | subscriptionId   	| <subscriptionId> 	 |
	Então a assinatura devera retornar o status <status_code> sucesso para operação da assinatura da promo
    Exemplos:
    |userId   |serviceId     |offerKey  |amountCharged |event       |digest                  |channel         |eventDate | nextRenewalDate | property1 | property2 | campaign         | channelProviderId | protocol | subscriptionId | status_code |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAULT     |SATPUSH         |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAIL      |SMS             |          |                 |           |           |HERO              |1                  |          |                |200          |    
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |WAP             |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |APP             |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |IVR             |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |USSD            |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SATPUSH         |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SIMCARD         |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |APP             |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SYSTEM          |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |CUSTOMER_SERVICE|          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |                  |1                  |          |                |200          |

@erro_subscribe_promo @erro_subscribe
Esquema do Cenario: Subscribe - PROMO - NEGATIVOS
    
    Dado que seja feita uma requisição de assinatura vindo do global
    Quando os parametros forem enviados com o método post com Event de SUBSCRIBE na PROMO
    | userId            | <userId>           |
    | serviceId         | <serviceId>        |
    | offerKey          | <offerKey>         |
    | amountCharged     | <amountCharged>    |
    | event             | <event>            |
    | digest            | <digest>           |
    | channel           | <channel>          |
    | eventDate         | <eventDate>        |
    | nextRenewalDate   | <nextRenewalDate>  |
    | property1         | <property1>        |
    | property2         | <property2>        |
    | campaign          | <campaign>         |
    | channelProviderId | <channelProviderId>|
    | protocol          | <protocol>         |
    | subscriptionId    | <subscriptionId>   |
    Então a assinatura devera retornar o status <status_code> não concluindo a operação da assinatura da promo
    Exemplos:
    |userId  |serviceId     |offerKey  |amountCharged |event       |digest                  |channel         |eventDate | nextRenewalDate | property1 | property2 | campaign         | channelProviderId | protocol | subscriptionId | status_code |
    |55309   |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_TRIAL     |SIMCARD         |          |                 |           |           |HERO              |1                  |          |                |200          |
    |asdf    |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |IVR             |          |                 |           |           |HERO              |1                  |          |                |200          |
    |        |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55303456|98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |CUSTOMER_SERVICE|          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309   |              |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309   |NAO_PODE      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |CUSTOMER_SERVICE|          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309   |98772322      |          |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309   |98772322      |off_promo |399           |            |SUBSCRIBED_BY_CHARGE    |SYSTEM          |          |                 |           |           |HERO              |1                  |          |                |400          |
    |55309   |98772322      |off_promo |399           |NAO_PODE    |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |HERO              |1                  |          |                |400          |
    |55309   |98772322      |off_promo |399           |SUBSCRIBE   |NAO_PODE                |SATPUSH         |          |                 |           |           |HERO              |1                  |          |                |400          |
    |55309   |98772322      |off_promo |399           |SUBSCRIBE   |                        |SMS             |          |                 |           |           |HERO              |1                  |          |                |400          |
    |55309   |98772322      |off_promo |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |                |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309   |98772322      |off_promo |399           |RENEW       |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |HERO              |1                  |          |                |200          |
    |55309   |98772322      |off_promo |399           |CANCEL      |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |HERO              |1                  |          |                |200          |
    



@subscribe_backend @subscribe @cenarios_positivos @backend_oi @rodar @extensao_promo
Esquema do Cenario: Subscribe - BACKEND - POSITIVOS
    
    Dado que seja feita uma requisição de assinatura vindo do global
    Quando os parametros forem enviados com o método post com Event de SUBSCRIBE no BACKEND
    | userId            | <userId>           |
    | serviceId         | <serviceId>        |
    | offerKey          | <offerKey>         |
    | amountCharged     | <amountCharged>    |
    | event             | <event>            |
    | digest            | <digest>           |
    | channel           | <channel>          |
    | eventDate         | <eventDate>        |
    | nextRenewalDate   | <nextRenewalDate>  |
    | property1         | <property1>        |
    | property2         | <property2>        |
    | campaign          | <campaign>         |
    | channelProviderId | <channelProviderId>|
    | protocol          | <protocol>         |
    | subscriptionId    | <subscriptionId>   |
    Então a assinatura devera retornar o status <status_code> sucesso para operação da assinatura no backend
    Exemplos:
    |userId   |serviceId   |offerKey                            |amountCharged |event       |digest                  |channel         |eventDate | nextRenewalDate | property1 | property2 | campaign         | channelProviderId | protocol | subscriptionId | status_code |
    |55119    |451         |FS-OS-PBM                           |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55119    |451         |FS-OS-PIM                           |399           |SUBSCRIBE   |SUBSCRIBED_BY_TRIAL     |SIMCARD         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55119    |451         |FS-OS-PAM                           |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAULT     |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55119    |451         |FS-OS-PTM                           |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAIL      |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55119    |467         |FS-PC-PC1M                          |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |WAP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55119    |467         |FS-PC-PC3M                          |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |APP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55119    |467         |FS-PC-PC5M                          |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |IVR             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    
    # ESSES 2 COMENTADOS NAO ASSINAM MAIS!
    # |55309    |480         |FS-ANTIRROUBO-AR1MENSAL            |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |USSD            |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    # |55309    |480         |FS-ANTIRROUBO-AR3MENSAL            |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    # |55309    |480         |FS-ANTIRROUBO-AR5MENSAL            |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SIMCARD         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    # |55309    |497         |FS-CONTROLEDOSPAIS-CP3MENSAL       |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |APP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    # |55309    |497         |FS-CONTROLEDOSPAIS-CP5MENSAL       |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SYSTEM          |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    


@erro_subscribe_backend @erro_subscribe
Esquema do Cenario: Subscribe - BACKEND - NEGATIVOS
    
    Dado que seja feita uma requisição de assinatura vindo do global
    Quando os parametros forem enviados com o método post com Event de SUBSCRIBE no BACKEND
    | userId            | <userId>           |
    | serviceId         | <serviceId>        |
    | offerKey          | <offerKey>         |
    | amountCharged     | <amountCharged>    |
    | event             | <event>            |
    | digest            | <digest>           |
    | channel           | <channel>          |
    | eventDate         | <eventDate>        |
    | nextRenewalDate   | <nextRenewalDate>  |
    | property1         | <property1>        |
    | property2         | <property2>        |
    | campaign          | <campaign>         |
    | channelProviderId | <channelProviderId>|
    | protocol          | <protocol>         |
    | subscriptionId    | <subscriptionId>   |
    Então a assinatura devera retornar o status <status_code> não concluindo a operação da assinatura no backend
    Exemplos:
    |userId  |serviceId   |offerKey  |amountCharged |event       |digest                  |channel         |eventDate | nextRenewalDate | property1 | property2 | campaign         | channelProviderId | protocol | subscriptionId | status_code |
    |asdf    |451         |FS-OS-PBM |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |IVR             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |        |451         |FS-OS-PBM |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55303456|451         |FS-OS-PBM |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |CUSTOMER_SERVICE|          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |            |FS-OS-PAM |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |NAO_PODE    |FS-OS-PAM |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |CUSTOMER_SERVICE|          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |98772321    |          |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |98772321    |NAO_PODE  |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |451         |FS-OS-PAM |399           |            |SUBSCRIBED_BY_CHARGE    |SYSTEM          |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |400          |
    |55309   |467         |FS-PC-PC1M|399           |NAO_PODE    |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |400          |
    |55309   |467         |FS-PC-PC1M|399           |SUBSCRIBE   |NAO_PODE                |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |400          |
    |55309   |467         |FS-PC-PC1M|399           |SUBSCRIBE   |                        |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |400          |
    |55309   |467         |FS-PC-PC1M|399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |                |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |467         |FS-PC-PC5M|399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |NAO_PODE        |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |467         |FS-PC-PC5M|399           |RENEW       |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |467         |FS-PC-PC5M|399           |CANCEL      |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |


@subscribe_interactivity @subscribe @cenarios_positivos
Esquema do Cenario: Subscribe - INTERACTIVITY - POSITIVOS
    
    Dado que seja feita uma requisição de assinatura vindo do global
    Quando os parametros forem enviados com o método post com Event de SUBSCRIBE no INTERACTIVITY
    | userId            | <userId>           |
    | serviceId         | <serviceId>        |
    | offerKey          | <offerKey>         |
    | amountCharged     | <amountCharged>    |
    | event             | <event>            |
    | digest            | <digest>           |
    | channel           | <channel>          |
    | eventDate         | <eventDate>        |
    | nextRenewalDate   | <nextRenewalDate>  |
    | property1         | <property1>        |
    | property2         | <property2>        |
    | campaign          | <campaign>         |
    | channelProviderId | <channelProviderId>|
    | protocol          | <protocol>         |
    | subscriptionId    | <subscriptionId>   |
    Então a assinatura devera retornar o status <status_code> sucesso para operação da assinatura no static
    Exemplos:
    |userId   |serviceId   |offerKey  |amountCharged |event       |digest                  |channel         |eventDate | nextRenewalDate | property1 | property2 | campaign         | channelProviderId | protocol | subscriptionId | status_code |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_TRIAL     |SIMCARD         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAULT     |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_FAIL      |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |WAP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |APP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |IVR             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |USSD            |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SIMCARD         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |APP             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SYSTEM          |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |CUSTOMER_SERVICE|          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |


@subscribe_depois_de_cancelamento @cenarios_positivos
Esquema do Cenario: Ativação após um Cancelamento
    
    Dado que eu tenha um plano ativo do INTERACTIVITY
    Quando vier uma requisicao <event> do Global
    E os parametros forem enviados com o método post <digest> e Event de CANCEL
    Então a solicitação devera ser executada obedecendo o status da requisição <status_code> ok para o static
    E Mandar a ativação do plano novamente
    Entao o plano deve ser instalado novamente mas não com status TRIAL e sim ACTIVE
    Exemplos:
    |event           |digest                        |status_code|
    |"CANCEL"        |"UNSUBSCRIBED_VOLUNTARY"      |200        |



@erro_subscribe_interactivity @erro_subscribe
Esquema do Cenario: Subscribe - INTERACTIVITY - NEGATIVOS

    Dado que seja feita uma requisição de assinatura vindo do global
    Quando os parametros forem enviados com o método post com Event de SUBSCRIBE no INTERACTIVITY
    | userId            | <userId>           |
    | serviceId         | <serviceId>        |
    | offerKey          | <offerKey>         |
    | amountCharged     | <amountCharged>    |
    | event             | <event>            |
    | digest            | <digest>           |
    | channel           | <channel>          |
    | eventDate         | <eventDate>        |
    | nextRenewalDate   | <nextRenewalDate>  |
    | property1         | <property1>        |
    | property2         | <property2>        |
    | campaign          | <campaign>         |
    | channelProviderId | <channelProviderId>|
    | protocol          | <protocol>         |
    | subscriptionId    | <subscriptionId>   |
    Então a assinatura devera retornar o status <status_code> não concluindo a operação da assinatura no static
    Exemplos:
    |userId  |serviceId   |offerKey  |amountCharged |event       |digest                  |channel         |eventDate | nextRenewalDate | property1 | property2 | campaign         | channelProviderId | protocol | subscriptionId | status_code |
    |asdf    |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |IVR             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |        |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55303456|98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |CUSTOMER_SERVICE|          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |            |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |NAO_PODE    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |CUSTOMER_SERVICE|          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    #|55309   |98772320    |          |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    #|55309   |98772320    |NAO_PODE  |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |98772320    |off_inter |399           |            |SUBSCRIBED_BY_CHARGE    |SYSTEM          |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |400          |
    |55309   |98772320    |off_inter |399           |NAO_PODE    |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |400          |
    |55309   |98772320    |off_inter |399           |SUBSCRIBE   |NAO_PODE                |SATPUSH         |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |400          |
    |55309   |98772320    |off_inter |399           |SUBSCRIBE   |                        |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |400          |
    #|55309   |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |                |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    #|55309   |98772320    |off_inter |399           |SUBSCRIBE   |SUBSCRIBED_BY_CHARGE    |NAO_PODE        |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |
    |55309   |98772320    |off_inter |399           |RENEW       |SUBSCRIBED_BY_CHARGE    |SMS             |          |                 |           |           |PLATFORM_LAUNCH   |1                  |          |                |200          |