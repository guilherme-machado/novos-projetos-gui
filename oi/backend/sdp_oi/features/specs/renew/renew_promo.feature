#language: pt
Funcionalidade: Renew
    Renovação

@renew @renew_promo @renewed @renewed_promo_ok
Cenario: Renew - RENEWED -> indica que a renovação foi realizada com sucesso
    
    Dado que eu tenha um plano ativo assinado na promo
    Quando vier uma requisicao de renew para a promo
    Então o plano devera ter sua renovação realizada na promo


@renew @renew_promo @renew_fault @fault_promo_ok
Cenario: Renew - RENEW_FAULT -> Falta de credito 
    
    Dado que eu tenha um plano ativo assinado na promo
    Quando vier uma requisicao de renew fault para a promo
    Então o plano na promo devera tentar novamente pois esta sem credito


@renew @renew_promo @renew_fail @fail_promo_ok
Cenario: Renew - RENEW_FAIL -> OCS fora do ar
    
    Dado que eu tenha um plano ativo assinado na promo
    Quando vier uma requisicao de renew fail para a promo
