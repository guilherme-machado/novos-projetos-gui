#language: pt
Funcionalidade: Renew
    Renovação

@renew @renew_backend @renewed @renewed_back_ok @cenarios_positivos @backend_oi @rodar
Cenario: Renew - RENEWED -> indica que a renovação foi realizada com sucesso
    
    Dado que eu tenha um plano ativo assinado no backend
    Quando vier uma requisicao de renew para o backend
    Então o plano devera ter sua renovação agendada para 7 dias


@renew @renew_backend @renew_fault @fault_back_ok @cenarios_positivos @backend_oi @rodar
Cenario: Renew - RENEW_FAULT -> Falta de credito 
    
    Dado que eu tenha um plano ativo assinado no backend
    Quando vier uma requisicao de renew fault para o backend
    Então o plano no backend devera tentar novamente pois esta sem credito


@renew @renew_backend @renew_fail @fail_back_ok @cenarios_positivos @backend_oi @rodar
Cenario: Renew - RENEW_FAIL -> OCS fora do ar
    
    Dado que eu tenha um plano ativo assinado no backend
    Quando vier uma requisicao de renew fail para o backend
    Então o plano no backend devera tentar novamente pois a OCS estava fora