#language: pt
Funcionalidade: Renew
    Renovação

@renew @renew_static @renewed @renewed_static_ok
Cenario: Renew - RENEWED -> indica que a renovação foi realizada com sucesso
    
    Dado que eu tenha um plano ativo assinado no static
    Quando vier uma requisicao de renew para o static
    Então o plano devera ter sua renovação realizada no static


@renew @renew_static @renew_fault @fault_static_ok
Cenario: Renew - RENEW_FAULT -> Falta de credito 
    
    Dado que eu tenha um plano ativo assinado no static
    Quando vier uma requisicao de renew fault para o static
    Então o plano no static devera tentar novamente pois esta sem credito


@renew @renew_static @renew_fail @fail_static_ok
Cenario: Renew - RENEW_FAIL -> OCS fora do ar
    
    Dado que eu tenha um plano ativo assinado no static
    Quando vier uma requisicao de renew fail para o static
    Então o plano no static devera tentar novamente pois a OCS estava fora