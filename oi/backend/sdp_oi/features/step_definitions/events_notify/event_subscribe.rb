# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Dado("que seja feita uma requisição de assinatura vindo do global") do 

  puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE\n\n"

end






Quando("os parametros forem enviados com o método post com Event de SUBSCRIBE na PROMO") do |table|

  post_service_provider_subscribe(table)

end






Então("a assinatura devera retornar o status {int} sucesso para operação da assinatura da promo") do |status_code|

  #VALIDA FLUXO DE ASSINATURA
  puts "\n\nIMPRIMINDO RESULTADO DA ASSINATURA\n\n"
  expect(@action_subscribe.code).to eq status_code


  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo_promo 
  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  expect(@getProduto.success).to eq true

end








Então("a assinatura devera retornar o status {int} não concluindo a operação da assinatura da promo") do |status_code|

  #VALIDA FLUXO DE ASSINATURA
  puts "\n\nIMPRIMINDO RESULTADO DA ASSINATURA\n\n"
  expect(@action_subscribe.code).to eq status_code

  @waitingTimeOut=1
  sleep(6) 
  consultar_produto_ativo_promo 
  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  expect(@getProduto.success).to eq false

end









Quando("os parametros forem enviados com o método post com Event de SUBSCRIBE no BACKEND") do |table|

  post_service_provider_subscribe(table)
  puts "\n\n"
  puts @parametros['subscriptionId']

end







Então("a assinatura devera retornar o status {int} sucesso para operação da assinatura no backend") do |status_code|

  #VALIDA FLUXO DE ASSINATURA
  puts "\n\nIMPRIMINDO RESULTADO DA ASSINATURA\n\n"
  expect(@action_subscribe.code).to eq status_code

  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  consultar_produto_ativo_backend


  expect(@getProduto.success).to eq true
  expect(@getProduto['data']['servicos']['MCAFEE'][0]['subscription_id']).to eq @parametros['subscriptionId']
  if (@parametros['serviceId']) == "451" 
    expect(@getProduto['data']['servicos']['FUNAMBOL'][0]['subscription_id']).to eq @parametros['subscriptionId']
    expect(@getProduto['data']['servicos']['FSVPN'][0]['subscription_id']).to eq @parametros['subscriptionId']
  end

  puts "\n\n"
  puts "GLOBAL - subscriptionId"
  puts "\n\n"
  puts @parametros['subscriptionId']
  puts "\n\n"

  puts "BACKEND OI - subscriptionId"
  puts "\n\n"
  puts (@getProduto['data']['servicos']['MCAFEE'][0]['subscription_id'])
  puts "\n\n"

  # puts "CONSULTANDO NUMERO DA SORTE"
  # luck_number

  # @get_ns = JSON.parse(@numero_sorte.body, object_class: OpenStruct)
  # expect(@get_ns['normal']).to eq 1
  # expect(@get_ns['especial']).to eq 1
  # puts @numero_sorte.body

end








Então("a assinatura devera retornar o status {int} não concluindo a operação da assinatura no backend") do |status_code|

  #VALIDA FLUXO DE ASSINATURA
  puts "\n\nIMPRIMINDO RESULTADO DA ASSINATURA\n\n"
  expect(@action_subscribe.code).to eq status_code

  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  consultar_produto_ativo_backend
   
  expect(@getProduto.success).to eq true
  expect(@getProduto['data']).to eq []

end









Quando("os parametros forem enviados com o método post com Event de SUBSCRIBE no INTERACTIVITY") do |table|

  post_service_provider_subscribe(table)

end







Então("a assinatura devera retornar o status {int} sucesso para operação da assinatura no static") do |status_code|

  #VALIDA FLUXO DE ASSINATURA
  puts "\n\nIMPRIMINDO RESULTADO DA ASSINATURA\n\n"
  expect(@action_subscribe.code).to eq status_code

  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  consultar_produto_ativo_static

  expect(@getProduto[0]['first_campaign_action']).to eq "SUBSCRIBE"
  puts (@getProduto[0]['first_campaign_action'])
  expect(@getProduto[0]['status']).to eq "TRIAL"
  puts (@getProduto[0]['status'])

end







Então("a assinatura devera retornar o status {int} não concluindo a operação da assinatura no static") do |status_code|

  #VALIDA FLUXO DE ASSINATURA
  puts "\n\nIMPRIMINDO RESULTADO DA ASSINATURA\n\n"
  expect(@action_subscribe.code).to eq status_code

  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  consultar_produto_ativo_static

  expect(@getProduto['']).to be_nil

end