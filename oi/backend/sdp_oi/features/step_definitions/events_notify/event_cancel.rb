# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Dado("que eu tenha um plano ativo da PROMO") do

  	puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE PROMO\n\n"
  	post_service_provider_subscribe_promo

    @waitingTimeOut=1
    sleep(1) until consultar_produto_ativo_promo_user_id 
    puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
    expect(@getProduto.success).to eq true

end








Quando("vier uma requisicao {string} do Global") do |event|

    @event = event

  	puts "\n\nINICIANDO OPERACAO DE CANCELAMENTO\n\n"

end









E("os parametros forem enviados com o método post {string} e Event de CANCEL") do |digest|

    @digest = digest
	
    post_service_provider_cancel

end






Então("a solicitação devera ser executada obedecendo o status da requisição {int} ok para a promo") do |status_code|

  	#VALIDA FLUXO DE ASSINATURA
  	puts "\n\nIMPRIMINDO RESULTADO DO CANCELAMENTO\n\n"
  	expect(@action_cancel.code).to eq status_code


    @waitingTimeOut=1
    sleep(1) 
    consultar_produto_ativo_promo_user_id 
    puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
    expect(@getProduto.success).to eq false

end









Então("a solicitação devera ser executada obedecendo o status da requisição {int} falha para a promo") do |status_code|

    #VALIDA FLUXO DE ASSINATURA
    puts "\n\nIMPRIMINDO RESULTADO DO CANCELAMENTO\n\n"
    expect(@action_cancel.code).to eq status_code

    @waitingTimeOut=1
    sleep(1) 
    consultar_produto_ativo_promo_user_id 
    puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
    expect(@getProduto.success).to eq true

end









Dado("que eu tenha um plano ativo do tipo POM do BACKEND") do

    puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE BACKEND\n\n"
    post_service_provider_subscribe_backend

    consultar_produto_ativo_backend_oi

    expect(@getProduto.success).to eq true
    puts "\n\nDATA COMPRA\n\n"
    puts "DATA COMPRA MCAFEE " + (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra']).to_s
    puts "DATA COMPRA FUNAMBOL " + (@getProduto['data']['servicos']['FUNAMBOL'][0]['chaves_datacompra']).to_s
    # puts "DATA COMPRA FS " + (@getProduto['data']['servicos']['FS'][0]['chaves_datacompra'])
    # puts "DATA COMPRA FSVPN " + (@getProduto['data']['servicos']['FSVPN'][0]['chaves_datacompra'])

    expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra']).not_to be_empty
    expect(@getProduto['data']['servicos']['FUNAMBOL'][0]['chaves_datacompra']).not_to be_empty
    # expect(@getProduto['data']['servicos']['FS'][0]['chaves_datacompra']).not_to be_empty
    # expect(@getProduto['data']['servicos']['FSVPN'][0]['chaves_datacompra']).not_to be_empty

end








Então("a solicitação devera ser executada obedecendo o status da requisição {int} ok para o backend - POM") do |status_code|

    consultar_produto_ativo_backend_oi

    #VALIDA FLUXO DE CANCELAMENTO
    puts "\n\nIMPRIMINDO RESULTADO DO CANCELAMENTO\n\n"
    expect(@action_cancel.code).to eq status_code

    puts "\n\nDATA CANCELAMENTO\n\n"
    puts "DATA CANCELAMENTO MCAFEE " + (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_data_cancelamento']).to_s
    puts "DATA CANCELAMENTO FUNAMBOL " + (@getProduto['data']['servicos']['FUNAMBOL'][0]['chaves_data_cancelamento']).to_s
    # puts "DATA CANCELAMENTO FS " + (@getProduto['data']['servicos']['FS'][0]['chaves_data_cancelamento']).to_s
    # puts "DATA CANCELAMENTO FSVPN " + (@getProduto['data']['servicos']['FSVPN'][0]['chaves_data_cancelamento']).to_s

    expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_data_cancelamento']).not_to be_empty
    expect(@getProduto['data']['servicos']['FUNAMBOL'][0]['chaves_data_cancelamento']).not_to be_empty
    # expect(@getProduto['data']['servicos']['FS'][0]['chaves_data_cancelamento']).not_to be_empty
    # expect(@getProduto['data']['servicos']['FSVPN'][0]['chaves_data_cancelamento']).not_to be_empty

end









Dado("que eu tenha um plano ativo do tipo STANDALONE do BACKEND") do

    puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE BACKEND\n\n"
    post_service_provider_subscribe_backend_standalone

    consultar_produto_ativo_backend_oi

    expect(@getProduto.success).to eq true
    puts "\n\nDATA COMPRA\n\n"
    puts "DATA COMPRA MCAFEE " + (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra'])

    expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra']).not_to be_empty

end








Então("a solicitação devera ser executada obedecendo o status da requisição {int} ok para o backend - STANDALONE") do |status_code|

    consultar_produto_ativo_backend_oi

    #VALIDA FLUXO DE CANCELAMENTO
    puts "\n\nIMPRIMINDO RESULTADO DO CANCELAMENTO\n\n"
    expect(@action_cancel.code).to eq status_code

    puts "\n\nDATA CANCELAMENTO\n\n"
    puts "DATA CANCELAMENTO MCAFEE " + (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_data_cancelamento']).to_s

    expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_data_cancelamento']).not_to be_empty

end










Então("a solicitação devera ser executada obedecendo o status da requisição {int} falha para o backend") do |status_code|

    #VALIDA FLUXO DE ASSINATURA
    consultar_produto_ativo_backend_oi
    puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"

    puts "\n\nDATA COMPRA\n\n"
    puts (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra'])
    expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra']).not_to be_empty

    puts "\n\nDATA CANCELAMENTO\n\n"
    puts (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_data_cancelamento'])
    expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_data_cancelamento']).to eq nil

    #VALIDA FLUXO DE ASSINATURA
    puts "\n\nIMPRIMINDO RESULTADO DO CANCELAMENTO\n\n"
    expect(@action_cancel.code).to eq status_code


    expect(@getProduto.success).to eq true

end









Dado("que eu tenha um plano ativo do INTERACTIVITY") do

    puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE INTERACTIVITY\n\n"
    post_service_provider_subscribe_interactivity

    consultar_produto_ativo_static_msisdn 

    expect(@getProduto[0]['first_campaign_action']).to eq "SUBSCRIBE"
    puts (@getProduto[0]['first_campaign_action'])
    expect(@getProduto[0]['status']).to eq "TRIAL"
    puts (@getProduto[0]['status'])
    (@getProduto[0]['status'])

end








Então("a solicitação devera ser executada obedecendo o status da requisição {int} ok para o static") do |status_code|

    #VALIDA FLUXO DE ASSINATURA
    puts "\n\nIMPRIMINDO RESULTADO DO CANCELAMENTO\n\n"
    expect(@action_cancel.code).to eq status_code

    puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
    consultar_produto_ativo_static_msisdn

    expect(@getProduto[0]).to be_nil

end








Então("a solicitação devera ser executada obedecendo o status da requisição {int} falha para o static") do |status_code|

    #VALIDA FLUXO DE ASSINATURA
    puts "\n\nIMPRIMINDO RESULTADO DO CANCELAMENTO\n\n"
    expect(@action_cancel.code).to eq status_code

    puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
    consultar_produto_ativo_static_msisdn 

    expect(@getProduto[0]['first_campaign_action']).to eq "SUBSCRIBE"
    puts (@getProduto[0]['first_campaign_action'])
    expect(@getProduto[0]['status']).to eq "TRIAL"
    puts (@getProduto[0]['status'])
    (@getProduto[0]['status'])

end