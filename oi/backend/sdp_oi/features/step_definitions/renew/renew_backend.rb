# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Dado("que eu tenha um plano ativo assinado no backend") do

  puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE\n\n"
  post_service_provider_subscribe_backend

  puts "\n\nIMPRIMINDO RESULTADO DA ASSINATURA\n\n"
  consultar_produto_ativo_backend_oi 
  expect(@getProduto.success).to eq true


end









Quando("vier uma requisicao de renew para o backend") do

    puts "\n\nSOLICITANDO UM RENEW\n\n"
    renewed_backend

end









Então("o plano devera ter sua renovação agendada para 7 dias") do

  puts "\n\nIMPRIMINDO RESULTADO DO RENEW\n\n"
  consultar_produto_ativo_backend_oi 
  expect(@getProduto.success).to eq true

  puts "\n\nDATA COMPRA\n\n"
  puts (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra'])
  expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra']).not_to be_empty
    
end








Quando("vier uma requisicao de renew fault para o backend") do

    puts "\n\nSOLICITANDO UM RENEW\n\n"
    fault_backend

end









Então("o plano no backend devera tentar novamente pois esta sem credito") do

  puts "\n\nIMPRIMINDO RESULTADO DO RENEW\n\n"
  consultar_produto_ativo_backend_oi 
  expect(@getProduto.success).to eq true

  puts "\n\nDATA COMPRA\n\n"
  puts (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra'])
  expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra']).not_to be_empty
    
end












Quando("vier uma requisicao de renew fail para o backend") do

    puts "\n\nSOLICITANDO UM RENEW\n\n"
    fail_backend

end









Então("o plano no backend devera tentar novamente pois a OCS estava fora") do

  puts "\n\nIMPRIMINDO RESULTADO DO RENEW\n\n"
  consultar_produto_ativo_backend_oi 
  expect(@getProduto.success).to eq true

  puts "\n\nDATA COMPRA\n\n"
  puts (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra'])
  expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra']).not_to be_empty
    
end