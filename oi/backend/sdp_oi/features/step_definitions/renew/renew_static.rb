# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Dado("que eu tenha um plano ativo assinado no static") do

  puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE\n\n"
  post_service_provider_subscribe_interactivity

  #VALIDA FLUXO DE ASSINATURA
  puts "\n\nIMPRIMINDO RESULTADO DA ASSINATURA\n\n"
  expect(@action_subscribe.code).to eq 200

  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  consultar_produto_ativo_static_msisdn

  expect(@getProduto[0]['first_campaign_action']).to eq "SUBSCRIBE"
  puts (@getProduto[0]['first_campaign_action'])
  expect(@getProduto[0]['status']).to eq "TRIAL"
  puts (@getProduto[0]['status'])

end









Quando("vier uma requisicao de renew para o static") do

    puts "\n\nSOLICITANDO UM RENEW\n\n"
    renewed_static

end









Então("o plano devera ter sua renovação realizada no static") do

  puts "\n\nIMPRIMINDO RESULTADO DO RENEW\n\n"
  consultar_produto_ativo_static_msisdn

  expect(@getProduto[0]['first_campaign_action']).to eq "SUBSCRIBE"
  puts (@getProduto[0]['first_campaign_action'])
  expect(@getProduto[0]['status']).to eq "TRIAL"
  puts (@getProduto[0]['status'])
    
end








Quando("vier uma requisicao de renew fault para o static") do

    puts "\n\nSOLICITANDO UM RENEW\n\n"
    fault_static

end









Então("o plano no static devera tentar novamente pois esta sem credito") do

  puts "\n\nIMPRIMINDO RESULTADO DO RENEW\n\n"
  consultar_produto_ativo_static_msisdn

  expect(@getProduto[0]['first_campaign_action']).to eq "SUBSCRIBE"
  puts (@getProduto[0]['first_campaign_action'])
  expect(@getProduto[0]['status']).to eq "TRIAL"
  puts (@getProduto[0]['status'])
    
end












Quando("vier uma requisicao de renew fail para o static") do

    puts "\n\nSOLICITANDO UM RENEW\n\n"
    fail_static

end









Então("o plano no static devera tentar novamente pois a OCS estava fora") do

  puts "\n\nIMPRIMINDO RESULTADO DO RENEW\n\n"
  consultar_produto_ativo_static_msisdn

  expect(@getProduto[0]['first_campaign_action']).to eq "SUBSCRIBE"
  puts (@getProduto[0]['first_campaign_action'])
  expect(@getProduto[0]['status']).to eq "TRIAL"
  puts (@getProduto[0]['status'])
    
end