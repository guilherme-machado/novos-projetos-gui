# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Dado("que eu tenha um plano ativo assinado na promo") do

  puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE PROMO\n\n"
  post_service_provider_subscribe_promo

  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo_promo_user_id 
  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  expect(@getProduto.success).to eq true


end









Quando("vier uma requisicao de renew para a promo") do

    puts "\n\nSOLICITANDO UM RENEW\n\n"
    renewed_promo

end









Então("o plano devera ter sua renovação realizada na promo") do

  puts "\n\nIMPRIMINDO RESULTADO DO RENEW\n\n"

  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo_promo_user_id 
  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  expect(@getProduto.success).to eq true
    
end









Quando("vier uma requisicao de renew fault para a promo") do

    puts "\n\nSOLICITANDO UM RENEW\n\n"
    fault_promo

end









Então("o plano na promo devera tentar novamente pois esta sem credito") do

  puts "\n\nIMPRIMINDO RESULTADO DO RENEW\n\n"

  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo_promo_user_id 
  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  expect(@getProduto.success).to eq true
    
end










Quando("vier uma requisicao de renew fail para a promo") do

    puts "\n\nSOLICITANDO UM RENEW\n\n"
    fail_promo

end









Então("o plano na promo devera tentar novamente pois a OCS estava fora") do

  puts "\n\nIMPRIMINDO RESULTADO DO RENEW\n\n"

  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo_promo_user_id 
  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  expect(@getProduto.success).to eq true
    
end