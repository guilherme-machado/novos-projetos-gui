# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Dado("que eu consulte um plano ativo") do

  	puts "\n\nINICIANDO MO\n\n"

end






Quando("vier uma requisicao mo com os parametros {int}, {int}, {int} e {string}") do |userId, serviceId, shortCode, text|

    puts "\n\nSIMULANDO O SMS\n\n"

    @userId       = userId
    @serviceId    = serviceId
    @shortCode    = shortCode
    @text         = text
  	

    mo_acao

end






Então("o feedback devera ser enviado para o cliente retornando o {int}") do |status_code|

  	#VALIDA FLUXO DE CONSULTA
  	puts "\n\nRESULTADO DA CONSULTA DE MO VINDO DO GLOBAL\n\n"
  	expect(@mo.code).to eq status_code
    puts status_code

end







E("a mensagem de tarifa para um assinante será enviada atraves do backend da promo") do

  dispatcher("TAX")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "TAX"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Assinatura do servico: R$2,99 por semana. Caso nao tenha credito suficiente, a assinatura podera ser de RS1,71 (por 4 dias) ou RS0,85 (por 2 dias)."

end







E("a mensagem de tarifa para um assinante será enviada atraves do backend") do

  dispatcher("TAX")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "TAX"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Assinatura do servico: R$2,99 por semana. Caso nao tenha credito suficiente, a assinatura podera ser de RS1,71 (por 4 dias) ou RS0,85 (por 2 dias)."

end







E("a mensagem de tarifa para um assinante será enviada atraves do backend do static") do

  dispatcher("TAX")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "TAX"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Assinatura do servico: R$2,99 por semana. Caso nao tenha credito suficiente, a assinatura podera ser de RS1,71 (por 4 dias) ou RS0,85 (por 2 dias)."

end







E("a mensagem de saldo para um assinante será enviada atraves do backend da promo") do

  dispatcher("LUCKY_NUMBER_BALANCE")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "LUCKY_NUMBER_BALANCE"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Voce tem ${raffle.daily.total} chances para o premio de RS RS2MIL MIL REAIS, ${raffle.weekly.total} para RS15MIL MIL REAIS e ${raffle.special.total} para o de RS 250 MIL MIL REAIS."

end







E("a mensagem de saldo para um assinante será enviada atraves do backend") do

  dispatcher("LUCKY_NUMBER_BALANCE")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "LUCKY_NUMBER_BALANCE"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Voce tem ${raffle.daily.total} chances para o premio de RS RS2MIL MIL REAIS, ${raffle.weekly.total} para RS15MIL MIL REAIS e ${raffle.special.total} para o de RS 250 MIL MIL REAIS."

end








E("a mensagem de saldo para um assinante será enviada atraves do backend do static") do

  dispatcher("LUCKY_NUMBER_BALANCE")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "LUCKY_NUMBER_BALANCE"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Mude de Vida: Voce tem ${raffle.daily.total} chances para o premio de hoje de ${config.promo.daily.prize}, ${raffle.weekly.total} para o de ${config.promo.weekly.prize} e ${raffle.special.total} para o sorteio de ${config.promo.prize.maximum}! Infos: promocaomudedevida.com.br"

end








E("a mensagem de ajuda para um assinante será enviada atraves do backend da promo") do

  dispatcher("HELP")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "HELP"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Escolha a opção que deseja saber mais. 1 - O que e o servico; 2 - Preco; 3 - Como cancelar; 4 - Mais informacoes"

end








E("a mensagem de ajuda para um assinante será enviada atraves do backend") do

  dispatcher("HELP")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "HELP"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Escolha a opção que deseja saber mais. 1 - O que e o servico; 2 - Preco; 3 - Como cancelar; 4 - Mais informacoes"

end







E("a mensagem de ajuda para um assinante será enviada atraves do backend do static") do

  dispatcher("HELP")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "HELP"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Escolha a opção que deseja saber mais. 1 - O que e o servico; 2 - Preco; 3 - Como cancelar; 4 - Mais informacoes"

end







E("a mensagem de erro para um assinante será enviada atraves do backend da promo") do

  dispatcher("INVALID_COMMAND_HAS_SUBSCRIPTION")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "INVALID_COMMAND_HAS_SUBSCRIPTION"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Comando invalido. Para assinar envie HERO. Para cancelar envie SAIR. Mais informações envie AJUDA para 10910."


end








E("a mensagem de erro para um assinante será enviada atraves do backend") do

  dispatcher("INVALID_COMMAND_HAS_SUBSCRIPTION")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "INVALID_COMMAND_HAS_SUBSCRIPTION"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Comando invalido. Para assinar envie HERO. Para cancelar envie SAIR. Mais informações envie AJUDA para 10910."


end







E("a mensagem de erro para um assinante será enviada atraves do backend do static") do

  dispatcher("INVALID_COMMAND_HAS_SUBSCRIPTION")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "INVALID_COMMAND_HAS_SUBSCRIPTION"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Comando invalido. Para assinar envie HERO. Para cancelar envie SAIR. Mais informações envie AJUDA para 10910."


end







E("a mensagem de tarifa para um nao assinante será enviada atraves do backend da promo") do

  @userId = "5530000000000"

  dispatcher("TAX_NO_SUBSCRIPTION_FOUND")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "TAX_NO_SUBSCRIPTION_FOUND"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Voce ainda nao eh assinante do HERO. Assine agora e concorra a ate 150 MIL REAIS! Envie H agora mesmo (RS2,99 por semana)"


end








E("a mensagem de tarifa para um nao assinante será enviada atraves do backend") do

  @userId = "5530000000000"

  dispatcher("TAX_NO_SUBSCRIPTION_FOUND")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "TAX_NO_SUBSCRIPTION_FOUND"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Voce ainda nao eh assinante do HERO. Assine agora e concorra a ate 150 MIL REAIS! Envie H agora mesmo (RS2,99 por semana)"


end







E("a mensagem de tarifa para um nao assinante será enviada atraves do backend do static") do

  @userId = "5530000000000"

  dispatcher("TAX_NO_SUBSCRIPTION_FOUND")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "TAX_NO_SUBSCRIPTION_FOUND"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Voce ainda nao eh assinante do HERO. Assine agora e concorra a ate 150 MIL REAIS! Envie H agora mesmo (RS2,99 por semana)"


end








E("a mensagem de saldo para um nao assinante será enviada atraves do backend da promo") do

  @userId = "5530000000000"

  dispatcher("LUCKY_NUMBER_BALANCE_NOT_FOUND")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "LUCKY_NUMBER_BALANCE_NOT_FOUND"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Voce ainda nao esta participando da promocao. Assine agora e concorra a premios! Envie H agora mesmo e assine por RS${price} por ${renew_days} dias."


end








E("a mensagem de saldo para um nao assinante será enviada atraves do backend") do

  @userId = "5530000000000"

  dispatcher("LUCKY_NUMBER_BALANCE_NOT_FOUND")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "LUCKY_NUMBER_BALANCE_NOT_FOUND"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Voce ainda nao esta participando da promocao. Assine agora e concorra a premios! Envie H agora mesmo e assine por RS${price} por ${renew_days} dias."


end








E("a mensagem de saldo para um nao assinante será enviada atraves do backend do static") do

  @userId = "5530000000000"

  dispatcher("LUCKY_NUMBER_BALANCE_NOT_FOUND")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "LUCKY_NUMBER_BALANCE_NOT_FOUND"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Voce ainda nao esta participando da promocao. Assine agora e concorra a premios! Envie H agora mesmo e assine por RS${price} por ${renew_days} dias."


end









E("a mensagem de ajuda para um nao assinante será enviada atraves do backend da promo") do

  @userId = "5530000000000"

  dispatcher("HELP")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "HELP"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Escolha a opção que deseja saber mais. 1 - O que e o servico; 2 - Preco; 3 - Como cancelar; 4 - Mais informacoes"


end









E("a mensagem de ajuda para um nao assinante será enviada atraves do backend") do

  @userId = "5530000000000"

  dispatcher("HELP")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "HELP"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Escolha a opção que deseja saber mais. 1 - O que e o servico; 2 - Preco; 3 - Como cancelar; 4 - Mais informacoes"


end








E("a mensagem de ajuda para um nao assinante será enviada atraves do backend do static") do

  @userId = "5530000000000"

  dispatcher("HELP")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "HELP"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Escolha a opção que deseja saber mais. 1 - O que e o servico; 2 - Preco; 3 - Como cancelar; 4 - Mais informacoes"


end








E("a mensagem de erro para nao assinante será enviada atraves do backend da promo") do

  @userId = "5530000000000"

  dispatcher("INVALID_COMMAND_NO_SUBSCRIPTION")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "INVALID_COMMAND_NO_SUBSCRIPTION"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Voce ainda nao e assinante. Assine agora e concorra a premios! Envie H agora mesmo e assine por RS${price} por semana."


end









E("a mensagem de erro para nao assinante será enviada atraves do backend") do

  @userId = "5530000000000"

  dispatcher("INVALID_COMMAND_NO_SUBSCRIPTION")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "INVALID_COMMAND_NO_SUBSCRIPTION"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Voce ainda nao e assinante. Assine agora e concorra a premios! Envie H agora mesmo e assine por RS${price} por semana."


end








E("a mensagem de erro para nao assinante será enviada atraves do backend do static") do

  @userId = "5530000000000"

  dispatcher("INVALID_COMMAND_NO_SUBSCRIPTION")

  expect(@result_mo['data']['messages_queued'][0]['type']).to eq "INVALID_COMMAND_NO_SUBSCRIPTION"
  expect(@result_mo['data']['messages_queued'][0]['text']).to eq "Hero: Voce ainda nao e assinante. Assine agora e concorra a premios! Envie H agora mesmo e assine por RS${price} por semana."


end