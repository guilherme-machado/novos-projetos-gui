# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Quando("vier uma requisicao de migration do Global para o backend promo") do

  puts "\n\nINICIANDO OPERACAO DE MIGRATION PROMO\n\n"
  post_service_provider_migration_promo
  expect(@action_migrate.code).to eq 200

  puts "\n\nNOVO SUBSCRIPTION ID\n\n"
  puts (@post_action_migrate[@subscription_Migracao])

end







Então("meu plano devera ser migrado com sucesso para a promo") do

	puts "\n\nIMPRIMINDO RESULTADO DA MIGRATION\n\n"

  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS PARA VALIDAR A MIGRAÇÃO\n\n"
  @waitingTimeOut=1
  sleep(5) 
  consultar_produto_ativo_promo_user_id 
  expect(@getProduto.success).to eq true


  puts "\n\nCONSULTA ASSINATURA PARA VALIDAR NOVO SUBSCRIPTION ID\n\n"
  puts (@getProduto['data'][0]['key'])

  puts "NOVO SUBSCRIPTION ID EFETIVAMENTE NA BASE"
  expect(@post_action_migrate[@subscription_Migracao]).to eq (@getProduto['data'][0]['key'])

end








Dado("que eu queira realizar uma migração para a promo") do

    puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE COM DIGEST MIGRATE\n\n"

end









Quando("eu ainda não tiver assinatura na promo") do

  @userId                           = "55309#{Faker::Number.number(8)}"
  @serviceId                        = 98772322
  @offerKey                         = "OFFER_KEY"
  @amountCharged                    = 399
  @event                            = "SUBSCRIBE"
  @digest                           = "MIGRATION"
  @channel                          = "SMS"
  @eventDate                        = Time.now.strftime("%Y-%m-%d")
  @nextRenewalDate                  = Time.now.strftime("%Y-%m-%d")
  @property1                        = Faker::Name.name
  @property2                        = Faker::Name.name
  @campaign                         = "HERO"
  @channelProviderId                = 1
  @protocol                         = Faker::Number.number(9).to_i
  @subscriptionId                   = SecureRandom.uuid
  @guuid                            = SecureRandom.uuid

  @post_action_migrate              = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => @event,
    "digest"                        => @digest,
    "channel"                       => @channel,
    "eventDate"                     => @eventDate,
    "nextRenewalDate"               => @nextRenewalDate,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => @protocol,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_migrate

  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)

  @action_migrate = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_migrate)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_migrate.body

end









Então("espero que o plano da promo seja assinado do lado FS") do

   #VALIDA FLUXO DE ASSINATURA
  puts "\n\nIMPRIMINDO RESULTADO DA ASSINATURA\n\n"
  expect(@action_migrate.code).to eq 200


  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo_promo_user_id 
  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  expect(@getProduto.success).to eq true

  puts "NOVO SUBSCRIPTION ID EFETIVAMENTE NA BASE"
  expect(@post_action_migrate[@subscriptionId]).to eq (@getProduto['data'][0]['key'])

    
end