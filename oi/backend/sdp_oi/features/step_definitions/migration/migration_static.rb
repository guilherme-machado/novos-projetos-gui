# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Quando("vier uma requisicao de migration do Global para o backend static") do

    puts "\n\nINICIANDO OPERACAO DE MIGRATION STATIC\n\n"
    post_service_provider_migration_interactivity 

    puts "\n\nSUBSCRIPTION ID DA MIGRATION\n\n"
    puts(@post_action_migrate[@subscription_Migracao])

end








Então("meu plano devera ser migrado com sucesso para o static") do

    #VALIDA FLUXO DE ASSINATURA
    puts "\n\nIMPRIMINDO RESULTADO DO MIGRATION\n\n"
    expect(@action_subscribe.code).to eq 200

    consultar_produto_ativo_static_msisdn

    expect(@getProduto[0]['first_campaign_action']).to eq "SUBSCRIBE"
    puts (@getProduto[0]['first_campaign_action'])
    expect(@getProduto[0]['status']).to eq "TRIAL"
    puts (@getProduto[0]['status'])
    (@getProduto[0]['status'])

    puts (@getProduto[0]['ext_ref'])
    expect(@post_action_migrate[@subscription_Migracao]).to eq (@getProduto[0]['ext_ref'])

end







Dado("que eu queira realizar uma migração no static") do

    puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE COM DIGEST MIGRATE\n\n"

end









Quando("eu ainda não tiver assinatura no static") do

  @userId                           = "55309#{Faker::Number.number(8)}"
  @serviceId                        = 98772320
  @offerKey                         = "OFFER_KEY"
  @amountCharged                    = 399
  @event                            = "SUBSCRIBE"
  @digest                           = "MIGRATION"
  @channel                          = "SMS"
  @eventDate                        = Time.now.strftime("%Y-%m-%d")
  @nextRenewalDate                  = Time.now.strftime("%Y-%m-%d")
  @property1                        = Faker::Name.name
  @property2                        = Faker::Name.name
  @campaign                         = "PLATFORM_LAUNCH"
  @channelProviderId                = 1
  @protocol                         = Faker::Number.number(9).to_i
  @subscription_Migracao            = SecureRandom.uuid
  @guuid                            = SecureRandom.uuid

  @post_action_migrate              = {
    "userId"                        => @userId,
    "serviceId"                     => @serviceId,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => @amountCharged,
    "event"                         => @event,
    "digest"                        => @digest,
    "channel"                       => @channel,
    "eventDate"                     => @eventDate,
    "nextRenewalDate"               => @nextRenewalDate,
    "tags"                          => {
      "property1"                   => @property1,
      "property2"                   => @property2
    },
    "campaign"                      => @campaign,
    "channelProviderId"             => @channelProviderId,
    "protocol"                      => @protocol,
    "subscriptionId"                => @subscription_Migracao
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_migrate


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscription_Migracao)

  @action_migrate = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_migrate)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_migrate.body

  puts "\n\nSUBSCRIPTION ID DA MIGRATION\n\n"
  puts(@post_action_migrate[@subscription_Migracao])

end









Então("espero que o plano do static seja assinado do lado FS") do

  #VALIDA FLUXO DE ASSINATURA
  puts "\n\nIMPRIMINDO RESULTADO DA ASSINATURA\n\n"
  expect(@action_migrate.code).to eq 200

  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  consultar_produto_ativo_static_msisdn

  expect(@getProduto[0]['first_campaign_action']).to eq "SUBSCRIBE"
  puts (@getProduto[0]['first_campaign_action'])
  expect(@getProduto[0]['status']).to eq "TRIAL"
  puts (@getProduto[0]['status'])

  puts (@getProduto[0]['ext_ref'])
  expect(@post_action_migrate[@subscription_Migracao]).to eq (@getProduto[0]['ext_ref'])
    
end