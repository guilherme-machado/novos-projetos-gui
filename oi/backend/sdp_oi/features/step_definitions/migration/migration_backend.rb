# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Quando("vier uma requisicao de migration do Global para o backend") do

    puts "\n\nINICIANDO OPERACAO DE MIGRATION BACKEND\n\n"
    post_service_provider_migration_backend
    expect(@action_migrate.code).to eq 200

    puts "\n\nNOVO SUBSCRIPTION ID\n\n"
    puts (@post_action_migrate[@subscription_Migracao])

end









Então("meu plano devera ser migrado com sucesso para o backend") do

    puts "\n\nIMPRIMINDO RESULTADO DA MIGRATION\n\n"

    puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS PARA VALIDAR A MIGRAÇÃO\n\n"
    consultar_produto_ativo_backend_oi 
    expect(@getProduto.success).to eq true

    puts "\n\nDATA COMPRA\n\n"
    puts (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra'])
    expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra']).not_to be_empty

    puts "\n\nCONSULTA ASSINATURA PARA VALIDAR NOVO SUBSCRIPTION ID\n\n"
    puts (@getProduto['data']['servicos']['MCAFEE'][0]['subscription_id'])

    puts "NOVO SUBSCRIPTION ID EFETIVAMENTE NA BASE"
    expect(@post_action_migrate[@subscription_Migracao]).to eq (@getProduto['data']['servicos']['MCAFEE'][0]['subscription_id'])
    
end










Dado("que eu queira realizar uma migração no backend") do

    puts "\n\nINICIANDO OPERACAO DE SUBSCRIBE COM DIGEST MIGRATE\n\n"

end









Quando("eu ainda não tiver assinatura no backend") do

    @offerKey                       = ['FS-OS-PBM', 'FS-OS-PIM', 'FS-OS-PAM', 'FS-OS-PTM'].sample #ESSE TESTE É PARA O SERVICEID = 451
    #@offerKey                       = ['FS-PC-PC1M', 'FS-PC-PC3M', 'FS-PC-PC5M'].sample    #ESSE TESTE É PARA O SERVICEID = 467
    @guuid                          = SecureRandom.uuid
    @subscriptionId                 = SecureRandom.uuid
    @userId                         = "55309#{Faker::Number.number(8)}"

    @post_action_migrate            = {
    "userId"                        => @userId,
    "serviceId"                     => 451,
    "offerKey"                      => @offerKey,
    "amountCharged"                 => 399,
    "event"                         => "SUBSCRIBE",
    "digest"                        => "MIGRATION",
    "channel"                       => "SMS",
    "eventDate"                     => Time.now.strftime("%Y-%m-%d"),
    "nextRenewalDate"               => Date.today + 7,
    "tags"                          => {
      "property1"                   => Faker::Name.name,
      "property2"                   => Faker::Name.name
    },
    "campaign"                      => "PLATFORM_LAUNCH",
    "channelProviderId"             => 1,
    "protocol"                      => Faker::Number.number(9).to_s,
    "subscriptionId"                => @subscriptionId
  }.to_json

  puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
  puts @post_action_migrate

  puts "\n\nSUBSCRIPTION ID\n\n"
  puts (@post_action_migrate[@subscriptionId])


  endpoint               = $api['notify_service_provider']
  mount                  = endpoint.gsub("<subscriptionId>", @subscriptionId)


  @action_migrate = HTTParty.post(mount,:headers => {"Content-Type" => 'application/json', "X-TRANSACTION-ID" => @guuid}, :body => @post_action_migrate)
  puts "\n\nIMPRIMINDO ENDPOINT\n\n"
  puts mount

  puts "\n\nIMPRIMINDO RESPONSE\n\n"
  puts @action_migrate.body
  expect(@action_migrate.code).to eq 200

end









Então("espero que o plano do backend seja assinado do lado FS") do

  puts "\n\nIMPRIMINDO RESULTADO DA MIGRATION\n\n"

  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS PARA VALIDAR A MIGRAÇÃO\n\n"
  consultar_produto_ativo_backend_oi 
  expect(@getProduto.success).to eq true

  puts "\n\nDATA COMPRA\n\n"
  puts (@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra'])
  expect(@getProduto['data']['servicos']['MCAFEE'][0]['chaves_datacompra']).not_to be_empty

  puts "\n\nCONSULTA ASSINATURA PARA VALIDAR NOVO SUBSCRIPTION ID\n\n"
  puts (@getProduto['data']['servicos']['MCAFEE'][0]['subscription_id'])

  puts "NOVO SUBSCRIPTION ID EFETIVAMENTE NA BASE"
  expect(@post_action_migrate[@subscriptionId]).to eq (@getProduto['data']['servicos']['MCAFEE'][0]['subscription_id'])
    
end