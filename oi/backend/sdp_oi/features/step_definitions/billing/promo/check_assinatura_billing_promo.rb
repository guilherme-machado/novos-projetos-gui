# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)
Quando("eu fizer o check credit") do

  sleep 5
  check_credit

  expect(@check_credit.code).to eq 200
  puts @check_credit.code
  puts @check_credit.body

end








E("rodar o datasync para a promo") do

  datasync_billing_promo("RENEWED")

  expect(@action_datasync.code).to eq 200
  puts @action_datasync.code
  puts @action_datasync.body


end









Então("a assinatura vai ser retornada com sucesso mostrando o status do billing da fatura paga e recorrencia aguardando check credit") do

  check_status_billing

  expect(@check_status.code).to eq 200
  puts @check_status.code
  puts @check_status.body
  
  expect(@check_status[1]['status']).to eq "Pago"
  puts ("CONFERINDO STATUS DO PAGAMENTO - " + @check_status[1]['status'])

  expect(@check_status[0]['status']).to eq "Aguardando check credit"
  puts ("CONFERINDO STATUS DA RENOVAÇÃO - " + @check_status[0]['status'])

  puts ("CONFERINDO DATA DE PAGAMENTO - " + @check_status[1]['payment_date'])
  puts ("CONFERINDO DATA DE RENOVAÇÃO - " + @check_status[0]['payment_date'])
  
  expect(@check_status[1]['payment_date']).to include Date.today.to_s
  expect(@check_status[0]['payment_date']).to include @nextRenewalDate.to_s
   
end









E("se o cancelamento for solicitado") do

  @event    =     "CANCEL"
  @digest   =     "UNSUBSCRIBED_VOLUNTARY"

  post_service_provider_cancel


end










Entao("o cancelamento deve ser feito no backend e no billing") do

  sleep 5
  check_status_billing

  expect(@check_status.code).to eq 200
  puts @check_status.code
  puts @check_status.body
  
  expect(@check_status[1]['status']).to eq "Pago"
  puts ("CONFERINDO STATUS DO PAGAMENTO DA PRIMEIRA FATURA- " + @check_status[1]['status'])

  expect(@check_status[0]['status']).to eq "Cancelado"
  puts ("CONFERINDO STATUS DO CANCELAMENTO DA RECORRENCIA- " + @check_status[0]['status'])
  
  expect(@check_status[1]['payment_date']).to include Date.today.to_s
  expect(@check_status[0]['gateway_response']).to be_nil


end