# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado("que eu envie uma solicitação de cancelamento para a {string} do subscriber para o datasync") do |operadora|
    
   @canceler                  = Faker::GameOfThrones.character
   @operadora                 = operadora

end




Quando("a requisição for feita corretamente para a oi") do 

    assinar_oi
    puts "\n\nmsisdn que será cancelado\n\n"
    puts @msisdns

      @deletebody = {
      "silent"              => false,
      "identifier"          => @subscriptionId,
      "la"                  => 10910,
      "fsProductId"         => "5f386317-59fb-46b0-9620-a1edd50fa241",
      "partner"             => @operadora,
      "reason"              => "eu quero",
      "canceler"            => @canceler,
      "force"               => false
    }.to_json

    endpoint                  = $api['cancela_subscription_datasyncs']
    @delete = HTTParty.delete(endpoint,:headers => {"Content-Type" => 'application/json'}, :body => @deletebody)
    puts @deletebody
    puts (endpoint)

end




Então(/^o subscription vai mandar uma solicitação para o Datasync realizar o cancelamento para a oi$/) do

  puts @delete.body
  case @delete.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@delete.code}"
  end

  expect(@delete.code).to eq 201


  @waitingTimeOut=1
  sleep(1) until consultar_produto_ativo_por_msisdn 
  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  expect(@getProduto.success).to eq false

end