# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado("que eu envie uma solicitação de cancelamento do static") do

  puts "\n\nINICIANDO ASSINATURA\n\n"
    
  post_service_provider_subscribe_interactivity

  consultar_produto_ativo_static_msisdn

  expect(@getProduto[0]['first_campaign_action']).to eq "SUBSCRIBE"
  puts (@getProduto[0]['first_campaign_action'])
  expect(@getProduto[0]['status']).to eq "TRIAL"
  puts (@getProduto[0]['status'])

end






Quando("a requisição for feita corretamente para o global") do 

    puts "\n\nmsisdn que será cancelado\n\n"
    puts @userId

    @cancel_static_callcenter   = {
      "carrier"                 => "oi",
      "cancellation_reason"     => "CALLCENTER",
      "las"                     => 22020,
      "msisdn"                  => @userId,
      "all"                     => true
    }.to_json

    puts "\n\nIMPRIMINDO BODY PASSADO\n\n"
    puts @cancel_static_callcenter

    endpoint                    = $api['cancela_static_global_internal']

    @cancel_static_subscribe = HTTParty.post(endpoint,:headers => {"Content-Type" => 'application/json'}, :body => @cancel_static_callcenter)
    puts endpoint

end







Então("a assinatura devera ser cancelada") do

  puts @cancel_static_subscribe.body
  case @cancel_static_subscribe.code
    when 200
      puts "Retornou 200, ok"
    when 404
      puts "Retornou 404, não existe"
    when 400
      puts "Retornou 400, problema de negócio"
    when 500...600
      puts "ops #{@cancel_static_subscribe.code}"
  end

  expect(@cancel_static_subscribe.code).to eq 200


  puts "\n\nESPERANDO RESULTADO TRUE DA BUSCA DE PRODUTOS\n\n"
  consultar_produto_ativo_static_msisdn 

  expect(@getProduto[0]).to be_nil

end