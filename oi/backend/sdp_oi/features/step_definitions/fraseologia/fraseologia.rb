# encoding: utf-8 (colocar o cerquilha antes)
# !/usr/bin/env ruby (colocar o cerquilha antes)

Dado("que eu queira saber qual a mensagem será envia ao usuario") do
	puts "INICIANDO FLUXO DE FRASEOLOGIAS"
	@msisdn 				= "5530#{Faker::Number.number(9)}"
end

Quando("eu chamar o endpoint do dispatcher passando {string}, {string} e {int}") do |type, carrier, shortnumber|
	@type 				= type
	@carrier 			= carrier
	@shortnumber  = shortnumber

	puts "ENTREI NA BAGACEIRA"
	puts @shortnumber

	fraseologia_dispatcher
end

Então("devo receber a mensagem correspondente ao tipo de comunicação enviada {string}") do |message |
	expect(@post_dispacher['data']['messages_queued'][0]['text']).to eq message
  puts "\n\n"
end


Dado(/^que eu queira saber quais as mensagens enviadas ao usuario$/) do
	puts "INICIANDO FLUXO DE FRASEOLOGIAS"
	@msisdn 				= "5530#{Faker::Number.number(9)}"
end

Quando(/^eu chamar o endpoint do dispatcher passando WELCOME$/) do
	@type 				= "WELCOME"
	@carrier 			= "oi"

	fraseologia_dispatcher
end

Então(/^devo receber 2 mensagens correspondente ao tipo de comunicação enviada$/) do 
	expect(@post_dispacher['data']['messages_queued'][0]['text']).to eq "Oi Seguranca: 2/2 Acesse ${link_download} e instale seus aplicativos. Depois acesse http://oiseguranca.com.br/minha-protecao com senha ${senha}"
	expect(@post_dispacher['data']['messages_queued'][1]['text']).to eq "Oi Seguranca: Voce esta concorrendo ate RS 1 MILHAO DE REAIS na Promocao Manda Premios! Veja seu numero da sorte: www.promocaomandapremios.com.br"
  puts "\n\n"
end
