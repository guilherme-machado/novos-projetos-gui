#encoding: utf-8

# Dado("que usuario HERO inicie o aplicativo do HERO") do |table|
#     @start_date = Time.now.strftime("%Y/%m/%d %H:%M:%S")
#     @base.wait_id(@page.hero_elements[:enter_button])
#     expect(@app.validate_text_app(table.cells_rows.count, table.rows_hash, "login")).to eql true
# end

Quando("usuario realiza o login no App com o plano {string}") do |plan|
    @app.sign_plan(plan)

    #Inicia o app de SMS para limpar todas as conversas antes de solicitar o pincode
    $driver.activate_app(@page.message_elements[:app_package])
    @app.cancel_conversation

    #Retorna para o app Hero
    $driver.activate_app(@page.hero_elements[:app_package])
    @base.wait_id(@page.hero_elements[:enter_button])
    # expect(@app.validate_text_app(table.cells_rows.count, table.rows_hash, "login")).to eql true

    @app.login_flow
    expect(@base.wait_id(@page.hero_elements[:activity_main])).to eql true
end

Quando("acessa a opcao {string} no menu Configuracoes") do |gestor|
    @base.click_id(@page.hero_elements[:settings])
    @base.wait_text(gestor)
    @base.click_text(gestor)
    @base.wait_id(@page.hero_elements[:toolbar_title])

    expect(@base.return_string_id(@page.hero_elements[:toolbar_title])).to eql gestor
end

Então("será exibido o nome do Pacote ativo") do
    @plan = @base.return_string_id(@page.hero_elements[:plan_value])
    puts "\nPLAN: #{@plan}"
    @signed_products = @db.product_search(DATA['msisdn'])

    for i in (0..@signed_products.size-1)
        if @signed_products[i]["product_type"] == "POM"
            @db_plan = @signed_products[i]["product_name"]
            @found = "sucesso"
            break
        else
            @found = "falha"
            @app.insert_result_license(@start_date, @db_plan, @found)
        end
    end

    puts "\nDB_PLAN: #{@db_plan}"
    expect(@db_plan == @plan.to_s).to eql true
end

Então("os produtos, com suas respectivas licenças, disponíveis.") do
    @base.wait_id(@page.hero_elements[:license_title], 20)

    @product = @base.return_strings_id(@page.hero_elements[:license_title])
    puts "PRODUTO: #{@product}"

    for i in (0..@product.size-1)
        if @product[i] == "Segurança"
            @product[i] = "Protect"
        elsif @product[i] == "Família"
            @product[i] = "Filho"
        end
    end

    @db_license = []
    @validate = []
    for a in (0..@product.size-1)
        for i in (0..@signed_products.size-1)
            if @signed_products[i]["product_type"] == "POM" || @signed_products[i]["product_type"] == "VPN" || @signed_products[i]["product_type"] == "Suporte"
                next
            end
    
            if @db_plan == "ESSENCIAL" && @signed_products[i]["product_type"] == "Parental Control"
                next
            end
            
            @db_license[a] = @signed_products[i]["license"]

            if @signed_products[i]["product_name"].include? @product[a]
                @validate[a] = true
                puts "PRODUCT: #{@product[a]} == #{@signed_products[i]["product_name"]}: #{@validate[a]}"
                break
            else
                @validate[a] = false
                puts "PRODUCT: #{@product[a]} == #{@signed_products[i]["product_name"]}: #{@validate[a]}"
            end
        end

        if @validate[a] == true
            @found = "sucesso"
        else
            @found = "falha"
            @app.insert_result_license(@start_date, @product[a], @found)
        end
        expect(@validate[a]).to eql true
    end
    
    @license = @base.return_strings_id(@page.hero_elements[:license_value])
    puts "LICENÇA: #{@license}"
    puts "DB_LICENSE: #{@db_license}"

    for i in (0..@license.size-1)
        if @db_license[i].to_i == @license[i].to_i
            @found = "sucesso"
            puts "VALIDAÇÃO #{@db_license[i]} == #{@license[i]}:  OK"
        else
            @found = "falha"
            @app.insert_result_license(@start_date, @signed_products[i]["product_name"], @found)
            puts "VALIDAÇÃO #{@db_license[i]} == #{@license[i]}:  NOK"
        end
        expect(@db_license[i].to_i == @license[i].to_i).to eql true
    end
    
    if @found == "sucesso"
        @app.insert_result_license(@start_date, @plan, @found)
    end
end
  