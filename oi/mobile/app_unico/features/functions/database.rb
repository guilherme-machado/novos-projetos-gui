#encoding: utf-8

require 'mysql2'
require 'json'

class Database
    def mysql_consult_conn(query)
        client = Mysql2::Client.new(:host => DATA['db_consult']['host'], :username => DATA['db_consult']['username'], :password => DATA['db_consult']['password'], :database => DATA['db_consult']['database'], :symbolize_keys => true)
        results = client.query(query, :as => :array)
        results
    end
    
    def mysql_edit_conn(query)
        client = Mysql2::Client.new(:host => DATA['db_edit']['host'], :username => DATA['db_edit']['username'], :password => DATA['db_edit']['password'], :database => DATA['db_edit']['database'], :symbolize_keys => true)
        results = client.query(query, :as => :array)
        results
    end

    def query_insert(date, scenario, sentPin, result)
        insert = "INSERT INTO #{DATA['db_edit']['table']}
        ( test_start_date, app_name, scenario, pin_wait, test_result )
        VALUES
        ( '#{date}', '#{DATA['db_edit']['app_name']}', '#{scenario}', '#{sentPin}', '#{result}' )"
        insert
    end

    def insert_oi(date, scenario, sentPin, result)
        result = mysql_edit_conn(query_insert(date, scenario, sentPin, result))
        result.to_a
    end

    def query_product(msisdn)
        select = "SELECT ch.produto AS product_id, pr.nome_amigavel AS product_name, pr.tipo_produto AS product_type, ch.extref, pr.tamanho AS license
        FROM clientes AS cl
        INNER JOIN chaves AS ch
        ON cl.id = ch.cliente
        INNER JOIN produtos_parceiros AS pr
        ON ch.produto = pr.id
        WHERE ch.cliente = (SELECT id FROM clientes WHERE id_estrangeiro = '55#{msisdn}') AND ch.data_cancelamento IS NULL;"
        select
    end

    def select_product_oi(msisdn)
        result = mysql_consult_conn(query_product(msisdn))
        result.to_a
    end

    def select_product_oi_fields(msisdn)
        result = mysql_consult_conn(query_product(msisdn))
        result.fields
    end


    def product_search(msisdn)
        result = select_product_oi(msisdn)
        signed_products = result.map{|s| {product_id: s[0].to_i, product_name: s[1].to_s, product_type: s[2].to_s, extref: s[3].to_s, license: s[4]} }
        signed_products = signed_products.to_json
        signed_products = JSON.parse(signed_products, object_class: OpenStruct)
        # puts signed_products[0]["product_name"]
        signed_products
    end
end


# @db = Database.new

# puts "Digite o nome do produto que deseja buscar:"
# produto = gets.strip

# msisdn = "11990076813"
# teste = @db.product_search(msisdn, produto)
# puts "\nTESTE: #{teste}"
# puts teste[:license]