#encoding: utf-8

class PageElements
    def oi_elements
        {
            #home_screen
            app_package: "br.com.fs.oiseguranca.new",
            insert_phone: "#{DATA['id']}fslogin_oi_security_edt_phone_number",
            enter_button: "#{DATA['id']}fslogin_oi_security_btn_login",

            #pincode_screen
            pincode: "#{DATA['id']}confirmation_code",
            continue_button: "#{DATA['id']}submit_confirmation_code",
            count_down_retry: "#{DATA['id']}tv_count_down_retry",
            resend_code: "#{DATA['id']}resend_code",

            #user_page
            user_detail: "#{DATA['id']}txtview_user_detail",
            edit_user: "#{DATA['id']}edittext_user_detail",
            enter_user: "#{DATA['id']}btn_submit_detail",
            edit_device: "#{DATA['id']}edittext_device_detail",
            enter_device: "#{DATA['id']}btn_submit_detail",

            #logged_screen
            activity_main: "#{DATA['id']}activity_main",
            warning: "#{DATA['id']}warning",
            check: "#{DATA['id']}check",
            dialog_tilte: "#{DATA['id']}txt_dialog_tilte",
            txt_action: "#{DATA['id']}txt_action",
            dialog_information:"#{DATA['id']}txt_dialog_information",
            txt_positive: "#{DATA['id']}txt_positive",
            settings: "#{DATA['id']}img_settings",

            #menu_screen
            toolbar_title: "#{DATA['id']}toolbar_title_prim",

            #License_screen
            plan_title: "#{DATA['id']}txt_plan_title",
            plan_value: "#{DATA['id']}txt_plan_value",
            license_title: "#{DATA['id']}txt_license_title",
            license_value: "#{DATA['id']}txt_license_value"
        }
    end

    def message_elements
        {
            #home_screen
            app_package: "com.google.android.apps.messaging",
            new_conversation: "#{DATA['message_id']}fslogin_oi_security_spinner_text_country_code",
            conversation: "#{DATA['message_id']}conversation_snippet",
            search: "#{DATA['message_id']}action_zero_state_search",
            options: "Mais opções",
            options_menu: "#{DATA['message_id']}title",
            delete_button: "android:id/button1",
            switch_widget: "android:id/switchWidget",

            #message_pincode
            message_pin: "e seu codigo de verificacao do OI SEGURANCA"
        }
    end
end