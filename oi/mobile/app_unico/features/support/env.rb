# This file provides setup and common functionality across all features.  It's
# included first before every test run, and the methods provided here can be 
# used in any of the step definitions used in a test.  This is a great place to
# put shared data like the location of your app, the capabilities you want to
# test with, and the setup of selenium.

require 'rspec/expectations'
require 'appium_lib'
require 'appium_console'
require 'cucumber'
require 'pry'
require 'capybara/dsl'
require 'capybara'
require 'selenium-webdriver'
require 'rspec'
require 'httparty'
require 'pry'
require 'mysql2'
require "faker"

ENV_TYPE = ENV['ENV_TYPE']
DATA = YAML.load_file(File.dirname(__FILE__) + "/data/#{ENV_TYPE}.yaml")

# Create a custom World class so we don't pollute `Object` with Appium methods
class AppiumWorld
end

# Load the desired configuration from appium.txt, create a driver then
# Add the methods to the world
caps_path = File.join(File.dirname(__FILE__), '..', '..', 'caps', 'appium_oi_seguranca.txt')
caps = Appium.load_appium_txt file: caps_path, verbose: true
Appium::Driver.new(caps, true)
Appium.promote_appium_methods AppiumWorld

World do
  AppiumWorld.new
end

Before { $driver.start_driver }
After { $driver.driver_quit }

#   $driver.start_driver

# Before { $driver.launch_app }

# After do
#   $driver.close_app
# end


=begin
# If you wanted one env.rb for both android and iOS, you could use logic similar to this:
world_class = ENV['PLATFORM_NAME'] == 'iOS' ? IosWorld : AndroidWorld
# each world class defines the `caps` method specific to that platform
Appium::Driver.new world_class.caps
Appium.promote_appium_methods world_class
World { world_class.new }
Before { $driver.start_driver }
After { $driver.driver_quit }
=end