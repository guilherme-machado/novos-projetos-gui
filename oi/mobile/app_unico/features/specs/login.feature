# language: pt

Funcionalidade: Usuario Oi que utiliza o App Oi Segurança
    Usuario Oi que inicia o app Oi Segurança

    Contexto:
        Dado que usuario Oi inicie o aplicativo Oi Segurança
            
            |login1|OI SEGURANÇA|
            |login2|Proteja a sua vida digital com aplicativos para você e sua famìlia usarem a internet com tranquilidade e segurança.|
            |login3|Insira o seu DDD + celular|
            |login4|Entrar|

@login
    Cenario: Usuario que possui o Oi realiza o login
        Quando usuario insere o msisdn e clica em "Entrar"
        Então recebe o pincode de validacao

            |confirma1|Confirmar seu número de telefone|
            |confirma2|Insira o código de 6 dígitos enviado para|
            |confirma3|Continuar|

        E exibe a home do app

            |home1|Proteção Completa|
            |home2|Proteção Sync|
            |home3|Controle dos Pais|
            # |home4|VERIFICAR VÍRUS|
            # |home5|CONEXÃO WI-FI|